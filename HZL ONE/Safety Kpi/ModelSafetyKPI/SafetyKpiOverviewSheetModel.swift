//
//  SafetyKpiOverviewSheetModel.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 27/03/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import Foundation


class MyKpiOverviewModel: NSObject {
    
    
 
    var SL = String()
    var Incident_Name = String()
    var FTDZone1 = String()
    var FTDZone2 = String()
    var FTDZone3 = String()
    var FTDZone4 = String()
    var FTDZone5 = String()
    var FTDZone6 = String()
    var MTDZone1 = String()
    var MTDZone2 = String()
    var MTDZone3 = String()
    var MTDZone4 = String()
    var MTDZone5 = String()
    var MTDZone6 = String()
    var YTDZone1 = String()
    var YTDZone2 = String()
    var YTDZone3 = String()
    var YTDZone4 = String()
    var YTDZone5 = String()
    var YTDZone6 = String()
    
    
    
    func setDataInModel(str:[String:AnyObject])
    {
        if str["SL"] is NSNull || str["SL"] == nil{
            self.SL = ""
        }else{
            
            let ros = str["SL"]
            
            self.SL = (ros?.description)!
            
            
        }
        
        if str["Incident_Name"] is NSNull || str["Incident_Name"] == nil{
            self.Incident_Name = ""
        }else{
            
            let ros = str["Incident_Name"]
            
            self.Incident_Name = (ros?.description)!
            
            
        }
        
        
        
        
        
        
        
        if str["FTDZone1"] is NSNull || str["FTDZone1"] == nil{
            self.FTDZone1 = ""
        }else{
            
            let ros = str["FTDZone1"]
            
            self.FTDZone1 = (ros?.description)!
            
            
        }
        
        if str["FTDZone2"] is NSNull || str["FTDZone2"] == nil{
            self.FTDZone2 = ""
        }else{
            
            let ros = str["FTDZone2"]
            
            self.FTDZone2 = (ros?.description)!
            
            
        }
        if str["FTDZone3"] is NSNull || str["FTDZone3"] == nil{
            self.FTDZone3 = ""
        }else{
            
            let ros = str["FTDZone3"]
            
            self.FTDZone3 = (ros?.description)!
            
            
        }
        
        if str["FTDZone4"] is NSNull || str["FTDZone4"] == nil{
            self.FTDZone4 = ""
        }else{
            
            let ros = str["FTDZone4"]
            
            self.FTDZone4 = (ros?.description)!
            
            
        }
        if str["FTDZone5"] is NSNull || str["FTDZone5"] == nil{
            self.FTDZone5 = ""
        }else{
            
            let ros = str["FTDZone5"]
            
            self.FTDZone5 = (ros?.description)!
            
            
        }
        if str["FTDZone6"] is NSNull || str["FTDZone6"] == nil{
            self.FTDZone6 = ""
        }else{
            
            let ros = str["FTDZone6"]
            
            self.FTDZone6 = (ros?.description)!
            
            
        }
        
        if str["MTDZone1"] is NSNull || str["MTDZone1"] == nil{
            self.MTDZone1 = ""
        }else{
            
            let ros = str["MTDZone1"]
            
            self.MTDZone1 = (ros?.description)!
            
            
        }
        
        if str["MTDZone2"] is NSNull || str["MTDZone2"] == nil{
            self.MTDZone2 = ""
        }else{
            
            let ros = str["MTDZone2"]
            
            self.MTDZone2 = (ros?.description)!
            
            
        }
        if str["MTDZone3"] is NSNull || str["MTDZone3"] == nil{
            self.MTDZone3 = ""
        }else{
            
            let ros = str["MTDZone3"]
            
            self.MTDZone3 = (ros?.description)!
            
            
        }
        
        if str["MTDZone4"] is NSNull || str["MTDZone4"] == nil{
            self.MTDZone4 = ""
        }else{
            
            let ros = str["MTDZone4"]
            
            self.MTDZone4 = (ros?.description)!
            
            
        }
        if str["MTDZone5"] is NSNull || str["MTDZone5"] == nil{
            self.MTDZone5 = ""
        }else{
            
            let ros = str["MTDZone5"]
            
            self.MTDZone5 = (ros?.description)!
            
            
        }
        if str["MTDZone6"] is NSNull || str["MTDZone6"] == nil{
            self.MTDZone6 = ""
        }else{
            
            let ros = str["MTDZone6"]
            
            self.MTDZone6 = (ros?.description)!
            
            
        }
        if str["YTDZone1"] is NSNull || str["YTDZone1"] == nil{
            self.YTDZone1 = ""
        }else{
            
            let ros = str["YTDZone1"]
            
            self.YTDZone1 = (ros?.description)!
            
            
        }
        
        if str["YTDZone2"] is NSNull || str["YTDZone2"] == nil{
            self.YTDZone2 = ""
        }else{
            
            let ros = str["YTDZone2"]
            
            self.YTDZone2 = (ros?.description)!
            
            
        }
        if str["YTDZone3"] is NSNull || str["YTDZone3"] == nil{
            self.YTDZone3 = ""
        }else{
            
            let ros = str["YTDZone3"]
            
            self.YTDZone3 = (ros?.description)!
            
            
        }
        
        if str["YTDZone4"] is NSNull || str["YTDZone4"] == nil{
            self.YTDZone4 = ""
        }else{
            
            let ros = str["YTDZone4"]
            
            self.YTDZone4 = (ros?.description)!
            
            
        }
        if str["YTDZone5"] is NSNull || str["YTDZone5"] == nil{
            self.YTDZone5 = ""
        }else{
            
            let ros = str["YTDZone5"]
            
            self.YTDZone5 = (ros?.description)!
            
            
        }
        if str["YTDZone6"] is NSNull || str["YTDZone6"] == nil{
            self.YTDZone6 = ""
        }else{
            
            let ros = str["YTDZone6"]
            
            self.YTDZone6 = (ros?.description)!
            
            
        }
        
       
        
    }
    
}





class MyKpiOverviewSheetAPI
{
    
    var reachablty = Reachability()!
    
    func serviceCalling(obj:KpiOverviewSheetViewController, param : [String: String] , success:@escaping (AnyObject)-> Void)
    {
        obj.startLoading()
        
        if(reachablty.connection != .none)
        {
            
            
            WebServices.sharedInstances.sendPostRequest(url: URLConstants.KPI_Overview_Sheet, parameters: param , successHandler: { (dict) in
                
                if let response = dict["response"]{
                    
                    let statusString : String = response["status"] as! String
                    let objectmsg = MessageCallServerModel()
            let msg = objectmsg.sealizeMessage(cell: response as! [String : AnyObject])
                    print(response)

                    if(statusString == "success")
                    {
                        obj.spreadView.isHidden = false
                        obj.noDataLabel(text: "" )
                        obj.label.isHidden = true
                        if let data = dict["data"] as? [AnyObject]
                        {
                            
                            var dataArray: [MyKpiOverviewModel] = []
                            
                            for i in 0..<data.count
                            {
                                
                                if let cell = data[i] as? [String:AnyObject]
                                {
                                    let object = MyKpiOverviewModel()
                                    object.setDataInModel(str: cell)
                                    dataArray.append(object)
                                }
                                
                            }
                            
                            success(dataArray as AnyObject)
                            
                            
                        }
                        
                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                    }
                    else
                    {
                        obj.label.isHidden = false
                        obj.spreadView.isHidden = true
                        obj.noDataLabel(text: msg )
                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                        
                    }
                    
                    
                }
                
                
                
                
            })
            { (error) in
                
                let errorStr : String = error.description
                
                print("DATA:error",errorStr)
                
                //                obj.errorChecking(error: error)
                obj.stopLoading()
            }
            
            
        }
        else{
            obj.refresh.endRefreshing()
            obj.label.isHidden = false
            obj.spreadView.isHidden = true
            obj.noDataLabel(text: "Internet is not available, please check your internet connection try again." )
            obj.stopLoading()
        }
        
        
    }
    
    
}
