//
//  LineChartZoneTableViewCell.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 26/03/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit
import Charts
class LineChartZoneTableViewCell: UITableViewCell,ChartViewDelegate ,UICollectionViewDelegate,UICollectionViewDataSource{
var options: [Option]!
    @IBOutlet weak var heightCollectionCon: NSLayoutConstraint!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var lblAxisY: UILabel!
    @IBOutlet weak var lblZoneName: UILabel!
    @IBOutlet weak var chartView: BarChartView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
       lblAxisY.text = "Y-Axis"
        lblAxisY.transform = CGAffineTransform(rotationAngle: -CGFloat.pi / 2)
      
    }


    func cellConfigure(LineDB : [Zone_DB_Data]){
        lineValDB = LineDB
        self.options = [.toggleValues,
                        .toggleHighlight,
                        .animateX,
                        .animateY,
                        .animateXY,
                        .saveToGallery,
                        .togglePinchZoom,
                        .toggleData,
                        .toggleBarBorders]
        
        // self.setup(barLineChartView: chartView)
        if(lineValDB.count > 0){
            for i in 0...lineValDB.count - 1 {
                colorsData.append(UIColor.random())
            }
        }
        chartView.delegate = self
        
        chartView.drawBarShadowEnabled = false
        chartView.drawValueAboveBarEnabled = false
      
        
        chartView.chartDescription?.text = ""
        chartView.maxVisibleCount = 60
        
        let xAxis = chartView.xAxis
        xAxis.labelPosition = .bottom
        xAxis.labelFont = .systemFont(ofSize: 10)
        xAxis.granularity = 1
        xAxis.labelCount = 7
        
        // xAxis.valueFormatter = DayAxisValueFormatter(chart: chartView)
        
        let leftAxisFormatter = NumberFormatter()
        leftAxisFormatter.minimumFractionDigits = 0
        leftAxisFormatter.maximumFractionDigits = 1
        leftAxisFormatter.negativeSuffix = ""
        leftAxisFormatter.positiveSuffix = ""
        
        let leftAxis = chartView.leftAxis
        leftAxis.labelFont = .systemFont(ofSize: 10)
        leftAxis.labelCount = 8
        leftAxis.valueFormatter = DefaultAxisValueFormatter(formatter: leftAxisFormatter)
        leftAxis.labelPosition = .outsideChart
        leftAxis.spaceTop = 0.15
        leftAxis.axisMinimum = 0 // FIXME: HUH?? this replaces startAtZero = YES
        
        let rightAxis = chartView.rightAxis
        rightAxis.enabled = true
        rightAxis.labelFont = .systemFont(ofSize: 10)
        rightAxis.labelCount = 8
        rightAxis.valueFormatter = leftAxis.valueFormatter
        rightAxis.spaceTop = 0.15
        rightAxis.axisMinimum = 0
        
        
        let l = chartView.legend
        l.horizontalAlignment = .left
        l.verticalAlignment = .bottom
        l.orientation = .horizontal
        l.drawInside = false
        l.form = .circle
        l.formSize = 9
        l.font = UIFont(name: "HelveticaNeue-Light", size: 11)!
        l.xEntrySpace = 4
        //        chartView.legend = l
        
        let marker = BalloonMarker(color: UIColor(white: 180/250, alpha: 1),
                                   font: .systemFont(ofSize: 12),
                                   textColor: .white,
                                   insets: UIEdgeInsets(top: 8, left: 8, bottom: 20, right: 8))
        marker.chartView = chartView
        marker.minimumSize = CGSize(width: 80, height: 40)
        chartView.marker = marker
        let count : Int = LineDB.count
        
      
        let start = 1
        
        let yVals = (start..<count).map { (i) -> BarChartDataEntry in
            
            let val = Double(UInt32(LineDB[i - 1].count_Data!)!)
           
                return BarChartDataEntry(x: Double(i), y: val)
            
        }
        
        
       
            set1 = BarChartDataSet(values: yVals, label: "")
            set1.colors = colorsData
            set1.drawValuesEnabled = false
     //  colorsData = set1.colors
            let data = BarChartData(dataSet: set1)
            data.setValueFont(UIFont(name: "HelveticaNeue-Light", size: 10)!)
            data.barWidth = 0.9
            chartView.data = data
       // print(colorsData)
      //  print(lineValDB)
        collectionView.delegate = self
        collectionView.dataSource = self
        if(lineValDB.count > 0){
            var v1 = lineValDB.count / 2
            let v2 = lineValDB.count % 2
            if(v2 != 0){
                v1 = v1 + 2
            }else{
               v1 = v1 + 1
            }
            self.heightCollectionCon.constant = CGFloat(v1 * 45) + 15
        }
        
    }
    var set1: BarChartDataSet! = nil
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    var reachablty = Reachability()!
    var dataArray: [BannerDataModel] = []
    var view = UIView()
    var timer = Timer()
    var lastXAxis = CGFloat()
    var contentOffset = CGPoint()
    var lineValDB : [Zone_DB_Data] = []
    
    var colorsData : [UIColor] = []
    
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return self.lineValDB.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellZone", for: indexPath) as! ZoneDashboardPieCollectionViewCell
        
       
        cell.imgView.backgroundColor = colorsData[indexPath.row]
        cell.lblZone.text = lineValDB[indexPath.row].incident_Name
        cell.lblCountZone.text = "- " + lineValDB[indexPath.row].count_Data!
       
        
        return cell
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        return UIEdgeInsetsMake(0, 0, 0, 0)
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        return CGSize(width: 165, height: 45)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        
        //print(indexPath.item)
        
        
    }
}
