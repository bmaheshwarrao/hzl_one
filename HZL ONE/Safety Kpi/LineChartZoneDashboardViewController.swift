//
//  LineChartZoneDashboardViewController.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 26/03/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit
import CoreData
import Charts
enum Option {
    case toggleValues
    case toggleIcons
    case toggleHighlight
    case animateX
    case animateY
    case animateXY
    case saveToGallery
    case togglePinchZoom
    case toggleAutoScaleMinMax
    case toggleData
    case toggleBarBorders
    // CandleChart
    case toggleShadowColorSameAsCandle
    case toggleShowCandleBar
    // CombinedChart
    case toggleLineValues
    case toggleBarValues
    case removeDataSet
    // CubicLineSampleFillFormatter
    case toggleFilled
    case toggleCircles
    case toggleCubic
    case toggleHorizontalCubic
    case toggleStepped
    // HalfPieChartController
    case toggleXValues
    case togglePercent
    case toggleHole
    case spin
    case drawCenter
    // RadarChart
    case toggleXLabels
    case toggleYLabels
    case toggleRotate
    case toggleHighlightCircle
    
    var label: String {
        switch self {
        case .toggleValues: return "Toggle Y-Values"
        case .toggleIcons: return "Toggle Icons"
        case .toggleHighlight: return "Toggle Highlight"
        case .animateX: return "Animate X"
        case .animateY: return "Animate Y"
        case .animateXY: return "Animate XY"
        case .saveToGallery: return "Save to Camera Roll"
        case .togglePinchZoom: return "Toggle PinchZoom"
        case .toggleAutoScaleMinMax: return "Toggle auto scale min/max"
        case .toggleData: return "Toggle Data"
        case .toggleBarBorders: return "Toggle Bar Borders"
        // CandleChart
        case .toggleShadowColorSameAsCandle: return "Toggle shadow same color"
        case .toggleShowCandleBar: return "Toggle show candle bar"
        // CombinedChart
        case .toggleLineValues: return "Toggle Line Values"
        case .toggleBarValues: return "Toggle Bar Values"
        case .removeDataSet: return "Remove Random Set"
        // CubicLineSampleFillFormatter
        case .toggleFilled: return "Toggle Filled"
        case .toggleCircles: return "Toggle Circles"
        case .toggleCubic: return "Toggle Cubic"
        case .toggleHorizontalCubic: return "Toggle Horizontal Cubic"
        case .toggleStepped: return "Toggle Stepped"
        // HalfPieChartController
        case .toggleXValues: return "Toggle X-Values"
        case .togglePercent: return "Toggle Percent"
        case .toggleHole: return "Toggle Hole"
        case .spin: return "Spin"
        case .drawCenter: return "Draw CenterText"
        // RadarChart
        case .toggleXLabels: return "Toggle X-Labels"
        case .toggleYLabels: return "Toggle Y-Labels"
        case .toggleRotate: return "Toggle Rotate"
        case .toggleHighlightCircle: return "Toggle highlight circle"
        }
    }
}
class LineChartZoneDashboardViewController: CommonVSClass ,UITableViewDelegate, UITableViewDataSource,ChartViewDelegate{
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var lblToDate: UILabel!
    @IBOutlet weak var lblFromDate: UILabel!
    @IBOutlet weak var heightCon: NSLayoutConstraint!
    
    
    @IBOutlet weak var FromHeightCon: NSLayoutConstraint!
    @IBOutlet weak var ToHeightCon: NSLayoutConstraint!
    @IBOutlet weak var filterHeightCon: NSLayoutConstraint!
    var refreshControl = UIRefreshControl()
    var Zone_MasterDB:[Zone_DB_Data] = []
    var LineChartZoneDB:[Zone_DB_Data] = []
    var LineChartZoneCountDB:[Zone_DB_Data] = []
    var LineChartZoneAPI = ZoneSafetyKPIDataAPI()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
   
        
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.estimatedRowHeight = 65
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.tableFooterView = UIView()
        
        refreshControl.addTarget(self, action: #selector(getLineChartZoneData), for: .valueChanged)
        
        self.tableView.addSubview(refreshControl)
        let swipe = UISwipeGestureRecognizer(target: self, action: #selector(self.swipeRecognizer))
        swipe.direction = ([.down])
        self.view.addGestureRecognizer(swipe)
        NotificationCenter.default.addObserver(self, selector: #selector(self.getLineChartZoneData), name: NSNotification.Name?.init(NSNotification.Name(rawValue: "BarOverviewFilter")), object: nil)
        
        
        
        
    }

    @objc func getZone_MasterData() {
        
        self.Zone_MasterDB = [Zone_DB_Data]()
        
        let fetchRequest: NSFetchRequest<Zone_DB_Data> = Zone_DB_Data.fetchRequest()
        let sort = NSSortDescriptor(key: #keyPath(Zone_DB_Data.sl), ascending: true)
        fetchRequest.sortDescriptors = [sort]
        do {
            
            self.Zone_MasterDB = try context.fetch(Zone_DB_Data.fetchRequest())
             let zoneDR = self.Zone_MasterDB.unique{$0.zone_id}
            self.LineChartZoneDB = zoneDR.sorted { $0.zone_Name! < $1.zone_Name! }
            let zoneDR1 = self.Zone_MasterDB.unique{$0.sl}
            self.LineChartZoneCountDB = zoneDR1.sorted { $0.zone_Name! < $1.zone_Name! }
           
        self.tableView.reloadData()
            
        } catch {
            print("Fetching Failed")
        }
        
    }
    var lineChartInternalDB : [Zone_DB_Data] = []
    func countData(zone : String) -> [Zone_DB_Data]{
        if(self.Zone_MasterDB.count > 0) {
            lineChartInternalDB = []
        for i in 0...self.Zone_MasterDB.count - 1 {
            if(self.Zone_MasterDB[i].zone_Name == zone){
                lineChartInternalDB.append(self.Zone_MasterDB[i])
            }
        }
        }
        return lineChartInternalDB
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.tabBarController?.tabBar.isHidden = true
        self.title = "Zone Dashboard"
        let nav = self.navigationController?.navigationBar
        nav?.tintColor = UIColor.white
        nav?.barTintColor = UIColor(hexString: "2c3e50", alpha: 1.0)
        nav?.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        if(FilterGraphStruct.fromDate == String() && FilterGraphStruct.toDate == String()){
            heightCon.constant = 0
            ToHeightCon.constant = 0
            FromHeightCon.constant = 0
            filterHeightCon.constant = 0
        }else{
            heightCon.constant = 60
            lblFromDate.text = "From Date : " + FilterGraphStruct.fromDate
            lblToDate.text = "To Date : " + FilterGraphStruct.toDate
            ToHeightCon.constant = 18
            FromHeightCon.constant = 18
            filterHeightCon.constant = 18
        }
        getLineChartZoneData()
    }
    var reachability = Reachability()!
    @objc func swipeRecognizer(_ sender: UISwipeGestureRecognizer) {
        if (sender.direction == .down) {
            
            if(self.reachability.connection != .none)
            {
                
                self.getLineChartZoneData()
            }
            else
            {
                self.startLoading(view: self.view)
                DispatchQueue.main.asyncAfter(deadline: .now() + 4.5) {
                    self.noDataLabel(text: "No internet connection found. Check your internet connection and try again.")
                    self.stopLoading(view: self.view)
                }
            }
            
        }
    }
    @IBAction func btnBarChartZoneFilterClicked(_ sender: UIBarButtonItem) {
        let storyBoard = UIStoryboard(name: "SafetyKpi", bundle: nil)
        let SearchFilterVC = storyBoard.instantiateViewController(withIdentifier: "FilterKpiOverviewViewController") as! FilterKpiOverviewViewController
        SearchFilterVC.identifierStr = "SearchKpiOverviewBar"
        self.navigationController?.pushViewController(SearchFilterVC, animated: true)
    }
    @IBOutlet var tapView: UITapGestureRecognizer!
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func setDataCount(_ count: Int, range: UInt32) {
        
    }
    var data: String?
    var lastObject: String?
    var indPath = IndexPath()
   
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.LineChartZoneDB.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! LineChartZoneTableViewCell
        let dbLine : [Zone_DB_Data] =  countData(zone: self.LineChartZoneDB[indexPath.row].zone_Name!)
        cell.lblZoneName.text = self.LineChartZoneDB[indexPath.row].zone_Name!
        cell.cellConfigure(LineDB: dbLine)
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableViewAutomaticDimension
        
    }
    
    @objc func getLineChartZoneData(){
        var param = [String:String]()
   
        
        
        if(FilterGraphStruct.fromDate == String() && FilterGraphStruct.toDate == String()){
            let frDate = Date().beginningOfMonth
            let toDate = Date().endOfMonth
            let dateFormatter2 = DateFormatter()
            dateFormatter2.timeZone = NSTimeZone.system
            dateFormatter2.dateFormat = "yyyy-MM-dd"
            let date_TimeFrom = dateFormatter2.string(from: frDate)
            let date_TimeTo = dateFormatter2.string(from: toDate)
            
            
            param = ["FromDate": date_TimeFrom ,"ToDate": date_TimeTo  ]
        }else{
            let dateFormatter2 = DateFormatter()
            dateFormatter2.timeZone = NSTimeZone.system
            dateFormatter2.dateFormat = "yyyy-MM-dd"
            let dateFrom = dateFormatter2.date(from: FilterGraphStruct.fromDate)
            let dateTo = dateFormatter2.date(from: FilterGraphStruct.toDate)
            let date_TimeFrom = dateFormatter2.string(from: dateFrom!)
            let date_TimeTo = dateFormatter2.string(from: dateTo!)
            
            
            param = ["FromDate": date_TimeFrom ,"ToDate": date_TimeTo ]
        }
        
        

        
        LineChartZoneAPI.serviceCalling(obj: self,  param: param ) { (dict) in
            
            self.getZone_MasterData()
            
            self.tableView.reloadData()
            
        }
    }
    
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
