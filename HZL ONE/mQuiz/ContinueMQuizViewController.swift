//
//  ContinueMQuizViewController.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 14/03/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit

class ContinueMQuizViewController:  UIViewController {
    
    @IBOutlet weak var lblResult: UILabel!
    @IBOutlet weak var lblResultVal: UILabel!
    
    var Total_Marks = String()
    var Achieved_Marks = String()
    var Result = String()
    override func viewDidLoad() {
        super.viewDidLoad()
        lblResult.text = Achieved_Marks + "/" + Total_Marks
        lblResultVal.text = Result
        // Do any additional setup after loading the view.
    }
    
    @IBAction func btnContinueClicked(_ sender: UIButton) {
        quizMove.isQuizMove = true
        self.navigationController?.popViewController(animated: false)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
