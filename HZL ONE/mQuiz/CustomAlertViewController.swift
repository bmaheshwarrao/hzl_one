//
//  CustomAlertViewController.swift
//  mQuiz
//
//  Created by SARVANG INFOTCH on 16/11/18.
//  Copyright © 2018 SARVANG INFOTCH. All rights reserved.
//
import UIKit

class CustomAlertViewController: UIViewController {
   
  

   
 
    static func instantiate() -> CustomAlertViewController? {
        return UIStoryboard(name: "mQuiz", bundle: nil).instantiateViewController(withIdentifier: "\(CustomAlertViewController.self)") as? CustomAlertViewController
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        

//        titleLabel.text = titleString
//        messageLabel.text = messageString
    }
    
    // MARK: Actions
    
    @IBAction func submitButtonTapped(_ sender: UIButton) {
         NotificationCenter.default.post(name:NSNotification.Name.init("submitQuiz"), object: nil)
        dismiss(animated: true, completion: nil)
    }
    @IBAction func closeButtonTapped(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
}
