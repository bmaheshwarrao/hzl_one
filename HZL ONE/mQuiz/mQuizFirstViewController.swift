//
//  mQuizFirstViewController.swift
//  mQuiz
//
//  Created by SARVANG INFOTCH on 12/11/18.
//  Copyright © 2018 SARVANG INFOTCH. All rights reserved.
//

import UIKit

class mQuizFirstViewController: CommonVSClass {
    var quizDB:[QuizListModel] = []
    var quizLoadMoreDB:[QuizListModel] = []
    var QuizAPI = QuizListAPI()
    var QuizLoadMoreAPI = QuizListLoadMoreAPI()
    var data: String?
    var lastObject: String?
    var QuizStrNav = String()
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
        tableView.tableFooterView?.backgroundColor = UIColor.groupTableViewBackground
        // Do any additional setup after loading the view.
    }
    @IBAction func mQuizHistoryClicked(_ sender: UIBarButtonItem) {
        let storyboard1 = UIStoryboard(name: "mQuiz", bundle: nil)
        let mQuizVC = storyboard1.instantiateViewController(withIdentifier: "mQuizHistoryViewController") as! mQuizHistoryViewController
        self.navigationController?.pushViewController(mQuizVC, animated: true)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        if(MoveStruct.isMove == true){
            MoveStruct.isMove = false
            self.view.makeToast(MoveStruct.message)
        }
      QuizResult()
        self.title = QuizStrNav
        self.tabBarController?.tabBar.isHidden = true
        let nav = self.navigationController?.navigationBar
        nav?.tintColor = UIColor.white
       nav?.barTintColor = UIColor(hexString: "2c3e50", alpha: 1.0)
        nav?.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
       
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnStudyMaterialClicked(_ sender: UIButton) {
        let buttonPosition:CGPoint = sender.convert(.zero, to:self.tableView)
        let indexPath = self.tableView.indexPathForRow(at: buttonPosition)
        let storyboard = UIStoryboard(name: "mQuiz", bundle: nil)
        let ZIVC = storyboard.instantiateViewController(withIdentifier: "ELPolicyViewController") as! ELPolicyViewController
        ZIVC.QuizId  = quizDB[(indexPath?.section)!].ID!
        self.navigationController?.pushViewController(ZIVC, animated: true)
        
        
    }
    @objc func tapCell(_ sender: UITapGestureRecognizer) {
        let buttonPosition:CGPoint = sender.view!.convert(.zero, to:self.tableView)
        let indexPath = self.tableView.indexPathForRow(at: buttonPosition)
        let storyboard = UIStoryboard(name: "mQuiz", bundle: nil)
        let ZIVC = storyboard.instantiateViewController(withIdentifier: "SubmitQuestionsViewController") as! SubmitQuestionsViewController
        quizMove.isQuizMove = false
        ZIVC.QuizId  = quizDB[(indexPath?.section)!].ID!
        self.navigationController?.pushViewController(ZIVC, animated: true)
        
    }
    @objc func QuizResult() {
       var para = [String:String]()
        let parameter = ["Employee_ID":UserDefaults.standard.string(forKey: "EmployeeID")!]
        
        para = parameter.filter { $0.value != ""}
        print("para",para)
        self.QuizAPI.serviceCalling(obj: self, parameter: para) { (dict) in
            
            self.quizDB = [QuizListModel]()
            self.quizDB = dict as! [QuizListModel]
            print(self.quizDB)
            self.tableView.reloadData()
        }
        
    }
      var constLeading : CGFloat = 0
    @objc func QuizResultLoadMore(Id:String) {
        
        
        var para = [String:String]()
        let parameter = ["ID":Id
            , "Employee_ID":UserDefaults.standard.string(forKey: "EmployeeID")!]
        
        para = parameter.filter { $0.value != ""}
        print("para",para)
        
        self.QuizLoadMoreAPI.serviceCalling(obj: self, parameter: para) { (dict) in
            
            self.quizLoadMoreDB = [QuizListModel]()
            self.quizLoadMoreDB = dict as! [QuizListModel]
            
            switch self.quizLoadMoreDB.count {
            case 0:
                break;
            default:
                self.quizDB.append(contentsOf: self.quizLoadMoreDB)
                self.tableView.reloadData()
                break;
            }
            
        }
        
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension mQuizFirstViewController : UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if(section == 0) {
            return 10.0
        } else {
            return 20.0
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
       // return 200
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return quizDB.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    
}

extension mQuizFirstViewController : UITableViewDataSource {
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! mQuizFirstTableViewCell
        
        cell.QuizName.text = quizDB[indexPath.section].Test_Title
         cell.totalQuestionLabel.text = quizDB[indexPath.section].No_of_Questions
         cell.totalmarksLabel.text = quizDB[indexPath.section].Total_Marks
        // cell.dateLabel.text = quizDB[indexPath.section].To_Date
        cell.marksperQuestionLabel.text = quizDB[indexPath.section].Marks_Per_Question
        
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = NSTimeZone.system
        dateFormatter.dateFormat = "MMM dd yyyy HH:mm:ss:SSSa"
        let date = dateFormatter.date(from: self.quizDB[indexPath.section].From_Date!)
        
        let dateFormatter2 = DateFormatter()
        dateFormatter2.timeZone = NSTimeZone.system
        dateFormatter2.dateFormat = "dd MMM"
        var dateStr = String()
        switch date {
        case nil:
            let date_TimeStr = dateFormatter2.string(from: Date())
           dateStr = date_TimeStr
            break;
        default:
            let date_TimeStr = dateFormatter2.string(from: date!)
            
           dateStr = date_TimeStr
            break;
        }
        
        let dateFormatter1 = DateFormatter()
        dateFormatter1.timeZone = NSTimeZone.system
        dateFormatter1.dateFormat = "MMM dd yyyy HH:mm:ss:SSSa"
        let date1 = dateFormatter.date(from: self.quizDB[indexPath.section].To_Date!)
        
        let dateFormatter21 = DateFormatter()
        dateFormatter21.timeZone = NSTimeZone.system
        dateFormatter21.dateFormat = "dd MMM"
        
        
       
        var dateStr1 = String()
        switch date1 {
        case nil:
            let date_TimeStr = dateFormatter21.string(from: Date())
          
            dateStr1 = date_TimeStr
            break;
        default:
            let date_TimeStr = dateFormatter21.string(from: date1!)
            
           
            dateStr1 = date_TimeStr
            break;
        }
        cell.dateLabel.text = dateStr + "-" + dateStr1
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tapCell(_:)))
        cell.isUserInteractionEnabled = true;
        cell.addGestureRecognizer(tapGesture)
        self.data = self.quizDB[indexPath.section].ID
        self.lastObject = self.quizDB[indexPath.section].ID
      
      
        if ( self.data ==  self.lastObject && indexPath.section == self.quizDB.count - 1)
        {
            
            self.QuizResultLoadMore(Id: self.quizDB[indexPath.section].ID!)
            
        }
        return cell;
}
}
