//
//  SafetyWhistleBlowerTableViewCell.swift
//  test
//
//  Created by Bunga Maheshwar Rao on 15/02/19.
//  Copyright © 2019 Bunga Maheshwar Rao. All rights reserved.
//

import UIKit

class SafetyWhistleBlowerTableViewCell: UITableViewCell {

    
    @IBOutlet weak var viewZone: UIView!
    @IBOutlet weak var btnZone: UIButton!
    @IBOutlet weak var lblZone: UILabel!
    @IBOutlet weak var viewUnit: UIView!
    @IBOutlet weak var btnUnit: UIButton!
    @IBOutlet weak var lblUnit: UILabel!
    @IBOutlet weak var viewArea: UIView!
    @IBOutlet weak var btnArea: UIButton!
    @IBOutlet weak var lblArea: UILabel!
    @IBOutlet weak var viewRiskPotential: UIView!
    @IBOutlet weak var btnRiskPotential: UIButton!
    @IBOutlet weak var lblRiskPotential: UILabel!
    
    @IBOutlet weak var textViewRiskObservation: UITextView!
    
    @IBOutlet weak var viewSituation: UIView!
    @IBOutlet weak var btnSituation: UIButton!
    @IBOutlet weak var lblSituation: UILabel!
    @IBOutlet weak var textViewOtherDetail: UITextView!
    @IBOutlet weak var viewReportee: UIView!
    @IBOutlet weak var textReportee: UITextField!
    @IBOutlet weak var viewContact: UIView!
    @IBOutlet weak var textContact: UITextField!
    
    @IBOutlet weak var lblOptional: UILabel!
    @IBOutlet weak var heightViewImage: NSLayoutConstraint!
    @IBOutlet weak var heightImageselected: NSLayoutConstraint!
    @IBOutlet weak var heightViewofClose: NSLayoutConstraint!
    
    @IBOutlet weak var btnClose: UIButton!
    @IBOutlet weak var imageViewPhoto: UIImageView!
    @IBOutlet weak var btnCamera: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        viewArea.layer.borderWidth = 1.0
        viewArea.layer.borderColor = UIColor.black.cgColor
        viewArea.layer.cornerRadius = 10.0
        viewUnit.layer.borderWidth = 1.0
        viewUnit.layer.borderColor = UIColor.black.cgColor
        viewUnit.layer.cornerRadius = 10.0
        
        viewZone.layer.borderWidth = 1.0
        viewZone.layer.borderColor = UIColor.black.cgColor
        viewZone.layer.cornerRadius = 10.0
        viewRiskPotential.layer.borderWidth = 1.0
        viewRiskPotential.layer.borderColor = UIColor.black.cgColor
        viewRiskPotential.layer.cornerRadius = 10.0
        
        textViewRiskObservation.layer.borderWidth = 1.0
        textViewRiskObservation.layer.borderColor = UIColor.black.cgColor
        textViewRiskObservation.layer.cornerRadius = 10.0
        
        viewSituation.layer.borderWidth = 1.0
        viewSituation.layer.borderColor = UIColor.black.cgColor
        viewSituation.layer.cornerRadius = 10.0
        
        textViewOtherDetail.layer.borderWidth = 1.0
        textViewOtherDetail.layer.borderColor = UIColor.black.cgColor
        textViewOtherDetail.layer.cornerRadius = 10.0
        
        
        viewReportee.layer.borderWidth = 1.0
        viewReportee.layer.borderColor = UIColor.black.cgColor
        viewReportee.layer.cornerRadius = 10.0
        
        viewContact.layer.borderWidth = 1.0
        viewContact.layer.borderColor = UIColor.black.cgColor
        viewContact.layer.cornerRadius = 10.0
        textContact.keyboardType = UIKeyboardType.numberPad
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
