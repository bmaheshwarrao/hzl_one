
//
//  SafetyWhisleBlowerViewController.swift
//  test
//
//  Created by Bunga Maheshwar Rao on 15/02/19.
//  Copyright © 2019 Bunga Maheshwar Rao. All rights reserved.
//

import UIKit
import Alamofire
class SafetyWhisleBlowerViewController: CommonVSClass, UIImagePickerControllerDelegate,UINavigationControllerDelegate {

    @IBOutlet weak var tableView: UITableView!
    
   var SafetyStrNav = String()
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
        // Do any additional setup after loading the view.
    }

    @IBAction func btnImageButtonClicked(_ sender: UIButton) {
        
        let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
            self.openCamera()
        }))
        
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
            self.openGallary()
        }))
        
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
        
    }
    
    
    @objc func pressCamera(_ sender : UITapGestureRecognizer) {
        
    }
    let imagePicker = UIImagePickerController()
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera))
        {
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func openGallary()
    {
        imagePicker.delegate = self
        imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
        imagePicker.allowsEditing = true
        self.present(imagePicker, animated: true, completion: nil)
    }
    internal func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
            cellData.imageViewPhoto.image = image
            imagedata = UIImageJPEGRepresentation(image, 1.0)!
            cellData.btnCamera.isHidden = true
            cellData.lblOptional.isHidden = true
            cellData.btnClose.isHidden = false
            cellData.imageViewPhoto.isHidden = false
            updateConst()
            
            
        }else{
            print("Something went wrong")
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    // var reachability = Reachability()!
    var imagedata: Data? = nil
    @IBAction func btnCloseButtonClicked(_ sender: UIButton) {
        
        self.imagedata = nil
        self.cellData.btnClose.isHidden = true
        cellData.btnCamera.isHidden = false
        cellData.lblOptional.isHidden = false
        updateConst()
    }
    func updateConst(){
       
            
            if(self.cellData.btnClose.isHidden == true){
                self.cellData.heightViewofClose.constant = 0
                self.cellData.heightImageselected.constant = 0
                self.cellData.heightViewImage.constant = 40
            }else{
                self.cellData.heightViewofClose.constant = 25
                self.cellData.heightImageselected.constant = 100
                self.cellData.heightViewImage.constant = 150
            }
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /// Fusuma
    
    
    func imageResize (image:UIImage, sizeChange:CGSize)-> UIImage{
        
        let hasAlpha = true
        let scale: CGFloat = 0.0 // Use scale factor of main screen
        
        UIGraphicsBeginImageContextWithOptions(sizeChange, !hasAlpha, scale)
        image.draw(in: CGRect(origin: CGPoint.zero, size: sizeChange))
        
        let scaledImage = UIGraphicsGetImageFromCurrentImageContext()
        return scaledImage!
    }
    var reachablty = Reachability()!
    @IBAction func btnSubmitClicked(_ sender: UIButton) {
        

    
    if reachablty.connection == .none{
    
    self.view.makeToast("Internet is not available, please check your internet connection try again.")
    
    }
    else if ( FilterDataFromServer.location_name == String()) {
    
    self.view.makeToast("Please Select Area")
    }
    else if ( FilterDataFromServer.unit_Name == String()) {
    
    self.view.makeToast("Please Select Unit")
    }
    else if ( FilterDataFromServer.area_Name == String()) {
    
    self.view.makeToast("Please Select Area")
    }
    
    else if (cellData.textViewRiskObservation.text == "") {
    
    self.view.makeToast("Please Write Risk Observation")
    }
    else if (cellData.lblRiskPotential.text == "" || cellData.lblRiskPotential.text == "Select") {
    
    self.view.makeToast("Please Select Risk Potential" )
    }else if (cellData.lblSituation.text == "" || cellData.lblRiskPotential.text == "Select") {
    
    self.view.makeToast("Please Select Situation")
    }else{
    
    
    
    self.startLoadingPK(view: self.view)
    
    
    
    
    
    let parameter = ["Zone_ID":String(FilterDataFromServer.location_id),
    "RiskPotential":cellData.lblRiskPotential.text!,
    "RiskObservation":cellData.textViewRiskObservation.text!,
    "Unit_ID": String(FilterDataFromServer.unit_Id),
    "Area_ID":String(FilterDataFromServer.area_Id) ,
    "AnyOtherDetails":cellData.textViewOtherDetail.text! ,
    "Reportee":cellData.textReportee.text!,
    "ContactNumber": cellData.textContact.text!,
     "Platform": URLConstants.platform,
    "Situation":  cellData.lblSituation.text!
    ] as [String:String]
    
    print(parameter)
    let jsonData: Data? = try? JSONSerialization.data(withJSONObject: parameter, options: [])
    let para: [String: Data] = ["data": jsonData!]
    
    Alamofire.upload(multipartFormData: { multipartFormData in
    
    if !(self.imagedata == nil) {
    var res : UIImage = UIImage()
    
    let size = CGSize(width: 150, height: 150)
    res = self.imageResize(image: self.cellData.imageViewPhoto.image!,sizeChange: size)
    
    self.imagedata = UIImageJPEGRepresentation(res, 1.0)!
    multipartFormData.append(self.imagedata!, withName: "file", fileName: "file.jpg", mimeType: "image/jpeg")
    }
    
    for (key, value) in para {
    multipartFormData.append(value, withName: key)
    
    
    }
    
    },
    to:URLConstants.Safety_whistle_blower)
    { (result) in
    switch result {
    case .success(let upload, _, _):
    
    upload.uploadProgress(closure: { (progress) in
    
    })
    
    
    upload.responseJSON { response in
    
    
    if let json = response.result.value
    {
    var dict = json as! [String: AnyObject]
    
    if let response = dict["response"]{
    print(response)
    let statusString = response["status"] as! String
    let objectmsg = MessageCallServerModel()
    let msg = objectmsg.sealizeMessage(cell: response as! [String : AnyObject])
    print("HZl Dict :",dict)
    if(statusString == "success")
    {
    
    
    self.stopLoadingPK(view: self.view)
    MoveStruct.isMove = true
    
    MoveStruct.message = msg
    
    
    self.navigationController?.popViewController(animated: false)
    
    
    
    
    }
    else{
    
    self.stopLoadingPK(view: self.view)
    self.view.makeToast(msg)
    
    }
    
    
    
    }
    
    
    }
    
    
    }
    
    case .failure(let encodingError):
    
    self.stopLoadingPK(view: self.view)
    self.errorChecking(error: encodingError)
    
    print(encodingError.localizedDescription)
    }
    }
    }
    
    
    
    
    
        
        
        
    }
    var riskPotentialArray = ["Fatality","Minor injury","Major injury","Property damage","Occupation health"]
     @IBAction func btnRiskPotentailClicked(_ sender: UIButton) {
        
        DispatchQueue.main.async(execute: {() -> Void in
            let popup = PopUpTableViewController()
            
            popup.itemsArray = self.riskPotentialArray
            
            popup.sourceView = self.cellData.lblRiskPotential
            popup.isScroll = true
            popup.backgroundColor = UIColor.white
            if popup.itemsArray.count > 5{
                popup.popUpHeight = 200
            }
            else{
                popup.popUpHeight = CGFloat(popup.itemsArray.count)*45
            }
            popup.popUpWidth = UIScreen.main.bounds.size.width-60
            popup.backgroundImage = nil
            popup.itemTitleColor = UIColor.white
            popup.itemSelectionColor = UIColor.lightGray
            popup.arrowDirections = .any
            popup.arrowColor = UIColor.white
            popup.popCellBlock = {(_ popupVC: PopUpTableViewController, _ popupCell: UITableViewCell, _ row: Int, _ section: Int) -> Void in
                
                
                
                
                
                self.cellData.lblRiskPotential.text = self.riskPotentialArray[row]
                self.cellData.lblRiskPotential.textColor = UIColor.black
                self.updateConst()
                
                popupVC.dismiss(animated: false, completion: nil)
                
            }
            self.present(popup, animated: true, completion: {() -> Void in
            })
            
            
        })
        
        
        
        
        
        
        
        
    }
    var sitautionArray = ["Unsafe Act","Unsafe Condition"]
    @IBAction func btnSituationClicked(_ sender: UIButton) {
        DispatchQueue.main.async(execute: {() -> Void in
            let popup = PopUpTableViewController()
            
            popup.itemsArray = self.sitautionArray
            
            popup.sourceView = self.cellData.lblSituation
            popup.isScroll = true
            popup.backgroundColor = UIColor.white
            if popup.itemsArray.count > 5{
                popup.popUpHeight = 200
            }
            else{
                popup.popUpHeight = CGFloat(popup.itemsArray.count)*45
            }
            popup.popUpWidth = UIScreen.main.bounds.size.width-60
            popup.backgroundImage = nil
            popup.itemTitleColor = UIColor.white
            popup.itemSelectionColor = UIColor.lightGray
            popup.arrowDirections = .any
            popup.arrowColor = UIColor.white
            popup.popCellBlock = {(_ popupVC: PopUpTableViewController, _ popupCell: UITableViewCell, _ row: Int, _ section: Int) -> Void in
                
                
                
                
                
                self.cellData.lblSituation.text = self.sitautionArray[row]
                self.cellData.lblSituation.textColor = UIColor.black
                self.updateConst()
                
                popupVC.dismiss(animated: false, completion: nil)
                
            }
            self.present(popup, animated: true, completion: {() -> Void in
            })
            
            
        })
    }
    
    
    
    
    
    
    
    
    var indexing : Int = 1
    var cellData : SafetyWhistleBlowerTableViewCell!
    @IBAction func btnAreaClicked(_ sender: UIButton) {
        if(FilterDataFromServer.unit_Name != "" && FilterDataFromServer.location_name != "") {
            
            
            FilterDataFromServer.filterType = "Area"
            FilterDataFromServer.area_Id = Int()
            FilterDataFromServer.area_Name = String()
            
            FilterDataFromServer.sub_Area_Id = Int()
            FilterDataFromServer.sub_Area_Name = String()
            updateData()
            
            let storyBoard = UIStoryboard(name: "ThemeSafety", bundle: nil)
            let ZIVC = storyBoard.instantiateViewController(withIdentifier: "ThemeBasedASUZViewController") as! ThemeBasedASUZViewController
            ZIVC.titleStr = "Select Area"
            self.navigationController?.pushViewController(ZIVC, animated: true)
            
            
            
            
        }
    }
    @IBAction func btnUnitClicked(_ sender: UIButton) {
        if(FilterDataFromServer.location_name != "") {
            FilterDataFromServer.filterType = "Unit"
            FilterDataFromServer.unit_Id = Int()
            FilterDataFromServer.unit_Name = String()
            FilterDataFromServer.area_Id = Int()
            FilterDataFromServer.area_Name = String()
            FilterDataFromServer.sub_Area_Id = Int()
            FilterDataFromServer.sub_Area_Name = String()
            updateData()
            
            
            let storyBoard = UIStoryboard(name: "ThemeSafety", bundle: nil)
            let ZIVC = storyBoard.instantiateViewController(withIdentifier: "ThemeBasedASUZViewController") as! ThemeBasedASUZViewController
            ZIVC.titleStr = "Select Unit"
            self.navigationController?.pushViewController(ZIVC, animated: true)
        }
    }
    @IBAction func btnZoneClicked(_ sender: UIButton) {
        FilterDataFromServer.filterType = "Location"
        FilterDataFromServer.unit_Id = Int()
        FilterDataFromServer.unit_Name = String()
        FilterDataFromServer.area_Id = Int()
        FilterDataFromServer.area_Name = String()
        FilterDataFromServer.sub_Area_Id = Int()
        FilterDataFromServer.sub_Area_Name = String()
        FilterDataFromServer.location_name = String()
        FilterDataFromServer.location_id = Int()
        updateData()
        
        
        let storyBoard = UIStoryboard(name: "ThemeSafety", bundle: nil)
        let ZIVC = storyBoard.instantiateViewController(withIdentifier: "ThemeBasedASUZViewController") as! ThemeBasedASUZViewController
        ZIVC.titleStr = "Select Zone"
        self.navigationController?.pushViewController(ZIVC, animated: true)
    }
    
    @objc func updateData(){
       
        if(FilterDataFromServer.location_name != "") {
            cellData.lblZone.text = FilterDataFromServer.location_name
            cellData.lblZone.textColor = UIColor.black
        } else {
            cellData.lblZone.text = "Select Zone"
            cellData.lblZone.textColor = UIColor.lightGray
        }
        if(FilterDataFromServer.unit_Name != "") {
            cellData.lblUnit.text = FilterDataFromServer.unit_Name
            cellData.lblUnit.textColor = UIColor.black
        } else {
            cellData.lblUnit.text = "Select Unit"
            cellData.lblUnit.textColor = UIColor.lightGray
        }
        if(FilterDataFromServer.area_Name != "") {
            cellData.lblArea.text = FilterDataFromServer.area_Name
            cellData.lblArea.textColor = UIColor.black
        } else {
            cellData.lblArea.text = "Select Area"
            cellData.lblArea.textColor = UIColor.lightGray
        }
        
        // tableView.reloadData()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.tabBarController?.tabBar.isHidden = true
        self.title = SafetyStrNav
        
        let nav = self.navigationController?.navigationBar
        nav?.tintColor = UIColor.white
       nav?.barTintColor = UIColor(hexString: "2c3e50", alpha: 1.0)
        nav?.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        if(FilterDataFromServer.dept_Name != "" || FilterDataFromServer.unit_Name != "" || FilterDataFromServer.area_Name != "" || FilterDataFromServer.location_name != ""){
            updateData()
        }
        //updateData()
        
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
extension SafetyWhisleBlowerViewController : UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 1035
        
    }
}
extension SafetyWhisleBlowerViewController : UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! SafetyWhistleBlowerTableViewCell
        
        cellData = cell
        
        if(indexing == 1){
            cellData.btnClose.isHidden = true
            indexing = indexing + 1
        }
        updateData()
        updateConst()
        if(cell.lblRiskPotential.text == ""){
            cell.lblRiskPotential.text = "Select"
             cell.lblRiskPotential                   .textColor  =  UIColor.lightGray    
        }
        if(cell.lblSituation.text == ""){
            cell.lblSituation.text = "Select"
            cell.lblSituation.textColor = UIColor.lightGray
        }
        cell.separatorInset = UIEdgeInsetsMake(0, 10000, 0, 0);
        return cell
        
        
        
    }
}
