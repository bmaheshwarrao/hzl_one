//
//  MessagesModel.swift
//  Balco Sustainability Plus
//
//  Created by sudheer-kumar on 19/12/17.
//  Copyright © 2017 safiqul islam. All rights reserved.
//

import Foundation
import Reachability


//class MessagesDataModel: NSObject {
//
//    var ID = Int()
//    var SenderID = String()
//    var Message = String()
//    var Entry_DateTime = String()
//    var Priority = String()
//    var Status = String()
//    var Action_ID = String()
//    var Action_Type = String()
//var DisplayStatus = String()
//    var ImageURL = String()
//    var PuchID = String()
//
//
//    func setMessagesDataInModel(str:[String:AnyObject])
//    {
//        if str["ID"] is NSNull{
//            self.ID = 0
//        }else{
//            self.ID = (str["ID"] as? Int)!
//        }
//        if str["PuchID"] is NSNull{
//
//        }else{
//            self.PuchID = (str["PuchID"] as? String)!
//        }
//        if str["DisplayStatus"] is NSNull{
//
//        }else{
//            self.DisplayStatus = (str["DisplayStatus"] as? String)!
//        }
//        if str["Message"] is NSNull{
//
//        }else{
//            self.Message = (str["Message"] as? String)!
//        }
//        if str["NotificationDateTime"] is NSNull{
//
//        }else{
//            self.Entry_DateTime = (str["NotificationDateTime"] as? String)!
//        }
//        if str["SenderID"] is NSNull{
//
//        }else{
//            self.SenderID = (str["SenderID"] as? String)!
//        }
//        if str["Priority"] is NSNull{
//
//        }else{
//            self.Priority = (str["Priority"] as? String)!
//        }
//        if str["Action_ID"] is NSNull{
//
//        }else{
//            self.Action_ID = (str["ActionData"] as? String)!
//        }
//
//        if str["Status"] is NSNull{
//
//        }else{
//            self.Status = (str["Status"] as? String)!
//        }
//        if str["ActionType"] is NSNull{
//
//        }else{
//            self.Action_Type = (str["ActionType"] as? String)!
//        }
//        if str["ImageURL"] is NSNull{
//
//        }else{
//            self.ImageURL = (str["ImageURL"] as? String)!
//        }
//
//    }
//
//}


class MessagesDataModel: NSObject {
    
    var ID = Int()
    var Content_Text = String()
    var Content_Type = String()
    var Date_Time = String()
    var Action = String()
    var Action_ID = String()
    var Action_Type = String()
  
    
   
    
    func setMessagesDataInModel(str:[String:AnyObject])
    {
        if str["ID"] is NSNull{
            self.ID = 0
        }else{
            self.ID = (str["ID"] as? Int)!
        }
        if str["Content_Text"] is NSNull{
            self.Content_Text = ""
        }else{
            self.Content_Text = (str["Content_Text"] as? String)!
        }
        if str["Content_Type"] is NSNull{
            self.Content_Type = ""
        }else{
            self.Content_Type = (str["Content_Type"] as? String)!
        }
       
        if str["Date_Time"] is NSNull{
            self.Date_Time = ""
        }else{
            self.Date_Time = (str["Date_Time"] as? String)!
        }
       
        if str["Action"] is NSNull{
            
        }else{
            self.Action = (str["Action"] as? String)!
        }
        if str["Action_ID"] is NSNull{
            
        }else{
            self.Action_ID = (str["Action_ID"] as? String)!
        }
        
       
        if str["Action_Type"] is NSNull{
            
        }else{
            self.Action_Type = (str["Action_Type"] as? String)!
        }
       
        
    }
    
}


class messagesNotiDataAPI
{
    
    var reachablty = Reachability()!
    
    func serviceCalling(obj:MessageViewController, param : [String: String] , success:@escaping (AnyObject)-> Void)
    {
        obj.startLoading()
        
        if(reachablty.connection != .none)
        {
            
          
            WebServices.sharedInstances.sendPostRequest(url: URLConstants.Notification_List, parameters: param , successHandler: { (dict) in
                
                if let response = dict["response"]{
                    
                    let statusString : String = response["status"] as! String
                    let objectmsg = MessageCallServerModel()
                    let msg = objectmsg.sealizeMessage(cell: response as! [String : AnyObject])
                    if(statusString == "success")
                    {
                        obj.tableView.isHidden = false
                        obj.noDataLabel(text: "" )
                        obj.label.isHidden = true;
                        if let data = dict["data"] as? [AnyObject]
                        {
                            
                            var dataArray: [MessagesDataModel] = []
                            
                            for i in 0..<data.count
                            {
                                
                                if let cell = data[i] as? [String:AnyObject]
                                {
                                    let object = MessagesDataModel()
                                    object.setMessagesDataInModel(str: cell)
                                    dataArray.append(object)
                                }
                                
                            }
                            
                            success(dataArray as AnyObject)
                            
                            
                        }
                        
                        obj.refreshControl.endRefreshing()
                        obj.stopLoading()
                        obj.label.isHidden = true;
                    }
                    else
                    {
                       
                        obj.tableView.isHidden = true
                        obj.noDataLabel(text: msg)
                        obj.refreshControl.endRefreshing()
                        obj.stopLoading()
                        obj.label.isHidden = false;
                        
                    }
                    
                    
                }
                
                
                
                
            })
            { (error) in
                
                let errorStr : String = error.description
                
                print("DATA:error",errorStr)
               
//                obj.errorChecking(error: error)
                obj.stopLoading()
            }
            
            
        }
        else{
            obj.tableView.isHidden = true
            obj.noDataLabel(text: "Internet is not available, please check your internet connection try again." )
            obj.label.isHidden = false;
            
            obj.stopLoading()
        }
        
        
    }
    
    
}

class messagesNotiDataAPILoadMore
{
    
    var reachablty = Reachability()!
    
    func serviceCalling(obj:MessageViewController, param : [String: String] , success:@escaping (AnyObject)-> Void)
    {
        obj.startLoading()
        
        if(reachablty.connection != .none)
        {
            
            
            WebServices.sharedInstances.sendPostRequest(url: URLConstants.Notification_List, parameters: param , successHandler: { (dict) in
                
                if let response = dict["response"]{
                    
                    let statusString : String = response["status"] as! String
                    
                    if(statusString == "success")
                    {
                        obj.tableView.isHidden = false
                        obj.noDataLabel(text: "" )
                        obj.label.isHidden = true;
                        
                        if let data = dict["data"] as? [AnyObject]
                        {
                            
                            var dataArray: [MessagesDataModel] = []
                            
                            for i in 0..<data.count
                            {
                                
                                if let cell = data[i] as? [String:AnyObject]
                                {
                                    let object = MessagesDataModel()
                                    object.setMessagesDataInModel(str: cell)
                                    dataArray.append(object)
                                }
                                
                            }
                            
                            success(dataArray as AnyObject)
                            
                            
                        }
                        
                        obj.refreshControl.endRefreshing()
                        obj.stopLoading()
                    }
                    else
                    {
                        
//                        obj.tableView.isHidden = true
//                        obj.noDataLabel(text: "No Data Found!" )
                        obj.refreshControl.endRefreshing()
                        obj.stopLoading()
                        
                    }
                    
                    
                }
                
                
                
                
            })
            { (error) in
                
                let errorStr : String = error.description
                
                print("DATA:error",errorStr)
                
                //                obj.errorChecking(error: error)
                obj.stopLoading()
            }
            
            
        }
        else{
//            obj.tableView.isHidden = true
//            obj.noDataLabel(text: "Internet is not available, please check your internet connection try again." )
            obj.stopLoading()
        }
        
        
    }
    
    
}
