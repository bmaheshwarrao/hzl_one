//
//  RoundButton.swift
//  HZL CSC
//
//  Created by Bunga Maheshwar Rao on 26/09/18.
//  Copyright © 2018 SARVANG INFOTCH. All rights reserved.
//

import UIKit
import Foundation
@IBDesignable class RoundShapeButton: UIButton
{
    var indexPath: NSIndexPath = NSIndexPath()
    @IBInspectable var cornerRadius: CGFloat = 0{
        didSet{
            self.layer.cornerRadius = cornerRadius
        }
    }
    
    @IBInspectable var borderWidth: CGFloat = 0{
        didSet{
            self.layer.borderWidth = borderWidth
        }
    }
    
    @IBInspectable var borderColor: UIColor = UIColor.clear{
        didSet{
            self.layer.borderColor = borderColor.cgColor
        }
    }
}
