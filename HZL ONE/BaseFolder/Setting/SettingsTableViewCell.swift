//
//  SettingsTableViewCell.swift
//  Balco CRP
//
//  Created by sudheer-kumar on 03/10/17.
//  Copyright © 2017 safiqul islam. All rights reserved.
//

import UIKit

class SettingsTableViewCell: UITableViewCell {

    @IBOutlet weak var profilePic: UIImageView!
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var settingsSwitch: UISwitch!
    
    @IBOutlet weak var viewLine: UIView!
    @IBOutlet weak var changeLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
