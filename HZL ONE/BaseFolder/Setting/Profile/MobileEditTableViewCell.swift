//
//  MobileEditTableViewCell.swift
//  Balco CRP
//
//  Created by sudheer-kumar on 12/10/17.
//  Copyright © 2017 safiqul islam. All rights reserved.
//

import UIKit

class MobileEditTableViewCell: UITableViewCell {

    @IBOutlet weak var userImage: UIImageView!
    
    
    @IBOutlet weak var userName: UILabel!
    
    @IBOutlet weak var personalNo: UILabel!
    
    @IBOutlet weak var mobileNo: UILabel!
    
    
    @IBOutlet weak var gender: UILabel!
    
    
    
    
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
