//
//  PersonalDetailsTableViewCell.swift
//  Balco CRP
//
//  Created by sudheer-kumar on 12/10/17.
//  Copyright © 2017 safiqul islam. All rights reserved.
//

import UIKit

class PersonalDetailsTableViewCell: UITableViewCell {

    @IBOutlet weak var userImage: UIImageView!
    
    @IBOutlet weak var emailId: UILabel!
    
    
//    @IBOutlet weak var SBUName: UILabel!
//
//
//    @IBOutlet weak var department: UILabel!
//
//
//    @IBOutlet weak var designation: UILabel!
    
    
    @IBOutlet weak var lblArea: UILabel!
    
    @IBOutlet weak var lblUnit: UILabel!
    
    @IBOutlet weak var lblZone: UILabel!
    
    @IBOutlet weak var lblDesignation: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
