//
//  NonEmployeeViewController.swift
// RAS
//
//  Created by Bunga Maheshwar Rao on 01/10/18.
//  Copyright © 2018 Bunga Maheshwar Rao. All rights reserved.
//

import UIKit
class NonEmployeeViewController: CommonVSClass {

    override func viewDidLoad() {
        super.viewDidLoad()
        txtNonEmployeePNO.clipsToBounds = true;
        txtNonEmployeePNO.layer.cornerRadius = 10;
        txtNonEmployeePNO.keyboardType = UIKeyboardType.numberPad
        txtPassword.clipsToBounds = true;
        txtPassword.layer.cornerRadius = 10;
        
        // Do any additional setup after loading the view.
    }
    

    @IBOutlet weak var btnLogin: UIButton!
    @IBOutlet weak var txtPassword: MytextField!
    @IBOutlet weak var txtNonEmployeePNO: MytextField!
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func btnLoginClicked(_ sender: UIButton) {
        loginMethod()
    }
    @IBAction func btnRegisterClicked(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let loginVC  =  storyboard.instantiateViewController(withIdentifier: "MainRegisterViewController") as! MainRegisterViewController
        
        let navigationVC = UINavigationController(rootViewController: loginVC)
        navigationVC.navigationBar.barTintColor =  UIColor(red: 41/255, green: 88/255, blue: 144/255, alpha: 1.0)
        navigationVC.navigationBar.isTranslucent = false;
        
        navigationVC.navigationBar.tintColor =  UIColor.white
        self.present(navigationVC, animated: true, completion: nil)
    }
    let reachability = Reachability()!
    func loginMethod() {
        if(self.reachability.connection != .none) {
            
            
            let parameters = ["Email_ID":self.txtNonEmployeePNO.text!,
                              "Password":self.txtPassword.text! , "AppName" : URLConstants.appName]
          
            self.startLoading(view: self.view)
            
            WebServices.sharedInstances.sendPostRequest(url: URLConstants.setLoginDataNonEmployee,parameters: parameters, successHandler: { (dict) in
                
               
                
                print("",dict)
                
                if let response = dict["response"] {
                    
                    let statusString : String = response["status"] as! String
                    let objectmsg = MessageCallServerModel()
            let msg = objectmsg.sealizeMessage(cell: response as! [String : AnyObject])
                    print(msg)
                    if(statusString == "success")
                    {
                        
                      
                        
                        if let data = dict["data"]
                        {
                            var EmployeeID = NSNumber()
                           
                            
                            var Profile_AuthKey = String()
                        
                            EmployeeID = data["Unique Id"] as! NSNumber
                            Profile_AuthKey = data["Profile_AuthKey"] as! String
                            
                           
                            let mobile = data["Mobile_Number"]
                            var mobileno = String()
                            UserDefaults.standard.set(EmployeeID, forKey: "EmployeeID")
                            UserDefaults.standard.set(Profile_AuthKey, forKey: "Profile_AuthKey")
                              UserDefaults.standard.set("C", forKey: "LoginType")
                            var Category = data["Category"]
                             if Category is NSNull {
                                Category = "**"
                            }
                            var Plant = data["Plant"]
                            if Plant is NSNull {
                                Plant = "**"
                            }
                            var Department = data["Department"]
                            if Department is NSNull {
                                Department = "**"
                            }
                            var BusinessType = data["BusinessType"]
                            if BusinessType is NSNull {
                                BusinessType = "**"
                            }
                            
                            var ValidityStart = data["Validity Start"]
                            if ValidityStart is NSNull {
                                ValidityStart = "**"
                            }
                            var ValidityEnd = data["Validity End"]
                            if ValidityEnd is NSNull {
                                ValidityEnd = "**"
                            }
                            var GatePassStatus = data["GatePassStatus"]
                            if GatePassStatus is NSNull {
                                GatePassStatus = "**"
                            }
                            var CName = data["Name"]
                            if CName is NSNull {
                                CName = "**"
                            }
                            var Parent = data["Father/Husband Name"]
                            if Parent is NSNull {
                                Parent = "**"
                            }
                            var Contractor = data["Contractor"]
                            if Contractor is NSNull {
                                Contractor = "**"
                            }
                            var PO = data["PO"]
                            if PO is NSNull {
                                PO = "**"
                            }
                            var IDNO = data["ID NO"]
                            if IDNO is NSNull {
                                IDNO = "**"
                            }
                            var SData = data["S"]
                            if SData is NSNull {
                                SData = "**"
                            }
                            UserDefaults.standard.set(Category, forKey: "Category")
                            UserDefaults.standard.set(Plant, forKey: "Plant")
                            UserDefaults.standard.set(Department, forKey: "Department")
                            UserDefaults.standard.set(BusinessType, forKey: "BusinessType")
                            UserDefaults.standard.set(ValidityStart, forKey: "ValidityStart")
                            UserDefaults.standard.set(ValidityEnd, forKey: "ValidityEnd")
                            UserDefaults.standard.set(GatePassStatus, forKey: "GatePassStatus")
                            UserDefaults.standard.set(CName, forKey: "CName")
                                 UserDefaults.standard.set(Parent, forKey: "Parent")
                                     UserDefaults.standard.set(Contractor, forKey: "Contractor")
                                        UserDefaults.standard.set(PO, forKey: "PO")
                                            UserDefaults.standard.set(IDNO, forKey: "IDNO")
                             UserDefaults.standard.set(SData, forKey: "S")
                            
                            
                            
                            
                            
                            
                            self.stopLoading(view: self.view)
                            
                            
                            
                            
                            if mobile is NSNull {
                                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                let loginVC  =  storyboard.instantiateViewController(withIdentifier: "RegisterViewController") as! RegisterViewController
                                
                                let navigationVC = UINavigationController(rootViewController: loginVC)
                                navigationVC.navigationBar.barTintColor =  UIColor(red: 41/255, green: 88/255, blue: 144/255, alpha: 1.0)
                                navigationVC.navigationBar.isTranslucent = false;
                                
                                navigationVC.navigationBar.tintColor =  UIColor.white
                                self.present(navigationVC, animated: true, completion: nil)
                            }   else {
                                mobileno = data["Mobile_Number"] as! String
                                if (mobileno == "") {
                                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                    let loginVC  =  storyboard.instantiateViewController(withIdentifier: "RegisterViewController") as! RegisterViewController
                                    
                                    let navigationVC = UINavigationController(rootViewController: loginVC)
                                    navigationVC.navigationBar.barTintColor =  UIColor(red: 41/255, green: 88/255, blue: 144/255, alpha: 1.0)
                                    navigationVC.navigationBar.isTranslucent = false;
                                    
                                    navigationVC.navigationBar.tintColor =  UIColor.white
                                    self.present(navigationVC, animated: true, completion: nil)
                                } else {
                                
                                UserDefaults.standard.set(true, forKey: "isLogin")
                                UserDefaults.standard.set(true, forKey: "isContractorLogin")
                                
                                    let storyboard = UIStoryboard(name: "HZLONE", bundle: nil)
                                    let loginVC  =  storyboard.instantiateViewController(withIdentifier: "MainTabBarViewController") as! MainTabBarViewController
                                let navigationVC = UINavigationController(rootViewController: loginVC)
                                 navigationVC.title = ProjectName
                                navigationVC.navigationBar.barTintColor =  UIColor(red: 41/255, green: 88/255, blue: 144/255, alpha: 1.0)
                                navigationVC.navigationBar.isTranslucent = false;
                                self.present(navigationVC, animated: true, completion: nil)
                                }
                            }
                            
                            
                            
                            
                        }
                        
                        
                    }
                    else
                    {
                          self.stopLoading(view: self.view)
                        self.showSingleButtonWithMessage(title: statusString, message: msg, buttonName: "OK")
                    }
                }
                
                
                
                
            }, failureHandler: { (error) in
                
                
               self.stopLoading(view: self.view)
                
                self.errorChecking(error: error[0])
                
                
                
            })
            
            
            
        }
        else {
              self.stopLoading(view: self.view)
            self.showSingleButtonWithMessage(title: "No Network!", message: "No internet connection found. Check your internet connection or try again.", buttonName: "OK")
            
            
            
        }
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
