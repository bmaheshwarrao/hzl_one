//
//  SocialTableViewCell.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 25/01/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit
import CoreData
import SDWebImage
    var dataSocial : [SocialDataModel] = []
class SocialTableViewCell: UITableViewCell,UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    var view = UIView()
    var timer = Timer()
    var lastXAxis = CGFloat()
    var contentOffset = CGPoint()
    var i = CGFloat()
    var imageDB :[BannerImage] = []
    
    var scrollingTimer = Timer()
    
    @IBOutlet weak var socialCollectionView: UICollectionView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.itemSize = self.socialCollectionView.frame.size
        layout.minimumLineSpacing = 0
        layout.minimumInteritemSpacing = 10
        
        self.socialCollectionView.setCollectionViewLayout(layout, animated: false)
        self.socialCollectionView.isPagingEnabled = true
        self.socialCollectionView.alwaysBounceVertical = false
        self.socialCollectionView.delegate = self
        self.socialCollectionView.dataSource = self
        
        getBannerImageData()
        
        // Initialization code
    }
    
    func pageFromMain()
    {
        self.timer.invalidate()
        
        
        self.timer = Timer()
        self.lastXAxis = CGFloat()
        self.contentOffset = CGPoint()
        self.socialCollectionView.contentOffset = CGPoint.init(x: 0, y: 0)
        
    }
    
    func saveimagePathData (imagePath:String, id:Int64,text_Message:String){
        
        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        
        let tasks = BannerImage(context: context)
        
        tasks.imagePath = imagePath
        tasks.id = id
        tasks.text_Message = text_Message
        
        (UIApplication.shared.delegate as! AppDelegate).saveContext()
        
        
    }
    
    @objc func getBannerImageData() {
        
        self.imageDB = [BannerImage]()
        do {
            
            self.imageDB = try context.fetch(BannerImage.fetchRequest())
            //self.refresh.endRefreshing()
            if(self.imageDB.count > 0) {
                //  BannerStruct.bannerPath = self.imageDB[0].text_Message!
            }
            self.socialCollectionView.reloadData()
            
        } catch {
            print("Fetching Failed")
        }
    }
    
    func deleteimagePathData()
    {
        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        
        let req = NSFetchRequest<NSFetchRequestResult>.init(entityName: "BannerImage")
        let batchDeleteRequest = NSBatchDeleteRequest(fetchRequest: req)
        
        do {
            try context.execute(batchDeleteRequest)
            
        } catch {
            // Error Handling
        }
    }
    
    
    var reachablty = Reachability()!
  
    func HomeSocialCalling()
    {
       
        if(reachablty.connection != .none)
        {
            
            let param : [String:String] = [:]
            WebServices.sharedInstances.sendPostRequest(url: URLConstants.Social_Media_Dashboard, parameters: param , successHandler: { (dict) in
                
                if let response = dict["response"]{
                    
                    let statusString : String = response["status"] as! String
                    
                    if(statusString == "success")
                    {
                        
                        if let data = dict["data"] as? [AnyObject]
                        {
                            self.isHidden = false
                            dataSocial = []
                            
                            for i in 0..<data.count
                            {
                                
                                if let cell = data[i] as? [String:AnyObject]
                                {
                                    let object = SocialDataModel()
                                    object.setDataInModel(str: cell)
                                    dataSocial.append(object)
                                }
                                
                            }
                            
                            self.socialCollectionView.reloadData()
                            
                            
                        }
                        
                        
                    }
                    else
                    {
                        self.frame.size.height = 0
                        self.isHidden = true
                        
                    }
                    
                    
                }
                
                
                
                
            })
            { (error) in
                
                let errorStr : String = error.description
                
                print("DATA:error",errorStr)
                
                //                obj.errorChecking(error: error)
                
            }
            
            
        }
        else{
            self.frame.size.height = 0
            self.isHidden = true
        }
        
        
    }
    func imageResize (image:UIImage, sizeChange:CGSize)-> UIImage{
        
        let hasAlpha = true
        let scale: CGFloat = 0.0 // Use scale factor of main screen
        
        UIGraphicsBeginImageContextWithOptions(sizeChange, !hasAlpha, scale)
        image.draw(in: CGRect(origin: CGPoint.zero, size: sizeChange))
        
        let scaledImage = UIGraphicsGetImageFromCurrentImageContext()
        return scaledImage!
    }
    
    
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return dataSocial.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "imageSocial", for: indexPath) as! SocialCollectionViewCell
        
        let urlString = dataSocial[indexPath.row].Banner_URL
        let imageView = UIImageView()
        
        let urlShow = urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
        if let url = NSURL(string: dataSocial[indexPath.row].Banner_URL) {
            
            SDWebImageManager.shared().downloadImage(with: NSURL(string: urlShow!) as URL!, options: .continueInBackground, progress: {
                (receivedSize :Int, ExpectedSize :Int) in
                
            }, completed: {
                (image : UIImage?, error : Error?, cacheType : SDImageCacheType, finished : Bool, url : URL?) in
                if(image != nil) {
                    let size = CGSize(width: 120, height: 100)
                    let imageCell = self.imageResize(image: image!, sizeChange: size)
                    cell.imageSlideData.image = imageCell
                    //  cell.viewImageHold.backgroundColor = UIColor(patternImage: imageCell)
                    
                }else{
                    imageView.image =  UIImage(named: "placed")
                    let size = CGSize(width: 120, height: 100)
                    let imageCell = self.imageResize(image: imageView.image!, sizeChange: size)
                    cell.imageSlideData.image = imageCell
                    // cell.viewImageHold.backgroundColor = UIColor(patternImage: imageCell)
                }
            })
        }else{
            imageView.image =  UIImage(named: "placed")
            let size = CGSize(width: 120, height: 100)
            let imageCell = imageResize(image: imageView.image!, sizeChange: size)
            cell.imageSlideData.image = imageCell
            //cell.viewImageHold.backgroundColor = UIColor(patternImage: imageCell)
        }
        cell.btnSocial.tag = indexPath.row
        return cell
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        return UIEdgeInsetsMake(0, 0, 0, 0)
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        return CGSize(width: 280, height: 180.0)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        
        //print(indexPath.item)
        
        
    }
    
    //    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
    //        if let myCell = cell as? PromotionCollectionViewCell {
    //            myCell.pageControl.currentPage = indexPath.item
    //        }
    //
    //    }
    
    //    @objc func scrollToNextCell(){
    //
    //        let cellSize = CGSize(width:self.contentView.frame.width, height:self.contentView.frame.height)
    //
    //
    //        self.contentOffset = self.socialCollectionView.contentOffset
    //
    //        if(self.lastXAxis == self.contentOffset.x)
    //        {
    //            self.socialCollectionView.contentOffset = CGPoint.init(x: 0, y: 0)
    //
    //        }
    //
    //        self.socialCollectionView.scrollRectToVisible(CGRect(x:self.contentOffset.x + cellSize.width , y:self.contentOffset.y, width:cellSize.width , height:cellSize.height), animated: true)
    //
    //
    //
    //        self.lastXAxis = self.contentOffset.x
    //
    //
    //    }
    //
    //    func startTimer() {
    //
    //        self.timer = Timer.scheduledTimer(timeInterval: 5.0, target: self, selector: #selector(PageTableViewCell.scrollToNextCell), userInfo: nil, repeats: true);
    //
    //
    //    }
    
    
    
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
class SocialDataModel: NSObject {
    
   
    var ID = Int()
    var Platform = String()
    var Banner_URL = String()
    var Account_URL = String()
    var Account_ID = String()
    var Account_Name = String()
    
    
    
    
    
    
    
    
    func setDataInModel(str:[String:AnyObject])
    {
        if str["Platform"] is NSNull || str["Platform"] == nil{
            self.Platform = ""
        }else{
            
            let tit = str["Platform"]
            
            self.Platform = (tit?.description)!
            
            
        }
        if str["Banner_URL"] is NSNull || str["Banner_URL"] == nil{
            self.Banner_URL =  ""
        }else{
            
            let desc = str["Banner_URL"]
            
            self.Banner_URL = (desc?.description)!
            
            
            //  self.Description = (str["Description"] as? String)!
        }
        if str["ID"] is NSNull || str["ID"] == nil{
            self.ID = 0
        }else{
            self.ID = (str["ID"] as? Int)!
        }
        if str["Account_URL"] is NSNull || str["Account_URL"] == nil{
            self.Account_URL = ""
        }else{
            let catId = str["Account_URL"]
            
            self.Account_URL = (catId?.description)!
            
        }
        if str["Account_ID"] is NSNull || str["Account_ID"] == nil{
            self.Account_ID = ""
        }else{
            
            let rem = str["Account_ID"]
            
            self.Account_ID = (rem?.description)!
            
        }
        
        if str["Account_Name"] is NSNull || str["Account_Name"] == nil{
            self.Account_Name = ""
        }else{
            
            let rem = str["Account_Name"]
            
            self.Account_Name = (rem?.description)!
            
        }
    }
    
}
