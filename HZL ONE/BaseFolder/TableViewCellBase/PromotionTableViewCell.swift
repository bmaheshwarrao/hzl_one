//
//  PromotionTableViewCell.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 20/11/18.
//  Copyright © 2018 SARVANG INFOTCH. All rights reserved.
//


import UIKit
import SDWebImage
import CoreData


var dataPromotion : [PromotionDataModel] = []

class PromotionTableViewCell: UITableViewCell,UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    var view = UIView()
    var timer = Timer()
    var lastXAxis = CGFloat()
    var contentOffset = CGPoint()
    var i = CGFloat()
    var imageDB :[BannerImage] = []
    
    var scrollingTimer = Timer()
    
    @IBOutlet weak var imageCollectionView: UICollectionView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        
        self.imageCollectionView.delegate = self
        self.imageCollectionView.dataSource = self
     //   self.imageCollectionView.backgroundColor = UIColor.red
        getBannerImageData()
        
        // Initialization code
    }
    
    func pageFromMain()
    {
        self.timer.invalidate()
        
        
        self.timer = Timer()
        self.lastXAxis = CGFloat()
        self.contentOffset = CGPoint()
        self.imageCollectionView.contentOffset = CGPoint.init(x: 0, y: 0)
        
    }
    
    func saveimagePathData (imagePath:String, id:Int64,text_Message:String){
        
        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        
        let tasks = BannerImage(context: context)
        
        tasks.imagePath = imagePath
        tasks.id = id
        tasks.text_Message = text_Message
        
        (UIApplication.shared.delegate as! AppDelegate).saveContext()
        
        
    }
    
  
    @objc func getBannerImageData() {
        
        self.imageDB = [BannerImage]()
        do {
            
            self.imageDB = try context.fetch(BannerImage.fetchRequest())
            //self.refresh.endRefreshing()
            if(self.imageDB.count > 0) {
                //  BannerStruct.bannerPath = self.imageDB[0].text_Message!
            }
            self.imageCollectionView.reloadData()
            
        } catch {
            print("Fetching Failed")
        }
    }
    
    func deleteimagePathData()
    {
        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        
        let req = NSFetchRequest<NSFetchRequestResult>.init(entityName: "BannerImage")
        let batchDeleteRequest = NSBatchDeleteRequest(fetchRequest: req)
        
        do {
            try context.execute(batchDeleteRequest)
            
        } catch {
            // Error Handling
        }
    }
    var reachablty = Reachability()!
   
    func HomePromotionCalling()
    {
        
        
        if(reachablty.connection != .none)
        {
            
            let param : [String:String] = [:]
            WebServices.sharedInstances.sendPostRequest(url: URLConstants.Dashboard_ShowCase, parameters: param , successHandler: { (dict) in
                
                if let response = dict["response"]{
                    
                    let statusString : String = response["status"] as! String
                    self.isHidden = false
                    if(statusString == "success")
                    {
                        
                        if let data = dict["data"] as? [AnyObject]
                        {
                            
                            dataPromotion = []
                            
                            for i in 0..<data.count
                            {
                                
                                if let cell = data[i] as? [String:AnyObject]
                                {
                                    let object = PromotionDataModel()
                                    object.setDataInModel(str: cell)
                                    dataPromotion.append(object)
                                }
                                
                            }
                            
                            self.imageCollectionView.reloadData()
                            
                            
                        }
                        
                        
                    }
                    else
                    {
                          self.isHidden = true
                        
                    }
                    
                    
                }
                
                
                
                
            })
            { (error) in
                
                let errorStr : String = error.description
                
                print("DATA:error",errorStr)
                
                //                obj.errorChecking(error: error)
                
            }
            
            
        }
        else{
              self.isHidden = true
        }
        
        
    }
    func imageResize (image:UIImage, sizeChange:CGSize)-> UIImage{
        
        let hasAlpha = true
        let scale: CGFloat = 0.0 // Use scale factor of main screen
        
        UIGraphicsBeginImageContextWithOptions(sizeChange, !hasAlpha, scale)
        image.draw(in: CGRect(origin: CGPoint.zero, size: sizeChange))
        
        let scaledImage = UIGraphicsGetImageFromCurrentImageContext()
        return scaledImage!
    }
   
    
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return dataPromotion.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "imagePromotion", for: indexPath) as! PromotionCollectionViewCell  

        
       cell.imagButton.tag = indexPath.row
        
        let urlString = dataPromotion[indexPath.row].Banner
        let imageView = UIImageView()
        
        let urlShow = urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
        if let url = NSURL(string: dataPromotion[indexPath.row].Banner) {
            
            SDWebImageManager.shared().downloadImage(with: NSURL(string: urlShow!) as URL!, options: .continueInBackground, progress: {
                (receivedSize :Int, ExpectedSize :Int) in
                
            }, completed: {
                (image : UIImage?, error : Error?, cacheType : SDImageCacheType, finished : Bool, url : URL?) in
                if(image != nil) {
                    let size = CGSize(width: 120, height: 100)
                    let imageCell = self.imageResize(image: image!, sizeChange: size)
                    cell.imageSlide.image = imageCell
                    //  cell.viewImageHold.backgroundColor = UIColor(patternImage: imageCell)
                    
                }else{
                    imageView.image =  UIImage(named: "placed")
                    let size = CGSize(width: 120, height: 100)
                    let imageCell = self.imageResize(image: imageView.image!, sizeChange: size)
                    cell.imageSlide.image = imageCell
                    // cell.viewImageHold.backgroundColor = UIColor(patternImage: imageCell)
                }
            })
        }else{
            imageView.image =  UIImage(named: "placed")
            let size = CGSize(width: 120, height: 100)
            let imageCell = imageResize(image: imageView.image!, sizeChange: size)
            cell.imageSlide.image = imageCell
            //cell.viewImageHold.backgroundColor = UIColor(patternImage: imageCell)
        }
        
        return cell
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        return UIEdgeInsetsMake(0, 0, 0, 0)
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        return CGSize(width: 280, height: 180.0)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        
        //print(indexPath.item)
        
        
    }
    
//    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
//        if let myCell = cell as? PromotionCollectionViewCell {
//            myCell.pageControl.currentPage = indexPath.item
//        }
//        
//    }
    
//    @objc func scrollToNextCell(){
//
//        let cellSize = CGSize(width:self.contentView.frame.width, height:self.contentView.frame.height)
//
//
//        self.contentOffset = self.imageCollectionView.contentOffset
//
//        if(self.lastXAxis == self.contentOffset.x)
//        {
//            self.imageCollectionView.contentOffset = CGPoint.init(x: 0, y: 0)
//
//        }
//
//        self.imageCollectionView.scrollRectToVisible(CGRect(x:self.contentOffset.x + cellSize.width , y:self.contentOffset.y, width:cellSize.width , height:cellSize.height), animated: true)
//
//
//
//        self.lastXAxis = self.contentOffset.x
//
//
//    }
//
//    func startTimer() {
//
//        self.timer = Timer.scheduledTimer(timeInterval: 5.0, target: self, selector: #selector(PageTableViewCell.scrollToNextCell), userInfo: nil, repeats: true);
//
//
//    }
    
    
    
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
class PromotionDataModel: NSObject {
    
   
    var ID = Int()
    var Banner = String()
    var Typeo = String()
    var Type_Data = String()
    var Date_Time = String()
    
    
    
    
    
    
    
    
    
    func setDataInModel(str:[String:AnyObject])
    {
        if str["Banner"] is NSNull || str["Banner"] == nil{
            self.Banner = ""
        }else{
            
            let tit = str["Banner"]
            
            self.Banner = (tit?.description)!
            
            
        }
        if str["Type_Data"] is NSNull || str["Type_Data"] == nil{
            self.Type_Data =  ""
        }else{
            
            let desc = str["Type_Data"]
            
            self.Type_Data = (desc?.description)!
            
            
            //  self.Description = (str["Description"] as? String)!
        }
        if str["ID"] is NSNull || str["ID"] == nil{
            self.ID = 0
        }else{
            self.ID = (str["ID"] as? Int)!
        }
        if str["Type"] is NSNull || str["Type"] == nil{
            self.Typeo = ""
        }else{
            let catId = str["Type"]
            
            self.Typeo = (catId?.description)!
            
        }
        if str["Date_Time"] is NSNull || str["Date_Time"] == nil{
            self.Date_Time = ""
        }else{
            
            let rem = str["Date_Time"]
            
            self.Date_Time = (rem?.description)!
            
        }
        
      
    }
    
}

