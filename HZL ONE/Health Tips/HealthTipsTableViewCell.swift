//
//  HealthTipsTableViewCell.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 02/02/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit

class HealthTipsTableViewCell: UITableViewCell {

    @IBOutlet weak var healthDateLbl: UILabel!
    @IBOutlet weak var healthTextView: UITextView!
    @IBOutlet weak var healthTiltleLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
