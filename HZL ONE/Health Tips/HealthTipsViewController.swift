//
//  HealthTipsViewController.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 02/02/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit

class HealthTipsViewController: CommonVSClass ,UITableViewDelegate, UITableViewDataSource{

    @IBOutlet weak var tableView: UITableView!
    
    var refreshControl = UIRefreshControl()
    
    var healthTipsDB:[HealthDataModel] = []
    var healthTipsAPI = healthDataAPI()
    
    var healthTipsLoadMoreDB : [HealthDataModel] = []
    
    var StrNav = String()
    
    
    var healthTipsLoadMoreAPI = healthDataAPILoadMore()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        getHealthData()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.estimatedRowHeight = 65
        tableView.rowHeight = UITableViewAutomaticDimension
        
        tableView.tableFooterView = UIView()
        refreshControl.addTarget(self, action: #selector(getHealthData), for: .valueChanged)
        
        self.tableView.addSubview(refreshControl)
        let swipe = UISwipeGestureRecognizer(target: self, action: #selector(self.swipeRecognizer))
        swipe.direction = ([.down])
        self.view.addGestureRecognizer(swipe)
        
        
        
        
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.tabBarController?.tabBar.isHidden = true
        self.title = StrNav
        let nav = self.navigationController?.navigationBar
        nav?.tintColor = UIColor.white
       nav?.barTintColor = UIColor(hexString: "2c3e50", alpha: 1.0)
        nav?.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        
    }
    var reachability = Reachability()!
    @objc func swipeRecognizer(_ sender: UISwipeGestureRecognizer) {
        if (sender.direction == .down) {
            
            if(self.reachability.connection != .none)
            {
                
                self.getHealthData()
            }
            else
            {
                self.startLoading(view: self.view)
                DispatchQueue.main.asyncAfter(deadline: .now() + 4.5) {
                    self.noDataLabel(text: "No internet connection found. Check your internet connection and try again.")
                    self.stopLoading(view: self.view)
                }
            }
            
        }
    }
    @IBAction func tapViewClicked(_ sender: Any) {
        
        
        
        
        
        
        
    }
    @objc func tapViewTapClicked(_ sender: UITapGestureRecognizer) {
        let buttonPosition:CGPoint = sender.view!.convert(.zero, to:self.tableView)
        let indexPath = self.tableView.indexPathForRow(at: buttonPosition)

let storyBoard = UIStoryboard(name: "Health", bundle: nil)
            let ZIVC = storyBoard.instantiateViewController(withIdentifier: "HealthDetailsViewController") as! HealthDetailsViewController
        ZIVC.File_title = self.healthTipsDB[(indexPath?.section)!].Title
        ZIVC.File_Description = self.healthTipsDB[(indexPath?.section)!].Description
            self.navigationController?.pushViewController(ZIVC, animated: true)
        
    
    }
    @IBOutlet var tapView: UITapGestureRecognizer!
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    var data: String?
    var lastObject: String?
    var indPath = IndexPath()
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        
        
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.healthTipsDB.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellHealth", for: indexPath) as! HealthTipsTableViewCell
        
        
        cell.healthTiltleLbl.text = self.healthTipsDB[indexPath.section].Title
        let TapGesture = UITapGestureRecognizer(target: self, action: #selector(self.tapViewTapClicked(_:)))
        cell.addGestureRecognizer(TapGesture)
        cell.isUserInteractionEnabled = true
        TapGesture.numberOfTapsRequired = 1
        if(self.healthTipsDB[indexPath.section].Created_Date != ""){
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = NSTimeZone.system
     
        dateFormatter.dateFormat = "MMM dd yyyy HH:mm:ss:SSSa"
        let date : Date = dateFormatter.date(from: self.healthTipsDB[indexPath.section].Created_Date)!
        dateFormatter.string(from: date)
        let dateFormatter2 = DateFormatter()
        dateFormatter2.timeZone = NSTimeZone.system
        dateFormatter2.dateFormat = "dd MMM yyyy"
        let finalDate = dateFormatter2.string(from: date)
            cell.healthDateLbl.text = finalDate
        }else{
            cell.healthDateLbl.text = ""
        }

        let paragraph = NSMutableParagraphStyle()
        paragraph.alignment = .left
        paragraph.lineSpacing = 0
        let titleStr = NSMutableAttributedString(string: self.healthTipsDB[indexPath.section].Description, attributes: [NSAttributedStringKey.foregroundColor: UIColor.darkGray, NSAttributedStringKey.font: UIFont.systemFont(ofSize: 15.0, weight: .medium)])
        titleStr.addAttribute(NSAttributedStringKey.paragraphStyle, value: paragraph, range: NSRange(location: 0, length: titleStr.length))
        cell.healthTextView.attributedText = titleStr
        cell.healthTextView.textContainer.maximumNumberOfLines = 3
        cell.healthTextView.textContainer.lineBreakMode = .byTruncatingTail
        self.data = String(self.healthTipsDB[indexPath.section].ID)
        self.lastObject = String(self.healthTipsDB[indexPath.section].ID)
        
        if ( self.data ==  self.lastObject && indexPath.section == self.healthTipsDB.count - 1)
        {
            
            self.getHealthDataLoadMore( ID: String(Int(self.healthTipsDB[indexPath.section].ID)))
            
        }
        
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableViewAutomaticDimension
        
    }
    
    @objc func getHealthData(){
        var param = [String:String]()
        
        param =  [:]
        healthTipsAPI.serviceCalling(obj: self,  param: param ) { (dict) in
            
            self.healthTipsDB = dict as! [HealthDataModel]
            
            self.tableView.reloadData()
            
        }
    }
    
    @objc func getHealthDataLoadMore(ID : String){
        var param = [String:String]()
        
        param =  ["ID":ID]
        healthTipsLoadMoreAPI.serviceCalling(obj: self,  param: param ) { (dict) in
            
            self.healthTipsLoadMoreDB =  [HealthDataModel]()
            self.healthTipsLoadMoreDB = dict as! [HealthDataModel]
            
            switch dict.count {
            case 0:
                break;
            default:
                self.healthTipsDB.append(contentsOf: self.healthTipsLoadMoreDB)
                self.tableView.reloadData()
                break;
            }
            
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
