//
//  ElearningHistoryAnsModel.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 08/03/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import Foundation
class QuizHistoryElearningModel: NSObject {
    
    
    
    var Title: String?
    var ID  = Int()
    var Quiz_ID : Int?
    var Achieved_Marks : String?
    var Total_Mark : String?
    var Passing_Mark : String?
    var Date_Time : String?
    var Result : String?
    
    
    
    
    
    func setDataInModel(cell:[String:AnyObject])
    {
        
        if cell["Title"] is NSNull || cell["Title"] == nil {
            self.Title = ""
        }else{
            let qz = cell["Title"]
            
            self.Title = qz?.description
        }
        if cell["Date_Time"] is NSNull || cell["Date_Time"] == nil{
            self.Date_Time = ""
        }else{
            let qz = cell["Date_Time"]
            
            self.Date_Time = qz?.description
        }
        if cell["Result"] is NSNull || cell["Result"] == nil{
            self.Result = ""
        }else{
            let qz = cell["Result"]
            
            self.Result = qz?.description
        }
        if cell["Achieved_Marks"] is NSNull || cell["Achieved_Marks"] == nil{
            self.Achieved_Marks = ""
        }else{
            let qz = cell["Achieved_Marks"]
            
            self.Achieved_Marks = qz?.description
        }
        if cell["Total_Mark"] is NSNull || cell["Total_Mark"] == nil{
            self.Total_Mark = ""
        }else{
            let qz = cell["Total_Mark"]
            
            self.Total_Mark = qz?.description
        }
        if cell["Passing_Mark"] is NSNull || cell["Passing_Mark"] == nil{
            self.Passing_Mark = ""
        }else{
            let qz = cell["Passing_Mark"]
            
            self.Passing_Mark = qz?.description
        }
        
        
        
        
        
        if cell["Quiz_ID"] is NSNull{
            self.Quiz_ID = 0
        }else{
            self.Quiz_ID = (cell["Quiz_ID"] as? Int)!
        }
        if cell["ID"] is NSNull{
            self.ID = 0
        }else{
            self.ID = (cell["ID"] as? Int)!
        }
        
        
        
    }
}


class QuizHistoryElearningAPI
{
    
    var reachablty = Reachability()!
    
    
    func serviceCalling(obj:QuizHistorymLearningViewController,parameter:[String:String], success:@escaping (AnyObject)-> Void)
    {
        
        obj.startLoading()
        
        if(reachablty.connection != .none)
        {
            
            WebServices.sharedInstances.sendPostRequest(url: URLConstants.Quiz_HistoryElearning, parameters: parameter, successHandler: { (dict) in
                
                if let response = dict["response"]{
                      print(response)
                    let statusString : String = response["status"] as! String
                     let objectmsg = MessageCallServerModel()
            let msg = objectmsg.sealizeMessage(cell: response as! [String : AnyObject])
                    if(statusString == "success")
                    {
                        
                        obj.tableView.isHidden = false
                        obj.noDataLabel(text: "" )
                        obj.label.isHidden = true
                        
                        if let data = dict["data"] as? [AnyObject]
                        {
                            
                            var dataArray : [QuizHistoryElearningModel] = []
                            
                            for i in 0..<data.count
                            {
                                
                                if let celll = data[i] as? [String:AnyObject]
                                {
                                    let object = QuizHistoryElearningModel()
                                    object.setDataInModel(cell: celll)
                                    dataArray.append(object)
                                }
                                
                            }
                            
                            success(dataArray as AnyObject)
                            
                            
                        }
                        
                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                    }
                    else
                    {
                        
                        
                        print("DATA:fail")
                        obj.tableView.isHidden = true
                        obj.noDataLabel(text: msg)
                        obj.refresh.endRefreshing()
                        obj.label.isHidden = false
                        obj.stopLoading()
                        
                    }
                    
                    
                }
                
                
                
            }, failureHandler: { (error) in
                let errorStr : String = error.description
                
                print("DATA:error",errorStr)
                
                // obj.errorChecking(error: error)
                obj.stopLoading()
            })
            
            
        }
        else{
            obj.noDataLabel(text:  "No internet connection found. Check your internet connection and try again.")
            obj.tableView.isHidden = true
            obj.label.isHidden = false
            obj.refresh.endRefreshing()
            obj.stopLoading()
        }
        
        
    }
    
    
}

class QuizHistoryElearningLoadMoreAPI
{
    
    var reachablty = Reachability()!
    
    
    func serviceCalling(obj:QuizHistorymLearningViewController,parameter:[String:String], success:@escaping (AnyObject)-> Void)
    {
        
        obj.startLoading()
        
        if(reachablty.connection != .none)
        {
            
            WebServices.sharedInstances.sendPostRequest(url: URLConstants.Quiz_HistoryElearning, parameters: parameter, successHandler: { (dict) in
                
                if let response = dict["response"]{
                      print(response)
                    let statusString : String = response["status"] as! String
                   
                    if(statusString == "success")
                    {
                        
                        //                        obj.tableView.isHidden = false
                        //                        obj.noDataLabel(text: "" )
                        
                        if let data = dict["data"] as? [AnyObject]
                        {
                            
                            var dataArray : [QuizHistoryElearningModel] = []
                            
                            for i in 0..<data.count
                            {
                                
                                if let celll = data[i] as? [String:AnyObject]
                                {
                                    let object = QuizHistoryElearningModel()
                                    object.setDataInModel(cell: celll)
                                    dataArray.append(object)
                                }
                                
                            }
                            
                            success(dataArray as AnyObject)
                            
                            
                        }
                        
                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                    }
                    else
                    {
                        
                        
                        print("DATA:fail")
                        //                        obj.tableView.isHidden = true
                        //                        obj.noDataLabel(text: "No hazard found with "+parameter["Search"]! )
                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                        
                    }
                    
                    
                }
                
                
                
            }, failureHandler: { (error) in
                let errorStr : String = error.description
                
                print("DATA:error",errorStr)
                
                //obj.errorChecking(error: error)
                obj.stopLoading()
            })
            
            
        }
        else{
            //            obj.noDataLabel(text:  "No internet connection found. Check your internet connection and try again.")
            //            obj.tableView.isHidden = true
            obj.refresh.endRefreshing()
            obj.stopLoading()
        }
        
        
    }
    
    
}


class QuizAttemptElearningModel: NSObject {
    
    
    
    var Title: String?
    var ID  = Int()
    var Quiz_ID : Int?
    var Achieved_Marks : String?
    var Total_Mark : String?
    var Passing_Mark : String?
    var Date_Time : String?
    var Result : String?
    
 
    
   
    
    func setDataInModel(cell:[String:AnyObject])
    {
        
        if cell["Title"] is NSNull || cell["Title"] == nil {
            self.Title = ""
        }else{
            let qz = cell["Title"]
            
            self.Title = qz?.description
        }
        if cell["Date_Time"] is NSNull || cell["Date_Time"] == nil{
            self.Date_Time = ""
        }else{
            let qz = cell["Date_Time"]
            
            self.Date_Time = qz?.description
        }
        if cell["Result"] is NSNull || cell["Result"] == nil{
            self.Result = ""
        }else{
            let qz = cell["Result"]
            
            self.Result = qz?.description
        }
        if cell["Achieved_Marks"] is NSNull || cell["Achieved_Marks"] == nil{
            self.Achieved_Marks = ""
        }else{
            let qz = cell["Achieved_Marks"]
            
            self.Achieved_Marks = qz?.description
        }
        if cell["Total_Mark"] is NSNull || cell["Total_Mark"] == nil{
            self.Total_Mark = ""
        }else{
            let qz = cell["Total_Mark"]
            
            self.Total_Mark = qz?.description
        }
        if cell["Passing_Mark"] is NSNull || cell["Passing_Mark"] == nil{
            self.Passing_Mark = ""
        }else{
            let qz = cell["Passing_Mark"]
            
            self.Passing_Mark = qz?.description
        }
        
        
        
        
        
        if cell["Quiz_ID"] is NSNull{
            self.Quiz_ID = 0
        }else{
            self.Quiz_ID = (cell["Quiz_ID"] as? Int)!
        }
        if cell["ID"] is NSNull{
              self.ID = 0
        }else{
            self.ID = (cell["ID"] as? Int)!
        }
        
        
        
    }
}


class QuizAttemptElearningAPI
{
    
    var reachablty = Reachability()!
    
    
    func serviceCalling(obj:QuizAttempedViewController,parameter:[String:String], success:@escaping (AnyObject)-> Void)
    {
        
        obj.startLoading()
        
        if(reachablty.connection != .none)
        {
            
            WebServices.sharedInstances.sendPostRequest(url: URLConstants.My_Course_Attempte_List, parameters: parameter, successHandler: { (dict) in
                
                if let response = dict["response"]{
                    
                    let statusString : String = response["status"] as! String
                     let objectmsg = MessageCallServerModel()
            let msg = objectmsg.sealizeMessage(cell: response as! [String : AnyObject])
                      print(response)
                    if(statusString == "success")
                    {
                        
                        obj.tableView.isHidden = false
                        obj.noDataLabel(text: "" )
                        obj.label.isHidden = true
                        
                        if let data = dict["data"] as? [AnyObject]
                        {
                            
                            var dataArray : [QuizAttemptElearningModel] = []
                            
                            for i in 0..<data.count
                            {
                                
                                if let celll = data[i] as? [String:AnyObject]
                                {
                                    let object = QuizAttemptElearningModel()
                                    object.setDataInModel(cell: celll)
                                    dataArray.append(object)
                                }
                                
                            }
                            
                            success(dataArray as AnyObject)
                            
                            
                        }
                        
                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                    }
                    else
                    {
                        
                        
                        print("DATA:fail")
                        obj.tableView.isHidden = true
                        obj.noDataLabel(text: msg)
                        obj.refresh.endRefreshing()
                        obj.label.isHidden = false
                        obj.stopLoading()
                        
                    }
                    
                    
                }
                
                
                
            }, failureHandler: { (error) in
                let errorStr : String = error.description
                
                print("DATA:error",errorStr)
                
                // obj.errorChecking(error: error)
                obj.stopLoading()
            })
            
            
        }
        else{
            obj.noDataLabel(text:  "No internet connection found. Check your internet connection and try again.")
            obj.tableView.isHidden = true
            obj.label.isHidden = false
            obj.refresh.endRefreshing()
            obj.stopLoading()
        }
        
        
    }
    
    
}

class QuizAttemptElearningLoadMoreAPI
{
    
    var reachablty = Reachability()!
    
    
    func serviceCalling(obj:QuizAttempedViewController,parameter:[String:String], success:@escaping (AnyObject)-> Void)
    {
        
        obj.startLoading()
        
        if(reachablty.connection != .none)
        {
            
            WebServices.sharedInstances.sendPostRequest(url: URLConstants.My_Course_Attempte_List, parameters: parameter, successHandler: { (dict) in
                
                if let response = dict["response"]{
                      print(response)
                    let statusString : String = response["status"] as! String
                    
                    if(statusString == "success")
                    {
                        
                        //                        obj.tableView.isHidden = false
                        //                        obj.noDataLabel(text: "" )
                        
                        if let data = dict["data"] as? [AnyObject]
                        {
                            
                            var dataArray : [QuizAttemptElearningModel] = []
                            
                            for i in 0..<data.count
                            {
                                
                                if let celll = data[i] as? [String:AnyObject]
                                {
                                    let object = QuizAttemptElearningModel()
                                    object.setDataInModel(cell: celll)
                                    dataArray.append(object)
                                }
                                
                            }
                            
                            success(dataArray as AnyObject)
                            
                            
                        }
                        
                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                    }
                    else
                    {
                        
                        
                        print("DATA:fail")
                        //                        obj.tableView.isHidden = true
                        //                        obj.noDataLabel(text: "No hazard found with "+parameter["Search"]! )
                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                        
                    }
                    
                    
                }
                
                
                
            }, failureHandler: { (error) in
                let errorStr : String = error.description
                
                print("DATA:error",errorStr)
                
                //obj.errorChecking(error: error)
                obj.stopLoading()
            })
            
            
        }
        else{
            //            obj.noDataLabel(text:  "No internet connection found. Check your internet connection and try again.")
            //            obj.tableView.isHidden = true
            obj.refresh.endRefreshing()
            obj.stopLoading()
        }
        
        
    }
    
    
}


class QuizAnswerdEleraningModel: NSObject {
    
    
    
    
    var Que_ID : Int?
    var Question : String?
    var Given_Ans : String?
    var Correct_Ans : String?
    var IS_Correct : String?
    var Mark : String?
    var Correct_Answer : String?
    var Given_Answer : String?
    
    
    var Title : String?
   

    
    func setDataInModel(cell:[String:AnyObject])
    {
        if cell["Title"] is NSNull || cell["Title"] == nil{
            self.Title = ""
        }else{
            let app = cell["Title"]
            self.Title = (app?.description)!
            
        }
        
        if cell["Question"] is NSNull || cell["Question"] == nil{
            self.Question = ""
        }else{
            let app = cell["Question"]
            self.Question = (app?.description)!
            
        }
        if cell["Given_Ans"] is NSNull || cell["Given_Ans"] == nil{
            self.Given_Ans = ""
        }else{
            let app = cell["Given_Ans"]
            self.Given_Ans = (app?.description)!
            
        }
        if cell["Correct_Ans"] is NSNull || cell["Correct_Ans"] == nil{
            self.Correct_Ans = ""
        }else{
            let app = cell["Correct_Ans"]
            self.Correct_Ans = (app?.description)!
            
        }
        if cell["IS_Correct"] is NSNull || cell["IS_Correct"] == nil{
            self.IS_Correct = ""
        }else{
            let app = cell["IS_Correct"]
            self.IS_Correct = (app?.description)!
            
        }
        if cell["Mark"] is NSNull || cell["Mark"] == nil{
            self.Mark = ""
        }else{
            let app = cell["Mark"]
            self.Mark = (app?.description)!
            
        }
        if cell["Correct_answer"] is NSNull || cell["Correct_answer"] == nil{
            self.Correct_Answer = ""
        }else{
            let app = cell["Correct_answer"]
            self.Correct_Answer = (app?.description)!
            
        }
        if cell["Given_answer"] is NSNull || cell["Given_answer"] == nil{
            self.Given_Answer = ""
        }else{
            let app = cell["Given_answer"]
            self.Given_Answer = (app?.description)!
            
        }
        
        
        if cell["Que_ID"] is NSNull{
            
        }else{
            self.Que_ID = (cell["Que_ID"] as? Int)!
        }
        
        
        
    }
}


class QuizAnswredEleraningAPI
{
    
    var reachablty = Reachability()!
    
    
    func serviceCalling(obj:QuizAnswermLearningViewController,parameter:[String:String], success:@escaping (AnyObject)-> Void)
    {
        
        obj.startLoading()
        
        if(reachablty.connection != .none)
        {
            
            WebServices.sharedInstances.sendPostRequest(url: URLConstants.Answered_Questions_HistoryElearning, parameters: parameter, successHandler: { (dict) in
                
                if let response = dict["response"]{
                    let objectmsg = MessageCallServerModel()
            let msg = objectmsg.sealizeMessage(cell: response as! [String : AnyObject])
                    print(response)
                    let statusString : String = response["status"] as! String
                    
                    if(statusString == "success")
                    {
                        
                        obj.tableView.isHidden = false
                        obj.noDataLabel(text: "" )
                        obj.label.isHidden = true
                        
                        if let data = dict["data"] as? [AnyObject]
                        {
                            
                            var dataArray : [QuizAnswerdEleraningModel] = []
                            
                            for i in 0..<data.count
                            {
                                
                                if let celll = data[i] as? [String:AnyObject]
                                {
                                    let object = QuizAnswerdEleraningModel()
                                    object.setDataInModel(cell: celll)
                                    dataArray.append(object)
                                }
                                
                            }
                            
                            success(dataArray as AnyObject)
                            
                            
                        }
                        
                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                    }
                    else
                    {
                        
                        
                        print("DATA:fail")
                        obj.tableView.isHidden = true
                        obj.noDataLabel(text: msg)
                        obj.refresh.endRefreshing()
                        obj.label.isHidden = false
                        obj.stopLoading()
                        
                    }
                    
                    
                }
                
                
                
            }, failureHandler: { (error) in
                let errorStr : String = error.description
                
                print("DATA:error",errorStr)
                
                // obj.errorChecking(error: error)
                obj.stopLoading()
            })
            
            
        }
        else{
            obj.noDataLabel(text:  "No internet connection found. Check your internet connection and try again.")
            obj.tableView.isHidden = true
            obj.label.isHidden = false
            obj.refresh.endRefreshing()
            obj.stopLoading()
        }
        
        
    }
    
    
}

class QuizAnswredEleraningLoadMoreAPI
{
    
    var reachablty = Reachability()!
    
    
    func serviceCalling(obj:QuizAnswermLearningViewController,parameter:[String:String], success:@escaping (AnyObject)-> Void)
    {
        
        obj.startLoading()
        
        if(reachablty.connection != .none)
        {
            
            WebServices.sharedInstances.sendPostRequest(url: URLConstants.Answered_Questions_HistoryElearning, parameters: parameter, successHandler: { (dict) in
                
                if let response = dict["response"]{
                    let objectmsg = MessageCallServerModel()
            let msg = objectmsg.sealizeMessage(cell: response as! [String : AnyObject])
                    print(response)
                    let statusString : String = response["status"] as! String
                    
                    if(statusString == "success")
                    {
                        
                        //                        obj.tableView.isHidden = false
                        //                        obj.noDataLabel(text: "" )
                        
                        if let data = dict["data"] as? [AnyObject]
                        {
                            
                            var dataArray : [QuizAnswerdEleraningModel] = []
                            
                            for i in 0..<data.count
                            {
                                
                                if let celll = data[i] as? [String:AnyObject]
                                {
                                    let object = QuizAnswerdEleraningModel()
                                    object.setDataInModel(cell: celll)
                                    dataArray.append(object)
                                }
                                
                            }
                            
                            success(dataArray as AnyObject)
                            
                            
                        }
                        
                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                    }
                    else
                    {
                        
                        
                        print("DATA:fail")
                        //                        obj.tableView.isHidden = true
                        //                        obj.noDataLabel(text: "No hazard found with "+parameter["Search"]! )
                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                        
                    }
                    
                    
                }
                
                
                
            }, failureHandler: { (error) in
                let errorStr : String = error.description
                
                print("DATA:error",errorStr)
                
                //obj.errorChecking(error: error)
                obj.stopLoading()
            })
            
            
        }
        else{
            //            obj.noDataLabel(text:  "No internet connection found. Check your internet connection and try again.")
            //            obj.tableView.isHidden = true
            obj.refresh.endRefreshing()
            obj.stopLoading()
        }
        
        
    }
    
    
}
