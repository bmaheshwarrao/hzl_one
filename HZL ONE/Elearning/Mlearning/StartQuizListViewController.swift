//
//  StartQuizListViewController.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 08/03/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation
import SDWebImage
class StartQuizListViewController: CommonVSClass ,UITableViewDelegate, UITableViewDataSource{
    
    @IBOutlet weak var btnStartQuiz: UIButton!
    @IBOutlet weak var tableView: UITableView!
    
    var refreshControl = UIRefreshControl()
    var StrNav = String()
    var CatId = String()
    var CourseID = String()
    var mLearningQuizTipsDB:[QuizmLearningStartDataModel] = []
    var mLearningQuizTipsAPI = QuizmLearningStartDataAPI()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        btnStartQuiz.isHidden = false
        tableView.isHidden = false
        getmLearningQuizData()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.estimatedRowHeight = 65
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.tableFooterView = UIView()
        
        refreshControl.addTarget(self, action: #selector(getmLearningQuizData), for: .valueChanged)
        
        self.tableView.addSubview(refreshControl)
        let swipe = UISwipeGestureRecognizer(target: self, action: #selector(self.swipeRecognizer))
        swipe.direction = ([.down])
        self.view.addGestureRecognizer(swipe)
        
        
        
        
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.tabBarController?.tabBar.isHidden = true
        self.title = StrNav
        let nav = self.navigationController?.navigationBar
        nav?.tintColor = UIColor.white
        nav?.barTintColor = UIColor(hexString: "2c3e50", alpha: 1.0)
        nav?.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        quizMove.isQuizMove = false
    }
    
    var CheckUsageDB:[QuizCheckModel] = []
    
    var CheckUsageAPI = QuizmLearningCheckUserCourseDataAPI()
    @objc func CheckUsageResult() {
        var para = [String:String]()
        let parameter = ["Employee_ID":UserDefaults.standard.string(forKey: "EmployeeID")! , "Course_ID" : CourseID]
        
        para = parameter.filter { $0.value != ""}
        print("para",para)
        self.CheckUsageAPI.serviceCalling(obj: self, param: para) { (dict) in
            
            self.CheckUsageDB = [QuizCheckModel]()
            self.CheckUsageDB = dict as! [QuizCheckModel]
            
            if(self.CheckUsageDB.count > 0){
                if(self.CheckUsageDB[0].S == "1"){
                    
                    let storyBoard = UIStoryboard(name: "eLearning", bundle: nil)
                    let ELPVC = storyBoard.instantiateViewController(withIdentifier: "SubmitQuizmLearningViewController") as! SubmitQuizmLearningViewController
                    ELPVC.QuizId = self.CourseID
                    self.navigationController?.pushViewController(ELPVC, animated: true)
               
                }else{
                    self.view.makeToast(self.CheckUsageDB[0].MSG)
                }
                
            }
            
        }
    }
    
    
    @IBAction func btnStartQuizClicked(_ sender: UIButton) {
        let storyBoard = UIStoryboard(name: "eLearning", bundle: nil)
        let ELPVC = storyBoard.instantiateViewController(withIdentifier: "SubmitQuizmLearningViewController") as! SubmitQuizmLearningViewController
        ELPVC.QuizId = CourseID
        self.navigationController?.pushViewController(ELPVC, animated: true)
    }
    @IBAction func btnStartQuizStartClicked(_ sender: UIButton) {
        CheckUsageResult()
    }
    @IBAction func btnAttemptQuizClicked(_ sender: UIButton) {
        let storyBoard = UIStoryboard(name: "eLearning", bundle: nil)
        let ELPVC = storyBoard.instantiateViewController(withIdentifier: "QuizAttempedViewController") as! QuizAttempedViewController
        ELPVC.courseId = self.CourseID
        self.navigationController?.pushViewController(ELPVC, animated: true)
    }
    var reachability = Reachability()!
    @objc func swipeRecognizer(_ sender: UISwipeGestureRecognizer) {
        if (sender.direction == .down) {
            
            if(self.reachability.connection != .none)
            {
                
                self.getmLearningQuizData()
            }
            else
            {
                self.startLoading(view: self.view)
                DispatchQueue.main.asyncAfter(deadline: .now() + 4.5) {
                    self.noDataLabel(text: "No internet connection found. Check your internet connection and try again.")
                    self.stopLoading(view: self.view)
                }
            }
            
        }
    }
    @IBAction func tapViewClicked(_ sender: Any) {
        
        
        
        
        
        
        
    }
    
    func playerDidFinishPlaying(note: NSNotification) {
        
        self.videoPlayer.pause()
        
    }
    
    func verifyUrl(url: String) -> Bool {
        
        var exists: Bool = false
        let url: NSURL = NSURL(string: url)!
        var request: NSMutableURLRequest = NSMutableURLRequest(url: url as URL)
        request.httpMethod = "HEAD"
        var response: URLResponse?
        do{
            try NSURLConnection.sendSynchronousRequest(request as URLRequest, returning: &response )
        }
        catch{
            
        }
        if let httpResponse = response as? HTTPURLResponse {
            
            if httpResponse.statusCode == 200 {
                
                exists =  true
            }else{
                exists  = false
            }
            
        }
        return exists
    }
    var videoPlayer = AVPlayer()
    func videoPlay(str: String) {
        
        let videoURL = URL(string: str)
        self.videoPlayer = AVPlayer(url: videoURL!)
        let playerViewController = AVPlayerViewController()
        playerViewController.player = videoPlayer
        NotificationCenter.default.addObserver(self, selector: Selector(("playerDidFinishPlaying:")), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: videoPlayer)
        self.present(playerViewController, animated: true)
        {
            playerViewController.player!.play()
        }
        do {
            try AVAudioSession.sharedInstance().overrideOutputAudioPort(AVAudioSessionPortOverride.speaker)
        } catch _ {
            
        }
    }
    @objc func tapViewTapClicked(_ sender: UITapGestureRecognizer) {
        let buttonPosition:CGPoint = sender.view!.convert(.zero, to:self.tableView)
        let indexPath = self.tableView.indexPathForRow(at: buttonPosition)
        let verifyUrlData = verifyUrl(url: self.mLearningQuizTipsDB[(indexPath?.section)!].FilePath)
        if(verifyUrlData == true){
        if(self.mLearningQuizTipsDB[(indexPath?.section)!].Filetype == "PDF"){
            let storyBoard = UIStoryboard(name: "HZLONE", bundle: nil)
            let ELPVC = storyBoard.instantiateViewController(withIdentifier: "HzlPdfReaderViewController") as! HzlPdfReaderViewController
            HZlPolicyTempData.ELCat_ID = String(self.mLearningQuizTipsDB[(indexPath?.row)!].ID)
            
            HZlPolicyTempData.file_Url = self.mLearningQuizTipsDB[(indexPath?.section)!].FilePath
            HZlPolicyTempData.file_Name = self.mLearningQuizTipsDB[(indexPath?.section)!].Filename
            self.navigationController?.pushViewController(ELPVC, animated: true)
        }else{
           
                let urlString = self.mLearningQuizTipsDB[(indexPath?.section)!].FilePath
                print(urlString)
                let urlShow = urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
                videoPlay(str: urlShow!)
            
        }
        }else{
            self.view.makeToast("Invalid Url")
        }
        
        
    }
    @IBOutlet var tapView: UITapGestureRecognizer!
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func imageResize (image:UIImage, sizeChange:CGSize)-> UIImage{
        
        let hasAlpha = true
        let scale: CGFloat = 0.0 // Use scale factor of main screen
        
        UIGraphicsBeginImageContextWithOptions(sizeChange, !hasAlpha, scale)
        image.draw(in: CGRect(origin: CGPoint.zero, size: sizeChange))
        
        let scaledImage = UIGraphicsGetImageFromCurrentImageContext()
        return scaledImage!
    }
    var data: String?
    var lastObject: String?
    var indPath = IndexPath()
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        
        
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.mLearningQuizTipsDB.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! QuizListmLearningTableViewCell
        
        if(mLearningQuizTipsDB[indexPath.section].Filetype == "Video") {
        
            let imageView = UIImageView()
            let urlString =  mLearningQuizTipsDB[indexPath.section].ThumbPath
            let urlShow = urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
            if let url = NSURL(string:  mLearningQuizTipsDB[indexPath.section].ThumbPath) {
                
                SDWebImageManager.shared().downloadImage(with: NSURL(string: urlShow!) as URL!, options: .continueInBackground, progress: {
                    (receivedSize :Int, ExpectedSize :Int) in
                    
                }, completed: {
                    (image : UIImage?, error : Error?, cacheType : SDImageCacheType, finished : Bool, url : URL?) in
                    if(image != nil) {
                        let size = CGSize(width: 100, height: 100)
                        let imageCell = self.imageResize(image: image!, sizeChange: size)
                        cell.imageFile.image = imageCell
                    }else{
                        imageView.image =  UIImage(named: "placed")
                        let size = CGSize(width: 100, height: 100)
                        let imageCell = self.imageResize(image: imageView.image!, sizeChange: size)
                        cell.imageFile.image = imageCell
                        //  cell.viewImage.backgroundColor = UIColor(patternImage: imageCell)
                    }
                })
            }else{
                imageView.image =  UIImage(named: "placed")
                let size = CGSize(width: 100, height: 100)
                let imageCell = imageResize(image: imageView.image!, sizeChange: size)
                cell.imageFile.image = imageCell
                //cell.viewImage.backgroundColor = UIColor(patternImage: imageCell)
            }
            
            
        }else if(mLearningQuizTipsDB[indexPath.section].Filetype == "PDF"){
            cell.imageFile.image = UIImage(named : "pdfFile")
        }
        
        cell.CategoryTitle.text = self.mLearningQuizTipsDB[indexPath.section].Filename
        cell.textViewDesc.text = self.mLearningQuizTipsDB[indexPath.section].Description
        let TapGesture = UITapGestureRecognizer(target: self, action: #selector(self.tapViewTapClicked(_:)))
        cell.addGestureRecognizer(TapGesture)
        cell.isUserInteractionEnabled = true
        TapGesture.numberOfTapsRequired = 1
        
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableViewAutomaticDimension
        
    }
    
    @objc func getmLearningQuizData(){
        var param = [String:String]()
        
        param =  ["Employee_ID":UserDefaults.standard.string(forKey: "EmployeeID")! , "Category_ID" : CatId, "Course_ID" : CourseID]
        mLearningQuizTipsAPI.serviceCalling(obj: self,  param: param ) { (dict) in
            
            self.mLearningQuizTipsDB = dict as! [QuizmLearningStartDataModel]
            
            self.tableView.reloadData()
            
        }
    }
    
    
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
