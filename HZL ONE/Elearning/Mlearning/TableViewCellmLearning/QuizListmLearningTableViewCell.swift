//
//  QuizListmLearningTableViewCell.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 08/03/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit

class QuizListmLearningTableViewCell: UITableViewCell {

    @IBOutlet weak var CategoryTitle: UILabel!
    @IBOutlet weak var imageFile: UIImageView!
    @IBOutlet weak var textViewDesc: UITextView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
