//
//  ModelElearning.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 07/03/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import Foundation

class DashBoardElearningDataModel: NSObject {
    
    
    
    var ID = String()
    var Filetype = String()
    var FilePath = String()
    
    var Filename = String()
    var Caterory_Name = String()
    var Is_Favourite = String()
    var Is_Watchlist = String()
    var Description = String()
    var ThumbPath = String()
    
   
    
    
    
    
    
    func setDataInModel(str:[String:AnyObject])
    {
        if str["ID"] is NSNull || str["ID"] == nil{
            self.ID = ""
        }else{
            
            let ros = str["ID"]
            
            self.ID = (ros?.description)!
            
            
        }
        if str["Filetype"] is NSNull || str["Filetype"] == nil{
            self.Filetype =  ""
        }else{
            
            let fross = str["Filetype"]
            
            self.Filetype = (fross?.description)!
            
            
            //  self.Description = (str["Description"] as? String)!
        }
        if str["Filename"] is NSNull || str["Filename"] == nil{
            self.Filename = ""
        }else{
            let emp1 = str["Filename"]
            
            self.Filename = (emp1?.description)!
            // print(self.Employee_1)
        }
        if str["FilePath"] is NSNull || str["FilePath"] == nil{
            self.FilePath = ""
        }else{
            let emp1 = str["FilePath"]
            
            self.FilePath = (emp1?.description)!
            // print(self.Employee_1)
        }
        
        
        if str["Caterory_Name"] is NSNull || str["Caterory_Name"] == nil{
            self.Caterory_Name = ""
        }else{
            let emp1 = str["Caterory_Name"]
            
            self.Caterory_Name = (emp1?.description)!
            // print(self.Employee_1)
        }
        if str["Is_Favourite"] is NSNull || str["Is_Favourite"] == nil{
            self.Is_Favourite = ""
        }else{
            let emp1 = str["Is_Favourite"]
            
            self.Is_Favourite = (emp1?.description)!
            // print(self.Employee_1)
        }
        if str["Is_Watchlist"] is NSNull || str["Is_Watchlist"] == nil{
            self.Is_Watchlist = ""
        }else{
            let emp1 = str["Is_Watchlist"]
            
            self.Is_Watchlist = (emp1?.description)!
            // print(self.Employee_1)
        }
        if str["Description"] is NSNull || str["Description"] == nil{
            self.Description = ""
        }else{
            let emp1 = str["Description"]
            
            self.Description = (emp1?.description)!
            // print(self.Employee_1)
        }
        if str["ThumbPath"] is NSNull || str["ThumbPath"] == nil{
            self.ThumbPath = ""
        }else{
            let emp1 = str["ThumbPath"]
            
            self.ThumbPath = (emp1?.description)!
            // print(self.Employee_1)
        }
        
        
        
        
    }
    
}
class DashBoardElearningCourseDataModel: NSObject {
    
    
    
    var ID = String()
    var Title = String()
    var Category_ID = String()
    
    var Category_Title = String()
    var No_of_Question = String()
    var Mark_Per_Question = String()
    var Total_Mark = String()
    var Passing_Mark = String()
    var CoverImage = String()
    var Description = String()
    var DateTime = String()
    var Create_By = String()
    var Status = String()
    var Is_Test = String()
    var Total_Question = String()
    var Result = String()
    
    
  
    
    
    func setDataInModel(str:[String:AnyObject])
    {
        if str["ID"] is NSNull || str["ID"] == nil{
            self.ID = ""
        }else{
            
            let ros = str["ID"]
            
            self.ID = (ros?.description)!
            
            
        }
        
        
    
        if str["Title"] is NSNull || str["Title"] == nil{
            self.Title =  ""
        }else{
            
            let fross = str["Title"]
            
            self.Title = (fross?.description)!
            
            
            //  self.Description = (str["Description"] as? String)!
        }
        if str["Category_ID"] is NSNull || str["Category_ID"] == nil{
            self.Category_ID = ""
        }else{
            let emp1 = str["Category_ID"]
            
            self.Category_ID = (emp1?.description)!
            // print(self.Employee_1)
        }
        if str["Category_Title"] is NSNull || str["Category_Title"] == nil{
            self.Category_Title = ""
        }else{
            let emp1 = str["Category_Title"]
            
            self.Category_Title = (emp1?.description)!
            // print(self.Employee_1)
        }
        
        
        if str["No_of_Question"] is NSNull || str["No_of_Question"] == nil{
            self.No_of_Question = ""
        }else{
            let emp1 = str["No_of_Question"]
            
            self.No_of_Question = (emp1?.description)!
            // print(self.Employee_1)
        }
        
     
        if str["Mark_Per_Question"] is NSNull || str["Mark_Per_Question"] == nil{
            self.Mark_Per_Question = ""
        }else{
            let emp1 = str["Mark_Per_Question"]
            
            self.Mark_Per_Question = (emp1?.description)!
            // print(self.Employee_1)
        }
        if str["Total_Mark"] is NSNull || str["Total_Mark"] == nil{
            self.Total_Mark = ""
        }else{
            let emp1 = str["Total_Mark"]
            
            self.Total_Mark = (emp1?.description)!
            // print(self.Employee_1)
        }
        if str["Description"] is NSNull || str["Description"] == nil{
            self.Description = ""
        }else{
            let emp1 = str["Description"]
            
            self.Description = (emp1?.description)!
            // print(self.Employee_1)
        }
        if str["Passing_Mark"] is NSNull || str["Passing_Mark"] == nil{
            self.Passing_Mark = ""
        }else{
            let emp1 = str["Passing_Mark"]
            
            self.Passing_Mark = (emp1?.description)!
            // print(self.Employee_1)
        }
        if str["CoverImage"] is NSNull || str["CoverImage"] == nil{
            self.CoverImage = ""
        }else{
            let emp1 = str["CoverImage"]
            
            self.CoverImage = (emp1?.description)!
            // print(self.Employee_1)
        }
 
        if str["DateTime"] is NSNull || str["DateTime"] == nil{
            self.DateTime = ""
        }else{
            let emp1 = str["DateTime"]
            
            self.DateTime = (emp1?.description)!
            // print(self.Employee_1)
        }
        if str["Status"] is NSNull || str["Status"] == nil{
            self.Status = ""
        }else{
            let emp1 = str["Status"]
            
            self.Status = (emp1?.description)!
            // print(self.Employee_1)
        }
        if str["Create_By"] is NSNull || str["Create_By"] == nil{
            self.Create_By = ""
        }else{
            let emp1 = str["Create_By"]
            
            self.Create_By = (emp1?.description)!
            // print(self.Employee_1)
        }
        
        
        if str["Is_Test"] is NSNull || str["Is_Test"] == nil{
            self.Is_Test = ""
        }else{
            let emp1 = str["Is_Test"]
            
            self.Is_Test = (emp1?.description)!
            // print(self.Employee_1)
        }
        
        if str["Result"] is NSNull || str["Result"] == nil{
            self.Result = ""
        }else{
            let emp1 = str["Result"]
            
            self.Result = (emp1?.description)!
            // print(self.Employee_1)
        }
        if str["Total_Question"] is NSNull || str["Total_Question"] == nil{
            self.Total_Question = ""
        }else{
            let emp1 = str["Total_Question"]
            
            self.Total_Question = (emp1?.description)!
            // print(self.Employee_1)
        }
        
    }
    
}
