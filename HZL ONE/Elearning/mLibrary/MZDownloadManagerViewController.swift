//
//  MZDownloadManagerViewController.swift
//  MZDownloadManager
//
//  Created by Muhammad Zeeshan on 22/10/2014.
//  Copyright (c) 2014 ideamakerz. All rights reserved.
//

import UIKit
import MZDownloadManager
import CoreData

let alertControllerViewTag: Int = 500

class MZDownloadManagerViewController: UITableViewController {
    
    var selectedIndexPath : IndexPath!
    
    lazy var downloadManager: MZDownloadManager = {
        [unowned self] in
        
        let dateFormatter2 = DateFormatter()
        dateFormatter2.timeZone = NSTimeZone.system
        dateFormatter2.dateFormat = "ddMMyyyyhhmmss"
        let finalDate = dateFormatter2.string(from: Date())
        let sessionIdentifer: String = "sarvang"+finalDate
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        var completion = appDelegate.backgroundSessionCompletionHandler
        
        let downloadmanager = MZDownloadManager(session: sessionIdentifer, delegate: self, completion: completion)
        return downloadmanager
        }()
    var strChange = String()
    var availableDownloadsArray: [String] = []
    
    let myDownloadPath = MZUtility.baseFilePath + "/DownloadData"
    
    override func viewDidLoad() {
        super.viewDidLoad()
     

    }
    override func viewWillAppear(_ animated: Bool) {
        //self.title = "Downloading"
      self.tableView.isScrollEnabled = false
        let nav = self.navigationController?.navigationBar
        nav?.barStyle = UIBarStyle.black
        nav?.tintColor = UIColor.white
        nav?.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        
        if !FileManager.default.fileExists(atPath: myDownloadPath) {
            try! FileManager.default.createDirectory(atPath: myDownloadPath, withIntermediateDirectories: true, attributes: nil)
        }
        debugPrint("custom download path: \(myDownloadPath)")
        if(dataArrayLoadFile.count > 0){
            
            for i in 0...dataArrayLoadFile.count - 1{
                let path = dataArrayLoadFile[i].data?.FilePath.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed)
                let fileURL  : NSString  = path as! NSString
                var fileName : NSString = fileURL.lastPathComponent as NSString
                fileName = MZUtility.getUniqueFileNameWithPath((myDownloadPath as NSString).appendingPathComponent(fileName as String) as NSString)
                downloadManager.addDownloadTask(fileName as String, fileURL: fileURL as String, destinationPath: myDownloadPath)
            }
        }
        let aString: NSString = "temp" as NSString
        aString.appendingPathComponent("")
    }
    var setData = 0
  
    
    static func instantiate() -> MZDownloadManagerViewController? {
        return UIStoryboard(name: "mLibrary", bundle: nil).instantiateViewController(withIdentifier: "\(MZDownloadManagerViewController.self)") as? MZDownloadManagerViewController
    }
    
    @IBAction func btnCancelClicked(_ sender: UIButton) {
       
        dataArrayLoadFile = []
        self.downloadManager.cancelTaskAtIndex(0)
        
        NotificationCenter.default.post(name:NSNotification.Name.init("cancelReload"), object: nil)
       
        dismiss(animated: true, completion: nil)
    }
    
    var ival = Int()

    @IBAction func btnDeleteClicked(_ sender : UIButton){
        
        let actionSheetController: UIAlertController = UIAlertController(title: "Alert", message: "Are you sure you want to delete?", preferredStyle: .actionSheet)
        
        let camera = UIAlertAction(title: "Delete", style: .default, handler: { (alert) in
            
            let buttonPosition:CGPoint = sender.convert(.zero, to:self.tableView)
            let indexPath = self.tableView.indexPathForRow(at: buttonPosition)
            let id = self.offlineFileDB[sender.tag].id
            let destinationURL =    URL(fileURLWithPath: self.offlineFileDB[sender.tag].filePath!)
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "OfflineFilesSaved")
            let predicate = NSPredicate(format: "id == %@", id!)
            fetchRequest.predicate = predicate
            let moc = self.getContext()
            let result = try? moc.fetch(fetchRequest)
            let resultData = result as! [OfflineFilesSaved]
            
            for object in resultData {
                
                moc.delete(object)
            }
            
            do {
                
                
                let fileManager = FileManager.default
                try? fileManager.removeItem(at: destinationURL)
                try moc.save()
                self.getofflineFileDetailsData()
                print("saved!")
            } catch let error as NSError  {
                print("Could not save \(error), \(error.userInfo)")
            }
            
            
            
            
        })
        
        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: { (alert) in
            
            
            
        })
        
        actionSheetController.addAction(camera)
        
        actionSheetController.addAction(cancel)
        self.present(actionSheetController, animated: true, completion: nil)
        
        
    }
    
    
  
    func getContext () -> NSManagedObjectContext {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        return appDelegate.persistentContainer.viewContext
    }
    var offlineFileDB =  [OfflineFilesSaved]()
    var indexpathh : [IndexPath] = []
     let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    @objc func getofflineFileDetailsData() {
        
//        self.offlineFileDB = [OfflineFilesSaved]()
//        
//        do {
//           indexpathh = []
//            self.offlineFileDB = try context.fetch(OfflineFilesSaved.fetchRequest())
//            if(self.offlineFileDB.count > 0){
////                for i in 0...self.offlineFileDB.count - 1 {
////                     let index = IndexPath(row: i, section: 1)
////                    indexpathh.append(index)
////
////                }
//                // self.tableView.reloadRows(at: indexpathh, with: .none)
//               tableView.reloadData()
//            }
//            
//            
//        } catch {
//            print("Fetching Failed")
//        }
    }
    var downloadedValue = Bool()
    func saveOfflineData(create_Date : String,descriptionVal : String,fileName : String,filepath : String,fileSavedPath : String,fileType : String,ID : Int,Is_Favourite : String,Is_Watchlist : String,thumbpath : String,downloadDate : String){
        
        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        
        let tasks = OfflineFilesSaved(context: context)
        
        tasks.category_ID = ""
        tasks.category_Title = ""
        tasks.created_Date = create_Date
        tasks.descriptionValue = descriptionVal
        tasks.filename = fileName
        tasks.filePath = filepath
        tasks.fileSize = ""
        tasks.filetype = fileType
        tasks.fileSavedPath = fileSavedPath
        tasks.filetype1 = ""
        tasks.id = String(ID)
        tasks.is_Favourite = Is_Favourite
        tasks.is_Watchlist = Is_Watchlist
        tasks.thumbPath = thumbpath
        tasks.downloadDate = downloadDate
        (UIApplication.shared.delegate as! AppDelegate).saveContext()
       
        getofflineFileDetailsData()
        dismiss(animated: true, completion: nil)
        //        let SWRVC = self.storyboard?.instantiateViewController(withIdentifier: "SWRevealViewController") as! MainTabBarController
        //        self.present(SWRVC, animated: true, completion: nil)
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func refreshCellForIndex(_ downloadModel: MZDownloadModel, index: Int) {
        let indexPath = IndexPath.init(row: index, section: 0)
        let cell = self.tableView.cellForRow(at: indexPath)
        if let cell = cell {
            let downloadCell = cell as! MZDownloadingCell
            downloadCell.updateCellForRowAtIndexPath(indexPath, downloadModel: downloadModel)
        }
    }
}

// MARK: UITableViewDatasource Handler Extension

extension MZDownloadManagerViewController {
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return downloadManager.downloadingArray.count
  
        
    }
   
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        let cellIdentifier : NSString = "MZDownloadingCell"
        let cell : MZDownloadingCell = self.tableView.dequeueReusableCell(withIdentifier: cellIdentifier as String, for: indexPath) as! MZDownloadingCell
        
        let downloadModel = downloadManager.downloadingArray[indexPath.row]
        cell.updateCellForRowAtIndexPath(indexPath, downloadModel: downloadModel)
        
        return cell
      
        
        
    }
}

// MARK: UITableViewDelegate Handler Extension

extension MZDownloadManagerViewController {
    override func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
  override  func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
    let view = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 40))
    view.backgroundColor =   UIColor(hexString: "2c3e50", alpha: 1.0)
    let label = UILabel(frame: CGRect(x: 0, y: 10, width: tableView.frame.size.width, height: 18))
    label.text = "Downloading..."
    label.textAlignment = NSTextAlignment.center
    label.font = UIFont.systemFont(ofSize: 15.0, weight: .medium)
    label.textColor = UIColor.white
    
    view.addSubview(label)
    
    
    return view
        
    }
    
  override  func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
    
            return 40.0
    
        
    }
//   override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//    if(indexPath.section == 1){
//        return UITableViewAutomaticDimension
//    }else{
//        return 400.0
//    }
//        // return 200
//    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedIndexPath = indexPath
        
        let downloadModel = downloadManager.downloadingArray[indexPath.row]
        self.showAppropriateActionController(downloadModel.status)
        
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
}

// MARK: UIAlertController Handler Extension

extension MZDownloadManagerViewController {
    
    func showAppropriateActionController(_ requestStatus: String) {
        
        if requestStatus == TaskStatus.downloading.description() {
            self.showAlertControllerForPause()
        } else if requestStatus == TaskStatus.failed.description() {
            self.showAlertControllerForRetry()
        } else if requestStatus == TaskStatus.paused.description() {
            self.showAlertControllerForStart()
        }
    }
    
    func showAlertControllerForPause() {
        
//        let pauseAction = UIAlertAction(title: "Pause", style: .default) { (alertAction: UIAlertAction) in
//            self.downloadManager.pauseDownloadTaskAtIndex(self.selectedIndexPath.row)
//        }
//
//        let removeAction = UIAlertAction(title: "Remove", style: .destructive) { (alertAction: UIAlertAction) in
//            self.downloadManager.cancelTaskAtIndex(self.selectedIndexPath.row)
//        }
//
//        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
//
//        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
//        alertController.view.tag = alertControllerViewTag
//        alertController.addAction(pauseAction)
//        alertController.addAction(removeAction)
//        alertController.addAction(cancelAction)
//        self.present(alertController, animated: true, completion: nil)
    }
    
    
    func showAlertControllerForRetry() {
        
//        let retryAction = UIAlertAction(title: "Retry", style: .default) { (alertAction: UIAlertAction) in
//            self.downloadManager.retryDownloadTaskAtIndex(self.selectedIndexPath.row)
//        }
        
        let removeAction = UIAlertAction(title: "Remove", style: .destructive) { (alertAction: UIAlertAction) in
            self.downloadManager.cancelTaskAtIndex(self.selectedIndexPath.row)
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        alertController.view.tag = alertControllerViewTag
     //   alertController.addAction(retryAction)
        alertController.addAction(removeAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func showAlertControllerForStart() {
        
        let startAction = UIAlertAction(title: "Start", style: .default) { (alertAction: UIAlertAction) in
            self.downloadManager.resumeDownloadTaskAtIndex(self.selectedIndexPath.row)
        }
        
        let removeAction = UIAlertAction(title: "Remove", style: .destructive) { (alertAction: UIAlertAction) in
            self.downloadManager.cancelTaskAtIndex(self.selectedIndexPath.row)
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        alertController.view.tag = alertControllerViewTag
        alertController.addAction(startAction)
        alertController.addAction(removeAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func safelyDismissAlertController() {
        /***** Dismiss alert controller if and only if it exists and it belongs to MZDownloadManager *****/
        /***** E.g App will eventually crash if download is completed and user tap remove *****/
        /***** As it was already removed from the array *****/
        if let controller = self.presentedViewController {
            guard controller is UIAlertController && controller.view.tag == alertControllerViewTag else {
                return
            }
            controller.dismiss(animated: true, completion: nil)
        }
    }
}

extension MZDownloadManagerViewController: MZDownloadManagerDelegate {
    
    func downloadRequestStarted(_ downloadModel: MZDownloadModel, index: Int) {
        let indexPath = IndexPath.init(row: index, section: 0)
        tableView.insertRows(at: [indexPath], with: UITableViewRowAnimation.fade)
    }
    
    func downloadRequestDidPopulatedInterruptedTasks(_ downloadModels: [MZDownloadModel]) {
        tableView.reloadData()
    }
    
    func downloadRequestDidUpdateProgress(_ downloadModel: MZDownloadModel, index: Int) {
        self.refreshCellForIndex(downloadModel, index: index)
    }
    
    func downloadRequestDidPaused(_ downloadModel: MZDownloadModel, index: Int) {
        self.refreshCellForIndex(downloadModel, index: index)
    }
    
    func downloadRequestDidResumed(_ downloadModel: MZDownloadModel, index: Int) {
        self.refreshCellForIndex(downloadModel, index: index)
    }
    
    func downloadRequestCanceled(_ downloadModel: MZDownloadModel, index: Int) {
        
        self.safelyDismissAlertController()
        
        let indexPath = IndexPath.init(row: index, section: 0)
        tableView.deleteRows(at: [indexPath], with: UITableViewRowAnimation.left)
         if(dataArrayLoadFile.count > 0){
        for i in 0...dataArrayLoadFile.count - 1{
            if(downloadModel.fileURL.description == dataArrayLoadFile[i].data?.FilePath){
       
                dataArrayLoadFile.remove(at: i)
                NotificationCenter.default.post(name:NSNotification.Name.init("refreshData"), object: nil)
               
                
                break;
                
            }
        }
        }
    }
    
    func downloadRequestFinished(_ downloadModel: MZDownloadModel, index: Int) {
        
        self.safelyDismissAlertController()
        
        downloadManager.presentNotificationForDownload("Ok", notifBody: "Download did completed")
        
        let indexPath = IndexPath.init(row: index, section: 0)
        tableView.deleteRows(at: [indexPath], with: UITableViewRowAnimation.left)
        
        let docDirectoryPath : NSString = (MZUtility.baseFilePath as NSString).appendingPathComponent(downloadModel.fileName) as NSString
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: MZUtility.DownloadCompletedNotif as String), object: docDirectoryPath)
       let savePath = myDownloadPath + "/" + downloadModel.fileName
        print(savePath)
        ival = ival + 1;
        
  
        
        let dateFormatter2 = DateFormatter()
        dateFormatter2.timeZone = NSTimeZone.system
        dateFormatter2.dateFormat = "dd MMM yyyy"
        
      
            let date_TimeStr = dateFormatter2.string(from: Date())
        
        
       
        
        
        if(dataArrayLoadFile.count > 0){
            for i in 0...dataArrayLoadFile.count - 1{
                if(downloadModel.fileURL.description == dataArrayLoadFile[i].data?.FilePath){
                    saveOfflineData( create_Date: (dataArrayLoadFile[i].data?.Created_Date)!, descriptionVal: (dataArrayLoadFile[i].data?.Description)!, fileName: (dataArrayLoadFile[i].data?.Filename)!, filepath: (dataArrayLoadFile[i].data?.FilePath)!,fileSavedPath: savePath as String, fileType: (dataArrayLoadFile[i].data?.Filetype)!,  ID: (dataArrayLoadFile[i].data?.ID)!, Is_Favourite: (dataArrayLoadFile[i].data?.Is_Favourite)!, Is_Watchlist: (dataArrayLoadFile[i].data?.Is_Watchlist)!, thumbpath: (dataArrayLoadFile[i].data?.ThumbPath)! , downloadDate: date_TimeStr)
                 dataArrayLoadFile.remove(at: i)
                    NotificationCenter.default.post(name:NSNotification.Name.init("refreshData"), object: nil)
                      NotificationCenter.default.post(name:NSNotification.Name.init("refreshDataDownload"), object: nil)
                  
                    break;
                    
                }
        }
            
            print(downloadModel.fileURL)
        }
    }
    
    func downloadRequestDidFailedWithError(_ error: NSError, downloadModel: MZDownloadModel, index: Int) {
        self.safelyDismissAlertController()
        self.refreshCellForIndex(downloadModel, index: index)
        
        debugPrint("Error while downloading file: \(String(describing: downloadModel.fileName))  Error: \(String(describing: error))")
    }
    
    //Oppotunity to handle destination does not exists error
    //This delegate will be called on the session queue so handle it appropriately
    func downloadRequestDestinationDoestNotExists(_ downloadModel: MZDownloadModel, index: Int, location: URL) {
        let myDownloadPath = MZUtility.baseFilePath + "/DownloadData"
        if !FileManager.default.fileExists(atPath: myDownloadPath) {
            try! FileManager.default.createDirectory(atPath: myDownloadPath, withIntermediateDirectories: true, attributes: nil)
        }
        let fileName = MZUtility.getUniqueFileNameWithPath((myDownloadPath as NSString).appendingPathComponent(downloadModel.fileName as String) as NSString)
        let path =  myDownloadPath + "/" + (fileName as String)
        try! FileManager.default.moveItem(at: location, to: URL(fileURLWithPath: path))
        debugPrint("Default folder path: \(myDownloadPath)")
    }
}


