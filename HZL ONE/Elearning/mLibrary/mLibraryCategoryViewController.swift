//
//  mLibraryCategoryViewController.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 08/03/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit

class mLibraryCategoryViewController: CommonVSClass ,UITableViewDelegate, UITableViewDataSource{
    
    @IBOutlet weak var tableView: UITableView!
    
    var refreshControl = UIRefreshControl()
    var StrNav = String()
    var mLibraryCategoryTipsDB:[CategorymLibraryDataModel] = []
    var mLibraryCategoryTipsAPI = CategorymLibraryDataAPI()
    var mLibraryCategoryTipsLoadMoreDB : [CategorymLibraryDataModel] = []

    var mLibraryCategoryTipsLoadMoreAPI = CategorymLibraryDataAPILoadMore()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        getmLibraryCategoryData()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.estimatedRowHeight = 65
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.tableFooterView = UIView()
        
        refreshControl.addTarget(self, action: #selector(getmLibraryCategoryData), for: .valueChanged)
        
        self.tableView.addSubview(refreshControl)
        let swipe = UISwipeGestureRecognizer(target: self, action: #selector(self.swipeRecognizer))
        swipe.direction = ([.down])
        self.view.addGestureRecognizer(swipe)
        
        
        
        
        
    }
    
    @IBOutlet weak var btnWatch: UIBarButtonItem!
    @IBOutlet weak var btnFavourite: UIBarButtonItem!
    @IBOutlet weak var btndownload: UIBarButtonItem!
    
    
    
    @IBAction func btnDownloadClicked(_ sender: UIBarButtonItem) {
        
        let storyBoard = UIStoryboard(name: "mLibrary", bundle: nil)
        let ELPVC = storyBoard.instantiateViewController(withIdentifier: "DownloadedViewController") as! DownloadedViewController
      
        
        self.navigationController?.pushViewController(ELPVC, animated: true)
        
        
    }
    @IBAction func btnFavouriteClicked(_ sender: UIBarButtonItem) {
        let storyBoard = UIStoryboard(name: "mLibrary", bundle: nil)
        let ELPVC = storyBoard.instantiateViewController(withIdentifier: "eLearningListViewController") as! eLearningListViewController
        ELPVC.ControlSet = 2
        
        self.navigationController?.pushViewController(ELPVC, animated: true)
    }
    @IBAction func btnWatchClicked(_ sender: UIBarButtonItem) {
        let storyBoard = UIStoryboard(name: "mLibrary", bundle: nil)
        let ELPVC = storyBoard.instantiateViewController(withIdentifier: "eLearningListViewController") as! eLearningListViewController
        ELPVC.ControlSet = 3
        
        self.navigationController?.pushViewController(ELPVC, animated: true)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.tabBarController?.tabBar.isHidden = true
        self.title = "mLibrary"
        let nav = self.navigationController?.navigationBar
        nav?.tintColor = UIColor.white
        nav?.barTintColor = UIColor(hexString: "2c3e50", alpha: 1.0)
        nav?.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        
    }
    var reachability = Reachability()!
    @objc func swipeRecognizer(_ sender: UISwipeGestureRecognizer) {
        if (sender.direction == .down) {
            
            if(self.reachability.connection != .none)
            {
                
                self.getmLibraryCategoryData()
            }
            else
            {
                self.startLoading(view: self.view)
                DispatchQueue.main.asyncAfter(deadline: .now() + 4.5) {
                    self.noDataLabel(text: "No internet connection found. Check your internet connection and try again.")
                    self.stopLoading(view: self.view)
                }
            }
            
        }
    }
    @IBAction func tapViewClicked(_ sender: Any) {
        
        
        
        
        
        
        
    }
    @objc func tapViewTapClicked(_ sender: UITapGestureRecognizer) {
        let buttonPosition:CGPoint = sender.view!.convert(.zero, to:self.tableView)
        let indexPath = self.tableView.indexPathForRow(at: buttonPosition)
        
        let storyBoard = UIStoryboard(name: "mLibrary", bundle: nil)
        let ELPVC = storyBoard.instantiateViewController(withIdentifier: "eLearningListViewController") as! eLearningListViewController
        ELPVC.ControlSet = 1
          ELPVC.Category_Title = mLibraryCategoryTipsDB[(indexPath?.section)!].Category_Title
      ELPVC.Category_ID = String(self.mLibraryCategoryTipsDB[(indexPath?.section)!].ID)
        self.navigationController?.pushViewController(ELPVC, animated: true)
        
        
    }
    @IBOutlet var tapView: UITapGestureRecognizer!
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    var data: String?
    var lastObject: String?
    var indPath = IndexPath()
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        
        
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.mLibraryCategoryTipsDB.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! CategorymLearningTableViewCell
        
        
        cell.lblTitle.text = self.mLibraryCategoryTipsDB[indexPath.section].Category_Title
        cell.lblDesc.text = self.mLibraryCategoryTipsDB[indexPath.section].Description
        let TapGesture = UITapGestureRecognizer(target: self, action: #selector(self.tapViewTapClicked(_:)))
        cell.addGestureRecognizer(TapGesture)
        cell.isUserInteractionEnabled = true
        TapGesture.numberOfTapsRequired = 1
        
        
        self.data = String(self.mLibraryCategoryTipsDB[indexPath.section].ID)
        self.lastObject = String(self.mLibraryCategoryTipsDB[indexPath.section].ID)
        
        if ( self.data ==  self.lastObject && indexPath.section == self.mLibraryCategoryTipsDB.count - 1)
        {
            
            self.getmLibraryCategoryDataLoadMore( ID: String(Int(self.mLibraryCategoryTipsDB[indexPath.section].ID)))
            
        }
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableViewAutomaticDimension
        
    }
   
    @objc func getmLibraryCategoryData(){
        var param = [String:String]()
         let empId : String = (UserDefaults.standard.string(forKey: "EmployeeID")!)
        
         param = ["Employee_ID": empId ,"Category_ID": "0"  ]
        mLibraryCategoryTipsAPI.serviceCalling(obj: self,  param: param ) { (dict) in
            
            self.mLibraryCategoryTipsDB = dict as! [CategorymLibraryDataModel]
            
            self.tableView.reloadData()
            
        }
    }
    
    @objc func getmLibraryCategoryDataLoadMore(ID : String){
        var param = [String:String]()
        let empId : String = (UserDefaults.standard.string(forKey: "EmployeeID")!)
       
        param = ["Employee_ID": empId ,"Category_ID": "0" , "ID" : ID ]
        
        mLibraryCategoryTipsLoadMoreAPI.serviceCalling(obj: self,  param: param ) { (dict) in
            
            self.mLibraryCategoryTipsLoadMoreDB =  [CategorymLibraryDataModel]()
            self.mLibraryCategoryTipsLoadMoreDB = dict as! [CategorymLibraryDataModel]
            
            switch dict.count {
            case 0:
                //self.countData = self.countData + 1
                break;
            default:
                self.mLibraryCategoryTipsDB.append(contentsOf: self.mLibraryCategoryTipsLoadMoreDB)
                self.tableView.reloadData()
           
                break;
            }
            
        }
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
