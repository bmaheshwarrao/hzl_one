//
//  eLearningListTableViewCell.swift
//  BaseProject
//
//  Created by SARVANG INFOTCH on 15/11/18.
//  Copyright © 2018 SARVANG INFOTCH. All rights reserved.
//

import UIKit
protocol eLearningListTableViewCellDelegate {
//    func pauseTapped(_ cell: eLearningListTableViewCell)
//    func resumeTapped(_ cell: eLearningListTableViewCell)
    func cancelTapped(_ cell: eLearningListTableViewCell)
    func downloadTapped(_ cell: eLearningListTableViewCell)
}
class eLearningListTableViewCell: UITableViewCell {
var delegate: eLearningListTableViewCellDelegate?
    @IBOutlet weak var imageWatchList: UIImageView!
    @IBOutlet weak var imageFavourite: UIImageView!
    @IBOutlet weak var imageDownload: UIButton!
    @IBOutlet weak var textViewDesc: UITextView!
    @IBOutlet weak var CategoryTitle: UILabel!
    @IBOutlet weak var imageFile: UIImageView!
    @IBOutlet weak var progressView: UIProgressView!
    @IBOutlet weak var progressLabel: UILabel!
    
    @IBOutlet weak var btnCancel: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    
    
    @IBAction func cancelTapped(_ sender: AnyObject) {
        delegate?.cancelTapped(self)
    }
    
    @IBAction func downloadTapped(_ sender: AnyObject) {
        delegate?.downloadTapped(self)
    }
    
    func configure(track: elearningDataModel, downloaded: Bool, download: Download?) {
//        titleLabel.text = track.name
//        artistLabel.text = track.artist
        
        // Download controls are Pause/Resume, Cancel buttons, progress info
        var showDownloadControls = false
        // Non-nil Download object means a download is in progress
        if let download = download {
            showDownloadControls = true
//            let title = download.isDownloading ? "Pause" : "Resume"
//            pauseButton.setTitle(title, for: .normal)
            progressLabel.text = "Downloading..."
        }
        
//        pauseButton.isHidden = !showDownloadControls
        btnCancel.isHidden = !showDownloadControls
        progressView.isHidden = !showDownloadControls
        progressLabel.isHidden = !showDownloadControls
       
        
        // If the track is already downloaded, enable cell selection and hide the Download button
        selectionStyle = downloaded ? UITableViewCellSelectionStyle.gray : UITableViewCellSelectionStyle.none
        imageDownload.setImage(downloaded ? UIImage.init(named: "downloaded") : UIImage.init(named: "download"), for: .normal)
    }
    
    func updateDisplay(progress: Float, totalSize : String) {
        btnCancel.isHidden = false
        progressView.isHidden = false
        progressLabel.isHidden = false
        progressView.progress = progress
        progressLabel.text = String(format: "%.1f%% of %@", progress * 100, totalSize)
    }
}
