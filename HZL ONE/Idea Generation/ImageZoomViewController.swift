//
//  ImageZoomViewController.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 11/02/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit

class ImageZoomViewController:UIViewController,
UIScrollViewDelegate {
    
    @IBOutlet weak var contentScrollView: UIScrollView!
    
    @IBOutlet weak var zoomImageView: UIImageView!
    
    var zoomImageUrl = String()
    var isCheck : Bool = false
    override func viewDidLoad() {
        super.viewDidLoad()
        
        contentScrollView.delegate = self
        
        if let url = NSURL(string: self.zoomImageUrl) {
            self.zoomImageView.sd_setImage(with: url as URL!)
        }
        
        
        contentScrollView.minimumZoomScale = 1.0
        contentScrollView.maximumZoomScale = 5.0
        
        let postImageTapGesture = UITapGestureRecognizer(target: self, action: #selector(self.ImageViewTapped(_:)))
        zoomImageView.addGestureRecognizer(postImageTapGesture)
        zoomImageView.isUserInteractionEnabled = true
        postImageTapGesture.numberOfTapsRequired = 1
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.tabBarController?.tabBar.isHidden = true
        self.navigationController?.navigationBar.isHidden = true
        let nav = self.navigationController?.navigationBar
        nav?.tintColor = UIColor.white
       nav?.barTintColor = UIColor(hexString: "2c3e50", alpha: 1.0)
        nav?.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        
    }
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return zoomImageView
    }
    
    @objc func ImageViewTapped(_ sender: UITapGestureRecognizer) {
        
        switch isCheck {
        case true:
            isCheck = false
            self.navigationController?.navigationBar.isHidden = true
            break;
        default:
            isCheck = true
            self.navigationController?.navigationBar.isHidden = false
            break;
        }
        
    }
    
    
}
