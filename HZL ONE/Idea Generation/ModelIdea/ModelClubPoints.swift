//
//  ModelClubPoints.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 11/02/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import Foundation
import Reachability

class ClubPointsListDataModel: NSObject {
    
    var PointsCr = String()
    var PointsDr = String()
    var DateTime = String()
    var Total_PointsCr = String()
    var Total_PointsDr = String()
    var Total_Balance = String()
    var Description = String()
    func setDataInModel(str:[String:AnyObject])
    {
        if str["Description"] is NSNull || str["Description"] == nil {
            self.Description = ""
        }else{
            let emp1 = str["Description"]
            
            self.Description = (emp1?.description)!
        }
        if str["PointsCr"] is NSNull || str["PointsCr"] == nil{
            self.PointsCr = ""
        }else{
            let emp1 = str["PointsCr"]
            
            self.PointsCr = (emp1?.description)!
        }
        if str["PointsDr"] is NSNull || str["PointsDr"] == nil{
            self.PointsDr = ""
        }else{
            let emp1 = str["PointsDr"]
            
            self.PointsDr = (emp1?.description)!
        }
        if str["DateTime"] is NSNull || str["DateTime"] == nil{
            self.DateTime = ""
        }else{
            let emp1 = str["DateTime"]
            
            self.DateTime = (emp1?.description)!
        }
        if str["Total_PointsCr"] is NSNull || str["Total_PointsCr"] == nil{
            self.Total_PointsCr = ""
        }else{
            let emp1 = str["Total_PointsCr"]
            
            self.Total_PointsCr = (emp1?.description)!
        }
        if str["Total_PointsDr"] is NSNull || str["Total_PointsDr"] == nil{
            self.Total_PointsDr = ""
        }else{
            let emp1 = str["Total_PointsDr"]
            
            self.Total_PointsDr = (emp1?.description)!
        }
        if str["Total_Balance"] is NSNull || str["Total_Balance"] == nil{
            self.Total_Balance = ""
        }else{
            let emp1 = str["Total_Balance"]
            
            self.Total_Balance = (emp1?.description)!
        }
        
    }
}

class ClubPointsListDataAPI
{
    
    var reachablty = Reachability()!
    
    
    func serviceCalling(obj:IdeaClubPointsViewController, parameter : [String:String], success:@escaping (AnyObject)-> Void)
    {
        
        obj.startLoading()
        
        if(reachablty.connection != .none)
        {
            WebServices.sharedInstances.sendPostRequest(url: URLConstants.My_Points_Ledger, parameters: parameter, successHandler: { (dict) in
                
                if let response = dict["response"]{
                    
                    let statusString : String = response["status"] as! String
                    let objectmsg = MessageCallServerModel()
            let msg = objectmsg.sealizeMessage(cell: response as! [String : AnyObject])
                    print(response)
                    if(statusString == "success")
                    {
                        
                        obj.tableView.isHidden = false
                        obj.noDataLabel(text: "" )
                        obj.label.isHidden = true
                        if let data = dict["data"] as? [AnyObject]
                        {
                            
                            var dataArray : [ClubPointsListDataModel] = []
                            
                            for i in 0..<data.count
                            {
                                
                                if let cell = data[i] as? [String:AnyObject]
                                {
                                    let object = ClubPointsListDataModel()
                                    object.setDataInModel(str: cell)
                                    dataArray.append(object)
                                }
                                
                            }
                            
                            success(dataArray as AnyObject)
                            
                            
                        }
                        
                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                    }
                    else
                    {
                        
                        print("DATA:fail")
                        obj.tableView.isHidden = true
                        obj.noDataLabel(text: msg)
                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                        obj.label.isHidden = false
                        
                    }
                    
                    
                }
                
                
                
            }, failureHandler: { (error) in
                let errorStr : String = error.description
                
                print("DATA:error",errorStr)
                
                // obj.errorChecking(error: error)
                obj.stopLoading()
            })
            
            
        }
        else{
            obj.noDataLabel(text:  "No internet connection found. Check your internet connection and try again.")
            obj.tableView.isHidden = true
            obj.refresh.endRefreshing()
            obj.stopLoading()
            obj.label.isHidden = false
        }
        
        
    }
    
    
}





class DataListIdeaApi
{
    var reachablty = Reachability()!
    func serviceCalling(obj:IdeaGenASUZViewController, success:@escaping (AnyObject)-> Void)
    {
        
        obj.startLoading(view: obj.tableViewASUZ)
        var statusString = String()
        
        if(reachablty.connection != .none)
        {
            
            var urlString = String()
            var keyString = String()
            http://hlzidea.dev.app6.in/API/Theme_List/
             if(FilterDataFromServer.filterType == "Department"){
                
                
                
                urlString = Base_Url_Idea+"Department_List/"
                keyString = "Department_Name"
            }else if(FilterDataFromServer.filterType == "Location"){
                
                
     
                urlString = Base_Url_Idea+"Location_List/"
                keyString = "Location_Name"
            }else if(FilterDataFromServer.filterType == "Unit"){
                
    
                
                urlString = Base_Url_Idea+"Location_Unit_List/Default.aspx?Location_ID=" + String(FilterDataFromServer.location_id)
                keyString = "Unit_Name"
            }
            
            
            print(urlString)
            WebServices.sharedInstances.sendGetRequest(Url: urlString, successHandler: { (dict) in
                
                
                if let response = dict["response"]{
                    
                    statusString  = response["status"] as! String
                    let objectmsg = MessageCallServerModel()
            let msg = objectmsg.sealizeMessage(cell: response as! [String : AnyObject])
                    print(response)
                    if(statusString == "success")
                    {
                        
                        obj.tableViewASUZ.isHidden = false
                        obj.noDataLabel(text: "" )
                        obj.label.isHidden = true
                        
                        if let data = dict["data"] as? [AnyObject]
                        {
                            
                            var dataArray : [DataListModel] = []
                            
                            for i in 0..<data.count
                            {
                                if let cell = data[i] as? [String:AnyObject]
                                {
                                    let object = DataListModel()
                                    
                                    if cell["ID"] is NSNull{
                                        object.id = ""
                                    }else{
                                        let idd : Int = (cell["ID"] as? Int)!
                                        object.id = String(idd)
                                    }
                                    
                                    if cell[keyString] is NSNull{
                                        object.name = ""
                                    }else{
                                        object.name = (cell[keyString] as? String)!
                                    }
                                    
                                    
                                    dataArray.append(object)
                                    
                                }
                            }
                            
                            obj.tableViewASUZ.reloadData()
                            success(dataArray as AnyObject)
                            
                            
                        }
                        
                        obj.refresh.endRefreshing()
                        obj.stopLoading(view: obj.tableViewASUZ)
                    }
                    else
                    {
                        statusString = "success"
                        obj.tableViewASUZ.reloadData()
                        obj.label.isHidden = false
                        obj.noDataLabel(text: msg)
                        print("DATA:fail")
                        //                        obj.tableView.isHidden = true
                        //                        obj.noDataLabel(text: "Sorry! No area manager found with name "+name)
                        obj.refresh.endRefreshing()
                        obj.stopLoading(view: obj.tableViewASUZ)
                        
                    }
                    
                    
                }
                
                
                
            }, failureHandler: { (error) in
                let errorStr : String = error.description
                statusString = "fail"
                print("DATA:error",errorStr)
                
                //  obj.errorChecking(error: error)
                
            })
            
            
        }
        else{
            obj.noDataLabel(text:  "No internet connection found. Check your internet connection and try again.")
            obj.tableViewASUZ.isHidden = true
            obj.label.isHidden = false
            obj.refresh.endRefreshing()
            obj.stopLoading()
            
            obj.tableViewASUZ.reloadData()
        }
        
        
    }
}

class DataListIdeaDeptApi
{
    var reachablty = Reachability()!
    func serviceCalling(obj:IdeaGenASUZViewController, success:@escaping (AnyObject)-> Void)
    {
        
        obj.startLoading(view: obj.tableViewASUZ)
        var statusString = String()
        
        if(reachablty.connection != .none)
        {
            var param : [String:String] = [:]
            var urlString = String()
            var keyString = String()
            http://hlzidea.dev.app6.in/API/Theme_List/
                 if(FilterDataFromServer.filterType == "Department"){
                
                
                
                urlString = Base_Url_Idea+"Location_Department_List/"
                keyString = "Department_Name"
                  param  = ["Unit_ID" : String(FilterDataFromServer.unit_Id)]
            }
           else if(FilterDataFromServer.filterType == "Theme") {
                urlString = Base_Url_Idea+"Theme_List/"
                keyString = "ThemeName"
                  param  = ["Location_ID" : String(FilterDataFromServer.location_id)]
                
            }
            
            print(urlString)
           
                 WebServices.sharedInstances.sendPostRequest(url: urlString, parameters: param, successHandler: { (dict) in
                
                if let response = dict["response"]{
                    
                    statusString  = response["status"] as! String
                    let objectmsg = MessageCallServerModel()
            let msg = objectmsg.sealizeMessage(cell: response as! [String : AnyObject])
                    print(response)
                    if(statusString == "success")
                    {
                        
                        obj.tableViewASUZ.isHidden = false
                        obj.noDataLabel(text: "" )
                        obj.label.isHidden = true
                        
                        if let data = dict["data"] as? [AnyObject]
                        {
                            
                            var dataArray : [DataListModel] = []
                            
                            for i in 0..<data.count
                            {
                                if let cell = data[i] as? [String:AnyObject]
                                {
                                    let object = DataListModel()
                                    
                                    if cell["ID"] is NSNull{
                                        object.id = ""
                                    }else{
                                        let idd : Int = (cell["ID"] as? Int)!
                                        object.id = String(idd)
                                    }
                                    
                                    if cell[keyString] is NSNull{
                                        object.name = ""
                                    }else{
                                        object.name = (cell[keyString] as? String)!
                                    }
                                    
                                    
                                    dataArray.append(object)
                                    
                                }
                            }
                            
                            obj.tableViewASUZ.reloadData()
                            success(dataArray as AnyObject)
                            
                            
                        }
                        
                        obj.refresh.endRefreshing()
                        obj.stopLoading(view: obj.tableViewASUZ)
                    }
                    else
                    {
                        statusString = "success"
                        obj.tableViewASUZ.reloadData()
                        obj.label.isHidden = false
                        obj.noDataLabel(text: msg )
                        print("DATA:fail")
                        //                        obj.tableView.isHidden = true
                        //                        obj.noDataLabel(text: "Sorry! No area manager found with name "+name)
                        obj.refresh.endRefreshing()
                        obj.stopLoading(view: obj.tableViewASUZ)
                        
                    }
                    
                    
                }
                
                
                
            }, failureHandler: { (error) in
                let errorStr : String = error.description
                statusString = "fail"
                print("DATA:error",errorStr)
                
                //  obj.errorChecking(error: error)
                
            })
            
            
        }
        else{
            obj.noDataLabel(text:  "No internet connection found. Check your internet connection and try again.")
            obj.tableViewASUZ.isHidden = true
            obj.label.isHidden = false
            obj.refresh.endRefreshing()
            obj.stopLoading()
            
            obj.tableViewASUZ.reloadData()
        }
        
        
    }
}






