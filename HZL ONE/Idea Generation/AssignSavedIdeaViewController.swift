//
//  AssignSavedIdeaViewController.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 11/02/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit
import CoreData
class AssignSavedIdeaViewController: CommonVSClass , UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate {
    @IBOutlet weak var employeeSearchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    
    
    
    let reachablty = Reachability()!
    //    var indPath = IndexPath()
    //    var spec = Bool()
    var searchBarActiveOrNot : Bool = false
    var Idea_ID = String()
    
    
    
    var areaManagersModel: [AreaManagersListDataModel] = []
    var areaManagersAPI = AreaManagersListDataAPI()
    
    //Save locally
    
    @IBAction func DoneAction(_ sender: UIBarButtonItem) {
        
        if(dataArray.count>0){
            for object in dataArray {
                saveAreaManagerData(email: object.email, name: object.name, empId: object.id)
            }
            self.navigationController?.popViewController(animated: true)
        }else {
            self.navigationController?.popViewController(animated: true)
        }
        
    }
    var AreaManagerDB:[AreaManager] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.employeeSearchBar.delegate = self
        self.employeeSearchBar.returnKeyType = UIReturnKeyType.done
        // self.employeeSearchBar.becomeFirstResponder()
        
        tableView.estimatedRowHeight = 124
        tableView.rowHeight = UITableViewAutomaticDimension
        
        tableView.tableFooterView = UIView()
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        
        return self.areaManagersModel.count
        
        
    }
    
    var LastSelected : IndexPath?
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "area", for: indexPath) as! AssignedAreaMangerSearchIdeaTableViewCell
        
        
        
        switch searchBarActiveOrNot {
        case false:
            
            //            cell.clearBtn.isEnabled = true
            //            cell.clearBtn.isHidden = false
            //            switch AreaManagerDB[indexPath.row].name!.removingWhitespaces() {
            //            case "":
            //                cell.employeeNameLabel.text = "**********"
            //                break;
            //            default:
            //                cell.employeeNameLabel.text = "\(AreaManagerDB[indexPath.row].name!.removingWhitespaces()), \(AreaManagerDB[indexPath.row].empId!.removingWhitespaces())"
            //                break;
            //            }
            //            switch AreaManagerDB[indexPath.row].mobileNo!.removingWhitespaces() {
            //            case "":
            //                cell.employeeMobileNoLabel.text = "**********"
            //                break;
            //            default:
            //                cell.employeeMobileNoLabel.text = AreaManagerDB[indexPath.row].mobileNo!.removingWhitespaces()
            //                break;
            //            }
            //            switch AreaManagerDB[indexPath.row].email!.removingWhitespaces() {
            //            case "":
            //                cell.employeeEmaillabel.text = "**********"
            //                break;
            //            default:
            //                cell.employeeEmaillabel.text = AreaManagerDB[indexPath.row].email!.removingWhitespaces()
            //                break;
            //            }
            
            break;
        default:
            
            
            
            
            switch areaManagersModel[indexPath.row].name {
            case "":
                cell.employeeNameLbl.text = "**********"
                break;
            default:
                cell.employeeNameLbl.text = "\(areaManagersModel[indexPath.row].name) ,  \(areaManagersModel[indexPath.row].empId)"
                break;
            }
            //            switch areaManagersModel[indexPath.row].mobileNo.removingWhitespaces() {
            //            case "":
            //                cell.employeeMobileNoLabel.text = "**********"
            //                break;
            //            default:
            //                cell.employeeMobileNoLabel.text = areaManagersModel[indexPath.row].mobileNo.removingWhitespaces()
            //                break;
            //            }
            //            switch areaManagersModel[indexPath.row].email.removingWhitespaces() {
            //            case "":
            //                cell.employeeEmaillabel.text = "**********"
            //                break;
            //            default:
            //                cell.employeeEmaillabel.text = areaManagersModel[indexPath.row].email.removingWhitespaces()
            //                break;
            //            }
            
            break;
        }
        
        
        
        //        if self.LastSelected == indexPath && spec == true {
        //           cell.areaView.backgroundColor = UIColor.lightGray
        //
        //        }else{
        //           cell.areaView.backgroundColor = UIColor.white
        //        }
        
        return cell
    }
    var dataArray : [AreaManagerList] = []
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell:AssignedAreaMangerSearchIdeaTableViewCell = tableView.cellForRow(at: indexPath)! as! AssignedAreaMangerSearchIdeaTableViewCell
        switch searchBarActiveOrNot {
        case false:
            // managerPNo = AreaManagerDB[indexPath.row].empId!
            break;
        default:
            //            managerPNo = String(Int(areaManagersModel[indexPath.row].empId))
            //            saveAreaManagerData(email: areaManagersModel[indexPath.row].email, mobileNo: areaManagersModel[indexPath.row].mobileNo, name: areaManagersModel[indexPath.row].name, empId: String(Int(areaManagersModel[indexPath.row].empId)))
            
            let cellBGView = UIView()
            cellBGView.backgroundColor = UIColor(red: 0, green: 0, blue: 200, alpha: 0.4)
            cell.selectedBackgroundView = cellBGView
            let object = AreaManagerList()
            var isSelect = true;
            self.AreaManagerDB = [AreaManager]()
            let fetchRequest: NSFetchRequest<AreaManager> = AreaManager.fetchRequest()
            fetchRequest.returnsObjectsAsFaults = false
            
            do {
                self.AreaManagerDB = try context.fetch(fetchRequest)
                if(self.AreaManagerDB.count > 0) {
                    
                    for data in self.AreaManagerDB{
                        if(data.empId == areaManagersModel[indexPath.row].empId){
                            isSelect = false;
                            break;
                        }
                    }
                    
                }
                
            } catch {
                
                print("Cannot fetch Expenses")
            }
            
            if(isSelect == true) {
                object.id = areaManagersModel[indexPath.row].empId
                
                
                
                object.name = areaManagersModel[indexPath.row].name
                object.email = areaManagersModel[indexPath.row].email
                
                
                dataArray.append(object)
                
                
            } else {
                self.view.makeToast("Already Selected ")
            }
            
            
            
            break;
        }
        
        
        
    }
    func saveAreaManagerData (email:String,name:String,empId:String){
        
        let entries : NSArray = self.AreaManagerDB as NSArray
        let predicate = NSPredicate(format: "empId = %@ ", empId)
        let FavouriteData = entries.filtered(using: predicate) as! [AreaManager]
        switch FavouriteData.count {
        case 0:
            
            let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
            let tasks = AreaManager(context: context)
            
            tasks.email = email
            
            tasks.name = name
            tasks.empId = empId
            
            (UIApplication.shared.delegate as! AppDelegate).saveContext()
            //   CheckingAreaManagerListData()
            
            break;
        default:
            break;
        }
        
    }
    @objc func CheckingAreaManagerListData() {
        
        //        self.AreaManagerDB = [AreaManager]()
        //        let fetchRequest: NSFetchRequest<AreaManager> = AreaManager.fetchRequest()
        //        fetchRequest.returnsObjectsAsFaults = false
        //
        //        do {
        //            self.AreaManagerDB = try context.fetch(fetchRequest)
        //        } catch {
        //            print("Cannot fetch Expenses")
        //        }
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    /// SEARCH BAR DELEGATES
    
    public func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool
    {
        // return NO to not become first responder
        
        return true
    }
    
    public func searchBarTextDidBeginEditing(_ searchBar: UISearchBar){
        
    }
    
    public func searchBarShouldEndEditing(_ searchBar: UISearchBar) -> Bool
    {
        // return NO to not resign first responder
        
        return true
    }
    
    public func searchBarTextDidEndEditing(_ searchBar: UISearchBar)
    {
        
        self.employeeSearchBar.endEditing(true)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar)
    {
        self.employeeSearchBar.endEditing(true)
        // self.employeeSearchBar.resignFirstResponder()
        // IQKeyboardManager.shared.enable = true;
    }
    
    
    
    public func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String)
    {
        searchBarActiveOrNot = true
        
        
        if searchText != ""{
            searchAreaManagerFromServer(searchText: searchText)
        }else{
            self.tableView.isHidden = true
            self.noDataLabel(text: "Enter Text For Searching...")
            self.label.isHidden = false
        }
        
    }
    
    func searchAreaManagerFromServer(searchText:String)
    {
        
        
        self.areaManagersAPI.serviceCalling(obj: self, name: searchText) { (dict) in
            
            self.areaManagersModel = dict as! [AreaManagersListDataModel]
            
            if self.areaManagersModel.count>0{
                self.dataArray  = []
                self.tableView.isHidden = false
                self.noDataLabel(text: "")
                self.label.isHidden = true
                
            }else{
                self.tableView.isHidden = true
                self.noDataLabel(text: "Sorry! No area manager Found")
                self.label.isHidden = false
            }
            
            self.tableView.reloadData()
            
        }
        
        
    }
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        self.employeeSearchBar.endEditing(true)
        
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

class AreaManagerList: NSObject {
    
    var name = String()
    var id = String()
    var email = String()
    
    
    
}
