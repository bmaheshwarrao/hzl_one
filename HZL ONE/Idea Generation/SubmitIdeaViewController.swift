//
//  SubmitIdeaViewController.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 11/02/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit
import MobileCoreServices
import Alamofire
import CoreData
import CRToast
import IQKeyboardManagerSwift
import AVFoundation


class SubmitIdeaViewController: CommonVSClass,UITextViewDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        createDesign()
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.title = "Submit Idea"
        self.tabBarController?.tabBar.isHidden = true
        let nav = self.navigationController?.navigationBar
        nav?.tintColor = UIColor.white
       nav?.barTintColor = UIColor(hexString: "2c3e50", alpha: 1.0)
        nav?.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        updateData()
    }
    override func viewDidDisappear(_ animated: Bool) {
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.enableAutoToolbar = true
    }
    override func viewDidAppear(_ animated: Bool) {
        IQKeyboardManager.shared.enable = false
        IQKeyboardManager.shared.enableAutoToolbar = false
        
    }
    var lblUnit = UILabel()
     var lblLocation = UILabel()
    var lblDepartment = UILabel()
    var lblTheme = UILabel()
    var textViewIdea = UITextView()
    var textViewBenefits = UITextView()
    var scrollView = UIScrollView()
    var cameraView = UIView()
    var lblClick = UILabel()
    var imgCmaeraicon = UIImageView()
    var imageData = UIImageView()
    var buttonCloseImage = UIButton()
    var btnSubmit = UIButton()
    var btnTheme = UIButton()
    func  createDesign() {
        
        
        var lblLocationLabel = UILabel()
        lblLocationLabel =  UILabel(frame: CGRect(x: 10, y:  5, width: self.view.frame.width - 40, height: 18))
        lblLocationLabel.text = "Select Location"
        lblLocationLabel.font  = UIFont.systemFont(ofSize: 15.0, weight: .medium)
        lblLocationLabel.textColor = UIColor.black
        scrollView.addSubview(lblLocationLabel)
        
        
        let btnLocation = UIButton()
        btnLocation.frame = CGRect(x: 10, y: lblLocationLabel.frame.origin.y + lblLocationLabel.frame.height + 5, width: self.view.frame.width - 20, height: 40)
        btnLocation.layer.borderWidth = 1.0
        btnLocation.addTarget(self, action: #selector(pressLocation(_:)), for: UIControlEvents.touchUpInside)
        let imgViewde = UIImageView(frame: CGRect(x: btnLocation.frame.width - 30, y: 15, width: 15    , height: 15))
        imgViewde.image = UIImage(named: "downArrow")
        btnLocation.addSubview(imgViewde)
        
        lblLocation = UILabel(frame: CGRect(x: 10, y: 5, width: btnLocation.frame.width - 40, height: 30))
        lblLocation.text = "Select Location"
        lblLocation.font  = UIFont.systemFont(ofSize: 15.0, weight: .medium)
        lblLocation.textColor = UIColor.lightGray
        btnLocation.addSubview(lblLocation)
        btnLocation.layer.borderColor = UIColor.black.cgColor
        btnLocation.layer.cornerRadius = 10.0
        btnLocation.clipsToBounds = true;
        
        scrollView.addSubview(btnLocation)
        
        
        
        var lblUnitLabel = UILabel()
        lblUnitLabel =  UILabel(frame: CGRect(x: 10, y: btnLocation.frame.origin.y + btnLocation.frame.height + 5, width: self.view.frame.width - 40, height: 18))
        lblUnitLabel.text = "Select Unit"
        lblUnitLabel.font  = UIFont.systemFont(ofSize: 15.0, weight: .medium)
        lblUnitLabel.textColor = UIColor.black
        scrollView.addSubview(lblUnitLabel)
        
        
        let btnUnit = UIButton()
        btnUnit.frame = CGRect(x: 10, y: lblUnitLabel.frame.origin.y + lblUnitLabel.frame.height + 5, width: self.view.frame.width - 20, height: 40)
        btnUnit.layer.borderWidth = 1.0
        btnUnit.addTarget(self, action: #selector(pressUnit(_:)), for: UIControlEvents.touchUpInside)
        let imgViewUn = UIImageView(frame: CGRect(x: btnUnit.frame.width - 30, y: 15, width: 15    , height: 15))
        imgViewUn.image = UIImage(named: "downArrow")
        btnUnit.addSubview(imgViewUn)
        
        lblUnit = UILabel(frame: CGRect(x: 10, y: 5, width: btnUnit.frame.width - 40, height: 30))
        lblUnit.text = "Select Unit"
        lblUnit.font  = UIFont.systemFont(ofSize: 15.0, weight: .medium)
        lblUnit.textColor = UIColor.lightGray
        btnUnit.addSubview(lblUnit)
        btnUnit.layer.borderColor = UIColor.black.cgColor
        btnUnit.layer.cornerRadius = 10.0
        btnUnit.clipsToBounds = true;
        
        scrollView.addSubview(btnUnit)
        
        
        var lblDepartmentLabel = UILabel()
        lblDepartmentLabel =  UILabel(frame: CGRect(x: 10, y: btnUnit.frame.origin.y + btnUnit.frame.height + 5, width: self.view.frame.width - 40, height: 18))
        lblDepartmentLabel.text = "Select Department"
        lblDepartmentLabel.font  = UIFont.systemFont(ofSize: 15.0, weight: .medium)
        lblDepartmentLabel.textColor = UIColor.black
        scrollView.addSubview(lblDepartmentLabel)
        
        
        let btnDepartment = UIButton()
        btnDepartment.frame = CGRect(x: 10, y: lblDepartmentLabel.frame.origin.y + lblDepartmentLabel.frame.height + 5, width: self.view.frame.width - 20, height: 40)
        btnDepartment.layer.borderWidth = 1.0
        btnDepartment.addTarget(self, action: #selector(pressDepartment(_:)), for: UIControlEvents.touchUpInside)
        let imgViewde1 = UIImageView(frame: CGRect(x: btnDepartment.frame.width - 30, y: 15, width: 15    , height: 15))
        imgViewde1.image = UIImage(named: "downArrow")
        btnDepartment.addSubview(imgViewde1)
        
        lblDepartment = UILabel(frame: CGRect(x: 10, y: 5, width: btnDepartment.frame.width - 40, height: 30))
        lblDepartment.text = "Select Department"
        lblDepartment.font  = UIFont.systemFont(ofSize: 15.0, weight: .medium)
        lblDepartment.textColor = UIColor.lightGray
        btnDepartment.addSubview(lblDepartment)
        btnDepartment.layer.borderColor = UIColor.black.cgColor
        btnDepartment.layer.cornerRadius = 10.0
        btnDepartment.clipsToBounds = true;
        
        scrollView.addSubview(btnDepartment)
        var lblThemeLabel = UILabel()
        lblThemeLabel = UILabel(frame: CGRect(x: 10, y: btnDepartment.frame.origin.y + btnDepartment.frame.height + 5, width: self.view.frame.width - 40, height: 18))
        lblThemeLabel.text = "Select Theme"
        lblThemeLabel.font  = UIFont.systemFont(ofSize: 15.0, weight: .medium)
        lblThemeLabel.textColor = UIColor.black
        scrollView.addSubview(lblThemeLabel)
        btnTheme = UIButton()
        btnTheme.frame = CGRect(x: 10, y: lblThemeLabel.frame.origin.y + lblThemeLabel.frame.height + 5, width: self.view.frame.width - 20, height: 40)
        btnTheme.layer.borderWidth = 1.0
        btnTheme.addTarget(self, action: #selector(pressTheme(_:)), for: UIControlEvents.touchUpInside)
        let imgViewth = UIImageView(frame: CGRect(x: btnTheme.frame.width - 30, y: 15, width: 15    , height: 15))
        imgViewth.image = UIImage(named: "downArrow")
        btnTheme.addSubview(imgViewth)
        
        lblTheme = UILabel(frame: CGRect(x: 10, y: 5, width: btnTheme.frame.width - 40, height: 30))
        lblTheme.text = "Select Theme"
        lblTheme.font  = UIFont.systemFont(ofSize: 15.0, weight: .medium)
        lblTheme.textColor = UIColor.lightGray
        btnTheme.addSubview(lblTheme)
        btnTheme.layer.borderColor = UIColor.black.cgColor
        btnTheme.layer.cornerRadius = 10.0
        btnTheme.clipsToBounds = true;
        
        scrollView.addSubview(btnTheme)
        
        var lblDescribeIdeaLabel = UILabel()
        lblDescribeIdeaLabel = UILabel(frame: CGRect(x: 10, y: btnTheme.frame.origin.y + btnTheme.frame.height + 10, width: self.view.frame.width - 40, height: 18))
        lblDescribeIdeaLabel.text = "Describe Idea"
        lblDescribeIdeaLabel.font  = UIFont.systemFont(ofSize: 15.0, weight: .medium)
        lblDescribeIdeaLabel.textColor = UIColor.black
        scrollView.addSubview(lblDescribeIdeaLabel)
        
        textViewIdea = UITextView(frame: CGRect(x: 10, y: lblDescribeIdeaLabel.frame.origin.y + lblDescribeIdeaLabel.frame.height + 5, width: (self.view.frame.width) - 20, height: 100))
        textViewIdea.delegate = self;
        //IQKeyboardManager.shared.enable = false;
        textViewIdea.layer.borderColor = UIColor.black.cgColor
        textViewIdea.layer.cornerRadius = 10
        textViewIdea.layer.borderWidth = 1.0
        textViewIdea.font = UIFont.systemFont(ofSize: 15.0, weight: .medium)
        textViewIdea.returnKeyType = UIReturnKeyType.done
        scrollView.addSubview(textViewIdea)
        applyPlaceholderStyle(aTextview: textViewIdea, placeholderText: PLACEHOLDER_TEXT)
        
        
        
        var lblDescribeBenefitsLabel = UILabel()
        lblDescribeBenefitsLabel = UILabel(frame: CGRect(x: 10, y: textViewIdea.frame.origin.y + textViewIdea.frame.height + 10, width: self.view.frame.width - 40, height: 18))
        lblDescribeBenefitsLabel.text = "Describe Benefit's"
        lblDescribeBenefitsLabel.font  = UIFont.systemFont(ofSize: 15.0, weight: .medium)
        lblDescribeBenefitsLabel.textColor = UIColor.black
        scrollView.addSubview(lblDescribeBenefitsLabel)
        
        textViewBenefits = UITextView(frame: CGRect(x: 10, y: lblDescribeBenefitsLabel.frame.origin.y + lblDescribeBenefitsLabel.frame.height + 5, width: (self.view.frame.width) - 20, height: 100))
        textViewBenefits.delegate = self;
        //IQKeyboardManager.shared.enable = false;
        textViewBenefits.layer.borderColor = UIColor.black.cgColor
        textViewBenefits.layer.cornerRadius = 10
        textViewBenefits.layer.borderWidth = 1.0
        textViewBenefits.font = UIFont.systemFont(ofSize: 15.0, weight: .medium)
        textViewBenefits.returnKeyType = UIReturnKeyType.done
        scrollView.addSubview(textViewBenefits)
        applyPlaceholderStyle(aTextview: textViewBenefits, placeholderText: PLACEHOLDER_TEXT)
        
        
        
 
        
        
      
        
        
        cameraView = UIView(frame: CGRect(x: 10, y: textViewBenefits.frame.origin.y + textViewBenefits.frame.height + 10, width: (self.view.frame.width) - 20, height: 40))
        let tapCam = UITapGestureRecognizer(target: self, action: #selector(pressCamera(_:)))
        cameraView.isUserInteractionEnabled = true;
        cameraView.addGestureRecognizer(tapCam)
        lblClick = UILabel(frame: CGRect(x: 0, y: 5, width: cameraView.frame.width - 40, height: 30))
        
        lblClick.textColor = UIColor.black
        lblClick.font  = UIFont.systemFont(ofSize: 15.0, weight: .medium)
        lblClick.text = "Click here to Select Idea Image"
        lblClick.textColor = UIColor.lightGray
        cameraView.addSubview(lblClick)
        
        imgCmaeraicon = UIImageView(frame: CGRect(x: cameraView.frame.width - 40, y: 5, width: 40, height: 40))
        
        imgCmaeraicon.image = UIImage(named: "camera");
        
        
        
        cameraView.addSubview(imgCmaeraicon)
        imageData = UIImageView(frame: CGRect(x: (self.view.frame.width/2) - 60, y: lblClick.frame.height + lblClick.frame.origin.y + 20, width: 120, height: 120))
        imageData.isHidden = true;
        cameraView.addSubview(imageData)
        buttonCloseImage = UIButton(frame: CGRect(x: imageData.frame.origin.x  + imageData.frame.width + 5 , y: imageData.frame.origin.y - 10 , width: 25   , height: 25))
        buttonCloseImage.setBackgroundImage(UIImage(named: "cross"), for: UIControlState.normal)
        buttonCloseImage.addTarget(self, action: #selector(pressCloseButton(_:)), for: UIControlEvents.touchUpInside)
        buttonCloseImage.isHidden = true;
        cameraView.addSubview(buttonCloseImage)
        scrollView.addSubview(cameraView)
        
        btnSubmit = UIButton()
        btnSubmit.frame = CGRect(x: 10, y: cameraView.frame.origin.y + cameraView.frame.height + 30, width: self.view.frame.width - 20, height: 40)
        
        //        let tapSubmit = UITapGestureRecognizer(target: self, action: #selector(pressSubmit(_:)))
        //        btnSubmit.isUserInteractionEnabled = true;
        //        btnSubmit.addGestureRecognizer(tapSubmit)
        btnSubmit.addTarget(self, action: #selector(pressSubmit(_:)), for: UIControlEvents.touchUpInside)
        
        btnSubmit.backgroundColor = UIColor(hexString: "2c3e50", alpha: 1.0)
        let lblSubmit = UILabel(frame: CGRect(x: 0, y: 5, width: btnSubmit.frame.width, height: 30))
        lblSubmit.text = "Submit"
        lblSubmit.textAlignment = NSTextAlignment.center
        lblSubmit.textColor = UIColor.white
        lblSubmit.font  = UIFont.systemFont(ofSize: 15.0, weight: .medium)
        btnSubmit.addSubview(lblSubmit)
        
        btnSubmit.clipsToBounds = true;
        
        scrollView.addSubview(btnSubmit)
        
        
        
        
        
        scrollView.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: btnSubmit.frame.origin.y + btnSubmit.frame.height + 30)
        scrollView.updateContentView()
        scrollView.frame.size.height = self.view.frame.size.height
        self.view.addSubview(scrollView)
        scrollView.updateContentView()
        
        
    }
    
    @objc func updateData(){
        if( FilterDataFromServer.unit_Name != "") {
            lblUnit.text = FilterDataFromServer.unit_Name
            lblUnit.textColor = UIColor.black
        } else {
            lblUnit.text = "Select Unit"
            lblUnit.textColor = UIColor.lightGray
        }
        if( FilterDataFromServer.location_name != "") {
            lblLocation.text = FilterDataFromServer.location_name
            lblLocation.textColor = UIColor.black
        } else {
            lblLocation.text = "Select Location"
            lblLocation.textColor = UIColor.lightGray
        }
        if( FilterDataFromServer.dept_Name != "") {
            lblDepartment.text = FilterDataFromServer.dept_Name
            lblDepartment.textColor = UIColor.black
        } else {
            lblDepartment.text = "Select Department"
            lblDepartment.textColor = UIColor.lightGray
        }
        if(FilterDataFromServer.theme != "") {
            lblTheme.text = FilterDataFromServer.theme
            lblTheme.textColor = UIColor.black
        } else {
            lblTheme.text = "Select Theme"
            lblTheme.textColor = UIColor.lightGray
        }
    }
    func reset(){
        FilterDataFromServer.location_id = Int()
        FilterDataFromServer.location_name = String()
        FilterDataFromServer.unit_Id = Int()
        FilterDataFromServer.unit_Name = String()
        FilterDataFromServer.theme_Id = Int()
        FilterDataFromServer.theme = String()
        
        FilterDataFromServer.dept_Id = Int()
        FilterDataFromServer.dept_Name = String()
        
        FilterDataFromServer.filterType = String()
        updateData()
        textViewIdea.text = PLACEHOLDER_TEXT
        textViewBenefits.text = PLACEHOLDER_TEXT
        self.imageData.isHidden = true;
        self.buttonCloseImage.isHidden = true;
        
        self.imageData.image = nil
        imagedataValue = nil
        
        
        self.lblClick.isHidden = false;
        self.imgCmaeraicon.isHidden = false;
        btnSubmit.frame = CGRect(x: 10, y: lblClick.frame.origin.y + lblClick.frame.height + 30, width: self.view.frame.width - 20, height: 40)
        scrollView.updateContentView()
        scrollView.frame.size.height = self.view.frame.size.height
        
    }
    func imageResize (image:UIImage, sizeChange:CGSize)-> UIImage{
        
        let hasAlpha = true
        let scale: CGFloat = 0.0 // Use scale factor of main screen
        
        UIGraphicsBeginImageContextWithOptions(sizeChange, !hasAlpha, scale)
        image.draw(in: CGRect(origin: CGPoint.zero, size: sizeChange))
        
        let scaledImage = UIGraphicsGetImageFromCurrentImageContext()
        return scaledImage!
    }
  
    var reachablty = Reachability()!
    @objc func pressSubmit(_ sender : UIButton) {
        
        
        
        if reachablty.connection == .none{
            
            
            
             if FilterDataFromServer.location_name == ""  {
                
                self.view.makeToast("Please Select Location")
            }
            else if FilterDataFromServer.unit_Name == ""  {
                
                self.view.makeToast("Please Select Unit")
            }
            
            else if FilterDataFromServer.dept_Name == ""  {
                
                self.view.makeToast("Please Select Department")
            }
            else if FilterDataFromServer.theme == ""  {
                
                self.view.makeToast("Please Select Theme")
            }
            else if textViewIdea.text == PLACEHOLDER_TEXT  {
                
                self.view.makeToast("Please Describe Benefit's")
            }
            else if textViewIdea.text == PLACEHOLDER_TEXT  {
                
                self.view.makeToast("Please Describe Idea")
            }
            
            else{
                let dateFormatter2 = DateFormatter()
                dateFormatter2.timeZone = NSTimeZone.system
                dateFormatter2.dateFormat = "ddMMyyyyHHmmssSSS"
                let date_TimeStr = dateFormatter2.string(from: Date())
                //  let hazardUniqueId = date_TimeStr + UserDefaults.standard.string(forKey: "EmployeeID")! + String(FilterDataFromServer.hazard_Id) + String(FilterDataFromServer.area_Id) + String(FilterDataFromServer.location_id) + String(FilterDataFromServer.sub_Area_Id) + String(FilterDataFromServer.department_Id)
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.0) {
                    
                    let actionSheetController: UIAlertController = UIAlertController(title: "No Network Connection", message: "Do you want to save hazard report locally? When network connection will be available, it will be Sync.", preferredStyle: .actionSheet)
                    self.startLoadingPK(view: self.view)
                    let report = UIAlertAction(title: "YES", style: .default, handler: { (alert) in
                        if(self.imageData.image != nil) {
                            var res : UIImage = UIImage()
                            //  res =  (self.imageData.image?.resizedTo1MB())!
                            let size = CGSize(width: 200, height: 200)
                            res = self.imageResize(image: self.imageData.image!,sizeChange: size)
                            self.imagedataValue = UIImageJPEGRepresentation(res, 1.0)!
                            
                        }
                        
                        //  self.saveOfflineHazardReportData(unique : hazardUniqueId)
                        self.stopLoading(view: self.view)
                        self.alert(title: "Idea saved successfully")
                        self.reset()
                        
                    })
                    
                    
                    let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: { (alert) in
                        // self.reset()
                        self.stopLoadingPK(view: self.view)
                    })
                    
                    actionSheetController.addAction(report)
                    actionSheetController.addAction(cancel)
                    self.present(actionSheetController, animated: true, completion: nil)
                    //self.stopLoading(view: self.view)
                    
                }
                
                
            }
            
            
        }
            
        else if FilterDataFromServer.location_name == ""  {
            
            self.view.makeToast("Please Select Location")
        }
        else if FilterDataFromServer.unit_Name == ""  {
            
            self.view.makeToast("Please Select Unit")
        }
       
        else if FilterDataFromServer.dept_Name == ""  {
            
            self.view.makeToast("Please Select Department")
        }
        else if FilterDataFromServer.theme == ""  {
            
            self.view.makeToast("Please Select Theme")
        }
        else if textViewIdea.text == PLACEHOLDER_TEXT  {
            
            self.view.makeToast("Please Describe Idea")
        }
        else if textViewIdea.text == PLACEHOLDER_TEXT  {
            
            self.view.makeToast("Please Describe Benefit's")
        }
            
            
        else{
            
            let dateFormatter2 = DateFormatter()
            dateFormatter2.timeZone = NSTimeZone.system
            dateFormatter2.dateFormat = "ddMMyyyyHHmmssSSS"
            
            
            
            
            
            let date_TimeStr = dateFormatter2.string(from: Date())
            
            //            let hazardUniqueId = date_TimeStr + UserDefaults.standard.string(forKey: "EmployeeID")! + String(FilterDataFromServer.hazard_Id) + String(FilterDataFromServer.area_Id) + String(FilterDataFromServer.location_id) + String(FilterDataFromServer.sub_Area_Id) + String(FilterDataFromServer.department_Id)
            
            
            
            self.startLoadingPK(view: self.view)
            
            DispatchQueue.global(qos: .background).async {
                print(UserDefaults.standard.string(forKey: "LoginType")!)
                let parameter = [
                    "Employee_ID": UserDefaults.standard.string(forKey: "EmployeeID")!,
                    "Dept_ID": String(FilterDataFromServer.dept_Id),
                    "Theme_ID": String(FilterDataFromServer.theme_Id) ,
                    "Benefit": self.textViewBenefits.text,
                    "User_Type": "E" ,
                    "Description": self.textViewIdea.text,
                    
                    "Platform": "iOS"
                    ] as [String : String]
                print(parameter)
                
                let jsonData: Data? = try? JSONSerialization.data(withJSONObject: parameter, options: [])
                let para: [String: Data] = ["data": jsonData!]
                if(self.imageData.image != nil) {
                    
                    var res : UIImage = UIImage()
                    //  res =  (self.imageData.image?.resizedTo1MB())!
                    let size = CGSize(width: 200, height: 200)
                    res = self.imageResize(image: self.imageData.image!,sizeChange: size)
                    self.imagedataValue = UIImageJPEGRepresentation(res, 1.0)!
                    
                }
                Alamofire.upload(multipartFormData: { multipartFormData in
                    
                    if !(self.imagedataValue == nil) {
                        
                        multipartFormData.append(self.imagedataValue!, withName: "file", fileName: "file.jpg", mimeType: "image/jpeg")
                    }
                    
                    for (key, value) in para {
                        multipartFormData.append(value, withName: key)
                        
                        
                    }
                    
                },
                                 to:URLConstants.Submit_Idea)
                { (result) in
                    switch result {
                    case .success(let upload, _, _):
                        
                        upload.uploadProgress(closure: { (progress) in
                            
                        })
                        
                        
                        upload.responseJSON { response in
                            
                            
                            if let json = response.result.value
                            {
                                var dict = json as! [String: AnyObject]
                                
                                if let response = dict["response"]{
                                    print(response)
                                    let statusString = response["status"] as! String
                                    let objectmsg = MessageCallServerModel()
                    let msg = objectmsg.sealizeMessage(cell: response as! [String : AnyObject])
                                    self.stopLoadingPK(view: self.view)
                                    if(statusString == "success")
                                    {
                                        
                                      self.stopLoadingPK(view: self.view)
                                        self.reset()
                                       MoveStruct.isMove = true
                                        MoveStruct.message = msg
                                        self.navigationController?.popViewController(animated: true)
                                        
                                        
                                        
                                    }
                                    else{
                                        
                                        self.stopLoadingPK(view: self.view)
                                        self.view.makeToast( msg)
                                        
                                        
                                        
                                    }
                                    
                                    //  self.stopLoadingPK()
                                    
                                }
                                
                                //self.stopLoadingPK()
                            }
                            
                            // self.stopLoadingPK()
                        }
                        
                    case .failure(let encodingError):
                        
                        self.errorChecking(error: encodingError)
                        self.stopLoadingPK(view: self.view)
                        print(encodingError.localizedDescription)
                    }
                    //self.stopLoadingPK()
                }
                // self.stopLoadingPK()
                
            }
        }
        // clearData()
    }
    
    @objc func pressDepartment(_ sender : UIButton) {
        if(FilterDataFromServer.unit_Name != String()){
        FilterDataFromServer.filterType = "Department"
        let stortyBoard = UIStoryboard(name: "IdeaGeneration", bundle: nil)
        let moveVC = stortyBoard.instantiateViewController(withIdentifier: "IdeaGenASUZViewController") as! IdeaGenASUZViewController
        moveVC.titleStr = "Select Department"
        self.navigationController?.pushViewController(moveVC, animated: true)
        }
        
    }
    @objc func pressTheme(_ sender : UIButton) {
        if(FilterDataFromServer.location_name != String()){
        FilterDataFromServer.filterType = "Theme"
        let stortyBoard = UIStoryboard(name: "IdeaGeneration", bundle: nil)
        let moveVC = stortyBoard.instantiateViewController(withIdentifier: "IdeaGenASUZViewController") as! IdeaGenASUZViewController
        moveVC.titleStr = "Select Theme"
        self.navigationController?.pushViewController(moveVC, animated: true)
        }
        
    }
    @objc func pressUnit(_ sender : UIButton) {
        if(FilterDataFromServer.location_name != String()){
        FilterDataFromServer.filterType = "Unit"
        let stortyBoard = UIStoryboard(name: "IdeaGeneration", bundle: nil)
        let moveVC = stortyBoard.instantiateViewController(withIdentifier: "IdeaGenASUZViewController") as! IdeaGenASUZViewController
        moveVC.titleStr = "Select Unit"
        self.navigationController?.pushViewController(moveVC, animated: true)
        }
        
    }
    @objc func pressLocation(_ sender : UIButton) {
        FilterDataFromServer.filterType = "Location"
        let stortyBoard = UIStoryboard(name: "IdeaGeneration", bundle: nil)
        let moveVC = stortyBoard.instantiateViewController(withIdentifier: "IdeaGenASUZViewController") as! IdeaGenASUZViewController
        moveVC.titleStr = "Select Location"
        self.navigationController?.pushViewController(moveVC, animated: true)
        
    }
    var imagedataValue: Data? = nil
    internal func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
            DispatchQueue.main.async {
                self.imageData.isHidden = false
                self.buttonCloseImage.isHidden = false;
                
                // let resizedImage = image.resizedTo1MB()
                self.imageData.image = image
                // self.imagedataValue = UIImageJPEGRepresentation(resizedImage!, 1.0)!
                self.cameraView.frame = CGRect(x: 10, y: self.textViewBenefits.frame.origin.y + self.textViewBenefits.frame.height + 10, width: (self.view.frame.width) - 20, height: self.imageData.frame.height + 50)
                
                self.btnSubmit.frame = CGRect(x: 10, y: self.cameraView.frame.origin.y + self.cameraView.frame.height + 30, width: self.view.frame.width - 20, height: 40)
                self.lblClick.isHidden = true;
                self.imgCmaeraicon.isHidden = true;
                self.scrollView.updateContentView()
                self.scrollView.frame.size.height = self.view.frame.size.height
                print(self.view.frame.size.height)
            }
            
        }else{
            print("Something went wrong")
        }
        self.dismiss(animated: true, completion: nil)
    }
    @objc func pressCamera(_ sender : UITapGestureRecognizer) {
        let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
            self.openCamera()
        }))
        
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
            self.openGallary()
        }))
        
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera))
        {
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func openGallary()
    {
        imagePicker.delegate = self
        imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
        imagePicker.allowsEditing = true
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    let imagePicker = UIImagePickerController()
    @objc func pressCloseButton(_ sender : UIButton) {
        
        
        self.imageData.isHidden = true;
        self.buttonCloseImage.isHidden = true;
        
        self.imageData.image = nil
        imagedataValue = nil
        
        
        self.lblClick.isHidden = false;
        self.imgCmaeraicon.isHidden = false;
        cameraView.frame = CGRect(x: 10, y: textViewBenefits.frame.origin.y + textViewBenefits.frame.height + 10, width: (self.view.frame.width) - 10, height: 40)
        btnSubmit.frame = CGRect(x: 10, y: cameraView.frame.origin.y + cameraView.frame.height + 30, width: self.view.frame.width - 20, height: 40)
        scrollView.updateContentView()
        scrollView.frame.size.height = self.view.frame.size.height
    }
    func applyPlaceholderStyle(aTextview: UITextView, placeholderText: String)
    {
        // make it look (initially) like a placeholder
        aTextview.textColor = UIColor.lightGray
        aTextview.text = placeholderText
    }
    
    var PLACEHOLDER_TEXT = "Write here..."
    func applyNonPlaceholderStyle(aTextview: UITextView)
    {
        // make it look like normal text instead of a placeholder
        aTextview.textColor = UIColor.darkText
        aTextview.alpha = 1.0
    }
    func textViewShouldBeginEditing(aTextView: UITextView) -> Bool
    {
        if aTextView == textViewIdea && aTextView.text == PLACEHOLDER_TEXT
        {
            // move cursor to start
            moveCursorToStart(aTextView: aTextView)
        } else  if aTextView == textViewBenefits && aTextView.text == PLACEHOLDER_TEXT
        {
            // move cursor to start
            moveCursorToStart(aTextView: aTextView)
        }
        return true
    }
    func moveCursorToStart(aTextView: UITextView)
    {
        DispatchQueue.main.async {
            aTextView.selectedRange = NSMakeRange(0, 0);
        }
        
    }
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        let newLength = textView.text.utf16.count + text.utf16.count - range.length
        if newLength > 0 // have text, so don't show the placeholder
        {
            // check if the only text is the placeholder and remove it if needed
            // unless they've hit the delete button with the placeholder displayed
            if textView == textViewIdea && textView.text == PLACEHOLDER_TEXT
            {
                if text.utf16.count == 0 // they hit the back button
                {
                    return false // ignore it
                }
                applyNonPlaceholderStyle(aTextview: textView)
                textView.text = ""
            } else if textView == textViewBenefits && textView.text == PLACEHOLDER_TEXT
            {
                if text.utf16.count == 0 // they hit the back button
                {
                    return false // ignore it
                }
                applyNonPlaceholderStyle(aTextview: textView)
                textView.text = ""
            }
            return true
        }
        else  // no text, so show the placeholder
        {
            applyPlaceholderStyle(aTextview: textView, placeholderText: PLACEHOLDER_TEXT)
            moveCursorToStart(aTextView: textView)
            return false
        }
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
extension UIScrollView {
    func updateContentView() {
        contentSize.height = (subviews.sorted(by: { $0.frame.maxY < $1.frame.maxY }).last?.frame.maxY)! + 70
        
    }
}



