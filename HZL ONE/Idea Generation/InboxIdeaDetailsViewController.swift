//
//  InboxIdeaDetailsViewController.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 11/02/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit
import CoreData
class InboxIdeaDetailsViewController:  CommonVSClass,UITableViewDelegate, UITableViewDataSource {
    
    
    //var description : String = ""
    
    @IBOutlet weak var assignedlblPlaced: UILabel!
    
    @IBOutlet weak var tableView: UITableView!
    var row = Int()
    
    @IBOutlet weak var viewAddButtonHeight: NSLayoutConstraint!
    @IBOutlet weak var viewAddButton: UIView!
    //    var MyReportedHazardDetailsAPI = MyReportedHazardDetailsDataAPI()
    ////    var MyReportedHazardDB:[MyReportedHazardDetailsDataModel] = []
    //    var MyReportedHazardDB : [ReportedInboxStatus] = []
    
    
    var MyReportedHazardDB  = [ReportedInboxStatus]()
    var AreaManagersListAPI = AreaManagersListDataInDetailsAPI()
    var AreaManagersListDB:[AreaManagersListDataInDetailsModel] = []
    
    var IdeaID = String()
    var empId = String()
    let statusReported : String = UserDefaults.standard.value(forKey: "Status") as! String
    let hazarddisplay = UserDefaults.standard.integer(forKey: "hazard")
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
        tableView.tableFooterView?.backgroundColor = UIColor.groupTableViewBackground
//        refresh.addTarget(self, action: #selector(callMyReportedHazardData), for: .valueChanged)
//
//        self.tableView.addSubview(refresh)
        let swipe = UISwipeGestureRecognizer(target: self, action: #selector(self.swipeRecognizer))
        swipe.direction = ([.down])
        self.view.addGestureRecognizer(swipe)
        print(statusReported)
        
        
        
        if(hazarddisplay == 1) {
            if(statusReported == "AssignedToMeIdea") {
                createDesignProgress()
            }
        } else {
            if(statusReported == "PendingToAccept") {
                createDesignAcceptorReject()
            } else if(statusReported == "PendingtoAssign") {
                createDesignAssign()
            }
        }
    }
    var reachability = Reachability()!
    
    @objc func swipeRecognizer(_ sender: UISwipeGestureRecognizer) {
        if (sender.direction == .down) {
            
            if(self.reachability.connection != .none)
            {
                
                //self.callMyReportedHazardData()
            }
            else
            {
                self.noDataLabel(text: "No internet connection found. Check your internet connection and try again.")
            }
            
        }
    }
    func createDesignAcceptorReject() {
        let  btnAccept = UIButton()
        btnAccept.frame = CGRect(x: 0, y: 0, width: self.view.frame.width/2, height: viewAddButtonHeight.constant)
        
        btnAccept.addTarget(self, action: #selector(pressAccept(_:)), for: UIControlEvents.touchUpInside)
        
        btnAccept.backgroundColor = UIColor.orange
        let lblAccept = UILabel(frame: CGRect(x: 0, y: 10, width: btnAccept.frame.width, height: 30))
        lblAccept.text = "Accept"
        lblAccept.textColor = UIColor.white
        lblAccept.textAlignment = NSTextAlignment.center
        //lblAccept.font  = UIFont(name: "Georgia", size: 13.0)
        btnAccept.addSubview(lblAccept)
        
        btnAccept.clipsToBounds = true;
        
        
        
        viewAddButton.addSubview(btnAccept)
        
        let viewDivider = UIView()
        viewDivider.frame = CGRect(x: self.view.frame.width/2, y: 0, width: 2, height: viewAddButtonHeight.constant)
        viewDivider.backgroundColor = UIColor.white
        viewAddButton.addSubview(viewDivider)
        let  btnReject = UIButton()
        btnReject.frame = CGRect(x: self.view.frame.width/2 + viewDivider.frame.width, y: 0, width: self.view.frame.width/2, height: viewAddButtonHeight.constant)
        
        btnReject.addTarget(self, action: #selector(pressReject(_:)), for: UIControlEvents.touchUpInside)
        
        btnReject.backgroundColor = UIColor.colorwithHexString("FF4500", alpha: 1.0)
        let lblReject = UILabel(frame: CGRect(x: 0, y: 10, width: btnReject.frame.width, height: 30))
        lblReject.text = "Reject"
        lblReject.textColor = UIColor.white
        lblReject.textAlignment = NSTextAlignment.center
        //lblReject.font  = UIFont(name: "Georgia", size: 13.0)
        btnReject.addSubview(lblReject)
        
        btnReject.clipsToBounds = true;
        
        
        
        viewAddButton.addSubview(btnReject)
    }
    func createDesignProgress() {
        
        
        
        
        let  btnProcess = UIButton()
        btnProcess.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: viewAddButtonHeight.constant)
        
        btnProcess.addTarget(self, action: #selector(pressProcess(_:)), for: UIControlEvents.touchUpInside)
        
        btnProcess.backgroundColor = UIColor(red: 30/255, green: 54/255, blue: 65/255, alpha: 1.0)
        let lblFor = UILabel(frame: CGRect(x: 0, y: 10, width: btnProcess.frame.width, height: 30))
        lblFor.text = "Process"
        lblFor.textColor = UIColor.white
        lblFor.textAlignment = NSTextAlignment.center
        //lblFor.font  = UIFont(name: "Georgia", size: 13.0)
        btnProcess.addSubview(lblFor)
        
        btnProcess.clipsToBounds = true;
        
        
        
        viewAddButton.addSubview(btnProcess)
        
        
        
    }
    func createDesignAssign() {
        
        
        
        
        let  btnAssign = UIButton()
        btnAssign.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: viewAddButtonHeight.constant)
        
        btnAssign.addTarget(self, action: #selector(pressAssign(_:)), for: UIControlEvents.touchUpInside)
        
        btnAssign.backgroundColor = UIColor(red: 30/255, green: 54/255, blue: 65/255, alpha: 1.0)
        let lblFor = UILabel(frame: CGRect(x: 0, y: 10, width: btnAssign.frame.width, height: 30))
        lblFor.text = "Assign"
        lblFor.textColor = UIColor.white
        lblFor.textAlignment = NSTextAlignment.center
        //lblFor.font  = UIFont(name: "Georgia", size: 13.0)
        btnAssign.addSubview(lblFor)
        
        btnAssign.clipsToBounds = true;
        
        
        
        viewAddButton.addSubview(btnAssign)
        
        
        
    }
    var AreaManagerDB : [AreaManager] = []
    @objc func pressAssign(_ sender : UIButton) {
        
        
        
        
        
        
        self.AreaManagerDB = [AreaManager]()
        let fetchRequest: NSFetchRequest<AreaManager> = AreaManager.fetchRequest()
        fetchRequest.returnsObjectsAsFaults = false
        
        do {
            self.AreaManagerDB = try context.fetch(fetchRequest)
            if(self.AreaManagerDB.count > 0) {
                for index in 0...self.AreaManagerDB.count - 1 {
                    self.getContext().delete(self.AreaManagerDB[index])
                    (UIApplication.shared.delegate as! AppDelegate).saveContext()
                }
            }
            
        } catch {
            
            print("Cannot fetch Expenses")
        }
        
        
        let storyBoard = UIStoryboard(name: "IdeaGeneration", bundle: nil)
        let ZIVC = storyBoard.instantiateViewController(withIdentifier: "AssignIdeaViewController") as! AssignIdeaViewController
        ZIVC.Idea_Id = String(self.MyReportedHazardDB[row].ID)
        self.navigationController?.pushViewController(ZIVC, animated: true)
    }
    func getContext () -> NSManagedObjectContext {
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        return appDelegate.persistentContainer.viewContext
    }
    
    @objc func pressReject(_ sender : UIButton) {
        
        let storyBoard = UIStoryboard(name: "IdeaGeneration", bundle: nil)
        let ZIVC = storyBoard.instantiateViewController(withIdentifier: "RejectIdeaViewController") as! RejectIdeaViewController
        ZIVC.Idea_Id = String(self.MyReportedHazardDB[row].ID)
        self.navigationController?.pushViewController(ZIVC, animated: true)
    }
    @objc func pressProcess(_ sender : UIButton) {
        
        
        let storyBoard = UIStoryboard(name: "IdeaGeneration", bundle: nil)
        let ZIVC = storyBoard.instantiateViewController(withIdentifier: "ProcessIdeaViewController") as! ProcessIdeaViewController
        
        ZIVC.Idea_Id = String(self.MyReportedHazardDB[row].ID)
        self.navigationController?.pushViewController(ZIVC, animated: true)
    }
    @objc func pressAccept(_ sender : UIButton) {
        
        let storyBoard = UIStoryboard(name: "IdeaGeneration", bundle: nil)
        
        let ZIVC = storyBoard.instantiateViewController(withIdentifier: "AcceptIdeaViewController") as! AcceptIdeaViewController
        ZIVC.Idea_Id = String(self.MyReportedHazardDB[row].ID)
        self.navigationController?.pushViewController(ZIVC, animated: true)
        
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        if(MoveStruct.isMove == true) {
           
            self.navigationController?.popViewController(animated: false)
        }
        print(MyReportedHazardDB)
        print(row)
        print(MyReportedHazardDB[row].Description!)
        self.title = "Idea Detail"
        self.tabBarController?.tabBar.isHidden = true
        let nav = self.navigationController?.navigationBar
        nav?.tintColor = UIColor.white
       nav?.barTintColor = UIColor(hexString: "2c3e50", alpha: 1.0)
        nav?.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        //  callMyReportedHazardData()
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0{
            return 1
        }else{
            return self.AreaManagersListDB.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0{
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "details", for: indexPath) as! MyReportedHazardIdeaDetailsTableViewCell
            cell.selectionStyle = UITableViewCellSelectionStyle.none
            cell.separatorInset = UIEdgeInsetsMake(0, 10000, 0, 0);
            
            
            cell.imageViewInDetails.tag = indexPath.row
            
            
            
            
            cell.lblStatus.text = MyReportedHazardDB[row].Idea_Status
            cell.lblDateIdea.textColor = UIColor.black
            cell.lblStatus.textColor = UIColor.black
            var stattusColor = MyReportedHazardDB[row].Idea_Status
            
            
            switch stattusColor {
            case "Implemented":
                cell.statusOfHazard.backgroundColor = UIColor.colorwithHexString("#5c9834", alpha: 1.0)
                cell.statusOfHazard1.backgroundColor = UIColor.colorwithHexString("#5c9834", alpha: 1.0)
                break;
            case "Pending":
                cell.statusOfHazard.backgroundColor = UIColor.colorwithHexString("#cfcf00", alpha: 1.0)
               cell.statusOfHazard1.backgroundColor = UIColor.colorwithHexString("#cfcf00", alpha: 1.0)
                break;
            case "Assigned":
                cell.statusOfHazard.backgroundColor = UIColor.colorwithHexString("#00800ff", alpha: 1.0)
                cell.statusOfHazard1.backgroundColor = UIColor.colorwithHexString("#00800ff", alpha: 1.0)
                break;
            case "Rejected":
                cell.statusOfHazard.backgroundColor = UIColor.colorwithHexString("#FF0000", alpha: 1.0)
               cell.statusOfHazard1.backgroundColor = UIColor.colorwithHexString("#FF0000", alpha: 1.0)
                break;
            case "Accepted":
                cell.statusOfHazard.backgroundColor = UIColor.colorwithHexString("#FF69B4", alpha: 1.0)
               cell.statusOfHazard1.backgroundColor = UIColor.colorwithHexString("#FF69B4", alpha: 1.0)
                break;
            default:
                break;
            }
            
            
            
            //            switch self.MyReportedHazardDB[row].Assign_Employee_Name {
            //            case ""?:
            //                cell.assignHeight.constant = 40
            //                cell.bottomHeight.constant = 10
            //                cell.assignManagerBtn.isHidden = false
            //                cell.assignedLabel.text = "Not assigned to anyone"
            //                break;
            //            case "null"?:
            //                cell.assignHeight.constant = 40
            //                cell.bottomHeight.constant = 10
            //                cell.assignManagerBtn.isHidden = false
            //                cell.assignedLabel.text = "Not assigned to anyone"
            //                break;
            //            default:
            //
            //                cell.assignHeight.constant = 0
            //                cell.bottomHeight.constant = 0
            //                cell.assignManagerBtn.isHidden = true
            //                cell.assignedLabel.text = self.MyReportedHazardDB[row].Assign_Employee_Name! + "- " + String(self.MyReportedHazardDB[row].Assign_Employee_ID!)
            //                break;
            //            }
            let dateFormatter = DateFormatter()
            dateFormatter.timeZone = NSTimeZone.system
            dateFormatter.dateFormat = "MMM dd yyyy HH:mm:ss:SSSa"
            let date = dateFormatter.date(from: self.MyReportedHazardDB[row].Status_DateTime!)
            
            let dateFormatter2 = DateFormatter()
            dateFormatter2.timeZone = NSTimeZone.system
            dateFormatter2.dateFormat = "dd MMM yyyy"
            
            
            let dateFormatterTime = DateFormatter()
            dateFormatterTime.timeZone = NSTimeZone.system
            dateFormatterTime.dateFormat = "HH:mm a"
            var dateStr = String()
            switch date {
            case nil:
                let date_TimeStr = dateFormatter2.string(from: Date())
                //  let time_TimeStr = dateFormatterTime.string(from: Date())
                dateStr = date_TimeStr
                break;
            default:
                let date_TimeStr = dateFormatter2.string(from: date!)
                
                // let time_TimeStr = dateFormatterTime.string(from: date!)
                dateStr = date_TimeStr
                break;
            }
            cell.lblDateIdea.text = dateStr
            let dateFormatter1 = DateFormatter()
            dateFormatter1.timeZone = NSTimeZone.system
            dateFormatter1.dateFormat = "MMM dd yyyy HH:mm:ss:SSSa"
            let date1 = dateFormatter.date(from: self.MyReportedHazardDB[row].Create_Date_Time!)
            
            let dateFormatter21 = DateFormatter()
            dateFormatter21.timeZone = NSTimeZone.system
            dateFormatter21.dateFormat = "dd MMM yyyy"
            
            
            let dateFormatterTime1 = DateFormatter()
            dateFormatterTime1.timeZone = NSTimeZone.system
            dateFormatterTime1.dateFormat = "HH:mm a"
            var dateStr1 = String()
            switch date1 {
            case nil:
                let date_TimeStr = dateFormatter21.string(from: Date())
                //  let time_TimeStr = dateFormatterTime.string(from: Date())
                dateStr1 = date_TimeStr
                break;
            default:
                let date_TimeStr = dateFormatter21.string(from: date1!)
                
                // let time_TimeStr = dateFormatterTime.string(from: date!)
                dateStr1 = date_TimeStr
                break;
            }
            let paragraph = NSMutableParagraphStyle()
            paragraph.alignment = .left
            paragraph.lineSpacing = 0
            let empId : String = UserDefaults.standard.string(forKey: "EmployeeID")!
            var startString : String = "You "
            if(empId != MyReportedHazardDB[row].Employee_ID){
                startString =  MyReportedHazardDB[row].Employee_Name! + "-" +   String(MyReportedHazardDB[row].Employee_ID!) + " "
            }
            
            let  mBuilder = startString  + "submitted Idea for benefit " + MyReportedHazardDB[row].Benefit! + " under department " +  MyReportedHazardDB[row].Department_Name! +
                " for theme " + MyReportedHazardDB[row].ThemeName! + " on " + dateStr1
            print(mBuilder)
            let agreeAttributedString = NSMutableAttributedString(string: mBuilder, attributes: [NSAttributedStringKey.foregroundColor: UIColor.black, NSAttributedStringKey.font: UIFont.systemFont(ofSize: CGFloat(15.0))])
            
            
            //Submitted
            let SubmittedAttributedString = NSAttributedString(string:"submitted Idea for benefit ", attributes: [NSAttributedStringKey.foregroundColor: UIColor.gray, NSAttributedStringKey.font: UIFont.systemFont(ofSize: 15.0)])
            
            let range: NSRange = (agreeAttributedString.string as NSString).range(of: "submitted Idea for benefit ")
            if range.location != NSNotFound {
                agreeAttributedString.replaceCharacters(in: range, with: SubmittedAttributedString)
            }
            
            //for
            let forAttributedString = NSAttributedString(string:" under department ", attributes: [NSAttributedStringKey.foregroundColor: UIColor.gray, NSAttributedStringKey.font: UIFont.systemFont(ofSize: 15.0)])
            
            let forRange: NSRange = (agreeAttributedString.string as NSString).range(of: " under department ")
            if forRange.location != NSNotFound {
                agreeAttributedString.replaceCharacters(in: forRange, with: forAttributedString)
            }
            let subAreaAttributedString = NSAttributedString(string:"for theme ", attributes: [NSAttributedStringKey.foregroundColor: UIColor.gray, NSAttributedStringKey.font: UIFont.systemFont(ofSize: 15.0)])
            let subAreaRange: NSRange = (agreeAttributedString.string as NSString).range(of: "for theme ")
            if subAreaRange.location != NSNotFound {
                agreeAttributedString.replaceCharacters(in: subAreaRange, with: subAreaAttributedString)
            }
            
            
            let onAttributedString = NSAttributedString(string:"on", attributes: [NSAttributedStringKey.foregroundColor: UIColor.gray, NSAttributedStringKey.font: UIFont.systemFont(ofSize: 15.0)])
            let onRange: NSRange = (agreeAttributedString.string as NSString).range(of: "on")
            if onRange.location != NSNotFound {
                agreeAttributedString.replaceCharacters(in: onRange, with: onAttributedString)
            }
            //            let extraAttributedString = NSAttributedString(string:MyReportedHazardDB[indexPath.section].Description!, attributes: [NSAttributedStringKey.foregroundColor: UIColor.gray, NSAttributedStringKey.font: UIFont.systemFont(ofSize: 15.0)])
            //            let extraRange: NSRange = (agreeAttributedString.string as NSString).range(of: MyReportedHazardDB[indexPath.section].Description!)
            //            if extraRange.location != NSNotFound {
            //                agreeAttributedString.replaceCharacters(in: extraRange, with: extraAttributedString)
            //            }
            agreeAttributedString.addAttribute(NSAttributedStringKey.paragraphStyle, value: paragraph, range: NSRange(location: 0, length: agreeAttributedString.length))
            cell.ownerTextView.attributedText = agreeAttributedString
            //   cell.deptLabel.text = MyReportedHazardDB[indexPath.section].Department_Name!
            //  cell.themeLabel.text = MyReportedHazardDB[indexPath.section].ThemeName!
            
            let paragraph1 = NSMutableParagraphStyle()
            paragraph1.alignment = .left
            paragraph1.lineSpacing = 0
            let titleStr = NSMutableAttributedString(string: self.MyReportedHazardDB[row].Benefit!, attributes: [NSAttributedStringKey.foregroundColor: UIColor.darkGray, NSAttributedStringKey.font: UIFont.systemFont(ofSize: CGFloat(15.0))])
            titleStr.addAttribute(NSAttributedStringKey.paragraphStyle, value: paragraph1, range: NSRange(location: 0, length: titleStr.length))
            
            
            cell.benefitLabelTextView.attributedText = titleStr
            
            
            let paragraph11 = NSMutableParagraphStyle()
            paragraph11.alignment = .left
            paragraph11.lineSpacing = 0
            let titleStr1 = NSMutableAttributedString(string: self.MyReportedHazardDB[row].Description!, attributes: [NSAttributedStringKey.foregroundColor: UIColor.darkGray, NSAttributedStringKey.font: UIFont.systemFont(ofSize: CGFloat(15.0))])
            titleStr1.addAttribute(NSAttributedStringKey.paragraphStyle, value: paragraph11, range: NSRange(location: 0, length: titleStr1.length))
            
            cell.benefitTextView.attributedText = titleStr1
            
            let postImageTapGesture = UITapGestureRecognizer(target: self, action: #selector(self.ImageViewTapped(_:)))
            cell.imageViewInDetails.addGestureRecognizer(postImageTapGesture)
            cell.imageViewInDetails.isUserInteractionEnabled = true
            postImageTapGesture.numberOfTapsRequired = 1
            
            if let url = NSURL(string: MyReportedHazardDB[row].Image_URL!) {
                cell.imageViewInDetails.sd_setImage(with: url as URL!, placeholderImage: UIImage.init(named: "placed"))
            }
            
            cell.ownerTextView.isEditable = false
            cell.ownerTextView.isSelectable = false
            cell.ownerTextView.isScrollEnabled = false
            if( statusReported == "PendingToAccept" || statusReported == "RejectedIdea" || statusReported == "PendingtoAssign" ) {
                cell.showAssignedBtn.isHidden = true
                cell.showAssignedBtn.isHidden = true
                cell.noAssignedLabel.isHidden = true
                cell.showButtonHeight.constant  = -5
                cell.assignedToLabel.isHidden = true
                cell.assignedToLabel.isHidden = true
                if(statusReported == "PendingToAccept" || statusReported == "PendingtoAssign"  ) {
                    viewAddButtonHeight.constant = 52
                } else {
                    viewAddButtonHeight.constant = 0
                }
            } else {
                viewAddButtonHeight.constant = 0
            }
            if(statusReported == "AssignedToMeIdea"  ) {
                viewAddButtonHeight.constant = 52
                cell.showAssignedBtn.isHidden = false
                cell.showAssignedBtn.isHidden = false
                cell.noAssignedLabel.isHidden = true
                cell.showButtonHeight.constant  = 40
                cell.assignedToLabel.isHidden = false
                cell.assignedToLabel.isHidden = false
            }
            if( statusReported == "MySubmittedIdea" ) {
                if(self.MyReportedHazardDB[row].Idea_Status! == "Pending" || self.MyReportedHazardDB[row].Idea_Status! == "Rejected"){
                    viewAddButtonHeight.constant = 0
                    cell.showAssignedBtn.isHidden = true
                    
                    cell.noAssignedLabel.isHidden = false
                    cell.showButtonHeight.constant  = 0
                    
                    cell.assignedToLabel.isHidden = true
                } else if(self.MyReportedHazardDB[row].Idea_Status! == "Implemented"){
                    viewAddButtonHeight.constant = 0
                    cell.showAssignedBtn.isHidden = false
                    
                    cell.noAssignedLabel.isHidden = false
                    cell.showButtonHeight.constant  = 40
                    cell.assignedToLabel.isHidden = false
                    
                }
                else if(self.MyReportedHazardDB[row].Idea_Status! == "Assigned"){
                    viewAddButtonHeight.constant = 52
                    cell.showAssignedBtn.isHidden = false
                    cell.showAssignedBtn.isHidden = false
                    cell.noAssignedLabel.isHidden = true
                    cell.showButtonHeight.constant  = 40
                    cell.assignedToLabel.isHidden = false
                    cell.assignedToLabel.isHidden = false
                    
                }
            }
            if(statusReported == "AssignedToMeIdea"){
                viewAddButtonHeight.constant = 52
                cell.showAssignedBtn.isHidden = false
                
                cell.noAssignedLabel.isHidden = false
                cell.showButtonHeight.constant  = 40
                cell.assignedToLabel.isHidden = false
            }
            
            
            cell.noAssignedLabel.isHidden = true
            if(self.AreaManagersListDB.count == 0 && lblInt == 1) {
                //  cell.areaManagersLbl.isHidden = true;
                //    cell.noManagerlbl.isHidden = false
                cell.noAssignedLabel.isHidden = false
                cell.lblNoHeight.constant = 21
                
            } else if(self.AreaManagersListDB.count > 0 ) {
                cell.showAssignedBtn.isHidden = true
                cell.noAssignedLabel.isHidden = true
                cell.showButtonHeight.constant  = -5
            }
            else {
                cell.lblNoHeight.constant = -5
                //   cell.areaManagersLbl.isHidden = true;
                //  cell.noManagerlbl.isHidden = true
            }
            
            
            return cell
            
        }else{
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "area", for: indexPath) as! MyReportedHazardDetailsAssignManagerIdeaTableViewCell
            
            switch AreaManagersListDB[indexPath.row].name {
            case "":
                cell.employeeNameLabel.text = ""
                break;
            default:
                cell.employeeNameLabel.text = "\(String(indexPath.row + 1)) \(AreaManagersListDB[indexPath.row].name)"
                break;
            }
            switch AreaManagersListDB[indexPath.row].Is_Implemented {
            case "":
                
                cell.employeeImplementedlabel.text = ""
                break;
            default:
                
                if(AreaManagersListDB[indexPath.row].Is_Implemented == "1") {
                    cell.employeeImplementedlabel.text = "Implemented"
                    cell.employeeImplementedlabel.textColor = UIColor.colorwithHexString("5c9834", alpha: 1.0)
                    cell.bottomArea.constant = 20
                    cell.heightEmp.constant = 16.5
                } else {
                    
                    cell.bottomArea.constant = 0
                    cell.heightEmp.constant = 0
                    cell.employeeImplementedlabel.isHidden = true
                }
                break;
            }
            
            cell.selectionStyle = .none
            
            
            
            return cell
        }
        
    }
    var reachablty = Reachability()!
    //    @IBAction func btnReminderClicked(_ sender: UIButton) {
    //        let buttonPosition:CGPoint = sender.convert(.zero, to:self.tableView)
    //        let indexPath = self.tableView.indexPathForRow(at: buttonPosition)
    //        let cell = tableView.dequeueReusableCell(withIdentifier: "details", for: indexPath!) as! MyReportedHazardDetailsTableViewCell
    //
    //
    //        cell.assignedReminderBtn.transform = CGAffineTransform(scaleX: 10, y: 20)
    //
    //        if reachablty.connection == .none{
    //
    //            self.view.makeToast("Internet is not available, please check your internet connection try again.")
    //
    //        } else{
    //
    //            self.startLoading()
    //
    //            let parameter = ["Hazard_ID":hazardID,
    //                             "MY_Personnel_Number":UserDefaults.standard.string(forKey: "EmployeeID")!,
    //
    //                             "Personnel_Number": self.MyReportedHazardDB[row].Assign_Employee_ID!
    //
    //                ] as [String:String]
    //
    //            print("parameter",parameter)
    //
    //            WebServices.sharedInstances.sendPostRequest(url: URLConstants.Remainder_Assign_But_User_Side_Pending, parameters: parameter, successHandler: { (response:[String : AnyObject]) in
    //
    //                let respon = response["response"] as! [String:AnyObject]
    //
    //                if respon["status"] as! String == "success" {
    //
    //                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.0) {
    //
    //                        self.stopLoading()
    //                        let msgData : String = respon["msg"] as! String
    //                        self.view.makeToast(msgData)
    //
    //
    //                    }
    //
    //
    //                }else{
    //                    self.stopLoading()
    //                    self.view.makeToast("Please try again, something went wrong.")
    //                }
    //
    //            }) { (err) in
    //                self.stopLoading()
    //                print(err.description)
    //            }
    //
    //        }
    //
    //
    //
    //
    //
    //    }
    @IBOutlet weak var assignManagerButtonWidthConstraint: NSLayoutConstraint!
    //    @objc func callMyReportedHazardData() {
    //        if(self.reachability.connection != .none)
    //        {
    //
    //            self.MyReportedHazardDetailsAPI.serviceCalling(obj: self, hazardID: self.hazardID, pNo: self.empId) { (dict) in
    //
    //                self.MyReportedHazardDB = [MyReportedHazardDetailsDataModel]()
    //                self.MyReportedHazardDB = dict as! [MyReportedHazardDetailsDataModel]
    //                self.tableView.reloadData()
    //
    //            }
    //        } else {
    //            self.noDataLabel(text: "No internet connection found. Check your internet connection and try again.")
    //        }
    //
    //    }
    //    @IBAction func areaManagerViewedTapped(_ sender: UITapGestureRecognizer) {
    //
    //        let buttonPosition:CGPoint = sender.view!.convert(.zero, to:self.tableView)
    //        let indexPath = self.tableView.indexPathForRow(at: buttonPosition)
    //
    //
    //        let phoneNo = AreaManagersListDB[(indexPath?.row)!].mobileNo
    //        if let url = URL(string: "tel://\(phoneNo)"), UIApplication.shared.canOpenURL(url) {
    //            if #available(iOS 10, *) {
    //                UIApplication.shared.open(url)
    //            } else {
    //                UIApplication.shared.openURL(url)
    //            }
    //        }
    //    }
    
    
    @IBAction func ImageViewTapped(_ sender: UITapGestureRecognizer) {
        if MyReportedHazardDB[row].Image_URL! != "" {
            print(MyReportedHazardDB[row].Image_URL!)
            let ZIVC = self.storyboard?.instantiateViewController(withIdentifier: "ImageZoom") as! ImageZoomViewController
            ZIVC.zoomImageUrl = MyReportedHazardDB[row].Image_URL!
            self.navigationController?.pushViewController(ZIVC, animated: true)
            
        }
    }
    
    var lblInt = 0
    @IBAction func assigneManagerAction(_ sender: Any) {
        if(lblInt == 0) {
            switch self.AreaManagersListDB.count {
            case 0:
                
                AreaManagersListAPI.serviceCalling(obj: self, Idea_ID: String(describing: MyReportedHazardDB[row].ID)) { (dict) in
                    
                    self.AreaManagersListDB = [AreaManagersListDataInDetailsModel]()
                    self.AreaManagersListDB = dict as! [AreaManagersListDataInDetailsModel]
                    print(self.AreaManagersListDB)
                    self.tableView.reloadData()
                    //self.lblInt = 1
                }
                print(lblInt)
                if(lblInt == 1) {
                    self.tableView.reloadData()
                }
                break;
            default:
                lblInt = 1;
                self.tableView.reloadData()
                break;
                
                
            }
        }
        
    }
    
    
}
