//
//  WorkPermitViewController.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 08/02/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit
import DLRadioButton
import Alamofire
class WorkPermitViewController: CommonVSClass, UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    
    
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        
        
        
        // Do any additional setup after loading the view.
    }
    var statusType = ["Employee", "Contractor"]
    @IBAction func btnPTWISSEMPClicked(_ sender: UIButton) {
        
        DispatchQueue.main.async(execute: {() -> Void in
            let popup = PopUpTableViewController()
            
            popup.itemsArray = self.statusType
            
            popup.sourceView = self.cellData.lblPTWIssuerEmpType
            popup.isScroll = true
            popup.backgroundColor = UIColor.white
            if popup.itemsArray.count > 5{
                popup.popUpHeight = 200
            }
            else{
                popup.popUpHeight = CGFloat(popup.itemsArray.count)*45
            }
            popup.popUpWidth = UIScreen.main.bounds.size.width-60
            popup.backgroundImage = nil
            popup.itemTitleColor = UIColor.white
            popup.itemSelectionColor = UIColor.lightGray
            popup.arrowDirections = .any
            popup.arrowColor = UIColor.white
            popup.popCellBlock = {(_ popupVC: PopUpTableViewController, _ popupCell: UITableViewCell, _ row: Int, _ section: Int) -> Void in
                
                
                
                
                
                self.cellData.lblPTWIssuerEmpType.text = self.statusType[row]
                self.cellData.lblPTWIssuerEmpType.textColor = UIColor.black
                self.updateConst()
                
                popupVC.dismiss(animated: false, completion: nil)
                
            }
            self.present(popup, animated: true, completion: {() -> Void in
            })
            
            
        })
        
    }
    var strAction = String()
    @IBAction func radioButtonClicked(_ sender: DLRadioButton){
        
        for button in sender.selectedButtons() {
            self.strAction = (button.titleLabel!.text!)
            print(String(format: "%@ is selected.\n", self.strAction));
            
        }
    }
    @IBAction func btnPTWRECEMPClicked(_ sender: UIButton) {
        
        DispatchQueue.main.async(execute: {() -> Void in
            let popup = PopUpTableViewController()
            
            popup.itemsArray = self.statusType
            
            popup.sourceView = self.cellData.lblPTWRecEmpType
            popup.isScroll = true
            popup.backgroundColor = UIColor.white
            if popup.itemsArray.count > 5{
                popup.popUpHeight = 200
            }
            else{
                popup.popUpHeight = CGFloat(popup.itemsArray.count)*45
            }
            popup.popUpWidth = UIScreen.main.bounds.size.width-60
            popup.backgroundImage = nil
            popup.itemTitleColor = UIColor.white
            popup.itemSelectionColor = UIColor.lightGray
            popup.arrowDirections = .any
            popup.arrowColor = UIColor.white
            popup.popCellBlock = {(_ popupVC: PopUpTableViewController, _ popupCell: UITableViewCell, _ row: Int, _ section: Int) -> Void in
                
                
                
                
                
                self.cellData.lblPTWRecEmpType.text = self.statusType[row]
                self.cellData.lblPTWRecEmpType.textColor = UIColor.black
                self.updateConst()
                
                popupVC.dismiss(animated: false, completion: nil)
                
            }
            self.present(popup, animated: true, completion: {() -> Void in
            })
            
            
        })
        
    }
    var MyVisitReportDB = MyVisitDataODSOModel()
    let reachablty = Reachability()!
    @IBAction func btnAddObClicked(_ sender: UIButton) {
        
        
        if reachablty.connection == .none{
            
            self.view.makeToast("Internet is not available, please check your internet connection try again.")
            
        }
        else if ( FilterDataFromServer.unit_Name == String()) {
            
            self.view.makeToast("Please Select Unit")
        }
        else if ( FilterDataFromServer.area_Name == String()) {
            
            self.view.makeToast("Please Select Area")
        }
        else if (cellData.textViewJob.text == "") {
            
            self.view.makeToast("Enter description")
        }
        else if (strAction == "") {
            
            self.view.makeToast("Select violation status")
        }else if (cellData.textPTWClassno.text == "") {
            
            self.view.makeToast("Enter PTW class no")
        }
        else if (cellData.textMeasureTaken.text == "") {
            
            self.view.makeToast("Enter Corrective maasure taken")
        }
        else if (cellData.lblPTWIssuerEmpType.text == "") {
            
            self.view.makeToast("Select PTW Issuer Employee type")
        }
        else if (cellData.textPTWIssuerEmpId.text == "") {
            
            self.view.makeToast("Enter PTW Issuer Employee Id/gatepass Id")
        }
        else if (cellData.lblPTWRecEmpType.text == "") {
            
            self.view.makeToast("Select PTW Reciever Employee type")
        }
        else if (cellData.textPTWRecEmpId.text == "") {
            
            self.view.makeToast("Enter PTW Reciever Employee Id/gatepass Id")
        }
        else if (cellData.textCorrectiveMeasureTaken.text == "") {
            
            self.view.makeToast("Enter Corrective maasure taken by")
        }
        
        
        else{
            
            
            
            self.startLoadingPK(view: self.view)
            
      
            
            
        let parameter = ["Team_ID":String(MyVisitReportDB.ID),
                         "Employee_ID":UserDefaults.standard.string(forKey: "EmployeeID")!,
                         "Location_ID":MyVisitReportDB.Location_ID,
                         "Unit_ID": String(FilterDataFromServer.unit_Id),
                         "Area_ID":String(FilterDataFromServer.area_Id) ,
                
                         "Description":cellData.textViewJob.text!,
                             "Violations_found":strAction ,
                             "PTW_Class_No":cellData.textPTWClassno.text!,
                             "Measures_taken": cellData.textMeasureTaken.text!,
                             "PTW_Issuer_Emp_Type":cellData.lblPTWIssuerEmpType.text!,
                             
                             "PTW_Issuer_Type_Id":cellData.textPTWIssuerEmpId.text!,
                             "PTW_receiver_Emp_Type":cellData.lblPTWRecEmpType.text! ,
                             "PTW_receiver_Type_Id":cellData.textPTWRecEmpId.text!,
                             "Measures_taken_By": cellData.textCorrectiveMeasureTaken.text!,
                             "Violation":strAction
                             
                             
                             
                ] as [String:String]
//
            print(parameter)
            let jsonData: Data? = try? JSONSerialization.data(withJSONObject: parameter, options: [])
            let para: [String: Data] = ["data": jsonData!]
            
            Alamofire.upload(multipartFormData: { multipartFormData in
                
                if !(self.imagedata == nil) {
                    var res : UIImage = UIImage()
                    
                    let size = CGSize(width: 150, height: 150)
                    res = self.imageResize(image: self.cellData.imageViewPhoto.image!,sizeChange: size)
                    
                    self.imagedata = UIImageJPEGRepresentation(res, 1.0)!
                    multipartFormData.append(self.imagedata!, withName: "file", fileName: "file.jpg", mimeType: "image/jpeg")
                }
                
                for (key, value) in para {
                    multipartFormData.append(value, withName: key)
                    
                    
                }
                
            },
                             to:URLConstants.Add_Work_Permit)
            { (result) in
                switch result {
                case .success(let upload, _, _):
                    
                    upload.uploadProgress(closure: { (progress) in
                        
                    })
                    
                    
                    upload.responseJSON { response in
                        
                        
                        if let json = response.result.value
                        {
                            var dict = json as! [String: AnyObject]
                            
                            if let response = dict["response"]{
                                print(response)
                                let statusString = response["status"] as! String
                                let objectmsg = MessageCallServerModel()
                    let msg = objectmsg.sealizeMessage(cell: response as! [String : AnyObject])
                                print("HZl Dict :",dict)
                                if(statusString == "success")
                                {
                                    
                                    
                                    self.stopLoadingPK(view: self.view)
                                    MoveStruct.isMove = true
                                    
                                    MoveStruct.message = msg
                                    
                                    
                                    self.navigationController?.popViewController(animated: false)
                                    
                                    
                                    
                                    
                                }
                                else{
                                    
                                    self.stopLoadingPK(view: self.view)
                                    self.view.makeToast(msg)
                                    
                                }
                                
                                
                                
                            }
                            
                            
                        }
                        
                        
                    }
                    
                case .failure(let encodingError):
                    
                    self.stopLoadingPK(view: self.view)
                    self.errorChecking(error: encodingError)
                    
                    print(encodingError.localizedDescription)
                }
            }
        }
        
        
        
    }
    
    
    @IBAction func btnAreaClicked(_ sender: UIButton) {
        if(FilterDataFromServer.unit_Name != "") {
            
            
            FilterDataFromServer.filterType = "Area"
            FilterDataFromServer.area_Id = Int()
            FilterDataFromServer.area_Name = String()
            
            FilterDataFromServer.sub_Area_Id = Int()
            FilterDataFromServer.sub_Area_Name = String()
            updateData()
            
            
            
            
            let StoryBoard = UIStoryboard(name: "NDSO", bundle: nil)
            let moveVC = StoryBoard.instantiateViewController(withIdentifier: "NDSOASUZViewController") as! NDSOASUZViewController
            moveVC.titleStr = "Select Area"
            self.navigationController?.pushViewController(moveVC, animated: true)
            
        }
    }
    @IBAction func btnUnitClicked(_ sender: UIButton) {
        FilterDataFromServer.filterType = "Unit"
        FilterDataFromServer.unit_Id = Int()
        FilterDataFromServer.unit_Name = String()
        FilterDataFromServer.area_Id = Int()
        FilterDataFromServer.area_Name = String()
        FilterDataFromServer.sub_Area_Id = Int()
        FilterDataFromServer.sub_Area_Name = String()
        updateData()
        let StoryBoard = UIStoryboard(name: "NDSO", bundle: nil)
        let moveVC = StoryBoard.instantiateViewController(withIdentifier: "NDSOASUZViewController") as! NDSOASUZViewController
        moveVC.titleStr = "Select Unit"
        self.navigationController?.pushViewController(moveVC, animated: true)
    }
    
    @objc func updateData(){
        if(FilterDataFromServer.unit_Name != "") {
            cellData.lblUnit.text = FilterDataFromServer.unit_Name
            cellData.lblUnit.textColor = UIColor.black
        } else {
            cellData.lblUnit.text = "Select Unit"
            cellData.lblUnit.textColor = UIColor.lightGray
        }
        if(FilterDataFromServer.area_Name != "") {
            cellData.lblArea.text = FilterDataFromServer.area_Name
            cellData.lblArea.textColor = UIColor.black
        } else {
            cellData.lblArea.text = "Select Area"
            cellData.lblArea.textColor = UIColor.lightGray
        }
        
        
        // tableView.reloadData()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.title = "Safety Work Permit Checked / Audited"
        
        let nav = self.navigationController?.navigationBar
        nav?.tintColor = UIColor.white
       nav?.barTintColor = UIColor(hexString: "2c3e50", alpha: 1.0)
        nav?.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        if( FilterDataFromServer.unit_Name != "" || FilterDataFromServer.area_Name != ""){
            updateData()
        }
        //updateData()
        
        
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    @IBAction func btnImageButtonClicked(_ sender: UIButton) {
        
        let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
            self.openCamera()
        }))
        
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
            self.openGallary()
        }))
        
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
        
    }
    
    
    @objc func pressCamera(_ sender : UITapGestureRecognizer) {
        
    }
    let imagePicker = UIImagePickerController()
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera))
        {
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func openGallary()
    {
        imagePicker.delegate = self
        imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
        imagePicker.allowsEditing = true
        self.present(imagePicker, animated: true, completion: nil)
    }
    internal func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
            cellData.imageViewPhoto.image = image
            imagedata = UIImageJPEGRepresentation(image, 1.0)!
            cellData.btnCamera.isHidden = true
            cellData.lblOptional.isHidden = true
            cellData.btnClose.isHidden = false
            cellData.imageViewPhoto.isHidden = false
            updateConst()
            
            
        }else{
            print("Something went wrong")
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    // var reachability = Reachability()!
    var imagedata: Data? = nil
    @IBAction func btnCloseButtonClicked(_ sender: UIButton) {
        
        self.imagedata = nil
        self.cellData.btnClose.isHidden = true
        cellData.btnCamera.isHidden = false
        cellData.lblOptional.isHidden = false
        updateConst()
    }
    func updateConst(){
      
        
            if(self.cellData.btnClose.isHidden == true){
                self.cellData.heightViewofClose.constant = 0
                self.cellData.heightImageselected.constant = 0
                self.cellData.heightViewImage.constant = 40
            }else{
                self.cellData.heightViewofClose.constant = 20
                self.cellData.heightImageselected.constant = 100
                self.cellData.heightViewImage.constant = 120
            }
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /// Fusuma
    
    
    func imageResize (image:UIImage, sizeChange:CGSize)-> UIImage{
        
        let hasAlpha = true
        let scale: CGFloat = 0.0 // Use scale factor of main screen
        
        UIGraphicsBeginImageContextWithOptions(sizeChange, !hasAlpha, scale)
        image.draw(in: CGRect(origin: CGPoint.zero, size: sizeChange))
        
        let scaledImage = UIGraphicsGetImageFromCurrentImageContext()
        return scaledImage!
    }
    
    var indexing : Int = 1
    var cellData : WorkPermitTableViewCell!
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
extension WorkPermitViewController : UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 1000
        
    }
}
extension WorkPermitViewController : UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! WorkPermitTableViewCell
        
        cellData = cell
        
        if(indexing == 1){
            cellData.btnClose.isHidden = true
            indexing = indexing + 1
        }
        updateData()
        updateConst()
        cell.separatorInset = UIEdgeInsetsMake(0, 10000, 0, 0);
        return cell
        
        
        
    }
}
