//
//  ModelVisitODSO.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 08/02/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import Foundation
import Reachability


class MyVisitDataODSOModel: NSObject {
    
    
    var ID = Int()
    var Rost_Date = String()
    var Roster_Status = String()
    var Employee_1 = String()
    var Employee_Name_1 = String()
    var Employee_2 = String()
    var Employee_Name_2 = String()
    var Location_ID = String()
    var Location_Name = String()
    
    
    
    
    
    
    
    
    func setDataInModel(str:[String:AnyObject])
    {
        if str["Rost_Date"] is NSNull || str["Rost_Date"] == nil{
            self.Rost_Date = ""
        }else{
            
            let ros = str["Rost_Date"]
            
            self.Rost_Date = (ros?.description)!
            
            
        }
        if str["Roster_Status"] is NSNull || str["Roster_Status"] == nil{
            self.Roster_Status =  ""
        }else{
            
            let fross = str["Roster_Status"]
            
            self.Roster_Status = (fross?.description)!
            
            
            //  self.Description = (str["Description"] as? String)!
        }
        if str["ID"] is NSNull || str["ID"] == nil{
            self.ID = 0
        }else{
            self.ID = (str["ID"] as? Int)!
        }
        if str["Employee_1"] is NSNull || str["Employee_1"] == nil{
            self.Employee_1 = "0"
        }else{
            let emp1 = str["Employee_1"]
            
            self.Employee_1 = (emp1?.description)!
            // print(self.Employee_1)
        }
        if str["Employee_Name_1"] is NSNull || str["Employee_Name_1"] == nil{
            self.Employee_Name_1 = ""
        }else{
            
            let empname = str["Employee_Name_1"]
            
            self.Employee_Name_1 = (empname?.description)!
            
        }
        
        if str["Employee_2"] is NSNull || str["Employee_2"] == nil{
            self.Employee_2 = ""
        }else{
            let emp2 = str["Employee_2"]
            
            self.Employee_2 = (emp2?.description)!
            
            
            
        }
        if str["Employee_Name_2"] is NSNull || str["Employee_Name_2"] == nil{
            self.Employee_Name_2 = ""
        }else{
            let empname2 = str["Employee_Name_2"]
            
            self.Employee_Name_2 = (empname2?.description)!
            
        }
        
        
        if str["Location_ID"] is NSNull || str["Location_ID"] == nil{
            self.Location_ID = ""
        }else{
            let locid = str["Location_ID"]
            
            self.Location_ID = (locid?.description)!
            
        }
        if str["Location_Name"] is NSNull || str["Location_Name"] == nil{
            self.Location_Name = ""
        }else{
            let locname = str["Location_Name"]
            
            self.Location_Name = (locname?.description)!
            
        }
        
    }
    
}





class MyVisitDataODSOAPI
{
    
    var reachablty = Reachability()!
    
    func serviceCalling(obj:MyVisitODSOViewController, param : [String: String] , success:@escaping (AnyObject)-> Void)
    {
        obj.startLoading()
        
        if(reachablty.connection != .none)
        {
            
            
           WebServices.sharedInstances.sendPostRequest(url: URLConstants.My_Visits_ListODSO, parameters: param , successHandler: { (dict) in
                
                if let response = dict["response"]{
                    
                    let statusString : String = response["status"] as! String
                    print(response)
                    let objectmsg = MessageCallServerModel()
            let msg = objectmsg.sealizeMessage(cell: response as! [String : AnyObject])
                    if(statusString == "success")
                    {
                        obj.tableView.isHidden = false
                        obj.noDataLabel(text: "" )
                        obj.label.isHidden = true
                        if let data = dict["data"] as? [AnyObject]
                        {
                            
                            var dataArray: [MyVisitDataODSOModel] = []
                            
                            for i in 0..<data.count
                            {
                                
                                if let cell = data[i] as? [String:AnyObject]
                                {
                                    let object = MyVisitDataODSOModel()
                                    object.setDataInModel(str: cell)
                                    dataArray.append(object)
                                }
                                
                            }
                            
                            success(dataArray as AnyObject)
                            
                            
                        }
                        
                        obj.refreshControl.endRefreshing()
                        obj.stopLoading()
                    }
                    else
                    {
                        obj.label.isHidden = false
                        obj.tableView.isHidden = true
                        obj.noDataLabel(text: msg )
                        obj.refreshControl.endRefreshing()
                        obj.stopLoading()
                        
                    }
                    
                    
                }
                
                
                
                
            })
            { (error) in
                
                let errorStr : String = error.description
                
                print("DATA:error",errorStr)
                
                //                obj.errorChecking(error: error)
                obj.stopLoading()
            }
            
            
        }
        else{
            obj.label.isHidden = false
            obj.tableView.isHidden = true
            obj.noDataLabel(text: "Internet is not available, please check your internet connection try again." )
            obj.stopLoading()
        }
        
        
    }
    
    
}

class MyVisitDataODSOAPILoadMore
{
    
    var reachablty = Reachability()!
    
    func serviceCalling(obj:MyVisitODSOViewController, param : [String: String] , success:@escaping (AnyObject)-> Void)
    {
        obj.startLoading()
        
        if(reachablty.connection != .none)
        {
            
            
            WebServices.sharedInstances.sendPostRequest(url: URLConstants.My_Visits_ListODSO, parameters: param , successHandler: { (dict) in
                
                if let response = dict["response"]{
                    
                    let statusString : String = response["status"] as! String
                    
                    if(statusString == "success")
                    {
                        //                        obj.tableView.isHidden = false
                        //                        obj.noDataLabel(text: "" )
                        
                        if let data = dict["data"] as? [AnyObject]
                        {
                            
                            var dataArray: [MyVisitDataODSOModel] = []
                            
                            for i in 0..<data.count
                            {
                                
                                if let cell = data[i] as? [String:AnyObject]
                                {
                                    let object = MyVisitDataODSOModel()
                                    object.setDataInModel(str: cell)
                                    dataArray.append(object)
                                }
                                
                            }
                            
                            success(dataArray as AnyObject)
                            
                            
                        }
                        
                        obj.refreshControl.endRefreshing()
                        obj.stopLoading()
                    }
                    else
                    {
                        
                        //                        obj.tableView.isHidden = true
                        //                        obj.noDataLabel(text: "No Data Found!" )
                        obj.refreshControl.endRefreshing()
                        obj.stopLoading()
                        
                    }
                    
                    
                }
                
                
                
                
            })
            { (error) in
                
                let errorStr : String = error.description
                
                print("DATA:error",errorStr)
                
                //                obj.errorChecking(error: error)
                obj.stopLoading()
            }
            
            
        }
        else{
            //            obj.tableView.isHidden = true
            //            obj.noDataLabel(text: "Internet is not available, please check your internet connection try again." )
            obj.stopLoading()
        }
        
        
    }
    
    
}

class MyCountDataODSOModel: NSObject {
    
    
  
    var Accident = String()
    var Penalty = String()
    var Tool_Box = String()
    var Work_Permit = String()
    
    var Observation = String()
    
    
    
    
    
    
    func setDataInModel(Accident : String, Penalty : String, Tool_Box : String, Work_Permit : String, Observation : String)
    {

        
        if Observation == ""{
            self.Observation = ""
        }else{
            
            let ob = Observation
            
            self.Observation = (ob.description)
            
            
        }
        
        
        if Accident == ""{
            self.Accident = ""
        }else{
            
            let qq1 = Accident
            
            self.Accident = (qq1.description)
            
            
        }
        if  Penalty == ""{
            self.Penalty = ""
        }else{
            
            let qq2 = Penalty
            
            self.Penalty = (qq2.description)
            
            
        }
        if  Tool_Box == ""{
            self.Tool_Box = ""
        }else{
            
            let qq3 = Tool_Box
            
            self.Tool_Box = (qq3.description)
            
            
        }
        if  Work_Permit == ""{
            self.Work_Permit = ""
        }else{
            
            let qq4 = Work_Permit
            
            self.Work_Permit = (qq4.description)
            
            
        }
        for i in 0...2 {
            var observe = String()
            var Quest = String()
            if(i == 0){
                observe = self.Observation
                Quest = "SITE OBSERVATIONS (UNSAFE ACT / CONDITION)"
            }else if(i == 1){
                observe = self.Work_Permit
                Quest = "SAFETY WORK PERMIT CHECKED / AUDITED"
            }
            else{
                Quest = "TOOL BOX TALK OBSERVED"
                observe = self.Tool_Box
            }
            let obObserve = QuestionListODSOModel()
            obObserve.observation = observe
            obObserve.Question = Quest
            dataArrayQuestionListODSO.append(obObserve)
        }
    }
    
}

class MyObCountDataODSOAPI
{
    
    var reachablty = Reachability()!
    
    func serviceCalling(obj:SubmitReportODSOViewController, param : [String: String] , success:@escaping (AnyObject)-> Void)
    {
        
        
        if(reachablty.connection != .none)
        {
            
            var dataArray: [MyCountDataODSOModel] = []
            WebServices.sharedInstances.sendPostRequest(url: URLConstants.Observation_Datails_Team_ID, parameters: param , successHandler: { (dict) in
                
                if let response = dict["response"]{
                    
                    let statusString : String = response["status"] as! String
                    dataArrayQuestionListODSO = []
                    if(statusString == "success")
                    {
                        
                        
                        var Accident = String()
                        var Penalty = String()
                        var Tool_Box = String()
                        var Work_Permit = String()
                        
                        var Observation = String()
                       
                        if let Q11 = dict["Accident"] as? [AnyObject]
                        {
                            Accident = String(Q11.count)
                        }
                        if let Q21 = dict["Penalty"] as? [AnyObject]
                        {
                            Penalty = String(Q21.count)
                        }
                        if let Q31 = dict["Tool_Box"] as? [AnyObject]
                        {
                            Tool_Box = String(Q31.count)
                        }
                        if let Q41 = dict["Work_Permit"] as? [AnyObject]
                        {
                            Work_Permit = String(Q41.count)
                        }
                        if let Observation1 = dict["Observation"] as? [AnyObject]
                        {
                            Observation = String(Observation1.count)
                        }
                         dataArray = []
                        let object = MyCountDataODSOModel()
                        object.setDataInModel( Accident: Accident, Penalty: Penalty, Tool_Box: Tool_Box, Work_Permit: Work_Permit, Observation: Observation)
                        dataArray.append(object)
                        
                        success(dataArray as AnyObject)
                        obj.tableView.reloadData()
                        
                    }
                    else
                    {
                        dataArrayQuestionListODSO = []
                        for i in 0...2 {
                            var observe = String()
                            var Quest = String()
                            if(i == 0){
                                observe = "0"
                                Quest = "SITE OBSERVATIONS (UNSAFE ACT / CONDITION)"
                            }else if(i == 1){
                                observe = "0"
                                Quest = "SAFETY WORK PERMIT CHECKED / AUDITED"
                            }
                            else{
                                Quest = "TOOL BOX TALK OBSERVED"
                                observe = "0"
                            }
                            let obObserve = QuestionListODSOModel()
                            obObserve.observation = observe
                            obObserve.Question = Quest
                            dataArrayQuestionListODSO.append(obObserve)
                            obj.tableView.reloadData()
                        }
                        
                    }
                    
                    
                }
                
                
                
                
            })
            { (error) in
                
                let errorStr : String = error.description
                
                print("DATA:error",errorStr)
                
                //                obj.errorChecking(error: error)
                obj.stopLoading()
            }
            
            
        }
        else{
            obj.label.isHidden = false
            obj.tableView.isHidden = true
            obj.noDataLabel(text: "Internet is not available, please check your internet connection try again." )
            obj.stopLoading()
        }
        
        
    }
    
    
}

//class MyQuestionDataODSOModel: NSObject {
//    
//    
//    var ID = Int()
//    var Q1 = String()
//    var Q2 = String()
//    var Q3 = String()
//    var Q4 = String()
//    
//    
//    
//    
//    
//    
//    
//    
//    func setDataInModel(str:[String:AnyObject])
//    {
//        if str["Q1"] is NSNull || str["Q1"] == nil{
//            self.Q1 = ""
//        }else{
//            
//            let qq1 = str["Q1"]
//            
//            self.Q1 = (qq1?.description)!
//            
//            
//        }
//        if str["Q2"] is NSNull || str["Q2"] == nil{
//            self.Q2 = ""
//        }else{
//            
//            let qq2 = str["Q2"]
//            
//            self.Q2 = (qq2?.description)!
//            
//            
//        }
//        if str["Q3"] is NSNull || str["Q3"] == nil{
//            self.Q3 = ""
//        }else{
//            
//            let qq3 = str["Q3"]
//            
//            self.Q3 = (qq3?.description)!
//            
//            
//        }
//        if str["Q4"] is NSNull || str["Q4"] == nil{
//            self.Q4 = ""
//        }else{
//            
//            let qq4 = str["Q4"]
//            
//            self.Q4 = (qq4?.description)!
//            
//            
//        }
//        
//       
//        
//    }
//    
//}
//
//
//
//
//
//class MyQuestionDataODSOAPI
//{
//    
//    var reachablty = Reachability()!
//    
//    func serviceCalling(obj:SubmitReportODSOViewController, param : [String: String] , success:@escaping (AnyObject)-> Void)
//    {
//        obj.startLoading()
//        
//        if(reachablty.connection != .none)
//        {
//            
//            
//            WebServices.sharedInstances.sendPostRequest(url: URLConstants.Question_ListNSDO, parameters: param , successHandler: { (dict) in
//                
//                if let response = dict["response"]{
//                    
//                    let statusString : String = response["status"] as! String
//                    
//                    if(statusString == "success")
//                    {
//                        obj.tableView.isHidden = false
//                        obj.noDataLabel(text: "" )
//                        obj.label.isHidden = true
//                        print(dict["data"])
//                        if let data = dict["data"] as? AnyObject
//                        {
//                            
//                            var dataArray: [MyQuestionDataODSOModel] = []
//                            
//                            
//                            
//                            if let cell = data as? [String:AnyObject]
//                            {
//                                let object = MyQuestionDataODSOModel()
//                                object.setDataInModel(str: cell)
//                                dataArray.append(object)
//                            }
//                            
//                            
//                            
//                            success(dataArray as AnyObject)
//                            
//                            
//                        }
//                        
//                        obj.refreshControl.endRefreshing()
//                        obj.stopLoading()
//                    }
//                    else
//                    {
//                        obj.label.isHidden = false
//                        obj.tableView.isHidden = true
//                        obj.noDataLabel(text: "No Data Found!" )
//                        obj.refreshControl.endRefreshing()
//                        obj.stopLoading()
//                        
//                    }
//                    
//                    
//                }
//                
//                
//                
//                
//            })
//            { (error) in
//                
//                let errorStr : String = error.description
//                
//                print("DATA:error",errorStr)
//                
//                //                obj.errorChecking(error: error)
//                obj.stopLoading()
//            }
//            
//            
//        }
//        else{
//            obj.label.isHidden = false
//            obj.tableView.isHidden = true
//            obj.noDataLabel(text: "Internet is not available, please check your internet connection try again." )
//            obj.stopLoading()
//        }
//        
//        
//    }
//    
//    
//}

class QuestionListODSOModel: NSObject {
    
    var Question = String()
    var observation = String()
    
    
}

