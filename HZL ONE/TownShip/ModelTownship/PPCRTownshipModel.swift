//
//  PPCRTownshipModel.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 09/03/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import Foundation


class PPCRTownshipDataModel: NSObject {
    
    
    var ID = Int()
    var EmployeeID = String()
    var Employee_Name = String()
    var Category_Name = String()
    var ComplaintSubject_Name = String()
    var ImagePath = String()
    var ComplainVia = String()
    var Remarks = String()
    var ComplaintStatus = String()
    var ComplaintDate_Status = String()
    var ComplaintDate = String()
    var Address = String()
    

    
    func setDataInModel(str:[String:AnyObject])
    {
        if str["EmployeeID"] is NSNull || str["EmployeeID"] == nil{
            self.EmployeeID = ""
        }else{
            
            let tit = str["EmployeeID"]
            
            self.EmployeeID = (tit?.description)!
            
            
        }
        if str["Employee_Name"] is NSNull || str["Employee_Name"] == nil{
            self.Employee_Name =  ""
        }else{
            
            let desc = str["Employee_Name"]
            
            self.Employee_Name = (desc?.description)!
 
        }
        if str["ID"] is NSNull || str["ID"] == nil{
            self.ID = 0
        }else{
            self.ID = (str["ID"] as? Int)!
        }
        
        
        if str["Category_Name"] is NSNull || str["Category_Name"] == nil{
            self.Category_Name =  ""
        }else{
            
            let desc = str["Category_Name"]
            
            self.Category_Name = (desc?.description)!
            
    
        }
        if str["ComplaintSubject_Name"] is NSNull || str["ComplaintSubject_Name"] == nil{
            self.ComplaintSubject_Name =  ""
        }else{
            
            let desc = str["ComplaintSubject_Name"]
            
            self.ComplaintSubject_Name = (desc?.description)!
            
       
        }
        if str["ImagePath"] is NSNull || str["ImagePath"] == nil{
            self.ImagePath =  ""
        }else{
            
            let desc = str["ImagePath"]
            
            self.ImagePath = (desc?.description)!
            
        
        }
        if str["ComplainVia"] is NSNull || str["ComplainVia"] == nil{
            self.ComplainVia =  ""
        }else{
            
            let desc = str["ComplainVia"]
            
            self.ComplainVia = (desc?.description)!
            
        
        }
        
        
        if str["Remarks"] is NSNull || str["Remarks"] == nil{
            self.Remarks =  ""
        }else{
            
            let desc = str["Remarks"]
            
            self.Remarks = (desc?.description)!
            
    
        }

        if str["ComplaintStatus"] is NSNull || str["ComplaintStatus"] == nil{
            self.ComplaintStatus =  ""
        }else{
            
            let desc = str["ComplaintStatus"]
            
            self.ComplaintStatus = (desc?.description)!
       
        }
        if str["ComplaintDate_Status"] is NSNull || str["ComplaintDate_Status"] == nil{
            self.ComplaintDate_Status =  ""
        }else{
            
            let desc = str["ComplaintDate_Status"]
            
            self.ComplaintDate_Status = (desc?.description)!
        
        }
        
        if str["ComplaintDate"] is NSNull || str["ComplaintDate"] == nil{
            self.ComplaintDate =  ""
        }else{
            
            let desc = str["ComplaintDate"]
            
            self.ComplaintDate = (desc?.description)!
            

        }
        
        if str["Address"] is NSNull || str["Address"] == nil{
            self.Address =  ""
        }else{
            
            let desc = str["Address"]
            
            self.Address = (desc?.description)!
            

        }
       
    }
    
}





class PPCRTownshipDataAPI
{
    
    var reachablty = Reachability()!
    
    func serviceCalling(obj:PPCRTownshipViewController, param : [String: String] , success:@escaping (AnyObject)-> Void)
    {
        obj.startLoading()
        
        if(reachablty.connection != .none)
        {
            
            
            WebServices.sharedInstances.sendPostRequest(url: URLConstants.Complaint_List, parameters: param , successHandler: { (dict) in
                
                if let response = dict["response"]{
                    
                    let statusString : String = response["status"] as! String
                    let objectmsg = MessageCallServerModel()
            let msg = objectmsg.sealizeMessage(cell: response as! [String : AnyObject])
                    print(response)
                    if(statusString == "success")
                    {
                        obj.tableViewTownship.isHidden = false
                        obj.noDataLabel(text: "" )
                        obj.label.isHidden = true
                        if let data = dict["data"] as? [AnyObject]
                        {
                            
                            var dataArray: [PPCRTownshipDataModel] = []
                            
                            for i in 0..<data.count
                            {
                                
                                if let cell = data[i] as? [String:AnyObject]
                                {
                                    let object = PPCRTownshipDataModel()
                                    object.setDataInModel(str: cell)
                                    dataArray.append(object)
                                }
                                
                            }
                            
                            success(dataArray as AnyObject)
                            
                            
                        }
                        
                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                    }
                    else
                    {
                        obj.label.isHidden = false
                        obj.tableViewTownship.isHidden = true
                        obj.noDataLabel(text: msg )
                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                        
                    }
                    
                    
                }
                
                
                
                
            })
            { (error) in
                
                let errorStr : String = error.description
                
                print("DATA:error",errorStr)
                
                //                obj.errorChecking(error: error)
                obj.stopLoading()
            }
            
            
        }
        else{
            obj.label.isHidden = false
            obj.tableViewTownship.isHidden = true
            obj.noDataLabel(text: "Internet is not available, please check your internet connection try again." )
            obj.stopLoading()
        }
        
        
    }
    
    
}
