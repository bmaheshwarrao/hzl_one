//
//  UserTownShipModel.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 14/03/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import Foundation
class UserTownshipDataModel: NSObject {
    
    
    var ID = Int()
    var EmployeeID = String()
    var Name = String()
    var Mobile = String()
    var Email_Id = String()
    var UserType = String()
    var QuarterNo = String()
    var QuarterType = String()
    var Sector = String()
    var Category = String()
    var Department = String()
    var Location_ID = String()
      var Location_Name = String()
    

    
    func setDataInModel(str:[String:AnyObject])
    {
        if str["EmployeeID"] is NSNull || str["EmployeeID"] == nil{
            self.EmployeeID = ""
        }else{
            
            let tit = str["EmployeeID"]
            
            self.EmployeeID = (tit?.description)!
            
            
        }
       
        if str["Name"] is NSNull || str["Name"] == nil{
            self.Name =  ""
        }else{
            
            let desc = str["Name"]
            
            self.Name = (desc?.description)!
            
        }
        if str["ID"] is NSNull || str["ID"] == nil{
            self.ID = 0
        }else{
            self.ID = (str["ID"] as? Int)!
        }
        
        
        if str["Mobile"] is NSNull || str["Mobile"] == nil{
            self.Mobile =  ""
        }else{
            
            let desc = str["Mobile"]
            
            self.Mobile = (desc?.description)!
            
            
        }
        if str["Email_Id"] is NSNull || str["Email_Id"] == nil{
            self.Email_Id =  ""
        }else{
            
            let desc = str["Email_Id"]
            
            self.Email_Id = (desc?.description)!
            
            
        }
        if str["UserType"] is NSNull || str["UserType"] == nil{
            self.UserType =  ""
        }else{
            
            let desc = str["UserType"]
            
            self.UserType = (desc?.description)!
            
            
        }
      
        if str["QuarterNo"] is NSNull || str["QuarterNo"] == nil{
            self.QuarterNo =  ""
        }else{
            
            let desc = str["QuarterNo"]
            
            self.QuarterNo = (desc?.description)!
            
            
        }
        
        
        if str["QuarterType"] is NSNull || str["QuarterType"] == nil{
            self.QuarterType =  ""
        }else{
            
            let desc = str["QuarterType"]
            
            self.QuarterType = (desc?.description)!
            
            
        }
        
        if str["Sector"] is NSNull || str["Sector"] == nil{
            self.Sector =  ""
        }else{
            
            let desc = str["Sector"]
            
            self.Sector = (desc?.description)!
            
        }
        if str["Category"] is NSNull || str["Category"] == nil{
            self.Category =  ""
        }else{
            
            let desc = str["Category"]
            
            self.Category = (desc?.description)!
            
        }
        
        if str["Department"] is NSNull || str["Department"] == nil{
            self.Department =  ""
        }else{
            
            let desc = str["Department"]
            
            self.Department = (desc?.description)!
            
            
        }
       
        if str["Location_ID"] is NSNull || str["Location_ID"] == nil{
            self.Location_ID =  ""
        }else{
            
            let desc = str["Location_ID"]
            
            self.Location_ID = (desc?.description)!
            
            
        }
        if str["Location_Name"] is NSNull || str["Location_Name"] == nil{
            self.Location_Name =  ""
        }else{
            
            let desc = str["Location_Name"]
            
            self.Location_Name = (desc?.description)!
            
            
        }
        
    }
    
}





class UserTownshipDataAPI
{
    
    var reachablty = Reachability()!
    
    func serviceCalling(obj:DashboardTownshipViewController, param : [String: String] , success:@escaping (AnyObject)-> Void)
    {
        obj.startLoadingPK(view: obj.view)
        
        if(reachablty.connection != .none)
        {
            
            
            WebServices.sharedInstances.sendPostRequest(url: URLConstants.User_Details, parameters: param , successHandler: { (dict) in
                
                if let response = dict["response"]{
                    
                    let statusString : String = response["status"] as! String
                   
                    let objectmsg = MessageCallServerModel()
                    let msgString = objectmsg.sealizeMessage(cell: response as! [String : AnyObject])
                    
                    
                    if(statusString == "success")
                    {
                        
                       
                        if let data = dict["data"]
                        {
                            
                            var dataArray: [UserTownshipDataModel] = []
                            
                           
                                if let cell = data as? [String : AnyObject]
                                {
                                    let object = UserTownshipDataModel()
                                    object.setDataInModel(str: cell)
                                    dataArray.append(object)
                                }
                                
                            
                            
                            success(dataArray as AnyObject)
                            
                            
                        }
                        obj.returnStrMsg = msgString
                        obj.refresh.endRefreshing()
                        obj.stopLoadingPK(view: obj.view)
                    }
                    else
                    {
                        obj.returnStrMsg = msgString
                        obj.refresh.endRefreshing()
                        obj.stopLoadingPK(view: obj.view)
                        
                    }
                    
                    
                }
                
                
                
                
            })
            { (error) in
                
                let errorStr : String = error.description
                
                print("DATA:error",errorStr)
                
                //                obj.errorChecking(error: error)
                obj.stopLoadingPK(view: obj.view)
            }
            
            
        }
        else{
         
            obj.stopLoadingPK(view: obj.view)
        }
        
        
    }
    
    
}



class EmpLocTownshipDataModel: NSObject {
    
    
//    var ID = Int()
//    var EmployeeID = String()
//    var Name = String()
//    var Mobile = String()
//    var Email_Id = String()
//    var UserType = String()
//    var QuarterNo = String()
//    var QuarterType = String()
//    var Sector = String()
//    var Category = String()
//    var Department = String()
    var Location = String()
    var Location_Name = String()
    
    
    
    func setDataInModel(str:[String:AnyObject])
    {
//        if str["EmployeeID"] is NSNull || str["EmployeeID"] == nil{
//            self.EmployeeID = ""
//        }else{
//
//            let tit = str["EmployeeID"]
//
//            self.EmployeeID = (tit?.description)!
//
//
//        }
//
//        if str["Name"] is NSNull || str["Name"] == nil{
//            self.Name =  ""
//        }else{
//
//            let desc = str["Name"]
//
//            self.Name = (desc?.description)!
//
//        }
//        if str["ID"] is NSNull || str["ID"] == nil{
//            self.ID = 0
//        }else{
//            self.ID = (str["ID"] as? Int)!
//        }
//
//
//        if str["Mobile"] is NSNull || str["Mobile"] == nil{
//            self.Mobile =  ""
//        }else{
//
//            let desc = str["Mobile"]
//
//            self.Mobile = (desc?.description)!
//
//
//        }
//        if str["Email_Id"] is NSNull || str["Email_Id"] == nil{
//            self.Email_Id =  ""
//        }else{
//
//            let desc = str["Email_Id"]
//
//            self.Email_Id = (desc?.description)!
//
//
//        }
//        if str["UserType"] is NSNull || str["UserType"] == nil{
//            self.UserType =  ""
//        }else{
//
//            let desc = str["UserType"]
//
//            self.UserType = (desc?.description)!
//
//
//        }
//
//        if str["QuarterNo"] is NSNull || str["QuarterNo"] == nil{
//            self.QuarterNo =  ""
//        }else{
//
//            let desc = str["QuarterNo"]
//
//            self.QuarterNo = (desc?.description)!
//
//
//        }
//
//
//        if str["QuarterType"] is NSNull || str["QuarterType"] == nil{
//            self.QuarterType =  ""
//        }else{
//
//            let desc = str["QuarterType"]
//
//            self.QuarterType = (desc?.description)!
//
//
//        }
//
//        if str["Sector"] is NSNull || str["Sector"] == nil{
//            self.Sector =  ""
//        }else{
//
//            let desc = str["Sector"]
//
//            self.Sector = (desc?.description)!
//
//        }
//        if str["Category"] is NSNull || str["Category"] == nil{
//            self.Category =  ""
//        }else{
//
//            let desc = str["Category"]
//
//            self.Category = (desc?.description)!
//
//        }
//
//        if str["Department"] is NSNull || str["Department"] == nil{
//            self.Department =  ""
//        }else{
//
//            let desc = str["Department"]
//
//            self.Department = (desc?.description)!
//
//
//        }
        
        if str["Location"] is NSNull || str["Location"] == nil{
            self.Location =  ""
        }else{
            
            let desc = str["Location"]
            
            self.Location = (desc?.description)!
            
            
        }
        if str["Location_Name"] is NSNull || str["Location_Name"] == nil{
            self.Location_Name =  ""
        }else{
            
            let desc = str["Location_Name"]
            
            self.Location_Name = (desc?.description)!
            
            
        }
        
    }
    
}





class EmpLocTownshipDataAPI
{
    
    var reachablty = Reachability()!
    
    func serviceCalling(obj:LocationQuarterSubmitViewController, param : [String: String] , success:@escaping (AnyObject)-> Void)
    {
        obj.startLoadingPK(view: obj.view)
        
        if(reachablty.connection != .none)
        {
            
            
            WebServices.sharedInstances.sendPostRequest(url: URLConstants.User_Details, parameters: param , successHandler: { (dict) in
                
                if let response = dict["response"]{
                    
                    let statusString : String = response["status"] as! String
                    let msgString : String = response["Msg"] as! String
                    if(statusString == "success")
                    {
                        
                        
                        if let data = dict["data"]
                        {
                            
                            var dataArray: [EmpLocTownshipDataModel] = []
                            
                            
                            if let cell = data as? [String : AnyObject]
                            {
                                let object = EmpLocTownshipDataModel()
                                object.setDataInModel(str: cell)
                                dataArray.append(object)
                            }
                            
                            
                            
                            success(dataArray as AnyObject)
                            
                            
                        }
                      
                        obj.refresh.endRefreshing()
                        obj.stopLoadingPK(view: obj.view)
                    }
                    else
                    {
                       
                        obj.refresh.endRefreshing()
                        obj.stopLoadingPK(view: obj.view)
                        
                    }
                    
                    
                }
                
                
                
                
            })
            { (error) in
                
                let errorStr : String = error.description
                
                print("DATA:error",errorStr)
                
                //                obj.errorChecking(error: error)
                obj.stopLoadingPK(view: obj.view)
            }
            
            
        }
        else{
            
            obj.stopLoadingPK(view: obj.view)
        }
        
        
    }
    
    
}
