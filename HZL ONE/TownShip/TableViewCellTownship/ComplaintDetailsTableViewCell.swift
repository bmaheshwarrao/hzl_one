//
//  ComplaintDetailsTableViewCell.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 09/03/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit

class ComplaintDetailsTableViewCell: UITableViewCell {

    @IBOutlet weak var textViewUpdateOn: UITextView!
    @IBOutlet weak var textViewStatus: UITextView!
    @IBOutlet weak var textViewRemark: UITextView!
    @IBOutlet weak var textViewSubject: UITextView!
    @IBOutlet weak var textViewCategory: UITextView!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblId: UILabel!
    @IBOutlet weak var imageCompalint: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
