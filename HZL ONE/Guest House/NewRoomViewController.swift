//
//  NewRoomViewController.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 22/02/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit
import DLRadioButton
import SwiftyJSON
    var dateData = 0
var startDate = Int()
var endDate = Int()
var DataDateStart = Date()
var DataDateEnd = Date()
class NewRoomViewController:CommonVSClass , WWCalendarTimeSelectorProtocol {
    
    
 
    
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        getGuestList()
        getApproverList()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
      
        // Do any additional setup after loading the view.
    }
   
   
    @IBAction func btnNextClicked(_ sender: UIButton) {
        if(cellData.lblGuest.text == "" || cellData.lblGuest.text == "Select"){
            self.view.makeToast("Select Guest House")
        }
//        else if(cellData.txtName.text == "" ){
//            self.view.makeToast("Enter name")
//        }else if(cellData.txtContact.text == "" ){
//            self.view.makeToast("Enter contact")
//        }
        else if(cellData.lblCheckIn.text == "" || cellData.lblCheckIn.text == "Date and Time"){
            self.view.makeToast("Select Check in datetime")
        }else if(cellData.lblCheckOut.text == "" || cellData.lblCheckOut.text == "Date and Time"){
           self.view.makeToast("Select Check out datetime")
        }else if(cellData.textViewPurpose.text == "" ){
            self.view.makeToast("Write Purpose")
        }else if(cellData.lblLocationApp.text == "" || cellData.lblLocationApp.text == "Select"){
            self.view.makeToast("Select Location Approver")
        }else{
            
            
            
            
           let IDData : String = UserDefaults.standard.string(forKey: "IDData")!
            let dateSetFor = "/Date(" + millisecStart + ")/"
          
            let dateSetTo = "/Date(" + millisecEnd + ")/"
            let object = RequestFoodNewData()
            object.Aadhar = ""
            object.ArrivalTime = arrivalTime
            object.BillToCompany = billing

            object.CheckInDate = dateSetFor
            object.CheckOutDate = dateSetTo
            
            
            object.BreakFast = breakFast
            object.Dinner = dinner
            object.Lunch = lunch
            
            object.CompanyName = cellData.txtCompanyName.text!
            object.EmpID = IDData
            object.FromDate = dateSetFor
            
           // object.Name = cellData.txtName.text!
            object.ID = "0"
            object.HouseID = idGuest
            
            
            object.Notes = ""
            object.Remark = ""
            object.ReqDate = dateSetFor
            
            object.SelectedApprover = idApproval
            object.ToDate = dateSetTo
            object.ReqType = requestForId
           
           object.purpose = cellData.textViewPurpose.text!
            
      
            
          
         
        
           
           
            
           
      
            
            
            


            dataNameListArray = []
            dataNameArray = []
            let storyBoard = UIStoryboard(name: "GuestHouse", bundle: nil)
            let submitVC = storyBoard.instantiateViewController(withIdentifier: "SelectRoomswithDetailsViewController") as! SelectRoomswithDetailsViewController
            submitVC.rowData = 0
          
            submitVC.requestModel = object
            self.navigationController?.pushViewController(submitVC, animated: true)
        }
        
    }
    
    var approverDB:[ApproverListModel] = []
    
    var approverAPI = ApproverListDataAPI()
    @objc func getApproverList(){
    
            let empId = UserDefaults.standard.string(forKey: "EmployeeID")!
            let Profile_AuthKey = UserDefaults.standard.string(forKey: "Profile_AuthKey")!
            let paramm  : [String:String] = ["UserID": empId , "AuthKey" :Profile_AuthKey ]
            
            self.approverAPI.serviceCalling(obj: self , param : paramm) { (dict) in
                
                self.approverDB = [ApproverListModel]()
                self.approverDB = dict as! [ApproverListModel]
                if(self.approverDB.count > 0){
                    for i in 0...self.approverDB.count - 1 {
                        self.ApproverListArray.append(self.approverDB[i].Full_Name!)
                        self.ApproverIdArray.append(self.approverDB[i].ID!)
                    }
                }
                
                
                
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
       
        if(MoveStruct.isMove == true){
            self.navigationController?.popViewController(animated: false)
        }
        self.title = "New Room Request"
        self.tabBarController?.tabBar.isHidden = true
        let nav = self.navigationController?.navigationBar
        nav?.tintColor = UIColor.white
       nav?.barTintColor = UIColor(hexString: "2c3e50", alpha: 1.0)
        nav?.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
       
        
        
    }
    
    
    
    var guestDB:[GuestHouseModel] = []
    
    var guestAPI = GuestHouseDataAPI()
    @objc func getGuestList(){
        
        let empId = UserDefaults.standard.string(forKey: "EmployeeID")!
          let Profile_AuthKey = UserDefaults.standard.string(forKey: "Profile_AuthKey")!
        let paramm  : [String:String] = ["UserID": empId , "AuthKey" :Profile_AuthKey ]
        
        self.guestAPI.serviceCalling(obj: self , param : paramm) { (dict) in
            
            self.guestDB = [GuestHouseModel]()
            self.guestDB = dict as! [GuestHouseModel]
            if(self.guestDB.count > 0){
                for i in 0...self.guestDB.count - 1 {
                    self.GuestListArray.append(self.guestDB[i].Name!)
                    self.GuestIdArray.append(self.guestDB[i].ID!)
                }
            }
            
       
            
            
        }
    }
    var idGuest = String()
    var GuestListArray :[String] = []
    var GuestIdArray :[String] = []
    @IBAction func btnGuestHouseClicked(_ sender: UIButton) {
//       if(self.guestDB.count == 0){
//        getGuestList()
//       }
        DispatchQueue.main.async(execute: {() -> Void in
            let popup = PopUpTableViewController()
            
            popup.itemsArray = self.GuestListArray
            
            popup.sourceView = self.cellData.lblGuest
            popup.isScroll = true
            popup.backgroundColor = UIColor.white
            if popup.itemsArray.count > 5{
                popup.popUpHeight = 200
            }
            else{
                popup.popUpHeight = CGFloat(popup.itemsArray.count)*45
            }
            popup.popUpWidth = UIScreen.main.bounds.size.width-60
            popup.backgroundImage = nil
            popup.itemTitleColor = UIColor.white
            popup.itemSelectionColor = UIColor.lightGray
            popup.arrowDirections = .any
            popup.arrowColor = UIColor.white
            popup.popCellBlock = {(_ popupVC: PopUpTableViewController, _ popupCell: UITableViewCell, _ row: Int, _ section: Int) -> Void in
                
                
                
                
                
                self.cellData.lblGuest.text = self.GuestListArray[row]
                self.cellData.lblGuest.textColor = UIColor.black
         
                self.idGuest = self.GuestIdArray[row]
          
                popupVC.dismiss(animated: false, completion: nil)
                
            }
            self.present(popup, animated: true, completion: {() -> Void in
            })
            
            
        })
        
        
    }
    
    var ApproverListArray :[String] = []
    var ApproverIdArray :[String] = []
    var requestFor = String()
    var requestForId = String()
    @IBAction func btnReuestForClicked(_ sender: DLRadioButton) {
        for button in sender.selectedButtons() {
            
            requestFor =   button.titleLabel!.text!
        }
         requestForData()
       
    }
    func requestForData(){
        if(requestFor == "Self"){
           
            requestForId = "1"
            cellData.companyViewHeightCon.constant = 0
            cellData.viewComapnyHeightCon.constant = 0
            cellData.companyLblheightCon.constant = 0
          
        }else{
            requestForId = "2"
            cellData.companyViewHeightCon.constant = 70
            cellData.viewComapnyHeightCon.constant = 40
            cellData.companyLblheightCon.constant = 21
        }
        self.tableView.reloadData()
    }
    var billing = Bool()
    @IBAction func btnBillingClicked(_ sender: DLRadioButton) {
       var tit : Bool = false
        for button in sender.selectedButtons() {
            
             tit = true
        }
       billing = tit
    }
    var breakFast = Bool()
    var lunch = Bool()
    var dinner = Bool()
    @IBAction func btnBreakFastClicked(_ sender: DLRadioButton) {
        var tit : Bool = false
        for button in sender.selectedButtons() {
            var dataButton = String()
            
            dataButton =    button.titleLabel!.text!
            if(dataButton == "Break Fast"){
                tit = true
            }
        }
        breakFast = tit
        
        
        
    }
    @IBAction func btnLunchClicked(_ sender: DLRadioButton) {
       
        
        var tit : Bool = false
        for button in sender.selectedButtons() {
            var dataButton = String()
            
            dataButton =    button.titleLabel!.text!
            if(dataButton == "Lunch"){
                tit = true
            }
        }
        lunch = tit
        
    }
    @IBAction func btnDinnerClicked(_ sender: DLRadioButton) {
        
        
        var tit : Bool = false
        for button in sender.selectedButtons() {
            var dataButton = String()
            
            dataButton =    button.titleLabel!.text!
            if(dataButton == "Dinner"){
                tit = true
            }
        }
        dinner = tit
        
        
    }
    
    var millisecStart = String()
    var millisecEnd = String()
    func getMilliseconds(date1 : Date) -> Int{
        //let strDate = date1
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd MMM yyyy  hh:mma"
        //let date = dateFormatter.date(from: strDate)
        
        let millieseconds = self.getDiffernce(toTime: date1)
        
        return millieseconds
    }
    func getDiffernce(toTime:Date) -> Int{
        let elapsed = toTime.timeIntervalSince1970
        return Int(elapsed * 1000)
    }
    var arrivalTime = String()
    fileprivate var singleDate: Date = Date()
    fileprivate var multipleDates: [Date] = []
    var selectdateVal = 0
    @IBAction func btnCheckInClicked(_ sender: UIButton) {
        
        let selector = UIStoryboard(name: "WWCalendarTimeSelector", bundle: nil).instantiateViewController(withIdentifier: "WWCalendarTimeSelector") as! WWCalendarTimeSelector
        
        selectdateVal = 1
         dateData = 1

        selector.delegate = self
        selector.optionCurrentDate = singleDate
        selector.optionCurrentDates = Set(multipleDates)
        selector.optionCurrentDateRange.setStartDate(multipleDates.first ?? singleDate)
        selector.optionCurrentDateRange.setEndDate(multipleDates.last ?? singleDate)
        

        selector.optionStyles.showDateMonth(true)
        selector.optionStyles.showMonth(false)
        selector.optionStyles.showYear(true)
        selector.optionStyles.showTime(false)
        
    
        present(selector, animated: true, completion: nil)
    }

    @IBAction func btnCheckOutClicked(_ sender: UIButton) {
        if(cellData.lblCheckIn.text == "Date and Time"){
            self.view.makeToast("Please Select Check In Date and time")
        }else{
        let selector = UIStoryboard(name: "WWCalendarTimeSelector", bundle: nil).instantiateViewController(withIdentifier: "WWCalendarTimeSelector") as! WWCalendarTimeSelector
        
        
         selectdateVal = 2
        
        selector.delegate = self
        selector.optionCurrentDate = singleDate
        selector.optionCurrentDates = Set(multipleDates)
        selector.optionCurrentDateRange.setStartDate(multipleDates.first ?? singleDate)
        selector.optionCurrentDateRange.setEndDate(multipleDates.last ?? singleDate)
        
        selector.optionStyles.showDateMonth(true)
        selector.optionStyles.showMonth(false)
        selector.optionStyles.showYear(true)
        selector.optionStyles.showTime(false)
        
        dateData = 1
        
        present(selector, animated: true, completion: nil)
        
        }
        
    
    }
    
    var idApproval = String()
    @IBAction func btnLocationApprovalClicked(_ sender: UIButton) {
//        if(self.approverDB.count == 0){
//            getApproverList()
//        }
        DispatchQueue.main.async(execute: {() -> Void in
            let popup = PopUpTableViewController()
            
            popup.itemsArray = self.ApproverListArray
            
            popup.sourceView = self.cellData.lblLocationApp
            popup.isScroll = true
            popup.backgroundColor = UIColor.white
            if popup.itemsArray.count > 5{
                popup.popUpHeight = 200
            }
            else{
                popup.popUpHeight = CGFloat(popup.itemsArray.count)*45
            }
            popup.popUpWidth = UIScreen.main.bounds.size.width-60
            popup.backgroundImage = nil
            popup.itemTitleColor = UIColor.white
            popup.itemSelectionColor = UIColor.lightGray
            popup.arrowDirections = .any
            popup.arrowColor = UIColor.white
            popup.popCellBlock = {(_ popupVC: PopUpTableViewController, _ popupCell: UITableViewCell, _ row: Int, _ section: Int) -> Void in
                
                
                
                
                
                self.cellData.lblLocationApp.text = self.ApproverListArray[row]
                self.cellData.lblLocationApp.textColor = UIColor.black
               self.idApproval =  self.ApproverIdArray[row]
                popupVC.dismiss(animated: false, completion: nil)
                
            }
            self.present(popup, animated: true, completion: {() -> Void in
            })
            
            
        })
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
  

   
    var cellData : NewRoomTableViewCell!

    func WWCalendarTimeSelectorDone(_ selector: WWCalendarTimeSelector, date: Date) {
        print("Selected \n\(date)\n---")
        singleDate = date
      
        
     
        if(selectdateVal == 1){
            cellData.lblCheckIn.text = date.stringFromFormat("d' 'MMM' 'yyyy' at 'h':'mma")
             startDate  = getMilliseconds(date1: date)
            millisecStart = String(startDate)
            arrivalTime = date.stringFromFormat("h':'mma")
            DataDateStart = date
        }
        if(selectdateVal == 2){
            cellData.lblCheckOut.text = date.stringFromFormat("d' 'MMM' 'yyyy' at 'h':'mma")
            endDate  = getMilliseconds(date1: date)
            millisecEnd = String(endDate)
            DataDateEnd = date
        }
        print(String(startDate) + "-----" + String(endDate))
      
        switch DataDateStart.compare(DataDateEnd) {
       
        case .orderedAscending:
           print("less")
        case .orderedSame:
            print("same")
        case .orderedDescending:
            millisecEnd = millisecStart
            cellData.lblCheckOut.text = cellData.lblCheckIn.text
        }
        
       
    }
    
    func WWCalendarTimeSelectorDone(_ selector: WWCalendarTimeSelector, dates: [Date]) {
        print("Selected Multiple Dates \n\(dates)\n---")
        if let date = dates.first {
            singleDate = date
           // dateLabel.text = date.stringFromFormat("d' 'MMMM' 'yyyy', 'h':'mma")
        }
        else {
            //dateLabel.text = "No Date Selected"
        }
        multipleDates = dates
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
extension NewRoomViewController : UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableViewAutomaticDimension
        
    }
}
extension NewRoomViewController : UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! NewRoomTableViewCell
        
        cellData = cell
        
        if(cell.lblGuest.text == ""){
            cell.lblGuest.text = "Select"
            cell.lblGuest.textColor = UIColor.lightGray
        }
        if(cell.lblLocationApp.text == ""){
            cell.lblLocationApp.text = "Select"
             cell.lblLocationApp.textColor = UIColor.lightGray
        }
        if(requestFor == String()){
            cell.btnSelf.isSelected = true
            requestFor = "Self"
            requestForData()
        }
        cell.separatorInset = UIEdgeInsetsMake(0, 10000, 0, 0);
        return cell
        
        
        
    }
}
class RequestFoodNewData :  NSObject {
    
   
    var Aadhar = String()
    var ArrivalTime = String()
    var BillToCompany = Bool()
    var BreakFast = Bool()
    var CheckInDate = String()
    var CheckOutDate = String()
    var Contact = String()
    var EmpID = String()
    var Dinner = Bool()
    var Lunch = Bool()
    var FromDate = String()
    var HouseID = String()
    var ID = String()
    var Name = String()
    var Notes = String()
    var Remark = String()
    var ReqDate = String()
    var ReqType = String()
    var CompanyName = String()
    var SelectedApprover = String()
    var ToDate = String()
    var purpose = String()
    
}
