//
//  RoomAddPersonTableViewCell.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 04/03/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit

class RoomAddPersonTableViewCell: UITableViewCell,UITextFieldDelegate {

    @IBOutlet weak var textViewAddress: UITextView!
    @IBOutlet weak var btnAdd: UIButton!
    @IBOutlet weak var lblSiNo: UILabel!
    @IBOutlet weak var btnDelete: UIButton!
    @IBOutlet weak var lblContact: UILabel!
    @IBOutlet weak var lblName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
  
    }
    

}
