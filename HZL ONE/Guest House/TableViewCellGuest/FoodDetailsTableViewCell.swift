//
//  FoodDetailsTableViewCell.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 23/02/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit

class FoodDetailsTableViewCell: UITableViewCell {

    @IBOutlet weak var lblFoodType: UILabel!
    @IBOutlet weak var btnFoodType: UIButton!
    @IBOutlet weak var viewfoodType: UIView!
    
    
    @IBOutlet weak var viewToDate: UIView!
    @IBOutlet weak var lblToDate: UILabel!
    @IBOutlet weak var btnToDate: UIButton!
    @IBOutlet weak var viewFordate: UIView!
    @IBOutlet weak var lblforDate: UILabel!
    @IBOutlet weak var btnForDate: UIButton!
    @IBOutlet weak var viewMember: UIView!
    
    @IBOutlet weak var txtMember: UITextField!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        viewfoodType.layer.borderWidth = 1.0
        viewfoodType.layer.borderColor = UIColor.black.cgColor
        viewfoodType.layer.cornerRadius = 10.0
        
        viewToDate.layer.borderWidth = 1.0
        viewToDate.layer.borderColor = UIColor.black.cgColor
        viewToDate.layer.cornerRadius = 10.0
        
        viewFordate.layer.borderWidth = 1.0
        viewFordate.layer.borderColor = UIColor.black.cgColor
        viewFordate.layer.cornerRadius = 10.0
        
        viewMember.layer.borderWidth = 1.0
        viewMember.layer.borderColor = UIColor.black.cgColor
        viewMember.layer.cornerRadius = 10.0
        txtMember.keyboardType = UIKeyboardType.numberPad
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
