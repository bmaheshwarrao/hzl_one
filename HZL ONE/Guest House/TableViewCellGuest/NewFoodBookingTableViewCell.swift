//
//  NewFoodBookingTableViewCell.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 23/02/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit
import DLRadioButton
class NewFoodBookingTableViewCell: UITableViewCell {
    @IBOutlet weak var btnGuest: DLRadioButton!
    @IBOutlet weak var btnSelf: DLRadioButton!
    
    
    @IBOutlet weak var btnCompany: DLRadioButton!
    @IBOutlet weak var btnSelfBill: DLRadioButton!
    @IBOutlet weak var btnGuestView: UIButton!
    @IBOutlet weak var lblGuest: UILabel!
    @IBOutlet weak var viewGuest: UIView!
    
    
    @IBOutlet weak var lblNoData: UILabel!
    @IBOutlet weak var viewName: UIView!
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var txtContact: UITextField!
    @IBOutlet weak var viewContact: UIView!
    
    @IBOutlet weak var textViewPurpose: UITextView!
    @IBOutlet weak var viewLocationApp: UIView!
    
    @IBOutlet weak var btnLocationApp: UIButton!
    @IBOutlet weak var lblLocationApp: UILabel!
    
    @IBOutlet weak var viewHeightCon: NSLayoutConstraint!
    
    @IBOutlet weak var imageHeightCon: NSLayoutConstraint!
    @IBOutlet weak var locationHeightCon: NSLayoutConstraint!
    @IBOutlet weak var lblHeightCon: NSLayoutConstraint!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        textViewPurpose.layer.borderWidth = 1.0
        textViewPurpose.layer.borderColor = UIColor.black.cgColor
        textViewPurpose.layer.cornerRadius = 10.0
        viewGuest.layer.borderWidth = 1.0
        viewGuest.layer.borderColor = UIColor.black.cgColor
        viewGuest.layer.cornerRadius = 10.0
        
        viewName.layer.borderWidth = 1.0
        viewName.layer.borderColor = UIColor.black.cgColor
        viewName.layer.cornerRadius = 10.0
        
        viewContact.layer.borderWidth = 1.0
        viewContact.layer.borderColor = UIColor.black.cgColor
        viewContact.layer.cornerRadius = 10.0
        viewLocationApp.layer.borderWidth = 1.0
        viewLocationApp.layer.borderColor = UIColor.black.cgColor
        viewLocationApp.layer.cornerRadius = 10.0
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
