//
//  RoomDetailsHeaderTableViewCell.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 04/03/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit
protocol CollapsibleRoomDetailsHeaderTableViewCellDelegate {
    func toggleSection(_ header: RoomDetailsHeaderTableViewCell, section: Int)
}
class RoomDetailsHeaderTableViewCell: UITableViewCell {
var delegate: CollapsibleRoomDetailsHeaderTableViewCellDelegate?
    var section: Int = 0
    @IBOutlet weak var imgArrow: UIImageView!
    @IBOutlet weak var lblGuestType: UILabel!
    @IBOutlet weak var lblRoomType: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        imgArrow.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(RoomDetailsHeaderTableViewCell.tapHeader(_:))))
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
  
    @objc func tapHeader(_ gestureRecognizer: UITapGestureRecognizer) {
        guard let cell = gestureRecognizer.view as? RoomDetailsHeaderTableViewCell else {
            return
        }
        
        delegate?.toggleSection(self, section: cell.section)
    }
    
    func setCollapsed(_ collapsed: Bool) {
        
        //  Animate the arrow rotation (see Extensions.swf)
        
        imgArrow.rotate(collapsed ? 0.0 : .pi / 2)
    }
}
extension UIView {
    
    func rotate(_ toValue: CGFloat, duration: CFTimeInterval = 0.2) {
        let animation = CABasicAnimation(keyPath: "transform.rotation")
        
        animation.toValue = toValue
        animation.duration = duration
        animation.isRemovedOnCompletion = false
        animation.fillMode = kCAFillModeForwards
        
        self.layer.add(animation, forKey: nil)
    }
    
}
