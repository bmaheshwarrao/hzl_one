//
//  MembersListTableViewCell.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 19/04/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit

class MembersListTableViewCell: UITableViewCell {

    @IBOutlet weak var lblMobile: UILabel!
    @IBOutlet weak var textViewAddress: UITextView!
   
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var lblName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
