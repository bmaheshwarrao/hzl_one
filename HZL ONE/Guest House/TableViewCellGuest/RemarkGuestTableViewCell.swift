//
//  RemarkGuestTableViewCell.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 30/03/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit

class RemarkGuestTableViewCell: UITableViewCell {

    
    @IBOutlet weak var textViewAdminRemark: UITextView!
    @IBOutlet weak var textViewRemark: UITextView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
