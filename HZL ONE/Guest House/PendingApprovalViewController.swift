//
//  PendingApprovalViewController.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 19/03/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit
import ViewPager_Swift
class PendingApprovalViewController: CommonVSClass {
    
    @IBOutlet weak var viewStack: UIView!
    @IBOutlet weak var lblRoomRequest: UILabel!
    @IBOutlet weak var lblFoodRequest: UILabel!
    @IBOutlet weak var lblRoomAdminRequest: UILabel!
    @IBOutlet weak var lblFoodAdminRequest: UILabel!
    @IBOutlet weak var viewSplitter: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        viewStack.backgroundColor = UIColor(hexString: "2c3e50", alpha: 1.0)
        Reportdata()
        let myOptions = ViewPagerOptions(viewPagerWithFrame: CGRect(x: 0, y: 0, width: viewSplitter.frame.width, height: viewSplitter.frame.height))
        
        myOptions.tabType = ViewPagerTabType.basic
        
        
        
        
        
        
        myOptions.isTabHighlightAvailable = true
        // myOptions.is
        // If I want indicator bar to show below current page tab
        myOptions.isTabIndicatorAvailable = true
        myOptions.fitAllTabsInView = true;
        myOptions.tabViewTextFont = UIFont.systemFont(ofSize: 15.0, weight: .medium)
        // Oh! and let's change color of tab to red
        myOptions.tabIndicatorViewBackgroundColor = UIColor.white
        myOptions.tabViewBackgroundHighlightColor = UIColor(hexString: "2c3e50", alpha: 1.0)!
        myOptions.tabViewBackgroundDefaultColor = UIColor(hexString: "2c3e50", alpha: 1.0)!
        myOptions.tabViewTextDefaultColor = UIColor.white
        myOptions.tabViewTextHighlightColor = UIColor.white
        
        let viewPager = ViewPagerController()
        
        viewPager.options = myOptions
        viewPager.dataSource = self
        
        //Now let me add this to my viewcontroller
        self.addChildViewController(viewPager)
        
        
        
        viewSplitter.addSubview(viewPager.view)
        viewPager.didMove(toParentViewController: self)
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.tabBarController?.tabBar.isHidden = true
        self.title = "Pending Approval"
        let nav = self.navigationController?.navigationBar
        nav?.tintColor = UIColor.white
        nav?.barTintColor = UIColor(hexString: "2c3e50", alpha: 1.0)
        nav?.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    let reachability = Reachability()!
    @objc func Reportdata() {
        if(self.reachability.connection != .none) {
            let pno : String = (UserDefaults.standard.string(forKey: "EmployeeID")!)
            let Profile_AuthKey = UserDefaults.standard.string(forKey: "Profile_AuthKey")!
            let parameters = ["UserID": pno ,"AuthKey": Profile_AuthKey  ]
            
            self.startLoading()
            print(parameters)
            WebServices.sharedInstances.sendPostRequest(url: URLConstants.MyApprovalsGuestHouse,parameters: parameters, successHandler: { (dict) in
                
                //self.stopLoading()
                
                print("",dict)
                
                if let response = dict["response"] {
                    
                    let statusString : String = response["status"] as! String
                    let objectmsg = MessageCallServerModel()
                    
                    let msg = objectmsg.sealizeMessage(cell: response as! [String : AnyObject])
                    print(msg)
              
                    if(statusString == "ok")
                    {
                        self.stopLoading()
                        
                        
                        
                        if let data = dict["data"]
                        {
                            
                            self.refresh.endRefreshing()
                            let MyRoomRequest : Int = Int(data["RoomRequestApproveCount"] as! NSNumber)
                            let MyFoodRequest : Int = Int(data["FoodRequestAproverCount"] as! NSNumber)
                            let MyRoomAdminRequest : Int = Int(data["RoomRequestAdminCount"] as! NSNumber)
                            let MyFoodAdminRequest : Int = Int(data["FoodRequestAdminCount"] as! NSNumber)
                            
                            self.lblFoodRequest.text = String(MyFoodRequest)
                            self.lblRoomRequest.text = String(MyRoomRequest)
                            self.lblFoodAdminRequest.text = String(MyFoodAdminRequest)
                            self.lblRoomAdminRequest.text = String(MyRoomAdminRequest)
                            
                            
                            
                            
                            
                        }
                        
                        
                    }
                    else
                    {
                        self.showSingleButtonWithMessage(title: statusString, message: msg, buttonName: "OK")
                    }
                }
                
                
                
                
            }, failureHandler: { (error) in
                
              
                
                self.errorChecking(error: error[0])
                
                
                
            })
            
            
            
        }
        else {
            
            // self.showSingleButtonWithMessage(title: "No Network!", message: "No internet connection found. Check your internet connection or try again.", buttonName: "OK")
            self.refresh.endRefreshing()
            self.stopLoading()
            
        }
        
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
extension PendingApprovalViewController : ViewPagerControllerDataSource {
    func numberOfPages() -> Int {
        return 4
    }
    
    func viewControllerAtPosition(position: Int) -> UIViewController {
        let storyBoard : UIStoryboard = UIStoryboard(name: "GuestHouse",bundle : nil)
        if(position == 0) {
            
             let empVC = storyBoard.instantiateViewController(withIdentifier: "FoodRequestRoportedViewController") as! FoodRequestRoportedViewController
            empVC.typeData = "3"
            empVC.valueData = "2"
               empVC.showUrlData = URLConstants.MyApprovalListGuestHouse
            return  empVC as CommonVSClass
        }else if(position == 1) {
            
          let empVC = storyBoard.instantiateViewController(withIdentifier: "RoomRequestReportedViewController") as! RoomRequestReportedViewController
            empVC.typeData = "1"
            empVC.valueData = "2"
               empVC.showUrlData = URLConstants.MyApprovalListGuestHouse
            return  empVC as CommonVSClass
        }else if(position == 2) {
            
            let empVC = storyBoard.instantiateViewController(withIdentifier: "FoodRequestRoportedViewController") as! FoodRequestRoportedViewController
            empVC.typeData = "4"
            empVC.valueData = "2"
               empVC.showUrlData = URLConstants.MyApprovalListGuestHouse
            return  empVC as CommonVSClass
        }
        
        else {
             let empVC = storyBoard.instantiateViewController(withIdentifier: "RoomRequestReportedViewController") as! RoomRequestReportedViewController
            empVC.typeData = "2"
            empVC.valueData = "2"
            empVC.showUrlData = URLConstants.MyApprovalListGuestHouse
            return  empVC as CommonVSClass
        }
    }
    
    func tabsForPages() -> [ViewPagerTab] {
        return [ViewPagerTab(title: "FOOD" , image: #imageLiteral(resourceName: "BackBlack")) , ViewPagerTab(title: "ROOM" , image: #imageLiteral(resourceName: "BackBlack")), ViewPagerTab(title: "FOOD ADMIN" , image: #imageLiteral(resourceName: "BackBlack")), ViewPagerTab(title: "ROOM ADMIN" , image: #imageLiteral(resourceName: "BackBlack"))]
    }
    
    
}
extension PendingApprovalViewController : ViewPagerControllerDelegate {
    
}



