//
//  FoodRequestRoportedViewController.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 23/02/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit

class FoodRequestRoportedViewController: CommonVSClass,UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    var refreshControl = UIRefreshControl()
    var typeData = String()
    var FoodDB:[RoomRequestReportedDataModel] = []
    var FoodAPI = FoodRequestReportedDataAPI()
    var showUrlData = String()
    var FoodLoadMoreDB : [RoomRequestReportedDataModel] = []
    
    
    
    
    var FoodLoadMoreAPI = FoodRequestReportedDataAPILoadMore()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getFoodData()
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.estimatedRowHeight = 65
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.tableFooterView = UIView()
        
        refreshControl.addTarget(self, action: #selector(getFoodData), for: .valueChanged)
        
        self.tableView.addSubview(refreshControl)
        let swipe = UISwipeGestureRecognizer(target: self, action: #selector(self.swipeRecognizer))
        swipe.direction = ([.down])
        self.view.addGestureRecognizer(swipe)
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        if(MoveStruct.isMove == true){
            MoveStruct.isMove = false
            self.view.makeToast(MoveStruct.message)
        }
        self.tabBarController?.tabBar.isHidden = true
        self.title = "Food Request"
        let nav = self.navigationController?.navigationBar
        nav?.tintColor = UIColor.white
       nav?.barTintColor = UIColor(hexString: "2c3e50", alpha: 1.0)
        nav?.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        getFoodData()
    }
    var reachability = Reachability()!
    @objc func swipeRecognizer(_ sender: UISwipeGestureRecognizer) {
        if (sender.direction == .down) {
            
            if(self.reachability.connection != .none)
            {
                
                self.getFoodData()
            }
            else
            {
                self.startLoading(view: self.view)
                DispatchQueue.main.asyncAfter(deadline: .now() + 4.5) {
                    self.noDataLabel(text: "No internet connection found. Check your internet connection and try again.")
                    self.stopLoading(view: self.view)
                }
            }
            
        }
    }
    @objc func tapViewTapClicked(_ sender: UITapGestureRecognizer) {
        let buttonPosition:CGPoint = sender.view!.convert(.zero, to:self.tableView)
        let indexPath = self.tableView.indexPathForRow(at: buttonPosition)
        
        let storyBoard = UIStoryboard(name: "GuestHouse", bundle: nil)
        let ZIVC = storyBoard.instantiateViewController(withIdentifier: "FoodRequestDetailsViewController") as! FoodRequestDetailsViewController
        ZIVC.FoodRequestDB = self.FoodDB[(indexPath?.row)!]
        ZIVC.statusData = valueData
        ZIVC.typeDataVal = typeData
        self.navigationController?.pushViewController(ZIVC, animated: true)
        //
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @objc func getFoodData(){
        var param = [String:String]()
        let pno : String = (UserDefaults.standard.string(forKey: "EmployeeID")!)
        let Profile_AuthKey = UserDefaults.standard.string(forKey: "Profile_AuthKey")!
        param = ["UserID": pno ,"AuthKey": Profile_AuthKey , "Type" : typeData  ]
        print(param)
        FoodAPI.serviceCalling(obj: self , urlStr: showUrlData , param: param ) { (dict) in
            
            self.FoodDB = dict as! [RoomRequestReportedDataModel]
            
            self.tableView.reloadData()
            
        }
    }
    var countData = 1
    @objc func getFoodDataLoadMore(ID : String){
        var param = [String:String]()
        let pno : String = (UserDefaults.standard.string(forKey: "EmployeeID")!)
        let Profile_AuthKey = UserDefaults.standard.string(forKey: "Profile_AuthKey")!
        param = ["UserID": pno ,"AuthKey": Profile_AuthKey , "Type" : typeData ,"pn":String(countData) ]
        
        FoodLoadMoreAPI.serviceCalling(obj: self, urlStr: showUrlData ,  param: param ) { (dict) in
            
            self.FoodLoadMoreDB =  [RoomRequestReportedDataModel]()
            self.FoodLoadMoreDB = dict as! [RoomRequestReportedDataModel]
            
            switch dict.count {
            case 0:
                //self.countData = self.countData + 1
                break;
            default:
                self.FoodDB.append(contentsOf: self.FoodLoadMoreDB)
                self.tableView.reloadData()
                self.countData = self.countData + 1
                break;
            }
            
        }
    }
var valueData = String()
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    var data: String?
    var lastObject: String?
    var indPath = IndexPath()
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        
        
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if(section == 0) {
            return 10.0
        } else {
            return 20.0
        }
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.FoodDB.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! FoodrequestreportedTableViewCell
        
        let TapGesture = UITapGestureRecognizer(target: self, action: #selector(self.tapViewTapClicked(_:)))
        cell.addGestureRecognizer(TapGesture)
        cell.isUserInteractionEnabled = true
        TapGesture.numberOfTapsRequired = 1
        
       
        cell.containerView.cornerRadius = 10;
        
        cell.textViewGuestName.text = self.FoodDB[indexPath.row].HouseName
        
        cell.lblName.text = self.FoodDB[indexPath.row].Name
        cell.lblContact.text = self.FoodDB[indexPath.row].Contact
        cell.textViewDesc.text = self.FoodDB[indexPath.row].Purpose
        
        let milisecondDate = self.FoodDB[indexPath.row].ReqDate
        let dateVarDate = Date.init(timeIntervalSince1970: TimeInterval(milisecondDate)!/1000)
        let dateFormatterday = DateFormatter()
        dateFormatterday.dateFormat = "dd"
        let valDate =  dateFormatterday.string(from: dateVarDate)
        cell.lblDate.text = valDate
        let dateFormattermon = DateFormatter()
        dateFormattermon.dateFormat = "MMM"
        let valDateMon =  dateFormattermon.string(from: dateVarDate)
        cell.lblMonth.text = valDateMon
        
        let dateFormatterWeekDay = DateFormatter()
        dateFormatterWeekDay.dateFormat = "EEEE"
        let valDateWeekDay =  dateFormatterWeekDay.string(from: dateVarDate)
        cell.lblDay.text = valDateWeekDay
        
        
        
        
        
       
        
        
        
        self.data = String(self.FoodDB[indexPath.row].ID)
        self.lastObject = String(self.FoodDB[indexPath.row].ID)
        
        if ( self.data ==  self.lastObject && indexPath.section == self.FoodDB.count - 1)
        {
            
            self.getFoodDataLoadMore( ID: String(Int(self.FoodDB[indexPath.row].ID)))
            
        }
        cell.separatorInset = UIEdgeInsetsMake(0, 10000, 0, 0);
        cell.selectionStyle = .none
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableViewAutomaticDimension
        
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}
