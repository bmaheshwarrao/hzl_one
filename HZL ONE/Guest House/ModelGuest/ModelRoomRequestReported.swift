//
//  ModelRoomRequestReported.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 23/02/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import Foundation
import Reachability


class RoomRequestReportedDataModel: NSObject {
    
    
    var ID = Int()
    var RoomID = String()
    var RoomNo = String()
    var LocationID = String()
    var HouseName = String()
    var RoomCount = String()
    var IsOut = String()
    var Persons = String()
    var EmpID = String()
    var HouseID = String()
    var ReqDate = String()
    var ReqType = String()
    var Name = String()
    var Contact = String()
 
    var Aadhar = String()
    var AdharVerified = Bool()
    var BillToCompany = Bool()
    var ApproveStatus = String()
    var ApproveDate = String()
    var ApprovedBy = String()
    var AdminStatus = String()
    var AdminDate = String()
     var Purpose = String()
    var AdminRemark = String()
    var ApproveRemark = String()
    var AdminBy = String()
    var Amount = String()
    var Stayed = Bool()
    var CheckInDate = String()
    var CheckOutDate = String()
    var Notes = String()
    var FromDate = String()
    var ToDate = String()
    
    var ArrivalTime = String()
    var Lunch = Bool()
    var Dinner = Bool()
    var BreakFast = Bool()
    var Remark = String()
    
    var BillPath = String()
    var SelectedApprover = String()
    var Cancelled = Bool()
    var BillPath_NoAmt = String()
    var FType = String()
    var ForDt = String()
    var listFood = [listFoodModel]()
   var listmember = [listMemberModel]()
    
    
    

    
    
    func setDataInModel(str:[String:AnyObject])
    {
       
        if str["RoomID"] is NSNull || str["RoomID"] == nil{
            self.RoomID = ""
        }else{
            
            let tit = str["RoomID"]
            
            self.RoomID = (tit?.description)!
            
            
        }
        if str["RoomNo"] is NSNull || str["RoomNo"] == nil{
            self.RoomNo =  ""
        }else{
            
            let desc = str["RoomNo"]
            
            self.RoomNo = (desc?.description)!
        }
        if str["Persons"] is NSNull || str["Persons"] == nil{
            self.Persons =  ""
        }else{
            let desc = str["Persons"]
            self.Persons = (desc?.description)!
        }
        
        if str["ForDt"] is NSNull || str["ForDt"] == nil{
            self.ForDt =  ""
        }else{
            let desc = str["ForDt"]
            self.ForDt = (desc?.description)!
        }
        if str["ID"] is NSNull || str["ID"] == nil{
            self.ID = 0
        }else{
            self.ID = (str["ID"] as? Int)!
        }
        if str["LocationID"] is NSNull || str["LocationID"] == nil{
            self.LocationID = "0"
        }else{
            let catId = str["LocationID"]
            
            self.LocationID = (catId?.description)!
           
        }
        if str["HouseName"] is NSNull || str["HouseName"] == nil{
            self.HouseName = ""
        }else{
            
            let rem = str["HouseName"]
            
            self.HouseName = (rem?.description)!
            
        }
        
        if str["RoomCount"] is NSNull || str["RoomCount"] == nil{
            self.RoomCount = ""
        }else{
            let cat = str["RoomCount"]
            
            self.RoomCount = (cat?.description)!
            
            
            
        }
        if str["IsOut"] is NSNull || str["IsOut"] == nil{
            self.IsOut = ""
        }else{
            let createDate = str["IsOut"]
            
            self.IsOut = (createDate?.description)!
      
        }
        
       
        if str["EmpID"] is NSNull || str["EmpID"] == nil{
            self.EmpID = ""
        }else{
            
            let tit = str["EmpID"]
            
            self.EmpID = (tit?.description)!
            
            
        }
        if str["HouseID"] is NSNull || str["HouseID"] == nil{
            self.HouseID =  ""
        }else{
            
            let desc = str["HouseID"]
            
            self.HouseID = (desc?.description)!
            
            
            //  self.Description = (str["Description"] as? String)!
        }
        
        if str["ReqDate"] is NSNull || str["ReqDate"] == nil{
            self.ReqDate = "0"
        }else{
            let catId = str["ReqDate"]
            
            self.ReqDate = (catId?.description)!
            
        }
        if str["ReqType"] is NSNull || str["ReqType"] == nil{
            self.ReqType = ""
        }else{
            
            let rem = str["ReqType"]
            
            self.ReqType = (rem?.description)!
            
        }
        
        if str["Name"] is NSNull || str["Name"] == nil{
            self.Name = ""
        }else{
            let cat = str["Name"]
            
            self.Name = (cat?.description)!
            
            
            
        }
        if str["Contact"] is NSNull || str["Contact"] == nil{
            self.Contact = ""
        }else{
            let createDate = str["Contact"]
            
            self.Contact = (createDate?.description)!
            
        }

        if str["Aadhar"] is NSNull || str["Aadhar"] == nil{
            self.Aadhar = ""
        }else{
            let cat = str["Aadhar"]
            
            self.Aadhar = (cat?.description)!
            
            
            
        }
        if str["AdharVerified"] is NSNull || str["AdharVerified"] == nil{
            self.AdharVerified = Bool()
        }else{
            self.AdharVerified = (str["AdharVerified"] as? Bool)!
            
        }
        
        
        if str["BillToCompany"] is NSNull || str["BillToCompany"] == nil{
            self.BillToCompany = Bool()
        }else{
            
           self.BillToCompany = (str["BillToCompany"] as? Bool)!
            
            
        }
        if str["ApproveStatus"] is NSNull || str["ApproveStatus"] == nil{
            self.ApproveStatus =  ""
        }else{
            
            let desc = str["ApproveStatus"]
            
            self.ApproveStatus = (desc?.description)!
            
            
            //  self.Description = (str["Description"] as? String)!
        }
        
        if str["ApproveDate"] is NSNull || str["ApproveDate"] == nil{
            self.ApproveDate = ""
        }else{
            let catId = str["ApproveDate"]
            
            self.ApproveDate = (catId?.description)!
            
        }
        if str["ApprovedBy"] is NSNull || str["ApprovedBy"] == nil{
            self.ApprovedBy = ""
        }else{
            
            let rem = str["ApprovedBy"]
            
            self.ApprovedBy = (rem?.description)!
            
        }
        
        if str["AdminStatus"] is NSNull || str["AdminStatus"] == nil{
            self.AdminStatus = ""
        }else{
            let cat = str["AdminStatus"]
            
            self.AdminStatus = (cat?.description)!
            
            
            
        }
        if str["AdminDate"] is NSNull || str["AdminDate"] == nil{
            self.AdminDate = ""
        }else{
            let createDate = str["AdminDate"]
            
            self.AdminDate = (createDate?.description)!
            
        }
       
        
        if str["AdminBy"] is NSNull || str["AdminBy"] == nil{
            self.AdminBy = ""
        }else{
            let cat = str["AdminBy"]
            
            self.AdminBy = (cat?.description)!
            
            
            
        }
        if str["Amount"] is NSNull || str["Amount"] == nil{
            self.Amount = ""
        }else{
            let cat = str["Amount"]
            
            self.Amount = (cat?.description)!
            
        }
        
        
        if str["Stayed"] is NSNull || str["Stayed"] == nil{
            self.Stayed = Bool()
        }else{
            
            self.Stayed = (str["Stayed"] as? Bool)!
            
            
        }
        if str["CheckInDate"] is NSNull || str["CheckInDate"] == nil{
            self.CheckInDate =  ""
        }else{
            
            let desc = str["CheckInDate"]
            
            self.CheckInDate = (desc?.description)!
            
            
            //  self.Description = (str["Description"] as? String)!
        }
        
        if str["CheckOutDate"] is NSNull || str["CheckOutDate"] == nil{
            self.CheckOutDate = ""
        }else{
            let catId = str["CheckOutDate"]
            
            self.CheckOutDate = (catId?.description)!
            
        }
        if str["Notes"] is NSNull || str["Notes"] == nil{
            self.Notes = ""
        }else{
            
            let rem = str["Notes"]
            
            self.Notes = (rem?.description)!
            
        }
        
        if str["FromDate"] is NSNull || str["FromDate"] == nil{
            self.FromDate = ""
        }else{
            let cat = str["FromDate"]
            
            self.FromDate = (cat?.description)!
            
            
            
        }
        if str["ToDate"] is NSNull || str["ToDate"] == nil{
            self.ToDate = ""
        }else{
            let createDate = str["ToDate"]
            
            self.ToDate = (createDate?.description)!
            
        }
     
       
      
        if str["ArrivalTime"] is NSNull || str["ArrivalTime"] == nil{
            self.ArrivalTime = ""
        }else{
            let createDate = str["ArrivalTime"]
            
            self.ArrivalTime = (createDate?.description)!
            
        }
        if str["Lunch"] is NSNull || str["Lunch"] == nil{
            self.Lunch = Bool()
        }else{
            
            self.Lunch = (str["Lunch"] as? Bool)!
            
            
        }
        
        if str["Dinner"] is NSNull || str["Dinner"] == nil{
            self.Dinner = Bool()
        }else{
            
            self.Dinner = (str["Dinner"] as? Bool)!
            
            
        }
        
        if str["BreakFast"] is NSNull || str["BreakFast"] == nil{
            self.BreakFast = Bool()
        }else{
            
            self.BreakFast = (str["BreakFast"] as? Bool)!
            
            
        }
        if str["Remark"] is NSNull || str["Remark"] == nil{
            self.Remark = ""
        }else{
            let createDate = str["Remark"]
            
            self.Remark = (createDate?.description)!
            
        }
        if str["BillPath"] is NSNull || str["BillPath"] == nil{
            self.BillPath = ""
        }else{
            let createDate = str["BillPath"]
            
            self.BillPath = (createDate?.description)!
            
        }
        if str["SelectedApprover"] is NSNull || str["SelectedApprover"] == nil{
            self.SelectedApprover = ""
        }else{
            let createDate = str["SelectedApprover"]
            
            self.SelectedApprover = (createDate?.description)!
            
        }
        
        if str["Cancelled"] is NSNull || str["Cancelled"] == nil{
            self.Cancelled = Bool()
        }else{
            
            self.Cancelled = (str["Cancelled"] as? Bool)!
            
            
        }
        if str["FType"] is NSNull || str["FType"] == nil {
            self.FType = ""
        }else{
            let app = str["FType"]
            self.FType = (app?.description)!
        }
        if str["BillPath_NoAmt"] is NSNull || str["BillPath_NoAmt"] == nil{
            self.BillPath_NoAmt = ""
        }else{
            let createDate = str["BillPath_NoAmt"]
            
            self.BillPath_NoAmt = (createDate?.description)!
            
        }
        if str["Purpose"] is NSNull || str["Purpose"] == nil{
            self.Purpose = ""
        }else{
            let createDate = str["Purpose"]
            
            self.Purpose = (createDate?.description)!
            
        }
      
        if str["AdminRemark"] is NSNull || str["AdminRemark"] == nil{
            self.AdminRemark = "---"
        }else{
            let createDate = str["AdminRemark"]
            
            self.AdminRemark = (createDate?.description)!
            
        }
        if str["ApproveRemark"] is NSNull || str["ApproveRemark"] == nil{
            self.ApproveRemark = "---"
        }else{
            let createDate = str["ApproveRemark"]
            
            self.ApproveRemark = (createDate?.description)!
            
        }
        if let data1 = str["listFood"] as? [AnyObject]
        {
            
            var dataArray1: [listFoodModel] = []
            if(data1.count  >  0){
            for i in 0..<data1.count
            {
                
                if let str = data1[i] as? [String:AnyObject]
                {
                    let object1 = listFoodModel()
                    object1.setDataInModel(cell: str)
                    dataArray1.append(object1)
                }
                
            }
            listFood = dataArray1
            
            }
            
        }
        
        if let data1 = str["listmember"] as? [AnyObject]
        {
            
            var dataArray1: [listMemberModel] = []
            if(data1.count  >  0){
                for i in 0..<data1.count
                {
                    
                    if let str = data1[i] as? [String:AnyObject]
                    {
                        let object1 = listMemberModel()
                        object1.setDataInModel(cell: str)
                        dataArray1.append(object1)
                    }
                    
                }
                listmember = dataArray1
                
            }
            
        }
        
        
    }
    
}

class listFoodModel: NSObject {
    
    
    
    var ID: String?
    var ForDate: String?
    
    var ReqDate: String?
    var HouseID: String?
    var PersonName: String?
    var FReqID: String?
    
    
    var FType: String?
    var Persons: String?
    var Detail: String?
    var Served: String?
    var ForDt: String?
    var ToDt: String?
    

    func setDataInModel(cell:[String:AnyObject])
    {
        
        if cell["ID"] is NSNull || cell["ID"] == nil {
            self.ID = ""
        }else{
            let app = cell["ID"]
            self.ID = (app?.description)!
        }
        
        if cell["ForDate"] is NSNull || cell["ForDate"] == nil {
            self.ForDate = ""
        }else{
            let app = cell["ForDate"]
            self.ForDate = (app?.description)!
        }
       
        
        
        if cell["ReqDate"] is NSNull || cell["ReqDate"] == nil {
            self.ReqDate = ""
        }else{
            let app = cell["ReqDate"]
            self.ReqDate = (app?.description)!
        }
        
        if cell["HouseID"] is NSNull || cell["HouseID"] == nil {
            self.HouseID = ""
        }else{
            let app = cell["HouseID"]
            self.HouseID = (app?.description)!
        }
        
        if cell["PersonName"] is NSNull || cell["PersonName"] == nil {
            self.PersonName = ""
        }else{
            let app = cell["PersonName"]
            self.PersonName = (app?.description)!
        }
        
        if cell["FReqID"] is NSNull || cell["FReqID"] == nil {
            self.FReqID = ""
        }else{
            let app = cell["FReqID"]
            self.FReqID = (app?.description)!
        }
        
        
        
        
        if cell["FType"] is NSNull || cell["FType"] == nil {
            self.FType = ""
        }else{
            let app = cell["FType"]
            self.FType = (app?.description)!
        }
        if cell["Persons"] is NSNull || cell["Persons"] == nil {
            self.Persons = ""
        }else{
            let app = cell["Persons"]
            self.Persons = (app?.description)!
        }
        if cell["Detail"] is NSNull || cell["Detail"] == nil {
            self.Detail = ""
        }else{
            let app = cell["Detail"]
            self.Detail = (app?.description)!
        }
        if cell["Served"] is NSNull || cell["Served"] == nil {
            self.Served = ""
        }else{
            let app = cell["Served"]
            self.Served = (app?.description)!
        }
        if cell["ForDt"] is NSNull || cell["ForDt"] == nil {
            self.ForDt = ""
        }else{
            let app = cell["ForDt"]
            self.ForDt = (app?.description)!
        }
        if cell["ToDt"] is NSNull || cell["ToDt"] == nil {
            self.ToDt = ""
        }else{
            let app = cell["ToDt"]
            self.ToDt = (app?.description)!
        }
        
        
    }
}

class listMemberModel: NSObject {
    
    
 var ID: String?
    var AadharFile: String?
    var canvasPicimg: String?
    
    var canvasAadharimg: String?
    var AdharDoc: String?
    
    var ReqDataID: String?
    
    
    var Name: String?
    var DOB: String?
    var Aadhar: String?
    var PicCapture: String?
    var IDCapture: String?
    var IDUpload: String?
    var RoomID: String?
    var Addresss: String?
    
    func setDataInModel(cell:[String:AnyObject])
    {
        
        if cell["ID"] is NSNull || cell["ID"] == nil {
            self.ID = ""
        }else{
            let app = cell["ID"]
            self.ID = (app?.description)!
        }
     
        if cell["AadharFile"] is NSNull || cell["AadharFile"] == nil {
            self.AadharFile = ""
        }else{
            let app = cell["AadharFile"]
            self.AadharFile = (app?.description)!
        }
        
        
        
        if cell["canvasPicimg"] is NSNull || cell["canvasPicimg"] == nil {
            self.canvasPicimg = ""
        }else{
            let app = cell["canvasPicimg"]
            self.canvasPicimg = (app?.description)!
        }
        
        if cell["canvasAadharimg"] is NSNull || cell["canvasAadharimg"] == nil {
            self.canvasAadharimg = ""
        }else{
            let app = cell["canvasAadharimg"]
            self.canvasAadharimg = (app?.description)!
        }
        
        if cell["AdharDoc"] is NSNull || cell["AdharDoc"] == nil {
            self.AdharDoc = ""
        }else{
            let app = cell["AdharDoc"]
            self.AdharDoc = (app?.description)!
        }
      
        if cell["Name"] is NSNull || cell["Name"] == nil {
            self.Name = ""
        }else{
            let app = cell["Name"]
            self.Name = (app?.description)!
        }
        
        
        
        
        if cell["DOB"] is NSNull || cell["DOB"] == nil {
            self.DOB = ""
        }else{
            let app = cell["DOB"]
            self.DOB = (app?.description)!
        }
        if cell["Aadhar"] is NSNull || cell["Aadhar"] == nil {
            self.Aadhar = ""
        }else{
            let app = cell["Aadhar"]
            self.Aadhar = (app?.description)!
        }
        if cell["PicCapture"] is NSNull || cell["PicCapture"] == nil {
            self.PicCapture = ""
        }else{
            let app = cell["PicCapture"]
            self.PicCapture = (app?.description)!
        }
      
        if cell["IDCapture"] is NSNull || cell["IDCapture"] == nil {
            self.IDCapture = ""
        }else{
            let app = cell["IDCapture"]
            self.IDCapture = (app?.description)!
        }
        if cell["IDUpload"] is NSNull || cell["IDUpload"] == nil {
            self.IDUpload = ""
        }else{
            let app = cell["IDUpload"]
            self.IDUpload = (app?.description)!
        }
        if cell["RoomID"] is NSNull || cell["RoomID"] == nil {
            self.RoomID = ""
        }else{
            let app = cell["RoomID"]
            self.RoomID = (app?.description)!
        }
        if cell["Addresss"] is NSNull || cell["Addresss"] == nil {
            self.Addresss = ""
        }else{
            let app = cell["Addresss"]
            self.Addresss = (app?.description)!
        }
        
    }
}







class RoomRequestReportedDataAPI
{
    
    var reachablty = Reachability()!
    
    func serviceCalling(obj:RoomRequestReportedViewController, urlStr : String ,param : [String: String] , success:@escaping (AnyObject)-> Void)
    {
        obj.startLoading()
        
        if(reachablty.connection != .none)
        {
            
            
            WebServices.sharedInstances.sendPostRequest(url: urlStr, parameters: param , successHandler: { (dict) in
                
                if let response = dict["response"]{
                    
                    let statusString : String = response["status"] as! String
                    print(response)
                    let objectmsg = MessageCallServerModel()
                    
                    let msg = objectmsg.sealizeMessage(cell: response as! [String : AnyObject])
                    print(msg)
                    
                    if(statusString == "ok")
                    {
                        obj.tableView.isHidden = false
                        obj.noDataLabel(text: "" )
                        obj.label.isHidden = true
                        if let data = dict["data"] as? [AnyObject]
                        {
                            
                            var dataArray: [RoomRequestReportedDataModel] = []
                            if(data.count > 0){
                            for i in 0..<data.count
                            {
                                
                                if let cell = data[i] as? [String:AnyObject]
                                {
                                    let object = RoomRequestReportedDataModel()
                                    object.setDataInModel(str: cell)
                                    dataArray.append(object)
                                }
                                
                            }
                            
                            success(dataArray as AnyObject)
                            }else{
                                obj.label.isHidden = false
                                obj.tableView.isHidden = true
                                obj.noDataLabel(text: msg )
                                obj.refreshControl.endRefreshing()
                                obj.stopLoading()
                            }
                            
                            
                        }
                        
                        obj.refreshControl.endRefreshing()
                        obj.stopLoading()
                    }
                    else
                    {
                        obj.label.isHidden = false
                        obj.tableView.isHidden = true
                        obj.noDataLabel(text: msg )
                        obj.refreshControl.endRefreshing()
                        obj.stopLoading()
                        
                    }
                    
                    
                }
                
                
                
                
            })
            { (error) in
                
                let errorStr : String = error.description
                
                print("DATA:error",errorStr)
                
                //                obj.errorChecking(error: error)
                obj.stopLoading()
            }
            
            
        }
        else{
            obj.label.isHidden = false
            obj.tableView.isHidden = true
            obj.noDataLabel(text: "Internet is not available, please check your internet connection try again." )
            obj.stopLoading()
        }
        
        
    }
    
    
}

class RoomRequestReportedDataAPILoadMore
{
    
    var reachablty = Reachability()!
    
    func serviceCalling(obj:RoomRequestReportedViewController,  urlStr : String ,param : [String: String] , success:@escaping (AnyObject)-> Void)
    {
        obj.startLoading()
        
        if(reachablty.connection != .none)
        {
            
            
            WebServices.sharedInstances.sendPostRequest(url: urlStr, parameters: param , successHandler: { (dict) in
                
                if let response = dict["response"]{
                    
                    let statusString : String = response["status"] as! String
                    
                    if(statusString == "ok")
                    {
                        //                        obj.tableView.isHidden = false
                        //                        obj.noDataLabel(text: "" )
                        
                        if let data = dict["data"] as? [AnyObject]
                        {
                            
                            var dataArray: [RoomRequestReportedDataModel] = []
                            
                            for i in 0..<data.count
                            {
                                
                                if let cell = data[i] as? [String:AnyObject]
                                {
                                    let object = RoomRequestReportedDataModel()
                                    object.setDataInModel(str: cell)
                                    dataArray.append(object)
                                }
                                
                            }
                            
                            success(dataArray as AnyObject)
                            
                            
                        }
                        
                        obj.refreshControl.endRefreshing()
                        obj.stopLoading()
                    }
                    else
                    {
                        
                        //                        obj.tableView.isHidden = true
                        //                        obj.noDataLabel(text: "No Data Found!" )
                        obj.refreshControl.endRefreshing()
                        obj.stopLoading()
                        
                    }
                    
                    
                }
                
                
                
                
            })
            { (error) in
                
                let errorStr : String = error.description
                
                print("DATA:error",errorStr)
                
                //                obj.errorChecking(error: error)
                obj.stopLoading()
            }
            
            
        }
        else{
            //            obj.tableView.isHidden = true
            //            obj.noDataLabel(text: "Internet is not available, please check your internet connection try again." )
            obj.stopLoading()
        }
        
        
    }
    
    
}













class FoodRequestReportedDataAPI
{
    
    var reachablty = Reachability()!
    
    func serviceCalling(obj:FoodRequestRoportedViewController,urlStr : String, param : [String: String] , success:@escaping (AnyObject)-> Void)
    {
        obj.startLoading()
        
        if(reachablty.connection != .none)
        {
            
            
            WebServices.sharedInstances.sendPostRequest(url: urlStr, parameters: param , successHandler: { (dict) in
                
                if let response = dict["response"]{
                    
                    let statusString : String = response["status"] as! String
                    print(response)
                    let objectmsg = MessageCallServerModel()
                    
                    let msg = objectmsg.sealizeMessage(cell: response as! [String : AnyObject])
                    print(msg)
                    if(statusString == "ok")
                    {
                        obj.tableView.isHidden = false
                        obj.noDataLabel(text: "" )
                        obj.label.isHidden = true
                        if let data = dict["data"] as? [AnyObject]
                        {
                            
                            var dataArray: [RoomRequestReportedDataModel] = []
                            
                            if(data.count > 0){
                                for i in 0..<data.count
                                {
                                    
                                    if let cell = data[i] as? [String:AnyObject]
                                    {
                                        let object = RoomRequestReportedDataModel()
                                        object.setDataInModel(str: cell)
                                        dataArray.append(object)
                                    }
                                    
                                }
                                
                                success(dataArray as AnyObject)
                            }else{
                                obj.label.isHidden = false
                                obj.tableView.isHidden = true
                                obj.noDataLabel(text: msg)
                                obj.refreshControl.endRefreshing()
                                obj.stopLoading()
                            }
                            
                            success(dataArray as AnyObject)
                            
                            
                        }
                        
                        obj.refreshControl.endRefreshing()
                        obj.stopLoading()
                    }
                    else
                    {
                        obj.label.isHidden = false
                        obj.tableView.isHidden = true
                        obj.noDataLabel(text: msg)
                        obj.refreshControl.endRefreshing()
                        obj.stopLoading()
                        
                    }
                    
                    
                }
                
                
                
                
            })
            { (error) in
                
                let errorStr : String = error.description
                
                print("DATA:error",errorStr)
                
                //                obj.errorChecking(error: error)
                obj.stopLoading()
            }
            
            
        }
        else{
            obj.label.isHidden = false
            obj.tableView.isHidden = true
            obj.noDataLabel(text: "Internet is not available, please check your internet connection try again." )
            obj.stopLoading()
        }
        
        
    }
    
    
}

class FoodRequestReportedDataAPILoadMore
{
    
    var reachablty = Reachability()!
    
    func serviceCalling(obj:FoodRequestRoportedViewController,urlStr : String, param : [String: String] , success:@escaping (AnyObject)-> Void)
    {
        obj.startLoading()
        
        if(reachablty.connection != .none)
        {
            
            
            WebServices.sharedInstances.sendPostRequest(url: urlStr, parameters: param , successHandler: { (dict) in
                
                if let response = dict["response"]{
                    
                    let statusString : String = response["status"] as! String
                    
                    if(statusString == "ok")
                    {
                        //                        obj.tableView.isHidden = false
                        //                        obj.noDataLabel(text: "" )
                        
                        if let data = dict["data"] as? [AnyObject]
                        {
                            
                            var dataArray: [RoomRequestReportedDataModel] = []
                           
                            for i in 0..<data.count
                            {
                                
                                if let cell = data[i] as? [String:AnyObject]
                                {
                                    let object = RoomRequestReportedDataModel()
                                    object.setDataInModel(str: cell)
                                    dataArray.append(object)
                                }
                                
                            }
                            
                            success(dataArray as AnyObject)
                           
                            
                            
                        }
                        
                        obj.refreshControl.endRefreshing()
                        obj.stopLoading()
                    }
                    else
                    {
                        
                        //                        obj.tableView.isHidden = true
                        //                        obj.noDataLabel(text: "No Data Found!" )
                        obj.refreshControl.endRefreshing()
                        obj.stopLoading()
                        
                    }
                    
                    
                }
                
                
                
                
            })
            { (error) in
                
                let errorStr : String = error.description
                
                print("DATA:error",errorStr)
                
                //                obj.errorChecking(error: error)
                obj.stopLoading()
            }
            
            
        }
        else{
            //            obj.tableView.isHidden = true
            //            obj.noDataLabel(text: "Internet is not available, please check your internet connection try again." )
            obj.stopLoading()
        }
        
        
    }
    
    
}



class RoomTypeDataAPI
{
    
    var reachablty = Reachability()!
   
    func serviceCalling(obj:SelectRoomswithDetailsViewController, param : [String: String] , success:@escaping (AnyObject)-> Void)
    {
        obj.startLoading()
        
        if(reachablty.connection != .none)
        {
            
            
            WebServices.sharedInstances.sendPostRequest(url: URLConstants.RoomType, parameters: param , successHandler: { (dict) in
                
                if let response = dict["response"]{
                    
                    let statusString : String = response["status"] as! String
                    
                    if(statusString == "ok")
                    {
                       obj.stopLoading()
                        if let data = dict["data"] as? [AnyObject]
                        {
                            
                            var dataArray: [RoomTypeDataModel] = []
                            if(data.count > 0){
                                for i in 0..<data.count
                                {
                                    
                                    if let cell = data[i] as? [String:AnyObject]
                                    {
                                        let object = RoomTypeDataModel()
                                        object.setDataInModel(str: cell)
                                        dataArray.append(object)
                                    }
                                    
                                }
                                
                                success(dataArray as AnyObject)
                            }else{
                             
                                obj.stopLoading()
                            }
                            
                            
                        }
                        
                    
                        obj.stopLoading()
                    }
                    else
                    {
                      
                        obj.stopLoading()
                        
                    }
                    
                    
                }
                
                
                
                
            })
            { (error) in
                
                let errorStr : String = error.description
                
                print("DATA:error",errorStr)
                
                //                obj.errorChecking(error: error)
                obj.stopLoading()
            }
            
            
        }
        else{
           
            obj.stopLoading()
        }
        
        
    }
    
    
}





