//
//  GhosnaFileListModel.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 10/04/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import Foundation


class GhiosnaFileListModel: NSObject {
    
    
    
    var ID = String()
    var Ghoshna = String()
    var Title = String()
    var Story = String()
    
    var File_Url = String()
    var BroadcastTo = String()
    var Scheduled_Time = String()
    var Scheduled_Date = String()

    
    
    
    
    
    
    
    
    
    
    func setDataInModel(cell:[String:AnyObject])
    {
        
        if cell["ID"] is NSNull || cell["ID"] == nil {
            self.ID = ""
        }else{
            let app = cell["ID"]
            self.ID = (app?.description)!
        }
        
        if cell["Ghoshna"] is NSNull || cell["Ghoshna"] == nil {
            self.Ghoshna = ""
        }else{
            let app = cell["Ghoshna"]
            self.Ghoshna = (app?.description)!
        }
        
        if cell["Title"] is NSNull || cell["Title"] == nil {
            self.Title = ""
        }else{
            let app = cell["Title"]
            self.Title = (app?.description)!
        }
        if cell["Story"] is NSNull || cell["Story"] == nil {
            self.Story = ""
        }else{
            let app = cell["Story"]
            self.Story = (app?.description)!
        }
        
        //
        
        
        if cell["File_Url"] is NSNull || cell["File_Url"] == nil {
            self.File_Url = ""
        }else{
            let app = cell["File_Url"]
            self.File_Url = (app?.description)!
        }
        
        if cell["BroadcastTo"] is NSNull || cell["BroadcastTo"] == nil {
            self.BroadcastTo = ""
        }else{
            let app = cell["BroadcastTo"]
            self.BroadcastTo = (app?.description)!
        }
        
        if cell["Scheduled_Time"] is NSNull || cell["Scheduled_Time"] == nil {
            self.Scheduled_Time = ""
        }else{
            let app = cell["Scheduled_Time"]
            self.Scheduled_Time = (app?.description)!
        }
        if cell["Scheduled_Date"] is NSNull || cell["Scheduled_Date"] == nil {
            self.Scheduled_Date = ""
        }else{
            let app = cell["Scheduled_Date"]
            self.Scheduled_Date = (app?.description)!
        }
        //
        
        
      
        
    }
}


class GhosnaFileListModelDataAPI
{
    
    var reachablty = Reachability()!
    
    
    func serviceCalling(obj:GhosnaFileListViewController,emp : String, success:@escaping (AnyObject)-> Void)
    {
        
        obj.startLoading()
        
        if(reachablty.connection != .none)
        {
            
            let urlGhosna = URLConstants.Ghoshna_data + emp
            WebServices.sharedInstances.sendGetRequest(Url: urlGhosna, successHandler: { (dict:[String:AnyObject]) in
           
                
                if let response = dict["response"]{
                    
                    let statusString : String = response["status"] as! String
                    let objectmsg = MessageCallServerModel()
                    let msg = objectmsg.sealizeMessage(cell: response as! [String : AnyObject])
                    if(statusString == "success")
                    {
                        obj.tableView.isHidden = false
                        obj.noDataLabel(text: "" )
                        obj.label.isHidden = true
                        
                        
                        if let data = dict["data"] as? [AnyObject]
                        {
                            
                            var dataArray : [GhiosnaFileListModel] = []
                            for i in 0..<data.count
                            {
                                
                                if let cell = data[i] as? [String:AnyObject]
                                {
                                    let object = GhiosnaFileListModel()
                                    object.setDataInModel(cell: cell)
                                    dataArray.append(object)
                                }
                                
                            }
                            
                            
                            success(dataArray as AnyObject)
                            
                            
                        }
                        
                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                    }
                    else
                    {
                        
                        
                        obj.label.isHidden = false
                        obj.tableView.isHidden = true
                        obj.noDataLabel(text: msg )
                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                        
                    }
                    
                    
                }
                
                
                
            }, failureHandler: { (error) in
                let errorStr : String = error.description
                
                print("DATA:error",errorStr)
                
                // obj.errorChecking(error: error)
                obj.stopLoading()
            })
            
            
        }
        else{
            obj.noDataLabel(text:  "No internet connection found. Check your internet connection and try again.")
            
            obj.label.isHidden = false
            obj.tableView.isHidden = true
            // obj.noDataLabel(text: "No Data Found!" )
            obj.refresh.endRefreshing()
            obj.stopLoading()
        }
        
        
    }
    
    
}
