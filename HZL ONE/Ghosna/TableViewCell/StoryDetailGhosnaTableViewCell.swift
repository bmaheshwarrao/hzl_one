//
//  StoryDetailGhosnaTableViewCell.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 10/04/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit

class StoryDetailGhosnaTableViewCell: UITableViewCell {

    @IBOutlet weak var imgStory: UIImageView!
    @IBOutlet weak var textViewStory: UITextView!
    @IBOutlet weak var textViewTitle: UITextView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
