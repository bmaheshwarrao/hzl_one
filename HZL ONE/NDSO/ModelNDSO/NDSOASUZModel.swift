//
//  NDSOASUZModel.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 06/02/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import Foundation




struct FilterDataFromServer {
    static var location_id = Int()
    static var location_name = String()
    static var setDataOb = String()
    static var area_Id = Int()
    static var area_Name = String()
    static var sub_Area_Id = Int()
    static var sub_Area_Name = String()
    static var unit_Id = Int()
    static var unit_Name = String()
    static var hazard_Id = Int()
    static var hazard_Name = String()
    static var filterType = String()
    static var textData = String()
    static var uniqueId = String()
    static var compliance = String()
    static var dept_Id = Int()
    static var dept_Name = String()
    static var employyetype = String()
    static var gatepass = String()
    static var remark = String()
    static var fromTime = String()
    static var toTime = String()
    static var theme_Id = Int()
    static var theme = String()
    static var department_Id = Int()
    static var department_Name = String()
    
    static var Authority_Id = String()
    static var Authority_Name = String()
    
    static var location_Movie = String()
    
    static var parent_Group = String()
    
    static var parent_Id = String()
    
    
}


class DataListApi
{
    var reachablty = Reachability()!
    func serviceCalling(obj:NDSOASUZViewController, success:@escaping (AnyObject)-> Void)
    {
        
        obj.startLoading(view: obj.tableViewASUZ)
        var statusString = String()
        
        if(reachablty.connection != .none)
        {
            
            var urlString = String()
            var keyString = String()
            
            if(FilterDataFromServer.filterType == "Unit") {
                urlString = Base_Url_NDSO+"G_Unit_List/?Location_ID=\(FilterDataFromServer.location_id)"
                keyString = "Unit_Name"
                
            } else if(FilterDataFromServer.filterType == "Area"){
                
              
                
                urlString = Base_Url_NDSO+"G_Area_List/?Unit_ID=\(FilterDataFromServer.unit_Id)"
                keyString = "Area_Name"
            }else if(FilterDataFromServer.filterType == "Department"){
                
              
                
                urlString = Base_Url_NDSO+"G_Department_List/?Unit_ID=\(FilterDataFromServer.unit_Id)"
                keyString = "Department_Name"
            }
            
            
            print(urlString)
            WebServices.sharedInstances.sendGetRequest(Url: urlString, successHandler: { (dict) in
                
                
                if let response = dict["response"]{
                    
                    statusString  = response["status"] as! String
                    print(response)
                    let objectmsg = MessageCallServerModel()
            let msg = objectmsg.sealizeMessage(cell: response as! [String : AnyObject])
                    if(statusString == "success")
                    {
                        
                        obj.tableViewASUZ.isHidden = false
                        obj.noDataLabel(text: "" )
                        obj.label.isHidden = true
                        
                        if let data = dict["data"] as? [AnyObject]
                        {
                            
                            var dataArray : [DataListModel] = []
                            
                            for i in 0..<data.count
                            {
                                if let cell = data[i] as? [String:AnyObject]
                                {
                                    let object = DataListModel()
                                    
                                    if cell["ID"] is NSNull{
                                        object.id = ""
                                    }else{
                                        let idd = cell["ID"]
                                        object.id = (idd?.description)!
                                    }
                                    
                                    if cell[keyString] is NSNull{
                                        object.name = ""
                                    }else{
                                        let idd = cell[keyString]
                                        object.name = (idd?.description)!
                                        
                                       
                                    }
                                    
                                    
                                    dataArray.append(object)
                                    
                                }
                            }
                            
                            obj.tableViewASUZ.reloadData()
                            success(dataArray as AnyObject)
                            
                            
                        }
                        
                        obj.refresh.endRefreshing()
                        obj.stopLoading(view: obj.tableViewASUZ)
                    }
                    else
                    {
                        statusString = "success"
                        obj.tableViewASUZ.reloadData()
                        obj.label.isHidden = false
                        obj.noDataLabel(text: msg )
                        print("DATA:fail")
                        //                        obj.tableView.isHidden = true
                        //                        obj.noDataLabel(text: "Sorry! No area manager found with name "+name)
                        obj.refresh.endRefreshing()
                        obj.stopLoading(view: obj.tableViewASUZ)
                        
                    }
                    
                    
                }
                
                
                
            }, failureHandler: { (error) in
                let errorStr : String = error.description
                statusString = "fail"
                print("DATA:error",errorStr)
                
                //  obj.errorChecking(error: error)
                
            })
         
            
        }
        else{
            obj.noDataLabel(text:  "No internet connection found. Check your internet connection and try again.")
            obj.tableViewASUZ.isHidden = true
            obj.label.isHidden = false
            obj.refresh.endRefreshing()
            obj.stopLoading()
            
            obj.tableViewASUZ.reloadData()
        }
        
        
    }
}
class DataListModel: NSObject {
    
    var name = String()
    var id = String()
    
    
}


class SubmitObNDSO: NSObject {
    
    var observation = String()
    var suggestion = String()
    var image = UIImage()
    var status = String()
    
    
}
