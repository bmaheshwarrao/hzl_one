//
//  ModelVisitNDSO.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 05/02/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import Foundation
import Reachability


class MyVisitDataModel: NSObject {
    
    
    var ID = Int()
    var Rost_Date = String()
    var Roster_Status = String()
    var Employee_1 = String()
    var Employee_Name_1 = String()
    var Employee_2 = String()
    var Employee_Name_2 = String()
    var Location_ID = String()
    var Location_Name = String()
    
    
    
   
    
    
    
    
    func setDataInModel(str:[String:AnyObject])
    {
        if str["Rost_Date"] is NSNull || str["Rost_Date"] == nil{
            self.Rost_Date = ""
        }else{
            
            let ros = str["Rost_Date"]
            
            self.Rost_Date = (ros?.description)!
            
            
        }
        if str["Roster_Status"] is NSNull || str["Roster_Status"] == nil{
            self.Roster_Status =  ""
        }else{
            
            let fross = str["Roster_Status"]
            
            self.Roster_Status = (fross?.description)!
            
           
            //  self.Description = (str["Description"] as? String)!
        }
        if str["ID"] is NSNull || str["ID"] == nil{
            self.ID = 0
        }else{
            self.ID = (str["ID"] as? Int)!
        }
        if str["Employee_1"] is NSNull || str["Employee_1"] == nil{
            self.Employee_1 = "0"
        }else{
            let emp1 = str["Employee_1"]
            
            self.Employee_1 = (emp1?.description)!
           // print(self.Employee_1)
        }
        if str["Employee_Name_1"] is NSNull || str["Employee_Name_1"] == nil{
            self.Employee_Name_1 = ""
        }else{
            
            let empname = str["Employee_Name_1"]
            
            self.Employee_Name_1 = (empname?.description)!
            
        }
        
        if str["Employee_2"] is NSNull || str["Employee_2"] == nil{
            self.Employee_2 = ""
        }else{
            let emp2 = str["Employee_2"]
            
            self.Employee_2 = (emp2?.description)!
            
            
            
        }
        if str["Employee_Name_2"] is NSNull || str["Employee_Name_2"] == nil{
            self.Employee_Name_2 = ""
        }else{
            let empname2 = str["Employee_Name_2"]
            
            self.Employee_Name_2 = (empname2?.description)!
       
        }
        
        
        if str["Location_ID"] is NSNull || str["Location_ID"] == nil{
            self.Location_ID = ""
        }else{
            let locid = str["Location_ID"]
            
            self.Location_ID = (locid?.description)!
            
        }
        if str["Location_Name"] is NSNull || str["Location_Name"] == nil{
            self.Location_Name = ""
        }else{
            let locname = str["Location_Name"]
            
            self.Location_Name = (locname?.description)!
            
        }
        
    }
    
}





class MyVisitDataAPI
{
    
    var reachablty = Reachability()!
    
    func serviceCalling(obj:MyVisitViewController, param : [String: String] , success:@escaping (AnyObject)-> Void)
    {
        obj.startLoading()
        
        if(reachablty.connection != .none)
        {
            
            
            WebServices.sharedInstances.sendPostRequest(url: URLConstants.My_Visits_ListNSDO, parameters: param , successHandler: { (dict) in
                
                if let response = dict["response"]{
                    
                    let statusString : String = response["status"] as! String
                    print(response)
                    let objectmsg = MessageCallServerModel()
            let msg = objectmsg.sealizeMessage(cell: response as! [String : AnyObject])
                    if(statusString == "success")
                    {
                        obj.tableView.isHidden = false
                        obj.noDataLabel(text: "" )
                        obj.label.isHidden = true
                        if let data = dict["data"] as? [AnyObject]
                        {
                            
                            var dataArray: [MyVisitDataModel] = []
                            
                            for i in 0..<data.count
                            {
                                
                                if let cell = data[i] as? [String:AnyObject]
                                {
                                    let object = MyVisitDataModel()
                                    object.setDataInModel(str: cell)
                                    dataArray.append(object)
                                }
                                
                            }
                            
                            success(dataArray as AnyObject)
                            
                            
                        }
                        
                        obj.refreshControl.endRefreshing()
                        obj.stopLoading()
                    }
                    else
                    {
                        obj.label.isHidden = false
                        obj.tableView.isHidden = true
                        obj.noDataLabel(text: msg )
                        obj.refreshControl.endRefreshing()
                        obj.stopLoading()
                        
                    }
                    
                    
                }
                
                
                
                
            })
            { (error) in
                
                let errorStr : String = error.description
                
                print("DATA:error",errorStr)
                
                //                obj.errorChecking(error: error)
                obj.stopLoading()
            }
            
            
        }
        else{
            obj.label.isHidden = false
            obj.tableView.isHidden = true
            obj.noDataLabel(text: "Internet is not available, please check your internet connection try again." )
            obj.stopLoading()
        }
        
        
    }
    
    
}

class MyVisitDataAPILoadMore
{
    
    var reachablty = Reachability()!
    
    func serviceCalling(obj:MyVisitViewController, param : [String: String] , success:@escaping (AnyObject)-> Void)
    {
        obj.startLoading()
        
        if(reachablty.connection != .none)
        {
            
            
            WebServices.sharedInstances.sendPostRequest(url: URLConstants.My_Visits_ListNSDO, parameters: param , successHandler: { (dict) in
                
                if let response = dict["response"]{
                    
                    let statusString : String = response["status"] as! String
                    
                    if(statusString == "success")
                    {
                        //                        obj.tableView.isHidden = false
                        //                        obj.noDataLabel(text: "" )
                        
                        if let data = dict["data"] as? [AnyObject]
                        {
                            
                            var dataArray: [MyVisitDataModel] = []
                            
                            for i in 0..<data.count
                            {
                                
                                if let cell = data[i] as? [String:AnyObject]
                                {
                                    let object = MyVisitDataModel()
                                    object.setDataInModel(str: cell)
                                    dataArray.append(object)
                                }
                                
                            }
                            
                            success(dataArray as AnyObject)
                            
                            
                        }
                        
                        obj.refreshControl.endRefreshing()
                        obj.stopLoading()
                    }
                    else
                    {
                        
                        //                        obj.tableView.isHidden = true
                        //                        obj.noDataLabel(text: "No Data Found!" )
                        obj.refreshControl.endRefreshing()
                        obj.stopLoading()
                        
                    }
                    
                    
                }
                
                
                
                
            })
            { (error) in
                
                let errorStr : String = error.description
                
                print("DATA:error",errorStr)
                
                //                obj.errorChecking(error: error)
                obj.stopLoading()
            }
            
            
        }
        else{
            //            obj.tableView.isHidden = true
            //            obj.noDataLabel(text: "Internet is not available, please check your internet connection try again." )
            obj.stopLoading()
        }
        
        
    }
    
    
}



