//
//  SubmitObservationViewController.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 05/02/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit
var dataArrayObNDSO : [SubmitObNDSO] = []
class SubmitObservationViewController:  CommonVSClass , WWCalendarTimeSelectorProtocol{
        var refreshControl = UIRefreshControl()
    @IBOutlet weak var tableView: UITableView!
     var visit_Id = String()
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.estimatedRowHeight = 65
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.tableFooterView = UIView()
        
        refreshControl.addTarget(self, action: #selector(getsubmitObData), for: .valueChanged)
        
        self.tableView.addSubview(refreshControl)
//        let swipe = UISwipeGestureRecognizer(target: self, action: #selector(self.swipeRecognizer))
//        swipe.direction = ([.down])
//        self.view.addGestureRecognizer(swipe)
        // Do any additional setup after loading the view.
    }
    var millisecStart = String()
    var millisecEnd = String()
    func getMilliseconds(date1 : Date) -> Int{
        //let strDate = date1
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd MMM yyyy  hh:mma"
        //let date = dateFormatter.date(from: strDate)
        
        let millieseconds = self.getDiffernce(toTime: date1)
        
        return millieseconds
    }
    func getDiffernce(toTime:Date) -> Int{
        let elapsed = toTime.timeIntervalSince1970
        return Int(elapsed * 1000)
    }
    var arrivalTime = String()
    fileprivate var singleDate: Date = Date()
    fileprivate var multipleDates: [Date] = []
    var selectdateVal = 0
    func WWCalendarTimeSelectorDone(_ selector: WWCalendarTimeSelector, date: Date) {
        print("Selected \n\(date)\n---")
        singleDate = date
        
    
        if(selectdateVal == 1){
            cellData.textFrom.text = date.stringFromFormat("hh':'mm a")
            startDate  = getMilliseconds(date1: date)
            millisecStart = String(startDate)
            arrivalTime = date.stringFromFormat("hh':'mma")
            DataDateStart = date
        }
        if(selectdateVal == 2){
            cellData.textTo.text = date.stringFromFormat("hh':'mm a")
            endDate  = getMilliseconds(date1: date)
            millisecEnd = String(endDate)
            DataDateEnd = date
        }
        print(String(startDate) + "-----" + String(endDate))
        
        switch DataDateStart.compare(DataDateEnd) {
            
        case .orderedAscending:
            print("less")
        case .orderedSame:
            print("same")
        case .orderedDescending:
            millisecEnd = millisecStart
            cellData.textTo.text = cellData.textFrom.text
        }
        
        
    }
    
    func WWCalendarTimeSelectorDone(_ selector: WWCalendarTimeSelector, dates: [Date]) {
        print("Selected Multiple Dates \n\(dates)\n---")
        if let date = dates.first {
            singleDate = date
            // dateLabel.text = date.stringFromFormat("d' 'MMMM' 'yyyy', 'h':'mma")
        }
        else {
            //dateLabel.text = "No Date Selected"
        }
        multipleDates = dates
    }
    var datePicker : UIDatePicker!
    @IBAction func btnFromDateClicked(_ sender: UIButton){
        
        let selector = UIStoryboard(name: "WWCalendarTimeSelector", bundle: nil).instantiateViewController(withIdentifier: "WWCalendarTimeSelector") as! WWCalendarTimeSelector
        
        selectdateVal = 1
        dateData = 1
        
        selector.delegate = self
        selector.optionCurrentDate = singleDate
        selector.optionCurrentDates = Set(multipleDates)
        selector.optionCurrentDateRange.setStartDate(multipleDates.first ?? singleDate)
        selector.optionCurrentDateRange.setEndDate(multipleDates.last ?? singleDate)
        
        
        selector.optionStyles.showDateMonth(false)
        selector.optionStyles.showMonth(false)
        selector.optionStyles.showYear(false)
        selector.optionStyles.showTime(true)
        
        
        present(selector, animated: true, completion: nil)
        
    }
    
     @IBAction func btnToDateClicked(_ sender: UIButton){
        
        if(cellData.textFrom.text == ""){
            self.view.makeToast("Please Select From time")
        }else{
            let selector = UIStoryboard(name: "WWCalendarTimeSelector", bundle: nil).instantiateViewController(withIdentifier: "WWCalendarTimeSelector") as! WWCalendarTimeSelector
            
            
            selectdateVal = 2
            
            selector.delegate = self
            selector.optionCurrentDate = singleDate
            selector.optionCurrentDates = Set(multipleDates)
            selector.optionCurrentDateRange.setStartDate(multipleDates.first ?? singleDate)
            selector.optionCurrentDateRange.setEndDate(multipleDates.last ?? singleDate)
            
            selector.optionStyles.showDateMonth(false)
            selector.optionStyles.showMonth(false)
            selector.optionStyles.showYear(false)
            selector.optionStyles.showTime(true)
            
            dateData = 1
            
            present(selector, animated: true, completion: nil)
            
        }
        
    }
    
   
    
    
    
    
    
    
    
    
    
    
    
    
    
      @IBAction func btnAddObClicked(_ sender: UIButton) {
        if(FilterDataFromServer.unit_Name == String()){
            self.view.makeToast("Select Unit")
        }else if(FilterDataFromServer.area_Name == String()){
            self.view.makeToast("Select Area")
        }else{
        FilterDataFromServer.dept_Id = Int()
        FilterDataFromServer.dept_Name = String()
        let storyBoard = UIStoryboard(name: "NDSO", bundle: nil)
        let ZIVC = storyBoard.instantiateViewController(withIdentifier: "AddObservationViewController") as! AddObservationViewController

        self.navigationController?.pushViewController(ZIVC, animated: true)
        }
      
        
        
    }
  
    @IBAction func btnAreaClicked(_ sender: UIButton) {
        if(FilterDataFromServer.unit_Name != "") {
            
            
            FilterDataFromServer.filterType = "Area"
            FilterDataFromServer.area_Id = Int()
            FilterDataFromServer.area_Name = String()
            
            FilterDataFromServer.sub_Area_Id = Int()
            FilterDataFromServer.sub_Area_Name = String()
            updateData()
            
            let storyBoard = UIStoryboard(name: "NDSO", bundle: nil)
            let ZIVC = storyBoard.instantiateViewController(withIdentifier: "NDSOASUZViewController") as! NDSOASUZViewController
             ZIVC.titleStr = "Select Area"
            self.navigationController?.pushViewController(ZIVC, animated: true)
            
            
        
            
        }
    }
    @IBAction func btnUnitClicked(_ sender: UIButton) {
        FilterDataFromServer.filterType = "Unit"
        FilterDataFromServer.unit_Id = Int()
        FilterDataFromServer.unit_Name = String()
        FilterDataFromServer.area_Id = Int()
        FilterDataFromServer.area_Name = String()
        FilterDataFromServer.sub_Area_Id = Int()
        FilterDataFromServer.sub_Area_Name = String()
        updateData()
      
        
        let storyBoard = UIStoryboard(name: "NDSO", bundle: nil)
        let ZIVC = storyBoard.instantiateViewController(withIdentifier: "NDSOASUZViewController") as! NDSOASUZViewController
        ZIVC.titleStr = "Select Unit"
        self.navigationController?.pushViewController(ZIVC, animated: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    
    
    
    
    
    
    

    @objc func updateData(){
        if(FilterDataFromServer.unit_Name != "") {
            cellData.lblUnit.text = FilterDataFromServer.unit_Name
            cellData.lblUnit.textColor = UIColor.black
        } else {
            cellData.lblUnit.text = "Select Unit"
            cellData.lblUnit.textColor = UIColor.lightGray
        }
        if(FilterDataFromServer.unit_Name != "" && FilterDataFromServer.area_Name != "") {
            cellData.lblArea.text = FilterDataFromServer.area_Name
            cellData.lblArea.textColor = UIColor.black
        } else {
            cellData.lblArea.text = "Select Area"
            cellData.lblArea.textColor = UIColor.lightGray
        }
//        if(FilterDataFromServer.fromTime != "") {
//            cellData.textFrom.text = FilterDataFromServer.fromTime
//            cellData.textFrom.textColor = UIColor.black
//        } else {
//            cellData.textFrom.text = ""
//            cellData.textFrom.textColor = UIColor.lightGray
//        }
//        if(FilterDataFromServer.toTime != "") {
//            cellData.textTo.text = FilterDataFromServer.toTime
//            cellData.textTo.textColor = UIColor.black
//        } else {
//            cellData.textTo.text = ""
//            cellData.textTo.textColor = UIColor.lightGray
//        }
      
        // tableView.reloadData()
    }
    var QuestionStrNo = String()
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        if(MoveStruct.isMove == true){
            MoveStruct.isMove = false
            self.view.makeToast(MoveStruct.message)
        }
        self.title = QuestionStrNo
        
        let nav = self.navigationController?.navigationBar
        nav?.tintColor = UIColor.white
       nav?.barTintColor = UIColor(hexString: "2c3e50", alpha: 1.0)
        nav?.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        tableView.reloadData()
        //updateData()
        
        
    }
    var reachablty = Reachability()!
    var submitObDB:[submitObDataModel] = []
    var submitObAPI = submitObDataAPI()
    @objc func getsubmitObData(){
        var param = [String:String]()

        
        
        
        
        
        
        
        
        
        if reachablty.connection == .none{
            
            self.view.makeToast("Internet is not available, please check your internet connection try again.")
            
        }
        else if FilterDataFromServer.unit_Name == "" {
            
            self.view.makeToast("Please Select Unit")
        }
        else if FilterDataFromServer.area_Name == "" {
            
            self.view.makeToast("Please Select Area")
        }else if cellData.textFrom.text == "" {
            
            self.view.makeToast("Please Select From Time")
        }
        else if cellData.textTo.text  == "" {
            
            self.view.makeToast("Please Select To Time")
        }
        else if dataArrayObNDSO.count == 0 {
            
            self.view.makeToast("Please Add Observation")
        }else{
            
            self.startLoadingPK(view: self.view)
            
            let parameter = ["Team_ID":visit_Id,
                             "Area_ID":String(FilterDataFromServer.area_Id) ,
                             "Location_ID":String(FilterDataFromServer.location_id),
                             "Unit_ID":String(FilterDataFromServer.unit_Id) ,
                             "Temp_ID":String(FilterDataFromServer.uniqueId) ,
                             "From_Time":cellData.textFrom.text! ,
                             "To_Time":cellData.textTo.text!,
                             "Employee_ID": UserDefaults.standard.string(forKey: "EmployeeID")!
                
                ] as [String:String]
            
            
            
            print("parameter",parameter)
            
            WebServices.sharedInstances.sendPostRequest(url: URLConstants.Observations_Submit, parameters: parameter, successHandler: { (response:[String : AnyObject]) in
                
                let respon = response["response"] as! [String:AnyObject]
                self.stopLoadingPK(view: self.view)
                 let objectmsg = MessageCallServerModel()
                let msg = objectmsg.sealizeMessage(cell: respon as! [String : AnyObject])
                if respon["status"] as! String == "success" {
                    
                    self.stopLoadingPK(view: self.view)
                    
                    MoveStruct.isMove = true
                    MoveStruct.message = msg
                    self.navigationController?.popViewController(animated: false)
                    
                    
                }else{
                    self.stopLoadingPK(view: self.view)
                    self.view.makeToast(msg)
                }
                
            }) { (err) in
                self.stopLoadingPK(view: self.view)
                print(err.description)
            }
            
        }
        
        
        
        
    }
    var cellData : SubmitObTableViewCell!
    
    
    @IBAction func btnSubmitClicked(_ sender: UIButton) {
        getsubmitObData()
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
extension SubmitObservationViewController : UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
    }
}
extension SubmitObservationViewController : UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
    
        return 2
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(section == 0){
           return 1
        }else{
            return dataArrayObNDSO.count
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if(indexPath.section == 0){
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! SubmitObTableViewCell
        cellData = cell
            if(dataArrayObNDSO.count == 0){
                cell.lblNoObConstraint.constant = 21
            }else{
                cell.lblNoObConstraint.constant = 0
            }
            let dateFormatter2 = DateFormatter()
            dateFormatter2.timeZone = NSTimeZone.system
            dateFormatter2.dateFormat = "hh:mm a"
            
            
            let date_TimeStr = dateFormatter2.string(from: Date())
            cell.textFrom.text = date_TimeStr
           
            startDate = getMilliseconds(date1: Date())
            cell.textTo.text = date_TimeStr
            endDate = getMilliseconds(date1: Date())
            millisecStart = String(startDate)
            millisecEnd = String(endDate)
        updateData()
        cell.separatorInset = UIEdgeInsetsMake(0, 0, 0, 0);
        return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellOb", for: indexPath) as! AddObSetTableViewCell
            cell.imgOb.image = dataArrayObNDSO[indexPath.row].image
            cell.lblStatus.text = dataArrayObNDSO[indexPath.row].status
            cell.textViewOb1.text = dataArrayObNDSO[indexPath.row].observation
            cell.textViewOb2.text = dataArrayObNDSO[indexPath.row].suggestion
            cell.separatorInset = UIEdgeInsetsMake(0, 0, 0, 0);
            return cell
        }
        
        
    }
}
