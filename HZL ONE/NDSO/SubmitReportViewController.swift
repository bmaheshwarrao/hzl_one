//
//  SubmitReportViewController.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 05/02/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit
import DLRadioButton
class SubmitReportViewController: CommonVSClass ,UITableViewDelegate, UITableViewDataSource {
var MyVisitReportDB = MyVisitDataModel()
    var PlaceHolderText = "Remark/Action Taken"
    @IBOutlet weak var tableView: UITableView!
    var refreshControl = UIRefreshControl()
    

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        FilterDataFromServer.location_id = Int(self.MyVisitReportDB.Location_ID)!
 getDataReport()
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.estimatedRowHeight = 65
        tableView.rowHeight = UITableViewAutomaticDimension
        
        
        refreshControl.addTarget(self, action: #selector(getDataReport), for: .valueChanged)
        
        self.tableView.addSubview(refreshControl)
        let swipe = UISwipeGestureRecognizer(target: self, action: #selector(self.swipeRecognizer))
        swipe.direction = ([.down])
        self.view.addGestureRecognizer(swipe)
        
        
        
        
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        if(MoveStruct.isMove == true){
            MoveStruct.isMove = false
            self.view.makeToast(MoveStruct.message)
        }
        self.tabBarController?.tabBar.isHidden = true
        self.title = "Submit Report"
        let nav = self.navigationController?.navigationBar
        nav?.tintColor = UIColor.white
       nav?.barTintColor = UIColor(hexString: "2c3e50", alpha: 1.0)
        nav?.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        getDataReport()
        
    }
    var reachability = Reachability()!
    @objc func swipeRecognizer(_ sender: UISwipeGestureRecognizer) {
        if (sender.direction == .down) {
            
            if(self.reachability.connection != .none)
            {
                
                self.getDataReport()
            }
            else
            {
                self.startLoading(view: self.view)
                DispatchQueue.main.asyncAfter(deadline: .now() + 4.5) {
                    self.noDataLabel(text: "No internet connection found. Check your internet connection and try again.")
                    self.stopLoading(view: self.view)
                }
            }
            
        }
    }
    @IBAction func tapViewClicked(_ sender: Any) {
        
        
        
        
        
        
        
    }
     var reachablty = Reachability()!
    @IBAction func BtnFinalSubmitClicked(_ sender: UIButton) {
        
       
      
            
            if reachablty.connection == .none{
                
                self.view.makeToast("Internet is not available, please check your internet connection try again.")
                
            }
            else if strAction == String() {
                
                self.view.makeToast("Please Answer Question no 4")
            }
            else if cellData.textViewRemark.text == "" {
                
                self.view.makeToast("Please Enter Remark for Question no 4")
            }
            else if cellData.textViewRemark.text == "" {
                
                self.view.makeToast("Please Enter Remark")
            }else{
                
                self.startLoadingPK(view: self.view)
                
                
                
                
            
                
                
                
                
                let parameter = ["Team_ID":String(self.MyVisitReportDB.ID),
                                 "Employee_ID": UserDefaults.standard.string(forKey: "EmployeeID")!,
                                
                                 "Observation_StatusQ4":strAction,
                                 "Observation_StatusQ5":String(),
                                 
                                 "RemarksQ4":cellData.textViewRemark.text ,
                                 "RemarksQ5":String()
                                 
                    
                    ] as [String:String]
                
                
                
                print("parameter",parameter)
                
                WebServices.sharedInstances.sendPostRequest(url: URLConstants.Submit_Final, parameters: parameter, successHandler: { (response:[String : AnyObject]) in
                    
                    let respon = response["response"] as! [String:AnyObject]
                    self.stopLoadingPK(view: self.view)
                     let objectmsg = MessageCallServerModel()
                let msg = objectmsg.sealizeMessage(cell: respon as! [String : AnyObject])
                    if respon["status"] as! String == "success" {
                        
                        self.stopLoadingPK(view: self.view)
                        
                        MoveStruct.isMove = true
                        MoveStruct.message = msg
                        self.navigationController?.popViewController(animated: false)
                        
                        
                    }else{
                        self.stopLoadingPK(view: self.view)
                        self.view.makeToast(msg)
                    }
                    
                }) { (err) in
                    self.stopLoadingPK(view: self.view)
                    print(err.description)
                }
                
            }
    
        
    }
    func clear(){
        FilterDataFromServer.filterType = String()
        FilterDataFromServer.unit_Id = Int()
        FilterDataFromServer.unit_Name = String()
        FilterDataFromServer.area_Id = Int()
        FilterDataFromServer.area_Name = String()
        FilterDataFromServer.sub_Area_Id = Int()
        FilterDataFromServer.sub_Area_Name = String()
        FilterDataFromServer.dept_Id = Int()
        FilterDataFromServer.dept_Name = String()
        FilterDataFromServer.toTime = String()
        FilterDataFromServer.fromTime = String()
        dataArrayObNDSO = []
    }
    @IBAction func BtnQuestion1Clicked(_ sender: UIButton) {
        
        clear()
        let storyBoard = UIStoryboard(name: "NDSO", bundle: nil)
        let ZIVC = storyBoard.instantiateViewController(withIdentifier: "AddQuestionDetailsViewController") as! AddQuestionDetailsViewController
        ZIVC.visit_Id = String(self.MyVisitReportDB.ID)
        ZIVC.Question_Id = String(1)
       ZIVC.Question = self.MyQuestionDB[0].Q1
        ZIVC.QuestionStrNo = "Add Question 1"
        self.navigationController?.pushViewController(ZIVC, animated: true)
        
        
        
        
    }
    @IBAction func BtnQuestion2Clicked(_ sender: UIButton) {
        
        clear()
        let storyBoard = UIStoryboard(name: "NDSO", bundle: nil)
        let ZIVC = storyBoard.instantiateViewController(withIdentifier: "AddQuestionDetailsViewController") as! AddQuestionDetailsViewController
        ZIVC.visit_Id = String(self.MyVisitReportDB.ID)
        ZIVC.Question_Id = String(2)
        ZIVC.QuestionStrNo = "Add Question 2"
        ZIVC.Question = self.MyQuestionDB[0].Q2
        self.navigationController?.pushViewController(ZIVC, animated: true)
        
        
        
        
    }
    @IBAction func BtnQuestion3Clicked(_ sender: UIButton) {
        clear()
        let storyBoard = UIStoryboard(name: "NDSO", bundle: nil)
        let ZIVC = storyBoard.instantiateViewController(withIdentifier: "AddQuestionDetailsViewController") as! AddQuestionDetailsViewController
         ZIVC.visit_Id = String(self.MyVisitReportDB.ID)
         ZIVC.Question_Id = String(3)
        ZIVC.QuestionStrNo = "Add Question 3"
        ZIVC.Question = self.MyQuestionDB[0].Q3
        self.navigationController?.pushViewController(ZIVC, animated: true)
    }
    @IBAction func BtnRewardClicked(_ sender: UIButton) {
        clear()
        let storyBoard = UIStoryboard(name: "NDSO", bundle: nil)
        let ZIVC = storyBoard.instantiateViewController(withIdentifier: "AddRewardViewController") as! AddRewardViewController
        ZIVC.visit_Id = String(self.MyVisitReportDB.ID)
       
        self.navigationController?.pushViewController(ZIVC, animated: true)
    }
    @IBAction func BtnObservationClicked(_ sender: UIButton) {
        clear()
        let storyBoard = UIStoryboard(name: "NDSO", bundle: nil)
        let ZIVC = storyBoard.instantiateViewController(withIdentifier: "SubmitObservationViewController") as! SubmitObservationViewController
        ZIVC.visit_Id = String(self.MyVisitReportDB.ID)
        ZIVC.QuestionStrNo = "Submit Observation"
       // let empId : String = UserDefaults.standard.string(forKey: "EmployeeID")!
        let empId = "1001"
        let dateFormatter2 = DateFormatter()
        dateFormatter2.timeZone = NSTimeZone.system
        dateFormatter2.dateFormat = "ddMMMyyyyHHmmssSSS"
        
        
        let date_TimeStr = dateFormatter2.string(from: Date())
        let dataUniqueId = date_TimeStr + empId
        
        
        FilterDataFromServer.uniqueId = dataUniqueId
        self.navigationController?.pushViewController(ZIVC, animated: true)
    }
    var MyCountDB:[MyCountDataModel] = []
    var MyObDataAPI = MyObCountDataAPI()
    
    
    var MyQuestionDB:[MyQuestionDataModel] = []
    var MyQuestionAPI = MyQuestionDataAPI()
    @objc func getDataReport() {
        
        var param = [String:String]()
        
        param =  [:]
        
        MyObDataAPI.serviceCalling(obj: self,  param: ["Team_ID":String(self.MyVisitReportDB.ID)] ) { (dict) in
            
            self.MyCountDB = dict as! [MyCountDataModel]
        
             self.tableView.reloadData()
        }
        
        
        
        MyQuestionAPI.serviceCalling(obj: self,  param: param ) { (dict) in
            
            self.MyQuestionDB = dict as! [MyQuestionDataModel]
            
            self.tableView.reloadData()
            
        }
        
    }
    @objc func tapViewTapClicked(_ sender: UITapGestureRecognizer) {
    
        
        
    }
    var strAction = String()
    @IBAction func radioButtonClicked(_ sender: DLRadioButton){
        
        for button in sender.selectedButtons() {
            self.strAction = (button.titleLabel!.text!)
            print(String(format: "%@ is selected.\n", self.strAction));
            
        }
    }
    
    @IBOutlet var tapView: UITapGestureRecognizer!
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
     var cellData : ObservationReportTableViewCell!
    var data: String?
    var lastObject: String?
    var indPath = IndexPath()
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        
        
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
            return 20.0
      
       
        
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 4
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if(indexPath.section == 0){
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellMyVisit", for: indexPath) as! MyVisitTableViewCell
        
        cell.visitIdLbl.text = "Visit Id #" + String(self.MyVisitReportDB.ID)
        let TapGesture = UITapGestureRecognizer(target: self, action: #selector(self.tapViewTapClicked(_:)))
        cell.addGestureRecognizer(TapGesture)
        cell.isUserInteractionEnabled = true
        TapGesture.numberOfTapsRequired = 1
        if(self.MyVisitReportDB.Rost_Date != ""){
            let dateFormatter = DateFormatter()
            dateFormatter.timeZone = NSTimeZone.system
            
            dateFormatter.dateFormat = "MMM dd yyyy"
            let date : Date = dateFormatter.date(from: self.MyVisitReportDB.Rost_Date)!
            dateFormatter.string(from: date)
            let dateFormatter2 = DateFormatter()
            dateFormatter2.timeZone = NSTimeZone.system
            dateFormatter2.dateFormat = "dd MMM yyyy"
            let finalDate = dateFormatter2.string(from: date)
            cell.dateLbl.text = finalDate
        }else{
            cell.dateLbl.text = ""
        }
        cell.nameEmpLbl.text = self.MyVisitReportDB.Employee_Name_1
        cell.nameEmp2Lbl.text = self.MyVisitReportDB.Employee_Name_2
        cell.zoneLbl.text = self.MyVisitReportDB.Location_Name
        
          return cell
        
        }else if(indexPath.section == 1){
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellOb", for: indexPath) as! ObservationTableViewCell
            if(self.MyCountDB.count > 0){
                 cell.observationLbl.text = "Observation : " + self.MyCountDB[0].Observation
            }else{
                cell.observationLbl.text = "Observation : 0"
            }
            return cell
        }
        else if(indexPath.section == 2){
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellReport", for: indexPath) as! ObservationReportTableViewCell
            cellData = cell
            cell.textViewRemark.delegate = self;
            cell.textViewRemark.layer.cornerRadius = 10
            cell.textViewRemark.layer.borderColor = UIColor.black.cgColor
            cell.textViewRemark.layer.borderWidth = 1.0
            cell.textViewRemark.textColor = .lightGray
           if(self.MyCountDB.count > 0){
            
            if(self.MyQuestionDB.count > 0){
                cell.textViewQ1.text = self.MyQuestionDB[0].Q1
                cell.textViewQ2.text = self.MyQuestionDB[0].Q2
                cell.textViewQ3.text = self.MyQuestionDB[0].Q3
                cell.textViewQ4.text = self.MyQuestionDB[0].Q4
            }else{
                cell.textViewQ1.text = ""
                cell.textViewQ2.text = ""
                cell.textViewQ3.text = ""
                cell.textViewQ4.text = ""
            }
      
            cell.observationQuestion1Lbl.text = "Observation : " + self.MyCountDB[0].Q1
            cell.observationQuestion2Lbl.text = "Observation : " + self.MyCountDB[0].Q2
            cell.observationQuestion3Lbl.text = "Observation : " + self.MyCountDB[0].Q3
           }else{
            if(self.MyQuestionDB.count > 0){
                cell.textViewQ1.text = self.MyQuestionDB[0].Q1
                cell.textViewQ2.text = self.MyQuestionDB[0].Q2
                cell.textViewQ3.text = self.MyQuestionDB[0].Q3
                cell.textViewQ4.text = self.MyQuestionDB[0].Q4
            }else{
            cell.textViewQ1.text = ""
            cell.textViewQ2.text = ""
            cell.textViewQ3.text = ""
            cell.textViewQ4.text = ""
            }
            
            cell.observationQuestion1Lbl.text = "Observation : 0"
            cell.observationQuestion2Lbl.text = "Observation : 0"
            cell.observationQuestion3Lbl.text = "Observation : 0"
            }
            
            return cell
        }else{
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellReward", for: indexPath) as! RewardTableViewCell
              if(self.MyCountDB.count > 0){
             cell.rewardLbl.text = "Observation : " + self.MyCountDB[0].Reward
              }else{
                  cell.rewardLbl.text = "Observation : 0"
            }
            return cell
        }
     
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableViewAutomaticDimension
        
    }
    
   
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
extension SubmitReportViewController : UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView)
    {
        if (textView.text == PlaceHolderText)
        {
            textView.text = ""
            textView.textColor = .black
        }
        
        
    }
    
    func textViewDidEndEditing(_ textView: UITextView)
    {
        if (textView.text == "")
        {
            textView.text = PlaceHolderText
            textView.textColor = .lightGray
        }
        self.cellData.textViewRemark.endEditing(true)
        
    }
}
