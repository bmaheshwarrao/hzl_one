//
//  SubmitObTableViewCell.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 06/02/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit

class SubmitObTableViewCell: UITableViewCell {

   
    
    
   
 
    @IBOutlet weak var lblNoObConstraint: NSLayoutConstraint!
    @IBOutlet weak var lblNoOb: UILabel!
    @IBOutlet weak var textTo: UILabel!
    @IBOutlet weak var viewTo: UIView!
    @IBOutlet weak var textFrom: UILabel!
    @IBOutlet weak var viewFrom: UIView!
    @IBOutlet weak var btnUnit: UIButton!
    @IBOutlet weak var viewUnit: UIView!
    @IBOutlet weak var lblUnit: UILabel!
    
   @IBOutlet weak var lblArea: UILabel!
    @IBOutlet weak var btnArea: UIButton!
    @IBOutlet weak var viewArea: UIView!
   
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        
        viewTo.layer.borderWidth = 1.0
        viewTo.layer.borderColor = UIColor.black.cgColor
        viewTo.layer.cornerRadius = 10.0
        viewFrom.layer.borderWidth = 1.0
        viewFrom.layer.borderColor = UIColor.black.cgColor
        viewFrom.layer.cornerRadius = 10.0
        
        viewArea.layer.borderWidth = 1.0
        viewArea.layer.borderColor = UIColor.black.cgColor
        viewArea.layer.cornerRadius = 10.0
        viewUnit.layer.borderWidth = 1.0
        viewUnit.layer.borderColor = UIColor.black.cgColor
        viewUnit.layer.cornerRadius = 10.0
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
