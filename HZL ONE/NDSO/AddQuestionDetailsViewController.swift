//
//  AddQuestionDetailsViewController.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 05/02/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit

class AddQuestionDetailsViewController: CommonVSClass {

    @IBOutlet weak var tableView: UITableView!
    var Question = String()
    var visit_Id = String()
    var Question_Id = String()
    var type_Id = String()
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
              tableView.tableFooterView = UIView()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func btnAddClicked(_ sender: UIButton) {
         submitData()
    }
    var empType = ["Employee", "Contractor"]
    @IBAction func btnEmployeeClicked(_ sender: UIButton) {
        
       
        
        DispatchQueue.main.async(execute: {() -> Void in
            let popup = PopUpTableViewController()
            
            popup.itemsArray = self.empType
            
            popup.sourceView = self.cellData.lblEmployee
            popup.isScroll = true
            popup.backgroundColor = UIColor.white
            if popup.itemsArray.count > 5{
                popup.popUpHeight = 200
            }
            else{
                popup.popUpHeight = CGFloat(popup.itemsArray.count)*45
            }
            popup.popUpWidth = UIScreen.main.bounds.size.width-60
            popup.backgroundImage = nil
            popup.itemTitleColor = UIColor.white
            popup.itemSelectionColor = UIColor.lightGray
            popup.arrowDirections = .any
            popup.arrowColor = UIColor.white
            popup.popCellBlock = {(_ popupVC: PopUpTableViewController, _ popupCell: UITableViewCell, _ row: Int, _ section: Int) -> Void in
                
                
                
                
                
               self.cellData.lblEmployee.text = self.empType[row]
                self.cellData.lblEmployee.textColor = UIColor.black
                popupVC.dismiss(animated: false, completion: nil)
                
            }
            self.present(popup, animated: true, completion: {() -> Void in
            })
            
            
        })
        
        
        
    }
    @IBAction func btnAreaClicked(_ sender: UIButton) {
        if(FilterDataFromServer.unit_Name != "") {
            
            
            FilterDataFromServer.filterType = "Area"
            FilterDataFromServer.area_Id = Int()
            FilterDataFromServer.area_Name = String()
            
            FilterDataFromServer.sub_Area_Id = Int()
            FilterDataFromServer.sub_Area_Name = String()
            updateData()
            
            
            
            
            
            let moveVC = self.storyboard?.instantiateViewController(withIdentifier: "NDSOASUZViewController") as! NDSOASUZViewController
            moveVC.titleStr = "Select Area"
            self.navigationController?.pushViewController(moveVC, animated: true)
            
        }
    }
    @IBAction func btnUnitClicked(_ sender: UIButton) {
        FilterDataFromServer.filterType = "Unit"
        FilterDataFromServer.unit_Id = Int()
        FilterDataFromServer.unit_Name = String()
        FilterDataFromServer.area_Id = Int()
        FilterDataFromServer.area_Name = String()
        FilterDataFromServer.sub_Area_Id = Int()
        FilterDataFromServer.sub_Area_Name = String()
        updateData()
        let moveVC = self.storyboard?.instantiateViewController(withIdentifier: "NDSOASUZViewController") as! NDSOASUZViewController
        moveVC.titleStr = "Select Unit"
        self.navigationController?.pushViewController(moveVC, animated: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
  var reachablty = Reachability()!
    @objc func submitData(){
        
        if reachablty.connection == .none{
            
            self.view.makeToast("Internet is not available, please check your internet connection try again.")
            
        }
        else if FilterDataFromServer.unit_Name == "" {
            
            self.view.makeToast("Please Select Unit")
        }
        else if FilterDataFromServer.area_Name == "" {
            
            self.view.makeToast("Please Select Area")
        }else if cellData.lblEmployee.text == "Select Employee" || cellData.lblEmployee.text == "" {
            
            self.view.makeToast("Please Select Employee Type")
        }
        else if cellData.textGatepass.text == "" {
            
            self.view.makeToast("Please Enter Employee Id/Gate pass Id")
        }
        else if cellData.textViewRemark.text == "" {
            
            self.view.makeToast("Please Enter Remark")
        }else{
            
            self.startLoadingPK(view: self.view)
            
           
            
            
            
            
            
            
            
            
            let parameter = ["Team_ID":visit_Id,
                             "Area_ID":String(FilterDataFromServer.area_Id) ,
                             "Area_Name":String(FilterDataFromServer.area_Name) ,
                             "Type_ID":cellData.textGatepass.text!,
                             "Employee_Type":cellData.lblEmployee.text!,
                             "Unit_ID":String(FilterDataFromServer.unit_Id) ,
                             "Unit_Name":String(FilterDataFromServer.unit_Name) ,
                             "Question_ID":Question_Id ,
                             "Remarks":cellData.textViewRemark.text! ,
                             
                             "Employee_ID": UserDefaults.standard.string(forKey: "EmployeeID")!
                
                ] as [String:String]
            
            
            
            print("parameter",parameter)
            
            WebServices.sharedInstances.sendPostRequest(url: URLConstants.Specific_Obs_Type_One, parameters: parameter, successHandler: { (response:[String : AnyObject]) in
                
                let respon = response["response"] as! [String:AnyObject]
                self.stopLoadingPK(view: self.view)
                 let objectmsg = MessageCallServerModel()
                let msg = objectmsg.sealizeMessage(cell: respon as! [String : AnyObject])
                if respon["status"] as! String == "success" {
                    
                    self.stopLoadingPK(view: self.view)
                    
                    MoveStruct.isMove = true
                    MoveStruct.message = msg
                    self.navigationController?.popViewController(animated: false)
                    
                    
                }else{
                    self.stopLoadingPK(view: self.view)
                    self.view.makeToast(msg)
                }
                
            }) { (err) in
                self.stopLoadingPK(view: self.view)
                print(err.description)
            }
            
        }
        
        
    }
    
    
    
    
  
   
    
    var cellData : QuestionDetailsTableViewCell!
    @objc func updateData(){
        if(FilterDataFromServer.unit_Name != "") {
            cellData.lblUnit.text = FilterDataFromServer.unit_Name
            cellData.lblUnit.textColor = UIColor.black
        } else {
            cellData.lblUnit.text = "Select Unit"
            cellData.lblUnit.textColor = UIColor.lightGray
        }
        if(FilterDataFromServer.unit_Name != "" && FilterDataFromServer.area_Name != "") {
            cellData.lblArea.text = FilterDataFromServer.area_Name
            cellData.lblArea.textColor = UIColor.black
        } else {
            cellData.lblArea.text = "Select Area"
            cellData.lblArea.textColor = UIColor.lightGray
        }
       
        if(FilterDataFromServer.employyetype != "" ) {
            cellData.lblEmployee.text = FilterDataFromServer.employyetype
            cellData.lblEmployee.textColor = UIColor.black
        } else {
            cellData.lblEmployee.text = "Select Employee"
            cellData.lblEmployee.textColor = UIColor.lightGray
        }
        
        if(FilterDataFromServer.gatepass != "" ) {
            cellData.textGatepass.text = FilterDataFromServer.gatepass
            cellData.textGatepass.textColor = UIColor.black
        }
        if(FilterDataFromServer.remark != "" ) {
            cellData.textViewRemark.text = FilterDataFromServer.remark
            cellData.textViewRemark.textColor = UIColor.black
        }
        cellData.textViewQuestion.text = Question
       // tableView.reloadData()
    }
    var QuestionStrNo = String()
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.title = QuestionStrNo

        let nav = self.navigationController?.navigationBar
        nav?.tintColor = UIColor.white
       nav?.barTintColor = UIColor(hexString: "2c3e50", alpha: 1.0)
        nav?.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
       tableView.reloadData()
        //updateData()
        
        
    }
    
    
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
extension AddQuestionDetailsViewController : UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
    }
}
extension AddQuestionDetailsViewController : UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! QuestionDetailsTableViewCell
      cellData = cell
        updateData()
                   cell.separatorInset = UIEdgeInsetsMake(0, 10000, 0, 0);
        return cell
        
        
        
    }
}
