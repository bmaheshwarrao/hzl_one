//
//  NDSOReportedDataViewController.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 08/02/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit
import SDWebImage
class NDSOReportedDataViewController: CommonVSClass ,UITextViewDelegate{
    
    //    var ID : [String] = ["16" ,"17"]
    //var Date : [String] = ["27-Sep-2018","27-Sep-2018"]
    
    var imagedata : [String] = []
    var hazarddisplay = Int()
    
    //@IBOutlet weak var textViewHeight: NSLayoutConstraint!
    // @IBOutlet weak var textViewHeight: NSLayoutConstraint!
    
    var DataAPI = ReportedDataAPI()
    var ReportedDB : [ReportedDataModel] = []
    var data: String?
    var lastObject: String?
    var indPath = IndexPath()
    var ReportedLoadMoreDB : [ReportedDataModel] = []
      var DataLoadMoreAPI = ReportedDataAPILoadMore()
    
    @IBOutlet weak var tableView: UITableView!
    
    var ViewStr = String()
    var statusStr  = String()
    var hazardTypeStr  = String()
    var locationStr  = String()
    var AreaStr  = String()
    var unitStr  = String()
    var departmentStr  = String()
    
    var hazardtypeId  = String()
    
  
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.contentInset = UIEdgeInsetsMake(0.0, 0.0, 0.0, 0.0)
        tableView.delegate = self;
        tableView.dataSource = self;
        tableView.estimatedRowHeight = 290
        tableView.rowHeight = UITableViewAutomaticDimension
        
        
        refresh.addTarget(self, action: #selector(ReportdatabyStatus), for: .valueChanged)
        
        self.tableView.addSubview(refresh)
        let swipe = UISwipeGestureRecognizer(target: self, action: #selector(self.swipeRecognizer))
        swipe.direction = ([.down])
        self.view.addGestureRecognizer(swipe)
        tableView.tableFooterView = UIView()
        
        
        // ReportdatabyStatus()
        
        
        //barSetup()
        
        
        // Do any additional setup after loading the view.
    }
    @objc func swipeRecognizer(_ sender: UISwipeGestureRecognizer) {
        if (sender.direction == .down) {
            
            if(self.reachability.connection != .none)
            {
                
                self.ReportdatabyStatus()
            }
            else
            {
                self.startLoading(view: self.view)
                DispatchQueue.main.asyncAfter(deadline: .now() + 4.5) {
                    self.noDataLabel(text: "No internet connection found. Check your internet connection and try again.")
                    self.stopLoading(view: self.view)
                }
                
            }
            
        }
    }
    func setUp() {
        
        
   
        
      
            let status : String = UserDefaults.standard.value(forKey: "Status") as! String
        self.title = status
        //self.tabBarController?.tabBar.isHidden = true
        let nav = self.navigationController?.navigationBar
        nav?.tintColor = UIColor.white
       nav?.barTintColor = UIColor(hexString: "2c3e50", alpha: 1.0)
        nav?.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        ReportdatabyStatus()
    }
    override func viewDidAppear(_ animated: Bool) {
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        if(MoveStruct.isMove == true){
            MoveStruct.isMove = false
            self.view.makeToast(MoveStruct.message)
        }
        setUp()
        
        if(MoveStruct.isMove == true) {
            MoveStruct.isMove = false
            self.view.makeToast(MoveStruct.message)
            
        }
    }
    override func viewWillDisappear(_ animated: Bool) {
        
    }
    
  
    @objc func tapBack(_ sender : UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    @objc func ImageViewTapped(_ sender : UITapGestureRecognizer) {
        let buttonPosition:CGPoint = sender.view!.convert(.zero, to:self.tableView)
        let indexPath = self.tableView.indexPathForRow(at: buttonPosition)
        
        if ReportedDB[(indexPath?.section)!].Before_Pic != "" {
            
            let urlString = self.ReportedDB[(indexPath?.section)!].Before_Pic
            let urlShow = urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
            
//            let ZIVC = self.storyboard?.instantiateViewController(withIdentifier: "ImageZoom") as! ImageZoomViewController
//            ZIVC.zoomImageUrl = urlShow!
//            self.navigationController?.pushViewController(ZIVC, animated: true)
            
        }
    }
    
    @objc func tapCell(_ sender : UITapGestureRecognizer) {
        
        
        let buttonPosition:CGPoint = sender.view!.convert(.zero, to:self.tableView)
        let indexPath = self.tableView.indexPathForRow(at: buttonPosition)
       // let status : String = UserDefaults.standard.value(forKey: "Status") as! String
          let storyBoard = UIStoryboard(name: "NDSO", bundle: nil)
            let ZIVC = storyBoard.instantiateViewController(withIdentifier: "NDSORportDetailsViewController") as! NDSORportDetailsViewController
            ZIVC.ReportedDataDB = ReportedDB[(indexPath?.section)!]
          
            self.navigationController?.pushViewController(ZIVC, animated: true)
        
        
    }
    let reachability = Reachability()!
    var countID : Int = 0
    
    @objc func ReportdatabyStatus() {
        

        
        var para = [String:String]()
        let status : String = UserDefaults.standard.value(forKey: "Status") as! String
        let empId : String = UserDefaults.standard.string(forKey: "EmployeeID")!
         var url : String = ""
        print(status)
        var parameter : [String : String ] = [:]
        var  dictWithoutNilValues = [String:String]()
    
            
            dictWithoutNilValues = ["Employee_ID": empId  ]
            
            parameter = dictWithoutNilValues.filter { $0.value != ""}
            
            
            
        if(status == "Pending to assign"){
            url = URLConstants.Pending_to_Assign_Observation
            
            dictWithoutNilValues = ["Employee_ID": empId  ]
            
            parameter = dictWithoutNilValues.filter { $0.value != ""}
            
        }else if(status == "Assigned to me"){
                let statusData : String = UserDefaults.standard.value(forKey: "staOpenClose") as! String
           
            if(statusData == "Open"){
                url = URLConstants.Assigned_Observation_MY
                
                
                dictWithoutNilValues = ["Employee_ID": empId , "Observation_Status" : "Open" ]
                
                parameter = dictWithoutNilValues.filter { $0.value != ""}
            }else{
                url = URLConstants.Assigned_Observation_MY
                dictWithoutNilValues = ["Employee_ID": empId , "Observation_Status" : "Close" ]
                
                parameter = dictWithoutNilValues.filter { $0.value != ""}
            }
        }
            
            
       
       
        //para = parameter.filter { $0.value != ""}
       
       
        print(parameter)
        print(url)
        self.DataAPI.serviceCalling(obj: self, param: parameter , status: status, statusData: self.title! , url: url) { (dict) in
            
            self.ReportedDB = [ReportedDataModel]()
            self.ReportedDB = dict as! [ReportedDataModel]
            self.tableView.reloadData()
        }
        
    }
    @objc func callMyReportedReportedHazardLoadMore(Id:String) {
        
        var para = [String:String]()
        let status : String = UserDefaults.standard.value(forKey: "Status") as! String
        let empId : String = UserDefaults.standard.string(forKey: "EmployeeID")!
        var url : String = ""
        print(status)
        var parameter : [String : String ] = [:]
        var  dictWithoutNilValues = [String:String]()
        
        
        dictWithoutNilValues = ["Employee_ID": empId  ]
        
        parameter = dictWithoutNilValues.filter { $0.value != ""}
        
        
        
        if(status == "Pending to assign"){
            url = URLConstants.Pending_to_Assign_Observation
            
            dictWithoutNilValues = ["Employee_ID": empId  , "ID" : Id]
            
            parameter = dictWithoutNilValues.filter { $0.value != ""}
            
        }else if(status == "Assigned to me"){
            let statusData : String = UserDefaults.standard.value(forKey: "staOpenClose") as! String
            
            if(statusData == "Open"){
                url = URLConstants.Assigned_Observation_MY
                
                
                dictWithoutNilValues = ["Employee_ID": empId , "Observation_Status" : "Open" , "ID" : Id]
                
                parameter = dictWithoutNilValues.filter { $0.value != ""}
            }else{
                url = URLConstants.Assigned_Observation_MY
                dictWithoutNilValues = ["Employee_ID": empId , "Observation_Status" : "Close" , "ID" : Id]
                
                parameter = dictWithoutNilValues.filter { $0.value != ""}
            }
        }
        
     
        
        print(parameter)
        print(url)
        self.DataLoadMoreAPI.serviceCalling(obj: self, param: parameter , url : url) { (dict) in
            
            self.ReportedLoadMoreDB = [ReportedDataModel]()
            self.ReportedLoadMoreDB = dict as! [ReportedDataModel]
            
            switch dict.count {
            case 0:
                break;
            default:
                self.ReportedDB.append(contentsOf: self.ReportedLoadMoreDB)
                self.tableView.reloadData()
                break;
            }
            
        }
        
    }
    func imageResize (image:UIImage, sizeChange:CGSize)-> UIImage{
        
        let hasAlpha = true
        let scale: CGFloat = 0.0 // Use scale factor of main screen
        
        UIGraphicsBeginImageContextWithOptions(sizeChange, !hasAlpha, scale)
        image.draw(in: CGRect(origin: CGPoint.zero, size: sizeChange))
        
        let scaledImage = UIGraphicsGetImageFromCurrentImageContext()
        return scaledImage!
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    var imageData : Data? = nil
  
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
extension NDSOReportedDataViewController : UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if(section == 0) {
            return 10.0
        } else {
            return 20.0
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return ReportedDB.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    
}

extension NDSOReportedDataViewController : UITableViewDataSource {
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! NDSOReportedDataTableViewCell
        
        
        
        let statusTextViewTapGesture = UITapGestureRecognizer(target: self, action: #selector(self.tapCell(_:)))
        cell.addGestureRecognizer(statusTextViewTapGesture)
        cell.isUserInteractionEnabled = true
        statusTextViewTapGesture.numberOfTapsRequired = 1
        
        cell.textViewSuggestion.text = self.ReportedDB[indexPath.section].Suggestion
         cell.textViewObservation.text = self.ReportedDB[indexPath.section].Observation
        cell.lblID.text = "ID #"+String(describing: self.ReportedDB[indexPath.section].Observation_detail_ID)
        print(self.ReportedDB[indexPath.section].Unit_Name)
         print(self.ReportedDB[indexPath.section].Area_Name)
         print(self.ReportedDB[indexPath.section].Responsible_Dept_Name)
        cell.lblList.text = self.ReportedDB[indexPath.section].Location_Name + " > " + self.ReportedDB[indexPath.section].Unit_Name + " > " + self.ReportedDB[indexPath.section].Area_Name + " > " + self.ReportedDB[indexPath.section].Responsible_Dept_Name
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = NSTimeZone.system
        dateFormatter.dateFormat = "MMM dd yyyy HH:mm:ss:SSSa"
        let date = dateFormatter.date(from: self.ReportedDB[indexPath.section].Observation_Date)
       
        let dateFormatter2 = DateFormatter()
        dateFormatter2.timeZone = NSTimeZone.system
        dateFormatter2.dateFormat = "dd MMM yyyy"
        
        switch date {
        case nil:
            let date_TimeStr = dateFormatter2.string(from: Date())
            cell.lblDate.text = date_TimeStr
            break;
        default:
            let date_TimeStr = dateFormatter2.string(from: date!)
            
            cell.lblDate.text = date_TimeStr
            break;
        }
        
        let urlString = self.ReportedDB[indexPath.section].Before_Pic
        let imageView = UIImageView()
        
        let urlShow = urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
        if let url = NSURL(string: self.ReportedDB[indexPath.section].Before_Pic) {
            
            SDWebImageManager.shared().downloadImage(with: NSURL(string: urlShow!) as URL!, options: .continueInBackground, progress: {
                (receivedSize :Int, ExpectedSize :Int) in
                
            }, completed: {
                (image : UIImage?, error : Error?, cacheType : SDImageCacheType, finished : Bool, url : URL?) in
                if(image != nil) {
                    let size = CGSize(width: 120, height: 100)
                    let imageCell = self.imageResize(image: image!, sizeChange: size)
                    cell.imgData.image = imageCell
                    //  cell.viewImageHold.backgroundColor = UIColor(patternImage: imageCell)
                    
                }else{
                    imageView.image =  UIImage(named: "placed")
                    let size = CGSize(width: 120, height: 100)
                    let imageCell = self.imageResize(image: imageView.image!, sizeChange: size)
                    cell.imgData.image = imageCell
                    // cell.viewImageHold.backgroundColor = UIColor(patternImage: imageCell)
                }
            })
        }else{
            imageView.image =  UIImage(named: "placed")
            let size = CGSize(width: 120, height: 100)
            let imageCell = imageResize(image: imageView.image!, sizeChange: size)
            cell.imgData.image = imageCell
            //cell.viewImageHold.backgroundColor = UIColor(patternImage: imageCell)
        }
        
        
        
        
        //   cell.heightImage.constant = cell.heightRiskLevel.constant
        
        if ( self.data ==  self.lastObject && indexPath.section == self.ReportedDB.count - 1)
        {
            
            self.callMyReportedReportedHazardLoadMore( Id: String(self.ReportedDB[indexPath.section].Observation_detail_ID))
            
        }
        
        cell.separatorInset = UIEdgeInsetsMake(0, 10000, 0, 0);
        
        return cell
        
        
        
        
        
    }
}
