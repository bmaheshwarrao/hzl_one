//
//  CloseListViewController.swift
//  sesagoa
//
//  Created by SARVANG INFOTCH on 19/12/18.
//  Copyright © 2018 Bunga Maheshwar Rao. All rights reserved.
//

import UIKit
import SDWebImage
class CloseListViewController: CommonVSClass {
    var CloseListDB:[CloseList] = []
    var employeeId = String()
    var AreaName = String()
    var SubAreaName = String()
    var Hazardname = String()
    var hazardID = String()
    var desc = String()
    var dateVal = String()
    var status = String()
    var imgdata = String()
    var employeeName = String()
    var riskLevel = String()
    var notify = Int()
    var ZoneName = String()
    var UnitName = String()
    var CloseListAPI = CloseListData()
     @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
       
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
        tableView.tableFooterView?.backgroundColor = UIColor.groupTableViewBackground
        // Do any additional setup after loading the view.
        refresh.addTarget(self, action: #selector(CloseProcessList), for: .valueChanged)
        let swipe = UISwipeGestureRecognizer(target: self, action: #selector(self.swipeRecognizer))
        swipe.direction = ([.down])
        self.view.addGestureRecognizer(swipe)
        
        
        self.tableView.addSubview(refresh)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    var reachability = Reachability()!
    @objc func swipeRecognizer(_ sender: UISwipeGestureRecognizer) {
        if (sender.direction == .down) {
            
            if(self.reachability.connection != .none)
            {
                
                self.CloseProcessList()
            }
            else
            {
                self.startLoading(view: self.view)
                DispatchQueue.main.asyncAfter(deadline: .now() + 4.5) {
                    self.noDataLabel(text: "No internet connection found. Check your internet connection and try again.")
                    self.stopLoading(view: self.view)
                }
                
            }
            
        }
    }
    @IBAction func btnProceesClicked(_ sender: UIButton) {
        
        let storyBoard = UIStoryboard(name: "ThemeSafety", bundle: nil)
        
        let ZIVC = storyBoard.instantiateViewController(withIdentifier: "CloseViewController") as! CloseViewController
        
        ZIVC.Hazardname = Hazardname
        ZIVC.AreaName = AreaName
        ZIVC.SubAreaName = SubAreaName
        ZIVC.employeeId = employeeId
        ZIVC.employeeName = employeeName
        ZIVC.notify = 0
        ZIVC.dateVal = dateVal
        ZIVC.desc = desc
        ZIVC.status = status
        ZIVC.hazardID = hazardID
        ZIVC.UnitName = UnitName
        ZIVC.ZoneName = ZoneName
        ZIVC.imgdata = imgdata
        ZIVC.riskLevel = riskLevel
        self.navigationController?.pushViewController(ZIVC, animated: true)
        
    }
    @objc func CloseProcessList() {
   
        var  parameter = [String:String]()
       
            parameter = ["Hazard_ID":hazardID]
        
        
        
        var para = [String:String]()
        // let parameter = ["emp_ID":UserDefaults.standard.string(forKey: "EmployeeID")!]
        
        para = parameter.filter { $0.value != ""}
        print("para",para)
        self.CloseListAPI.serviceCalling(obj: self, parameter: para) { (dict) in
            
            self.CloseListDB = [CloseList]()
            self.CloseListDB = dict as! [CloseList]
            print(self.CloseListDB)
           
            self.tableView.reloadData()
        }
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        if(MoveStruct.message == "Successfully Closed Post."){
            if(MoveStruct.isMove == true){
                //  self.view.makeToast(MoveStruct.message)
                MoveStruct.isMove = true;
                self.navigationController?.popViewController(animated: true)
            }
        }else{
            
            
            if(MoveStruct.isMove == true){
                self.view.makeToast(MoveStruct.message)
                MoveStruct.isMove = false;
            }
        }
        
        
        
        self.title = "Process Hazard"
        self.tabBarController?.tabBar.isHidden = true
        let nav = self.navigationController?.navigationBar
        nav?.tintColor = UIColor.white
       nav?.barTintColor = UIColor(hexString: "2c3e50", alpha: 1.0)
        nav?.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        tableView.delegate = self;
        tableView.dataSource = self;
        tableView.tableFooterView = UIView()
        
        
        CloseProcessList()
        
        
        
    }
    func imageResize (image:UIImage, sizeChange:CGSize)-> UIImage{
        
        let hasAlpha = true
        let scale: CGFloat = 0.0 // Use scale factor of main screen
        
        UIGraphicsBeginImageContextWithOptions(sizeChange, !hasAlpha, scale)
        image.draw(in: CGRect(origin: CGPoint.zero, size: sizeChange))
        
        let scaledImage = UIGraphicsGetImageFromCurrentImageContext()
        return scaledImage!
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
extension CloseListViewController : UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if(section == 0) {
            return 10.0
        } else {
            return 20.0
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return CloseListDB.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    
}

extension CloseListViewController : UITableViewDataSource {
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! CloseListTableViewCell
        
  
        
        //  cell.containerView.layer.cornerRadius = 8
        
        
        
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = NSTimeZone.system
        dateFormatter.dateFormat = "MMM dd yyyy"
        let date = dateFormatter.date(from: self.CloseListDB[indexPath.section].Date_Time!)
        print(self.CloseListDB[indexPath.section].Date_Time!)
        let dateFormatter2 = DateFormatter()
        dateFormatter2.timeZone = NSTimeZone.system
        dateFormatter2.dateFormat = "dd MMM yyyy"
        var dateStrStart = String()
        switch date {
        case nil:
            let date_TimeStr = dateFormatter2.string(from: Date())
            dateStrStart = date_TimeStr
            break;
        default:
            let date_TimeStr = dateFormatter2.string(from: date!)
            
            dateStrStart = date_TimeStr
            break;
        }
        cell.dateLabel.text = dateStrStart
      
      

        
        
        
        
        
        
        
        
        
        cell.employeeLabel.tag = indexPath.section
        
        
        cell.statusOfHazard.text =  CloseListDB[indexPath.section].status!
        cell.idOfHazard.text = "ID #"+String(describing:  CloseListDB[indexPath.section].ID)
        
        let paragraph1 = NSMutableParagraphStyle()
        paragraph1.alignment = .left
        paragraph1.lineSpacing = 0
        let titleStr = NSMutableAttributedString(string: CloseListDB[indexPath.section].Remark!, attributes: [NSAttributedStringKey.foregroundColor: UIColor.black, NSAttributedStringKey.font: UIFont.systemFont(ofSize: CGFloat(15.0))])
        titleStr.addAttribute(NSAttributedStringKey.paragraphStyle, value: paragraph1, range: NSRange(location: 0, length: titleStr.length))
        cell.titleTextView.attributedText = titleStr
        
        cell.employeeLabel.text = CloseListDB[indexPath.section].Employee_Name!
        switch cell.statusOfHazard.text! {
        case "Close":
            cell.statusOfHazard.textColor = UIColor.colorwithHexString("#5c9834", alpha: 1)
            break;
        case "Pending":
            cell.statusOfHazard.textColor = UIColor.colorwithHexString("#cfcf00", alpha: 1)
            break;
        case "Reject":
            cell.statusOfHazard.textColor = UIColor.colorwithHexString("#FF0000", alpha: 1)
            break;
        default:
            break;
        }
        
       
        
   
        
        // cell.titleTextView.text = self.hazardDB[indexPath.section].descriptionn
        cell.titleTextView.font = UIFont.systemFont(ofSize: 15.0)
      
        
        
        
        
        
        
        
        
    
        cell.titleTextView.isEditable = false
        cell.titleTextView.isSelectable = false
        cell.titleTextView.isScrollEnabled = false
        cell.titleTextView.textContainer.maximumNumberOfLines = 3
        cell.titleTextView.textContainer.lineBreakMode = .byTruncatingTail
        cell.titleTextView.textColor = UIColor.colorwithHexString("295890", alpha: 1.0)
        cell.selectionStyle = .none
        cell.image1left.constant = self.view.frame.width/3
        cell.image2left.constant = self.view.frame.width/3
        
       
        //  cell.heightImage.constant = cell.viewImageHeight.constant
        
        var v1 = 0
        var v2 = 0
        let urlString = self.CloseListDB[indexPath.section].image_path!
        let imageView = UIImageView()
        
        let urlShow = urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
        if let url = NSURL(string: self.CloseListDB[indexPath.section].image_path!) {
            
            SDWebImageManager.shared().downloadImage(with: NSURL(string: urlShow!) as URL!, options: .continueInBackground, progress: {
                (receivedSize :Int, ExpectedSize :Int) in
                
            }, completed: {
                (image : UIImage?, error : Error?, cacheType : SDImageCacheType, finished : Bool, url : URL?) in
                if(image != nil) {
                    let size = CGSize(width: 100, height: 100)
                    let imageCell = self.imageResize(image: image!, sizeChange: size)
                    cell.imageFile.image = imageCell
                    //  cell.viewImageHold.backgroundColor = UIColor(patternImage: imageCell)
                    
                }else{
                    imageView.image =  UIImage(named: "placed")
                    let size = CGSize(width: 100, height: 100)
                    let imageCell = self.imageResize(image: imageView.image!, sizeChange: size)
                    cell.imageFile.image = imageCell
                    // cell.viewImageHold.backgroundColor = UIColor(patternImage: imageCell)
                }
            })
        }else{
            imageView.image =  UIImage(named: "placed")
            let size = CGSize(width: 100, height: 100)
            let imageCell = imageResize(image: imageView.image!, sizeChange: size)
            cell.imageFile.image = imageCell
            //cell.viewImageHold.backgroundColor = UIColor(patternImage: imageCell)
        }
        
     
        cell.separatorInset = UIEdgeInsetsMake(0, 10000, 0, 0);
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        return cell;
    }
}
