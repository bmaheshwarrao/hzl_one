//
//  AllTopPendencyViewController.swift
//  Balco Sustainability Plus
//
//  Created by sudheer-kumar on 17/03/18.
//  Copyright © 2018 safiqul islam. All rights reserved.
//

import UIKit
import Parchment
import ViewPager_Swift

class AllTopPendencyViewController: CommonVSClass {

    @IBOutlet weak var viewHandler: UIView!
    
    
    @IBOutlet weak var filterMenuItem: UIBarButtonItem!
    //Filter
    
    @IBOutlet weak var firstthCon: NSLayoutConstraint!
    
    @IBOutlet weak var secondThCon: NSLayoutConstraint!
    @IBOutlet weak var thirdthCon: NSLayoutConstraint!
    
    @IBOutlet weak var forthcon: NSLayoutConstraint!
    @IBOutlet weak var filterAppcon: NSLayoutConstraint!
    @IBOutlet weak var hazardTypeLabel: UILabel!
    @IBOutlet weak var locationTypeLabel: UILabel!
    @IBOutlet weak var fromDateLabel: UILabel!
    @IBOutlet weak var toDateLabel: UILabel!
    @IBOutlet weak var filterAppLabel: UILabel!
    @IBOutlet weak var viewOf: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
//        let storyboard = UIStoryboard(name: "Main", bundle: nil)
//        let TopTenCloser = storyboard.instantiateViewController(withIdentifier: "TopPendencyForAssigning")
//        let TopTenReported = storyboard.instantiateViewController(withIdentifier: "TopPendencyForClosure")
//        
//        TopTenCloser.title = "FOR ASSIGNING"
//        TopTenReported.title = "FOR CLOSURE"
//        
//        views = [TopTenCloser,TopTenReported]
//        
//        let pagingViewController = FixedPagingViewController.init(viewControllers: views , options: PendencyIconsPagingSustainability() as PagingOptions)
//        
//        
//        self.addChildViewController(pagingViewController)
//        self.view.addSubview(pagingViewController.view)
//        self.view.constrainToEdges(pagingViewController.view)
//        pagingViewController.didMove(toParentViewController: self)
        
        createDesign()
        
    }
    
    
    func createDesign() {
        
        
        
        
        let  myOptions = ViewPagerOptions(viewPagerWithFrame: CGRect(x: 0, y: 0, width: self.view.frame.width + 1, height: self.view.frame.height))
        
        myOptions.tabType = ViewPagerTabType.basic
        
        
        
        
        
        
        myOptions.isTabHighlightAvailable = false
        myOptions.tabViewTextFont = UIFont.systemFont(ofSize: 15.0, weight: .medium)
        // If I want indicator bar to show below current page tab
        myOptions.isTabIndicatorAvailable = true
        myOptions.fitAllTabsInView = true;
        // Oh! and let's change color of tab to red
        myOptions.tabIndicatorViewBackgroundColor = UIColor.white
        myOptions.tabViewBackgroundDefaultColor = UIColor(hexString: "2c3e50", alpha: 1.0)!
        
        myOptions.tabViewTextDefaultColor = UIColor.white
        // myOptions.tabViewTextHighlightColor = UIColor.yellow
        let viewPager = ViewPagerController()
        // viewPager.view.backgroundColor = UIColor.red
        viewPager.options = myOptions
        viewPager.dataSource = self
        
        //Now let me add this to my viewcontroller
        self.addChildViewController(viewPager)
        
        self.viewHandler.addSubview(viewPager.view)
       // self.viewHandler.constrainToEdges(viewPager.view)
       
        
        // self.view.addSubview(viewPager.view)
        viewPager.didMove(toParentViewController: self)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.tabBarController?.tabBar.isHidden = true
        self.title = "Top Pendency"
        switch FilterPendencyStruct.toppendencyFlag {
        case "PendancyHazard":
            filterMenuItem.tintColor = UIColor.white
            break;
        default:
            filterMenuItem.tintColor = UIColor.clear
            break;
        }
        
        self.viewContentShape()
        
        //        let nav = self.navigationController?.navigationBar
        //        nav?.tintColor = UIColor.white
        //       nav?.barTintColor = UIColor(hexString: "2c3e50", alpha: 1.0)
        //        nav?.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        
    }
    
    //    override func viewWillDisappear(_ animated: Bool) {
    //        super.viewWillDisappear(true)
    //
    //        FilterPendencyStruct.fromDate = String()
    //        FilterPendencyStruct.toDate = String()
    //        FilterPendencyStruct.hazard_Name_ID = String()
    //        FilterPendencyStruct.business_NameID = String()
    //        FilterPendencyStruct.location_ID = String()
    //        FilterPendencyStruct.area_ID = String()
    //        FilterPendencyStruct.sub_area_ID = String()
    //    }
    
    func viewContentShape(){
       
        
        
        switch FilterPendencyStruct.sub_Area_Name {
        case "":
            FilterPendencyStruct.locationType = String()
            break;
        default:
            if FilterPendencyStruct.sub_Area_Name != "" {
                FilterPendencyStruct.locationType = "Area location : "+FilterPendencyStruct.sub_Area_Name
            }else{
                //
            }
            
            break;
        }
        switch FilterPendencyStruct.area_Name {
        case "":
            FilterPendencyStruct.locationType = String()
            break;
        default:
            if FilterPendencyStruct.sub_Area_Name == "" {
                FilterPendencyStruct.locationType = "Area : "+FilterPendencyStruct.area_Name
            }else{
                //
            }
            
            break;
        }
        switch FilterPendencyStruct.unit_Name {
        case "":
            FilterPendencyStruct.locationType = String()
            break;
        default:
            if FilterPendencyStruct.area_Name == "" {
                FilterPendencyStruct.locationType = "Unit : "+FilterPendencyStruct.unit_Name
            } else{
                
            }
            
            break;
        }
        switch FilterPendencyStruct.location_name {
        case "":
            FilterPendencyStruct.locationType = String()
            break;
        default:
            if FilterPendencyStruct.location_name != "" && FilterPendencyStruct.unit_Name == "" && FilterPendencyStruct.area_Name == "" && FilterPendencyStruct.sub_Area_Name == "" {
                FilterPendencyStruct.locationType = "Zone : "+FilterPendencyStruct.location_name
            }else{
                //
            }
            
            break;
        }
        
        switch FilterPendencyStruct.hazard_Name_ID {
        case "":
            FilterPendencyStruct.hazard_NameByFilter = String()
            break;
        default:
            FilterPendencyStruct.hazard_NameByFilter = FilterPendencyStruct.hazard_Name
            break;
        }
        
        
        if FilterPendencyStruct.hazard_NameByFilter.isEmpty == true{
            self.firstthCon.constant = 0
            self.hazardTypeLabel.text = ""
        }else{
            self.firstthCon.constant = 5
            self.hazardTypeLabel.text = "Hazard Type : "+FilterPendencyStruct.hazard_NameByFilter
        }
        
        if FilterPendencyStruct.locationType.isEmpty == true{
            self.secondThCon.constant = 0
            self.locationTypeLabel.text = ""
        }else{
            self.secondThCon.constant = 5
            self.locationTypeLabel.text = FilterPendencyStruct.locationType
        }
        
        if FilterPendencyStruct.isHazardView.isEmpty == true {
            self.thirdthCon.constant = 0
            self.fromDateLabel.text = ""
        }else{
            
            self.thirdthCon.constant = 5
            self.fromDateLabel.text = "View : "+FilterPendencyStruct.isHazardView
        }
        
        if FilterPendencyStruct.toDate.isEmpty == true && FilterPendencyStruct.fromDate.isEmpty == true{
            self.forthcon.constant = 0
            self.toDateLabel.text = ""
        }else{
            self.forthcon.constant = 5
            self.toDateLabel.text = "From Date : "+FilterPendencyStruct.fromDate+"   To Date : "+FilterPendencyStruct.toDate
        }
        
        if FilterPendencyStruct.hazard_NameByFilter.isEmpty == true && FilterPendencyStruct.locationType.isEmpty == true && FilterPendencyStruct.isHazardView.isEmpty == true && FilterPendencyStruct.toDate.isEmpty == true  {
            
            self.filterAppcon.constant = 0
            self.viewOf.isHidden = true
            self.filterAppLabel.text = ""
            self.viewOf.frame.size.height = 0
            
        }else{
            
            self.viewOf.isHidden = false
            self.filterAppcon.constant = 10
            self.filterAppLabel.text = "Filter Applied"
            self.filterAppLabel.font = UIFont.boldSystemFont(ofSize: 14)
        }
        if(self.viewOf.isHidden == true) {
            self.view.backgroundColor = UIColor(hexString: "2c3e50", alpha: 1.0)
            self.view.layer.borderWidth = 0.5
            self.view.layer.borderColor = UIColor.white.cgColor
        } else {
            self.viewHandler.backgroundColor = UIColor(hexString: "2c3e50", alpha: 1.0)
            self.view.backgroundColor = UIColor(hexString: "2c3e50", alpha: 1.0)
            self.viewHandler.layer.borderWidth = 0.5
            self.viewHandler.layer.borderColor = UIColor.white.cgColor
            self.view.layer.borderWidth = 0.5
            self.view.layer.borderColor = UIColor.white.cgColor
        }
          
        
    }
    
    @IBOutlet weak var filterPendancyBtn: UIBarButtonItem!
    @IBAction func filterPendancyClicked(_ sender: UIBarButtonItem) {
        let TopPendencyFilterVC = self.storyboard?.instantiateViewController(withIdentifier: "TopPendencyFilter") as! TopPendencyFilterViewController
        
        TopPendencyFilterVC.identifierStr = "PendancyHazard"
        self.navigationController?.pushViewController(TopPendencyFilterVC, animated: true)
    }
//    override func viewWillAppear(_ animated: Bool) {
//        super.viewWillAppear(true)
//
//        self.tabBarController?.tabBar.isHidden = true
//
//
//        self.title = "Top Pendency"
//
//
//
//    }
    
   
    
}


extension AllTopPendencyViewController : ViewPagerControllerDataSource {
    func numberOfPages() -> Int {
        return 2
    }
    
    func viewControllerAtPosition(position: Int) -> UIViewController {
        let storyBoard : UIStoryboard = UIStoryboard(name: "ThemeSafety",bundle : nil)
        if(position == 0) {
            
            let assignVC = storyBoard.instantiateViewController(withIdentifier: "TopPendencyForAssigning") as! TopPendencyForAssigningViewController
            return  assignVC as CommonVSClass
        }else {
            let closeVC = storyBoard.instantiateViewController(withIdentifier: "TopPendencyForClosure") as! TopPendencyForClosureViewController
            return  closeVC as CommonVSClass
        }
    }
    
    func tabsForPages() -> [ViewPagerTab] {
        return [ViewPagerTab(title: "NOT YET ASSIGNED" , image: #imageLiteral(resourceName: "BackBlack")) , ViewPagerTab(title: "NOT YET CLOSED" , image: #imageLiteral(resourceName: "BackBlack"))]
    }
    
    
}
extension AllTopPendencyViewController : ViewPagerControllerDelegate {
    
}
