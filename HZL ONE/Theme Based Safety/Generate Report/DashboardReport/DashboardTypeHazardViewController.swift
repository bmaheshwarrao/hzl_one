//
//  DashboardTypeHazardViewController.swift
//  Balco Sustainability Plus
//
//  Created by sudheer-kumar on 20/01/18.
//  Copyright © 2018 safiqul islam. All rights reserved.
//

import UIKit

class DashboardTypeHazardViewController: CommonVSClass,UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var tableView: UITableView!
    
    var DashboardTypeAPI = DashboardTypeHazardAPI()
    var DashboardDB:[DashboardTypeHazardDataModel] = []
    
    var filterWithTypeName = Bool()
    var verifyWhere = String()
    var filterBylocation_ID = String()
    var filterBylocation_Name = String()
    var viewSorting = String()
    var iiii = Int()
    override func viewDidLoad() {
        super.viewDidLoad()
        refresh.addTarget(self, action: #selector(self.callFilterWithTypenameDashboardTypeAPI), for: .valueChanged)
        self.tableView.addSubview(refresh)
        FilterGraphStruct.isHazardView = "Hazard"
        UserDefaults.standard.set(false, forKey: "fromDashboardTypeFilter")
        
        //
        switch UserDefaults.standard.bool(forKey: "filterWithTypeName") {
        case true:
            
//            if FilterGraphStruct.hazard_Name_ID != "" && FilterGraphStruct.business_NameID == ""
//            {
//                verifyWhere = "Business"
//                NotificationCenter.default.addObserver(self, selector: #selector(self.callBusinessDashboardTypeAPI), name: NSNotification.Name?.init(NSNotification.Name(rawValue: "BusinessdashboardTypeHazardFilter")), object: nil)
//            }
//            else if FilterGraphStruct.business_NameID != "" && FilterGraphStruct.location_ID == ""
//            {
//                verifyWhere = "Location"
//                filterBylocation_ID = FilterGraphStruct.filterBylocation_ID
//                filterBylocation_Name = "Business"
//                 NotificationCenter.default.addObserver(self, selector: #selector(self.callLocationDashboardTypeAPI), name: NSNotification.Name?.init(NSNotification.Name(rawValue: "LocationdashboardTypeHazardFilter")), object: nil)
//            }
//             if  FilterGraphStruct.area_ID == ""
//            {
//                verifyWhere = "Area"
//                filterBylocation_ID = FilterGraphStruct.filterBylocation_ID
//                filterBylocation_Name = "Location"
//                NotificationCenter.default.addObserver(self, selector: #selector(self.callAreaDashboardTypeAPI), name: NSNotification.Name?.init(NSNotification.Name(rawValue: "AreadashboardTypeHazardFilter")), object: nil)
//            }
            if FilterGraphStruct.area_ID != ""
            {
                verifyWhere = "Sub-area"
               // filterBylocation_ID = FilterGraphStruct.area_ID
                filterBylocation_Name = "Area"
            }
            else
            {
                //
            }
            
            callFilterWithTypenameDashboardTypeAPI()
            break;
        default:
             NotificationCenter.default.addObserver(self, selector: #selector(self.callDashboardTypeAPI), name: NSNotification.Name?.init(NSNotification.Name(rawValue: "dashboardTypeHazardFilter")), object: nil)
            callDashboardTypeAPI()
            //FilterGraphStruct.isHazardView = "Hazard"
            break;
        }
        
        tableView.delegate = self
        tableView.dataSource = self
        
        let swipe = UISwipeGestureRecognizer(target: self, action: #selector(self.swipeRecognizer))
        swipe.direction = ([.down])
        self.view.addGestureRecognizer(swipe)
//        tableView.estimatedRowHeight = 135
//        tableView.rowHeight = UITableViewAutomaticDimension
        
       
        
    }
    
    @objc func swipeRecognizer(_ sender: UISwipeGestureRecognizer) {
        if (sender.direction == .down) {
            
            if(self.reachability.connection != .none)
            {
                
                self.callDashboardTypeAPI()
            }
            else
            {
                self.startLoading(view: self.view)
                DispatchQueue.main.asyncAfter(deadline: .now() + 4.5) {
                    self.noDataLabel(text: "No internet connection found. Check your internet connection and try again.")
                    self.stopLoading(view: self.view)
                }
                
                
            }
            
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(true)
        self.tabBarController?.tabBar.isHidden = true
        self.title = "Report Dashboard"
        
        
        switch FilterGraphStruct.RiskLevel {
        case "Risk Level":
            FilterGraphStruct.minRiskLevel = String()
            FilterGraphStruct.mixRiskLevel = String()
            break;
        default:
            break;
        }
       
        switch verifyWhere {
//        case "Business":
//            print("Business")
//        //    FilterGraphStruct.business_NameID = String()
//            //FilterGraphStruct.business_Name = String()
//         //   FilterGraphStruct.location_ID = String()
//         //   FilterGraphStruct.location_Name = String()
//            FilterGraphStruct.area_ID = String()
//            FilterGraphStruct.area_Name = String()
//            filterBylocation_Name = String()
//            if FilterGraphStruct.isHazardView == "Hazard" {
//
//            }else{
//                FilterGraphStruct.isHazardView = "Location"
//            }
//            FilterGraphStruct.flagOfReportFilte = String()
//
//            break;
//        case "Location":
//            print("Location")
//          //  FilterGraphStruct.location_ID = String()
//            FilterGraphStruct.area_ID = String()
//            FilterGraphStruct.area_Name = String()
//            filterBylocation_Name = "Business"
//            if FilterGraphStruct.isHazardView == "Hazard" {
//
//            }else{
//                FilterGraphStruct.isHazardView = "Location"
//            }
//
//
//            break;
        case "Area":
            print("Area")
            FilterGraphStruct.area_ID = String()
            if FilterGraphStruct.isHazardView == "Hazard" {
                
            }else{
                FilterGraphStruct.isHazardView = "Location"
            }
            filterBylocation_Name = "Location"
            
            
            break;
        case "Sub-area":
            print("Sub-area")
            filterBylocation_Name = "Area"
            if FilterGraphStruct.isHazardView == "Hazard" {
                
            }else{
                FilterGraphStruct.isHazardView = "Location"
            }
            
            break;
        default:
            
            if FilterGraphStruct.flagOfReportFilte == "reportFilter"{
                filterBylocation_Name = FilterGraphStruct.area_ID
            }else{
                
                FilterGraphStruct.hazard_Name_ID = String()
               // FilterGraphStruct.business_NameID = String()
               // FilterGraphStruct.location_ID = String()
                FilterGraphStruct.area_ID = String()
                filterBylocation_ID = String()
                filterBylocation_Name = String()
                
                
            }
            
            UserDefaults.standard.set(false, forKey: "filterWithTypeName")
            print("parent")
            break;
        }
        
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        
        UserDefaults.standard.set(false, forKey: "fromDashboardTypeFilter")
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
         if section == 0 {
            return 1
         }else if section == 1{
            return 1
         }else if section == 2{
            return DashboardDB.count
         } else{
            return 1
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0{
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "filter", for: indexPath) as! DashboardTypeHazardFilterTableViewCell
            
            switch FilterGraphStruct.sub_area_ID {
            case "":
                FilterGraphStruct.locationType = String()
                break;
            default:
                if  FilterGraphStruct.area_ID != "" {
                    FilterGraphStruct.locationType = "Sub-Area : "+FilterGraphStruct.sub_area_Name
                }else{
                    //
                }
                
                break;
            }
            switch FilterGraphStruct.area_ID {
            case "":
                FilterGraphStruct.locationType = String()
                break;
            default:
                if  FilterGraphStruct.sub_area_ID == "" {
                    FilterGraphStruct.locationType = "Area : "+FilterGraphStruct.area_Name
                }else{
                    //
                }
                
                break;
            }
//            switch FilterGraphStruct.location_ID {
//            case "":
//                FilterGraphStruct.locationType = String()
//                break;
//            default:
//                if FilterGraphStruct.business_NameID != "" && FilterGraphStruct.area_ID == "" && FilterGraphStruct.sub_area_ID == "" {
//                    FilterGraphStruct.locationType = "Location : "+FilterGraphStruct.location_Name
//                }else{
//                    //
//                }
//
//                break;
//            }
//            switch FilterGraphStruct.business_NameID {
//            case "":
//                FilterGraphStruct.locationType = String()
//                break;
//            default:
//                if FilterGraphStruct.location_ID == "" && FilterGraphStruct.area_ID == "" && FilterGraphStruct.sub_area_ID == "" {
//                    FilterGraphStruct.locationType = "Business : "+FilterGraphStruct.business_Name
//                }else{
//                    //
//                }
//
//                break;
//            }
            
            switch FilterGraphStruct.hazard_Name_ID {
            case "":
                FilterGraphStruct.hazard_NameByFilter = String()
                break;
            default:
                
                break;
            }
            
            
                if FilterGraphStruct.hazard_Name_ID == ""{
                    cell.firstthCon.constant = 0
                    cell.hazardTypeLabel.text = ""
                }else{
                    cell.firstthCon.constant = 5
                    cell.hazardTypeLabel.text = "Hazard Type : "+FilterGraphStruct.hazard_Name
                }
            
            switch FilterGraphStruct.department_NameID {
            case "":
                FilterGraphStruct.locationType = String()
                break;
            default:
                if FilterGraphStruct.location_ID != "" && FilterGraphStruct.area_ID != "" && FilterGraphStruct.sub_area_ID != "" {
                    FilterGraphStruct.locationType = "Department : "+FilterGraphStruct.department_Name
                }else{
                    //
                }
                
                break;
            }
            switch FilterGraphStruct.sub_area_ID {
            case "":
                FilterGraphStruct.locationType = String()
                break;
            default:
                if FilterGraphStruct.department_NameID == "" {
                    FilterGraphStruct.locationType = "Sub-Area : "+FilterGraphStruct.sub_area_Name
                }else{
                    //
                }
                
                break;
            }
            switch FilterGraphStruct.area_ID {
            case "":
                FilterGraphStruct.locationType = String()
                break;
            default:
                if FilterGraphStruct.sub_area_ID == "" {
                    FilterGraphStruct.locationType = "Area : "+FilterGraphStruct.area_Name
                }else{
                    //
                }
                
                break;
            }
            switch FilterGraphStruct.location_ID {
            case "":
                FilterGraphStruct.locationType = String()
                break;
            default:
                if FilterGraphStruct.location_ID != "" && FilterGraphStruct.area_ID == "" && FilterGraphStruct.sub_area_ID == "" {
                    FilterGraphStruct.locationType = "Location : "+FilterGraphStruct.location_Name
                }else{
                    //
                }
                
                break;
            }
            
            
            
            
            
                if FilterGraphStruct.locationType == ""{
                    cell.secondThCon.constant = 0
                    cell.locationTypeLabel.text = ""
                }else{
                    cell.secondThCon.constant = 5
                    cell.locationTypeLabel.text = FilterGraphStruct.locationType
                }
                
                if FilterGraphStruct.isHazardView == "" && viewSorting == ""{
                    cell.thirdthCon.constant = 0
                    cell.fromDateLabel.text = ""
                }else{
                    cell.thirdthCon.constant = 5
                    cell.fromDateLabel.text = "View : "+FilterGraphStruct.isHazardView
                }
                
                if FilterGraphStruct.toDate == "" && FilterGraphStruct.fromDate == ""{
                    cell.forthcon.constant = 0
                    cell.toDateLabel.text = ""
                }else{
                    cell.forthcon.constant = 5
                    cell.toDateLabel.text = "From Date : "+FilterGraphStruct.fromDate+"   To Date : "+FilterGraphStruct.toDate
                }
            
                if FilterGraphStruct.hazard_NameByFilter == "" && FilterGraphStruct.locationType == "" && FilterGraphStruct.isHazardView == "" && FilterGraphStruct.toDate == "" && viewSorting == ""{
                    
                    cell.filterAppcon.constant = 0
                    cell.viewOf.isHidden = true
                    cell.filterAppLabel.text = ""
                    cell.viewOf.frame.size.height = 0
                    
                }else{
                    
                    cell.viewOf.isHidden = false
                    cell.filterAppcon.constant = 10
                    cell.filterAppLabel.text = "Filter Applied"
                    cell.filterAppLabel.font = UIFont.boldSystemFont(ofSize: 14)
                }
            if(cell.viewOf.isHidden == true) {
                self.view.backgroundColor = UIColor.white
                self.view.layer.borderWidth = 0.5
                self.view.layer.borderColor = UIColor.white.cgColor
            } else {
                //self.viewHandler.backgroundColor = UIColor.white
                self.view.backgroundColor = UIColor.white
               
                self.view.layer.borderWidth = 0.5
                self.view.layer.borderColor = UIColor.white.cgColor
            }
            
            
            return cell
            
        }
        
        if indexPath.section == 1{
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
            
            return cell
            
        }
        
        if indexPath.section == 2 {
            
        let cell = tableView.dequeueReusableCell(withIdentifier: "DashboardType", for: indexPath) as! DashboardTypeHazardTableViewCell
            
            
            cell.TypeNameLabel.tag = indexPath.row
            cell.YesterdayLabel.tag = indexPath.row
            cell.MTDLabel.tag = indexPath.row
            cell.YTDLabel.tag = indexPath.row
            cell.ClosedLabel.tag = indexPath.row
            cell.openLabel.tag = indexPath.row
            
            //name
//            let nameTapGesture = UITapGestureRecognizer(target: self, action: #selector(self.nameLabelTapped(_:)))
//            cell.TypeNameLabel.addGestureRecognizer(nameTapGesture)
//            cell.TypeNameLabel.isUserInteractionEnabled = true
//            nameTapGesture.numberOfTapsRequired = 1
            
            //Yesterday
            let YesterdayTapGesture = UITapGestureRecognizer(target: self, action: #selector(self.YesterdayLabelTapped(_:)))
            cell.YesterdayLabel.addGestureRecognizer(YesterdayTapGesture)
            cell.YesterdayLabel.isUserInteractionEnabled = true
            YesterdayTapGesture.numberOfTapsRequired = 1
            
            //MTD
            let MTDTapGesture = UITapGestureRecognizer(target: self, action: #selector(self.MTDLabelTapped(_:)))
            cell.MTDLabel.addGestureRecognizer(MTDTapGesture)
            cell.MTDLabel.isUserInteractionEnabled = true
            MTDTapGesture.numberOfTapsRequired = 1
            
            //YTD
            let YTDTapGesture = UITapGestureRecognizer(target: self, action: #selector(self.YTDLabelTapped(_:)))
            cell.YTDLabel.addGestureRecognizer(YTDTapGesture)
            cell.YTDLabel.isUserInteractionEnabled = true
            YTDTapGesture.numberOfTapsRequired = 1
            
            //Closed
            let ClosedTapGesture = UITapGestureRecognizer(target: self, action: #selector(self.ClosedLabelTapped(_:)))
            cell.ClosedLabel.addGestureRecognizer(ClosedTapGesture)
            cell.ClosedLabel.isUserInteractionEnabled = true
            ClosedTapGesture.numberOfTapsRequired = 1
            
            //open
            let openTapGesture = UITapGestureRecognizer(target: self, action: #selector(self.openLabelTapped(_:)))
            cell.openLabel.addGestureRecognizer(openTapGesture)
            cell.openLabel.isUserInteractionEnabled = true
            openTapGesture.numberOfTapsRequired = 1
            
            
            
            cell.TypeNameLabel.text = DashboardDB[indexPath.row].TypeName
            cell.YesterdayLabel.text = String(DashboardDB[indexPath.row].Yesterday)
            cell.openLabel.text = String(DashboardDB[indexPath.row].Open)
            
            cell.MTDLabel.text = String(DashboardDB[indexPath.row].MTD)
            cell.YTDLabel.text = String(DashboardDB[indexPath.row].YTD)
            cell.ClosedLabel.text = String(DashboardDB[indexPath.row].Closed)
        
        return cell
            
        }else {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "total", for: indexPath) as! DashboardTypeHazradTotalTableViewCell
            
          
            
            //Yesterday
            let YesterdayTapGesture = UITapGestureRecognizer(target: self, action: #selector(self.YesterdayLabelTotalTapped(_:)))
            cell.YesterdayLabel.addGestureRecognizer(YesterdayTapGesture)
            cell.YesterdayLabel.isUserInteractionEnabled = true
            YesterdayTapGesture.numberOfTapsRequired = 1
            
            //MTD
            let MTDTapGesture = UITapGestureRecognizer(target: self, action: #selector(self.MTDLabelTotalTapped(_:)))
            cell.MTDLabel.addGestureRecognizer(MTDTapGesture)
            cell.MTDLabel.isUserInteractionEnabled = true
            MTDTapGesture.numberOfTapsRequired = 1
            
            //YTD
            let YTDTapGesture = UITapGestureRecognizer(target: self, action: #selector(self.YTDLabelTotalTapped(_:)))
            cell.YTDLabel.addGestureRecognizer(YTDTapGesture)
            cell.YTDLabel.isUserInteractionEnabled = true
            YTDTapGesture.numberOfTapsRequired = 1
            
            //Closed
            let ClosedTapGesture = UITapGestureRecognizer(target: self, action: #selector(self.ClosedLabelTotalTapped(_:)))
            cell.ClosedLabel.addGestureRecognizer(ClosedTapGesture)
            cell.ClosedLabel.isUserInteractionEnabled = true
            ClosedTapGesture.numberOfTapsRequired = 1
            
            //open
            let openTapGesture = UITapGestureRecognizer(target: self, action: #selector(self.openLabelTotalTapped(_:)))
            cell.openLabel.addGestureRecognizer(openTapGesture)
            cell.openLabel.isUserInteractionEnabled = true
            openTapGesture.numberOfTapsRequired = 1
           
            
            
            cell.TypeNameLabel.text = "Total"
            
//            cell.YesterdayLabel.text = String(self.DashboardDB.map({$0.Yesterday}).reduce(0, { x, y in x + y }))
//            cell.MTDLabel.text = String(self.DashboardDB.map({$0.MTD}).reduce(0, { x, y in x + y }))
//            cell.YTDLabel.text = String(self.DashboardDB.map({$0.YTD}).reduce(0, { x, y in x + y }))
//            cell.ClosedLabel.text = String(self.DashboardDB.map({$0.Closed}).reduce(0, { x, y in x + y }))
//            cell.openLabel.text = String(self.DashboardDB.map({$0.Open}).reduce(0, { x, y in x + y }))
            
//            print(String(self.DashboardDB.map({$0.Yesterday})))
            if(DashboardDB.count != 0) {
           cell.YesterdayLabel.text = String(self.DashboardDB[0].totalYesterday)
                        cell.MTDLabel.text = String(self.DashboardDB[0].totalMTD)
                        cell.YTDLabel.text = String(self.DashboardDB[0].totalYTD)
                        cell.ClosedLabel.text = String(self.DashboardDB[0].totalClosed)
                        cell.openLabel.text = String(self.DashboardDB[0].totalOpen)
            } else {
                cell.YesterdayLabel.text = String(0)
                cell.MTDLabel.text = String(0)
                cell.YTDLabel.text = String(0)
                cell.ClosedLabel.text = String(0)
                cell.openLabel.text = String(0)
            }
            return cell
            
        }
        
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        //print("I'm scrolling!")
        if #available(iOS 11.0, *) {
            tableView.isSpringLoaded = false
        } else {
            // Fallback on earlier versions
        }
        
    }
    
    func filterSorting(){
        
        switch FilterGraphStruct.sub_area_ID {
        case "":
//            FilterGraphStruct.filterBylocation_ID = String()
//            FilterGraphStruct.filterBylocation_Name = String()
            break;
        default:
            if  FilterGraphStruct.area_ID != "" {
              //  FilterGraphStruct.filterBylocation_ID = FilterGraphStruct.sub_area_ID
               // FilterGraphStruct.filterBylocation_Name = "Sub_Area"
            }else{
                //
            }
            
            break;
        }
        switch FilterGraphStruct.area_ID {
        case "":
          //  FilterGraphStruct.filterBylocation_ID = String()
         //   FilterGraphStruct.filterBylocation_Name = String()
            break;
        default:
            if FilterGraphStruct.sub_area_ID == "" {
              //  FilterGraphStruct.filterBylocation_ID = FilterGraphStruct.area_ID
              //  FilterGraphStruct.filterBylocation_Name = "Area"
            }else{
                //
            }
            
            break;
        }
//        switch FilterGraphStruct.location_ID {
//        case "":
//            FilterGraphStruct.filterBylocation_ID = String()
//            FilterGraphStruct.filterBylocation_Name = String()
//            break;
//        default:
//            if FilterGraphStruct.business_NameID != "" && FilterGraphStruct.area_ID == "" && FilterGraphStruct.sub_area_ID == "" {
//
//                FilterGraphStruct.filterBylocation_ID = FilterGraphStruct.location_ID
//                FilterGraphStruct.filterBylocation_Name = "Location"
//
//            }else{
//                //
//            }
//
//            break;
//        }
//        switch FilterGraphStruct.business_NameID {
//        case "":
//            FilterGraphStruct.filterBylocation_ID = String()
//            FilterGraphStruct.filterBylocation_Name = String()
//            break;
//        default:
//            if FilterGraphStruct.location_ID == "" && FilterGraphStruct.area_ID == "" && FilterGraphStruct.sub_area_ID == "" {
//                FilterGraphStruct.filterBylocation_ID = FilterGraphStruct.business_NameID
//                FilterGraphStruct.filterBylocation_Name = "Business"
//
//            }else{
//                //
//            }
//
//            break;
//        }
        
    }
    
    var parameter = [String:String]()
    var dictWithoutNilValues = [String:String]()
    
    @objc func callDashboardTypeAPI (){
        if(self.reachability.connection != .none)
        {
            self.view.backgroundColor = UIColor.white
            self.tableView.isHidden = false
        if(FilterGraphStruct.isHazardView == "") {
            FilterGraphStruct.isHazardView = "Location"
        }
        filterSorting()
       print(FilterGraphStruct.isHazardView)
        switch UserDefaults.standard.bool(forKey: "fromDashboardTypeFilter") {
        case true:
            
            dictWithoutNilValues = ["Hazard_Type_ID":FilterGraphStruct.hazard_Name_ID,
                                    "Area_ID":FilterGraphStruct.sub_area_ID,
                                    "Sub_Area_ID":FilterGraphStruct.department_NameID,
                                    "Unit_ID":FilterGraphStruct.area_ID,
                                    "Zone_ID":FilterGraphStruct.location_ID,
                                    "View":FilterGraphStruct.isHazardView,
                                    "FromDate":FilterGraphStruct.fromDate,
                                    "ToDate":FilterGraphStruct.toDate,
                                    "FromNo":FilterGraphStruct.minRiskLevel,
                                    "ToNo":FilterGraphStruct.mixRiskLevel]
            
            parameter = dictWithoutNilValues.filter { $0.value != ""}
            
            break;
        default:
            parameter = ["View":FilterGraphStruct.isHazardView]
            break;
        }
        
        print("initialPar",parameter)
        DashboardTypeAPI.serviceCalling(obj: self, parameter: parameter) { (dict) in
            
            self.DashboardDB = [DashboardTypeHazardDataModel]()
            self.DashboardDB = dict as! [DashboardTypeHazardDataModel]
            
            self.tableView.reloadData()
            
        }
//            if(self.DashboardDB.count == 0) {
//                self.view.backgroundColor = UIColor.white
//                self.tableView.backgroundColor = UIColor.white
//            }else {
//                 self.tableView.backgroundColor = UIColor.white
//                self.view.backgroundColor = UIColor.white
//            }
        }else {
            self.view.backgroundColor = UIColor.white
            self.tableView.isHidden = true
            self.noDataLabel(text: "No internet connection found. Check your internet connection and try again.")
        }
        
    }
    var reachability = Reachability()!
    @objc func callAreaDashboardTypeAPI (){
        
        if(self.reachability.connection != .none)
        {
            self.view.backgroundColor = UIColor.white
            self.tableView.isHidden = false
        filterSorting()
        switch UserDefaults.standard.bool(forKey: "fromDashboardTypeFilter") {
        case true:
            
            dictWithoutNilValues = ["Hazard_Type_ID":FilterGraphStruct.hazard_Name_ID,
                                    "Area_ID":FilterGraphStruct.sub_area_ID,
                                    "Sub_Area_ID":FilterGraphStruct.department_NameID,
                                    "Unit_ID":FilterGraphStruct.area_ID,
                                    "Zone_ID":FilterGraphStruct.location_ID,
                                    "View":FilterGraphStruct.isHazardView,
                                    "FromDate":FilterGraphStruct.fromDate,
                                    "ToDate":FilterGraphStruct.toDate,
                                    "FromNo":FilterGraphStruct.minRiskLevel,
                                    "ToNo":FilterGraphStruct.mixRiskLevel]
          
            parameter = dictWithoutNilValues.filter { $0.value != ""}
            
            break;
        default:
            parameter = ["":""]
            break;
        }
        
        print("initialPar",parameter)
        DashboardTypeAPI.serviceCalling(obj: self, parameter: parameter) { (dict) in
            
            self.DashboardDB = [DashboardTypeHazardDataModel]()
            self.DashboardDB = dict as! [DashboardTypeHazardDataModel]
            self.tableView.reloadData()
            
        }
        }else {
            self.view.backgroundColor = UIColor.white
            self.tableView.isHidden = true
            self.noDataLabel(text: "No internet connection found. Check your internet connection and try again.")
        }
        
    }
    
    @objc func callFilterWithTypenameDashboardTypeAPI(){
        if(self.reachability.connection != .none)
        {
            self.tableView.isHidden = false
        filterSorting()
        parameter = [String:String]()
        dictWithoutNilValues = [String:String]()
        
        switch FilterGraphStruct.hazard_Name_ID {
        case "":
            

            dictWithoutNilValues = ["Hazard_Type_ID":FilterGraphStruct.hazard_Name_ID,
                                    "Area_ID":FilterGraphStruct.sub_area_ID,
                                    "Sub_Area_ID":FilterGraphStruct.department_NameID,
                                    "Unit_ID":FilterGraphStruct.area_ID,
                                    "Zone_ID":FilterGraphStruct.location_ID,
                                    "View":FilterGraphStruct.isHazardView,
                                    "FromDate":FilterGraphStruct.fromDate,
                                    "ToDate":FilterGraphStruct.toDate,
                                    "FromNo":FilterGraphStruct.minRiskLevel,
                                    "ToNo":FilterGraphStruct.mixRiskLevel]
            
            parameter = dictWithoutNilValues.filter { $0.value != ""}
            
            break;
        default:
            
            dictWithoutNilValues = ["Hazard_Type_ID":FilterGraphStruct.hazard_Name_ID,
                                    "Area_ID":FilterGraphStruct.sub_area_ID,
                                    "Sub_Area_ID":FilterGraphStruct.department_NameID,
                                    "Unit_ID":FilterGraphStruct.area_ID,
                                    "Zone_ID":FilterGraphStruct.location_ID,
                                    "View":FilterGraphStruct.isHazardView,
                                    "FromDate":FilterGraphStruct.fromDate,
                                    "ToDate":FilterGraphStruct.toDate,
                                    "FromNo":FilterGraphStruct.minRiskLevel,
                                    "ToNo":FilterGraphStruct.mixRiskLevel]
            
            parameter = dictWithoutNilValues.filter { $0.value != ""}
            
            break;
        }
        
            
        
        print("par",parameter)
        DashboardTypeAPI.serviceCalling(obj: self, parameter: parameter) { (dict) in
            
            self.DashboardDB = [DashboardTypeHazardDataModel]()
            self.DashboardDB = dict as! [DashboardTypeHazardDataModel]
            self.tableView.reloadData()
            
        }
        } else {
            self.tableView.isHidden = true
            self.noDataLabel(text: "No internet connection found. Check your internet connection and try again.")
        }
        
        
    }


    @IBAction func filterOfDashboardTypeHazardAction(_ sender: Any) {
            let storyBoard = UIStoryboard(name: "ThemeSafety", bundle: nil)
        let DTHFVC = storyBoard.instantiateViewController(withIdentifier: "DashboardTypeHazardFilter") as! DashboardTypeHazardFilterViewController
        DTHFVC.whereAmI = verifyWhere
        self.navigationController?.pushViewController(DTHFVC, animated: true)
        viewSorting = "sdfij"
        
    }
    
    //Tap Gesture
    //name
    @objc func nameLabelTapped(_ sender: UITapGestureRecognizer) {
        
        UserDefaults.standard.set(true, forKey: "filterWithTypeName")
        let buttonPosition:CGPoint = sender.view!.convert(.zero, to:self.tableView)
        let indexPath = self.tableView.indexPathForRow(at: buttonPosition)
        
         let storyBoard = UIStoryboard(name: "ThemeSafety", bundle: nil)
        let DTHVC = storyBoard.instantiateViewController(withIdentifier: "DashboardTypeHazard") as! DashboardTypeHazardViewController
        
        if FilterGraphStruct.hazard_Name_ID == ""
        {
            FilterGraphStruct.hazard_Name_ID = String(describing: DashboardDB[(indexPath?.row)!].Type_ID)
            FilterGraphStruct.hazard_Name = DashboardDB[(indexPath?.row)!].TypeName
            FilterGraphStruct.hazard_NameByFilter = DashboardDB[(indexPath?.row)!].TypeName
            FilterGraphStruct.locationType = String()
            FilterGraphStruct.isHazardView = "Location"
           
            //
          //  FilterGraphStruct.location_ID = String()
          //  FilterGraphStruct.location_Name = String()
            FilterGraphStruct.area_ID = String()
            FilterGraphStruct.area_Name = String()
          //  FilterGraphStruct.business_NameID = String()
          //  FilterGraphStruct.business_Name = String()
            FilterGraphStruct.flagOfReportFilte = String()
            
            self.navigationController?.pushViewController(DTHVC, animated: true)
            
        }
//        else if FilterGraphStruct.business_NameID == ""
//        {
//            FilterGraphStruct.filterBylocation_ID = String(describing: DashboardDB[(indexPath?.row)!].Type_ID)
//            FilterGraphStruct.business_NameID = String(describing: DashboardDB[(indexPath?.row)!].Type_ID)
//            FilterGraphStruct.business_Name = DashboardDB[(indexPath?.row)!].TypeName
//            FilterGraphStruct.locationType = "Business : "+DashboardDB[(indexPath?.row)!].TypeName
//            FilterGraphStruct.filterBylocation_Name = "Business"
//            FilterGraphStruct.isHazardView = "Location"
//            
//            //
//            FilterGraphStruct.location_ID = String()
//            FilterGraphStruct.location_Name = String()
//            FilterGraphStruct.area_ID = String()
//            FilterGraphStruct.area_Name = String()
//            FilterGraphStruct.flagOfReportFilte = String()
//            
//            self.navigationController?.pushViewController(DTHVC, animated: true)
//        }
//        else if FilterGraphStruct.location_ID == ""
//        {
//            FilterGraphStruct.filterBylocation_ID = String(describing: DashboardDB[(indexPath?.row)!].Type_ID)
//            FilterGraphStruct.location_ID = String(describing: DashboardDB[(indexPath?.row)!].Type_ID)
//            FilterGraphStruct.location_Name = DashboardDB[(indexPath?.row)!].TypeName
//            FilterGraphStruct.locationType = "Location : "+DashboardDB[(indexPath?.row)!].TypeName
//            FilterGraphStruct.filterBylocation_Name = "Location"
//            FilterGraphStruct.isHazardView = "Location"
//            
//            //
//            FilterGraphStruct.area_ID = String()
//            FilterGraphStruct.area_Name = String()
//            FilterGraphStruct.flagOfReportFilte = String()
//            
//            self.navigationController?.pushViewController(DTHVC, animated: true)
//        }
        else if FilterGraphStruct.area_ID == ""
        {
          //  FilterGraphStruct.filterBylocation_ID = String(describing: DashboardDB[(indexPath?.row)!].Type_ID)
            FilterGraphStruct.area_ID = String(describing: DashboardDB[(indexPath?.row)!].Type_ID)
            FilterGraphStruct.area_Name = DashboardDB[(indexPath?.row)!].TypeName
          //  FilterGraphStruct.filterBylocation_Name = "Area"
            FilterGraphStruct.locationType = "Area : "+DashboardDB[(indexPath?.row)!].TypeName
            FilterGraphStruct.isHazardView = "Location"
            FilterGraphStruct.flagOfReportFilte = String()
            
            self.navigationController?.pushViewController(DTHVC, animated: true)
            
        }
        else
        {
           //
        }
        
        
    }
    //Yesterday
    @objc func YesterdayLabelTapped(_ sender: UITapGestureRecognizer) {
        
        UserDefaults.standard.set(4, forKey: "hazard")
        let buttonPosition:CGPoint = sender.view!.convert(.zero, to:self.tableView)
        let indexPath = self.tableView.indexPathForRow(at: buttonPosition)
        
           let storyBoard = UIStoryboard(name: "ThemeSafety", bundle: nil)
        let ZIVC = storyBoard.instantiateViewController(withIdentifier: "ReportedHazardViewController") as! ReportedHazardViewController
       
            ZIVC.hazardtypeId = String(DashboardDB[(indexPath?.row)!].Type_ID)
        
        ZIVC.statusStr = "Yesterday"
        ZIVC.locationStr = FilterGraphStruct.location_ID
        ZIVC.unitStr = FilterGraphStruct.area_ID
        ZIVC.AreaStr = FilterGraphStruct.sub_area_ID
        ZIVC.departmentStr = FilterGraphStruct.department_NameID
        ZIVC.ViewStr = FilterGraphStruct.isHazardView
        self.navigationController?.pushViewController(ZIVC, animated: true)
        
        
    }
    //MTD
    @objc func MTDLabelTapped(_ sender: UITapGestureRecognizer) {
        
        UserDefaults.standard.set(4, forKey: "hazard")
        let buttonPosition:CGPoint = sender.view!.convert(.zero, to:self.tableView)
        let indexPath = self.tableView.indexPathForRow(at: buttonPosition)
        
        let storyBoard = UIStoryboard(name: "ThemeSafety", bundle: nil)
        let ZIVC = storyBoard.instantiateViewController(withIdentifier: "ReportedHazardViewController") as! ReportedHazardViewController
       
        ZIVC.statusStr = "MTD"
        ZIVC.hazardtypeId = String(DashboardDB[(indexPath?.row)!].Type_ID)
        
       
        ZIVC.locationStr = FilterGraphStruct.location_ID
        ZIVC.unitStr = FilterGraphStruct.area_ID
        ZIVC.AreaStr = FilterGraphStruct.sub_area_ID
        ZIVC.departmentStr = FilterGraphStruct.department_NameID
        ZIVC.ViewStr = FilterGraphStruct.isHazardView
        self.navigationController?.pushViewController(ZIVC, animated: true)
        
        
    }
    //YTD
    @objc func YTDLabelTapped(_ sender: UITapGestureRecognizer) {
        
        UserDefaults.standard.set(4, forKey: "hazard")
        let buttonPosition:CGPoint = sender.view!.convert(.zero, to:self.tableView)
        let indexPath = self.tableView.indexPathForRow(at: buttonPosition)
        
        let storyBoard = UIStoryboard(name: "ThemeSafety", bundle: nil)
        let ZIVC = storyBoard.instantiateViewController(withIdentifier: "ReportedHazardViewController") as! ReportedHazardViewController
       
        ZIVC.statusStr = "YTD"
        ZIVC.hazardtypeId = String(DashboardDB[(indexPath?.row)!].Type_ID)
        
        
        ZIVC.locationStr = FilterGraphStruct.location_ID
        ZIVC.unitStr = FilterGraphStruct.area_ID
        ZIVC.AreaStr = FilterGraphStruct.sub_area_ID
        ZIVC.departmentStr = FilterGraphStruct.department_NameID
        ZIVC.ViewStr = FilterGraphStruct.isHazardView
        self.navigationController?.pushViewController(ZIVC, animated: true)
        
    }
    //Closed
    @objc func ClosedLabelTapped(_ sender: UITapGestureRecognizer) {
        
        
        
        
        UserDefaults.standard.set(4, forKey: "hazard")
        let buttonPosition:CGPoint = sender.view!.convert(.zero, to:self.tableView)
        let indexPath = self.tableView.indexPathForRow(at: buttonPosition)
        
        
        let storyBoard = UIStoryboard(name: "ThemeSafety", bundle: nil)
        let ZIVC = storyBoard.instantiateViewController(withIdentifier: "ReportedHazardViewController") as! ReportedHazardViewController
       
        ZIVC.statusStr = "Closer"
        ZIVC.hazardtypeId = String(DashboardDB[(indexPath?.row)!].Type_ID)
        
        
        ZIVC.locationStr = FilterGraphStruct.location_ID
        ZIVC.unitStr = FilterGraphStruct.area_ID
        ZIVC.AreaStr = FilterGraphStruct.sub_area_ID
        ZIVC.departmentStr = FilterGraphStruct.department_NameID
        ZIVC.ViewStr = FilterGraphStruct.isHazardView
        self.navigationController?.pushViewController(ZIVC, animated: true)
        
    }
    //open
    @objc func openLabelTapped(_ sender: UITapGestureRecognizer) {
        UserDefaults.standard.set(4, forKey: "hazard")
        let buttonPosition:CGPoint = sender.view!.convert(.zero, to:self.tableView)
        let indexPath = self.tableView.indexPathForRow(at: buttonPosition)
        
        
        let storyBoard = UIStoryboard(name: "ThemeSafety", bundle: nil)
        let ZIVC = storyBoard.instantiateViewController(withIdentifier: "ReportedHazardViewController") as! ReportedHazardViewController
       
        ZIVC.statusStr = "Opened"
        ZIVC.hazardtypeId = String(DashboardDB[(indexPath?.row)!].Type_ID)
        
        
        ZIVC.locationStr = FilterGraphStruct.location_ID
        ZIVC.unitStr = FilterGraphStruct.area_ID
        ZIVC.AreaStr = FilterGraphStruct.sub_area_ID
        ZIVC.departmentStr = FilterGraphStruct.department_NameID
        ZIVC.ViewStr = FilterGraphStruct.isHazardView
        self.navigationController?.pushViewController(ZIVC, animated: true)
        
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    

    
    
    
    //name
//    @objc func nameLabelTotalTapped(_ sender: UITapGestureRecognizer) {
//
//        UserDefaults.standard.set(4, forKey: "hazard")
//        let buttonPosition:CGPoint = sender.view!.convert(.zero, to:self.tableView)
//        let indexPath = self.tableView.indexPathForRow(at: buttonPosition)
//
//
//        let ZIVC = self.storyboard?.instantiateViewController(withIdentifier: "ReportedHazardViewController") as! ReportedHazardViewController
//        switch FilterGraphStruct.hazard_Name_ID {
//        case "":
//            FilterGraphStruct.hazard_Name_ID = String(describing: DashboardDB[(indexPath?.row)!].Type_ID)
//            break;
//        default:
//            ZIVC.hazardTypeStr = String(describing: DashboardDB[(indexPath?.row)!].Type_ID)
//            break;
//        }
//        ZIVC.statusStr = "Yesterday"
//        ZIVC.areaStr = FilterGraphStruct.area_ID
//        ZIVC.subAreaStr = FilterGraphStruct.sub_area_ID
//        ZIVC.subAreaStr = FilterGraphStruct.sub_area_ID
//        ZIVC.ViewStr = FilterGraphStruct.isHazardView
//        self.navigationController?.pushViewController(ZIVC, animated: true)
//
//
//    }
    //Yesterday
    @objc func YesterdayLabelTotalTapped(_ sender: UITapGestureRecognizer) {
        
        UserDefaults.standard.set(4, forKey: "hazard")
        let buttonPosition:CGPoint = sender.view!.convert(.zero, to:self.tableView)
        let indexPath = self.tableView.indexPathForRow(at: buttonPosition)
        
        
        let storyBoard = UIStoryboard(name: "ThemeSafety", bundle: nil)
        let ZIVC = storyBoard.instantiateViewController(withIdentifier: "ReportedHazardViewController") as! ReportedHazardViewController
         ZIVC.hazardtypeId = FilterGraphStruct.hazard_Name_ID
        ZIVC.statusStr = "Total_Yesterday"
        ZIVC.locationStr = FilterGraphStruct.location_ID
        ZIVC.unitStr = FilterGraphStruct.area_ID
        ZIVC.AreaStr = FilterGraphStruct.sub_area_ID
        ZIVC.departmentStr = FilterGraphStruct.department_NameID
        ZIVC.ViewStr = FilterGraphStruct.isHazardView
        self.navigationController?.pushViewController(ZIVC, animated: true)
      
    }
    //MTD
    @objc func MTDLabelTotalTapped(_ sender: UITapGestureRecognizer) {
        
        UserDefaults.standard.set(4, forKey: "hazard")
        let buttonPosition:CGPoint = sender.view!.convert(.zero, to:self.tableView)
        let indexPath = self.tableView.indexPathForRow(at: buttonPosition)
        
        
        let storyBoard = UIStoryboard(name: "ThemeSafety", bundle: nil)
        let ZIVC = storyBoard.instantiateViewController(withIdentifier: "ReportedHazardViewController") as! ReportedHazardViewController
         ZIVC.hazardtypeId = FilterGraphStruct.hazard_Name_ID
        ZIVC.statusStr = "Total_MTD"
        ZIVC.locationStr = FilterGraphStruct.location_ID
        ZIVC.unitStr = FilterGraphStruct.area_ID
        ZIVC.AreaStr = FilterGraphStruct.sub_area_ID
        ZIVC.departmentStr = FilterGraphStruct.department_NameID
        ZIVC.ViewStr = FilterGraphStruct.isHazardView
        self.navigationController?.pushViewController(ZIVC, animated: true)
        
    }
    //YTD
    @objc func YTDLabelTotalTapped(_ sender: UITapGestureRecognizer) {
        
        UserDefaults.standard.set(4, forKey: "hazard")
        let buttonPosition:CGPoint = sender.view!.convert(.zero, to:self.tableView)
        let indexPath = self.tableView.indexPathForRow(at: buttonPosition)
        
        
        let storyBoard = UIStoryboard(name: "ThemeSafety", bundle: nil)
        let ZIVC = storyBoard.instantiateViewController(withIdentifier: "ReportedHazardViewController") as! ReportedHazardViewController
        ZIVC.hazardtypeId = FilterGraphStruct.hazard_Name_ID
        ZIVC.statusStr = "Total_YTD"
        ZIVC.locationStr = FilterGraphStruct.location_ID
        ZIVC.unitStr = FilterGraphStruct.area_ID
        ZIVC.AreaStr = FilterGraphStruct.sub_area_ID
        ZIVC.departmentStr = FilterGraphStruct.department_NameID
        ZIVC.ViewStr = FilterGraphStruct.isHazardView
        self.navigationController?.pushViewController(ZIVC, animated: true)
        
    }
    //Closed
    @objc func ClosedLabelTotalTapped(_ sender: UITapGestureRecognizer) {
        
        
       
        
        UserDefaults.standard.set(4, forKey: "hazard")
        let buttonPosition:CGPoint = sender.view!.convert(.zero, to:self.tableView)
        let indexPath = self.tableView.indexPathForRow(at: buttonPosition)
        
        
        let storyBoard = UIStoryboard(name: "ThemeSafety", bundle: nil)
        let ZIVC = storyBoard.instantiateViewController(withIdentifier: "ReportedHazardViewController") as! ReportedHazardViewController
         ZIVC.hazardtypeId = FilterGraphStruct.hazard_Name_ID
        ZIVC.statusStr = "Total_Closer"
        ZIVC.locationStr = FilterGraphStruct.location_ID
        ZIVC.unitStr = FilterGraphStruct.area_ID
        ZIVC.AreaStr = FilterGraphStruct.sub_area_ID
        ZIVC.departmentStr = FilterGraphStruct.department_NameID
        ZIVC.ViewStr = FilterGraphStruct.isHazardView
        self.navigationController?.pushViewController(ZIVC, animated: true)
        
    }
    //open
    @objc func openLabelTotalTapped(_ sender: UITapGestureRecognizer) {
        UserDefaults.standard.set(4, forKey: "hazard")
        let buttonPosition:CGPoint = sender.view!.convert(.zero, to:self.tableView)
        let indexPath = self.tableView.indexPathForRow(at: buttonPosition)
        
        
        let storyBoard = UIStoryboard(name: "ThemeSafety", bundle: nil)
        let ZIVC = storyBoard.instantiateViewController(withIdentifier: "ReportedHazardViewController") as! ReportedHazardViewController
        ZIVC.hazardtypeId = FilterGraphStruct.hazard_Name_ID
        ZIVC.statusStr = "Total_Opened"
        ZIVC.locationStr = FilterGraphStruct.location_ID
        ZIVC.unitStr = FilterGraphStruct.area_ID
        ZIVC.AreaStr = FilterGraphStruct.sub_area_ID
        ZIVC.departmentStr = FilterGraphStruct.department_NameID
        ZIVC.ViewStr = FilterGraphStruct.isHazardView
        self.navigationController?.pushViewController(ZIVC, animated: true)
        
    }
    
}
