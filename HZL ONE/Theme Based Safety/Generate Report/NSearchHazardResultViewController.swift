//
//  NSearchHazardResultViewController.swift
//  VallSafety
//
//  Created by SARVANG INFOTCH on 14/10/18.
//  Copyright © 2018 Bunga Maheshwar Rao. All rights reserved.
//

import UIKit
import Reachability
import SDWebImage

class NSearchHazardResultViewController: CommonVSClass, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    var rejectedDB:[NSearchHazardResultModel] = []
    var rejectedLoadMoreDB:[NSearchHazardResultModel] = []
    var rejectedAPI = NSearchHazardResultAPI()
    var rejectedLoadMoreAPI = NSearchHazardResultLoadMoreAPI()
    
    
    var data: String?
    var lastObject: String?
    
    
    //Filter
    
    @IBOutlet weak var firstthCon: NSLayoutConstraint!
    
    @IBOutlet weak var secondThCon: NSLayoutConstraint!
    @IBOutlet weak var thirdthCon: NSLayoutConstraint!
    
    @IBOutlet weak var forthcon: NSLayoutConstraint!
    @IBOutlet weak var filterAppcon: NSLayoutConstraint!
    @IBOutlet weak var hazardTypeLabel: UILabel!
    @IBOutlet weak var locationTypeLabel: UILabel!
    @IBOutlet weak var fromDateLabel: UILabel!
    @IBOutlet weak var toDateLabel: UILabel!
    @IBOutlet weak var filterAppLabel: UILabel!
    @IBOutlet weak var viewOf: UIView!
    
    @IBOutlet weak var viewHandler: UIView!
    
    
    var reachability = Reachability()!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.estimatedRowHeight = 269
        tableView.rowHeight = UITableViewAutomaticDimension
        
        tableView.tableFooterView = UIView()
        tableView.tableFooterView?.backgroundColor = UIColor.lightGray
        
        refresh.addTarget(self, action: #selector(self.callNSearchHazardResult), for: .valueChanged)
        self.tableView.addSubview(refresh)
        
        let swipe = UISwipeGestureRecognizer(target: self, action: #selector(self.swipeRecognizer))
        swipe.direction = ([.down])
        self.view.addGestureRecognizer(swipe)
        
       NotificationCenter.default.addObserver(self, selector: #selector(self.callNSearchHazardResult), name: NSNotification.Name?.init(NSNotification.Name(rawValue: "fromsearchHazardTypeFilter")), object: nil)
        
        callNSearchHazardResult()
        tableView.tableFooterView = UIView()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
        override func viewWillDisappear(_ animated: Bool) {
            super.viewWillDisappear(true)
    
          
        }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(true)
        self.tabBarController?.tabBar.isHidden = true
        self.title = "Search Hazard"
        
       // viewContentShape()
        
    }
    
    @objc func swipeRecognizer(_ sender: UISwipeGestureRecognizer) {
        if (sender.direction == .down) {
            
            if(self.reachability.connection != .none)
            {
                
                self.callNSearchHazardResult()
            }
            else
            {
                self.startLoading(view: self.view)
                DispatchQueue.main.asyncAfter(deadline: .now() + 4.5) {
                    self.noDataLabel(text: "No internet connection found. Check your internet connection and try again.")
                    self.stopLoading(view: self.view)
                }
                
            }
            
        }
    }
    
    func viewContentShape(){
      
        switch FilterGraphStruct.sub_area_ID {
        case "":
            FilterGraphStruct.locationType = String()
            break;
        default:
            if FilterGraphStruct.sub_area_Name != "" {
                FilterGraphStruct.locationType = "Area Location : "+FilterGraphStruct.sub_area_Name
            }else{
                //
            }
            
            break;
        }
        switch FilterGraphStruct.area_ID {
        case "":
            FilterGraphStruct.locationType = String()
            break;
        default:
            if FilterGraphStruct.sub_area_ID == "" {
                FilterGraphStruct.locationType = "Area : "+FilterGraphStruct.area_Name
            }else{
                //
            }
            
            break;
        }
        
        switch FilterGraphStruct.unit_Id {
        case "":
            FilterGraphStruct.locationType = String()
            break;
        default:
            if FilterGraphStruct.area_ID == "" {
                FilterGraphStruct.locationType = "Unit : "+FilterGraphStruct.unit_Name
            }else{
                //
            }
            
            break;
        }
        switch FilterGraphStruct.location_ID {
        case "":
            FilterGraphStruct.locationType = String()
            break;
        default:
            if FilterGraphStruct.location_ID != "" && FilterGraphStruct.unit_Id == "" && FilterGraphStruct.area_ID == "" && FilterGraphStruct.sub_area_ID == "" {
                FilterGraphStruct.locationType = "Zone : "+FilterGraphStruct.location_Name
            }else{
                //
            }

            break;
        }
        
        switch FilterGraphStruct.hazard_Name_ID {
        case "":
            FilterGraphStruct.hazard_NameByFilter = String()
            break;
        default:
            FilterGraphStruct.hazard_NameByFilter = FilterGraphStruct.hazard_Name
            break;
        }
        
        
        if FilterGraphStruct.hazard_NameByFilter.isEmpty == true{
            self.firstthCon.constant = 0
            self.hazardTypeLabel.text = ""
        }else{
            self.firstthCon.constant = 5
            self.hazardTypeLabel.text = "Hazard Type : "+FilterGraphStruct.hazard_NameByFilter
        }
        
        if FilterGraphStruct.locationType.isEmpty == true{
            self.secondThCon.constant = 0
            self.locationTypeLabel.text = ""
        }else{
            self.secondThCon.constant = 5
            self.locationTypeLabel.text = FilterGraphStruct.locationType
        }
        
        if FilterGraphStruct.isHazardView.isEmpty == true {
            self.thirdthCon.constant = 0
            self.fromDateLabel.text = ""
        }else{
            
            self.thirdthCon.constant = 5
            self.fromDateLabel.text = "View : "+FilterGraphStruct.isHazardView
        }
        
        if FilterGraphStruct.toDate.isEmpty == true && FilterGraphStruct.fromDate.isEmpty == true{
            self.forthcon.constant = 0
            self.toDateLabel.text = ""
        }else{
            self.forthcon.constant = 5
            self.toDateLabel.text = "From Date : "+FilterGraphStruct.fromDate+"   To Date : "+FilterGraphStruct.toDate
        }
        
        if FilterGraphStruct.hazard_NameByFilter.isEmpty == true && FilterGraphStruct.locationType.isEmpty == true && FilterGraphStruct.isHazardView.isEmpty == true && FilterGraphStruct.toDate.isEmpty == true {
            
            self.filterAppcon.constant = 0
            self.viewOf.isHidden = true
            self.filterAppLabel.text = ""
            self.viewOf.frame.size.height = 0
            
        }else{
            
            self.viewOf.isHidden = false
            self.filterAppcon.constant = 10
            self.filterAppLabel.text = "Filter Applied"
            self.filterAppLabel.font = UIFont.boldSystemFont(ofSize: 14)
        }
        if(self.viewOf.isHidden == true) {
            self.view.backgroundColor = UIColor(hexString: "2c3e50", alpha: 1.0)
            self.view.layer.borderWidth = 0.5
            self.view.layer.borderColor = UIColor.white.cgColor
        } else {
            self.viewHandler.backgroundColor = UIColor(hexString: "2c3e50", alpha: 1.0)
            self.view.backgroundColor = UIColor(hexString: "2c3e50", alpha: 1.0)
            self.viewHandler.layer.borderWidth = 0.5
            self.viewHandler.layer.borderColor = UIColor.white.cgColor
            self.view.layer.borderWidth = 0.5
            self.view.layer.borderColor = UIColor.white.cgColor
        }
        
    }
    func imageResize (image:UIImage, sizeChange:CGSize)-> UIImage{
        
        let hasAlpha = true
        let scale: CGFloat = 0.0 // Use scale factor of main screen
        
        UIGraphicsBeginImageContextWithOptions(sizeChange, !hasAlpha, scale)
        image.draw(in: CGRect(origin: CGPoint.zero, size: sizeChange))
        
        let scaledImage = UIGraphicsGetImageFromCurrentImageContext()
        return scaledImage!
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if(section == 0) {
           return 10.0
        } else {
        return 0.0
        }
    }
    func numberOfSections(in tableView: UITableView) -> Int {
       
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       return rejectedDB.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! ReportedDataTableViewCell
        
        cell.statusTextView.tag = indexPath.row
        
        cell.statusOfHazard.text = self.rejectedDB[indexPath.row].status
        cell.idOfHazard.text = "ID-"+String(describing: self.rejectedDB[indexPath.row].ID)
        
        let paragraph1 = NSMutableParagraphStyle()
        paragraph1.alignment = .left
        paragraph1.lineSpacing = 0
        let titleStr = NSMutableAttributedString(string: self.rejectedDB[indexPath.row].descriptionn!, attributes: [NSAttributedStringKey.foregroundColor: UIColor.black, NSAttributedStringKey.font: UIFont.systemFont(ofSize: CGFloat(15.0))])
        titleStr.addAttribute(NSAttributedStringKey.paragraphStyle, value: paragraph1, range: NSRange(location: 0, length: titleStr.length))
        cell.titleTextView.attributedText = titleStr
        
        
        switch cell.statusOfHazard.text! {
        case "Close":
            cell.statusOfHazard.textColor = UIColor.colorwithHexString("#5c9834", alpha: 1)
            break;
        case "Pending":
            cell.statusOfHazard.textColor = UIColor.colorwithHexString("#cfcf00", alpha: 1)
            break;
        case "Reject":
            cell.statusOfHazard.textColor = UIColor.colorwithHexString("#FF0000", alpha: 1)
            break;
        default:
            break;
        }
        
        
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = NSTimeZone.system
        dateFormatter.dateFormat = "MMM dd yyyy HH:mm:ss:SSSa"
        let date = dateFormatter.date(from: self.rejectedDB[indexPath.row].date_Time!)
        
        let dateFormatter2 = DateFormatter()
        dateFormatter2.timeZone = NSTimeZone.system
        dateFormatter2.dateFormat = "dd MMM yyyy"
        
        switch date {
        case nil:
            let date_TimeStr = dateFormatter2.string(from: Date())
            cell.dateLabel.text = date_TimeStr
            break;
        default:
            let date_TimeStr = dateFormatter2.string(from: date!)
            
            cell.dateLabel.text = date_TimeStr
            break;
        }
        
        
        let statusTextViewTapGesture = UITapGestureRecognizer(target: self, action: #selector(self.tapCell(_:)))
        cell.statusTextView.addGestureRecognizer(statusTextViewTapGesture)
        cell.statusTextView.isUserInteractionEnabled = true
        statusTextViewTapGesture.numberOfTapsRequired = 1
        
        
        // cell.titleTextView.text = self.rejectedDB[indexPath.row].descriptionn
        cell.titleTextView.font = UIFont.systemFont(ofSize: 15.0)
        let paragraph = NSMutableParagraphStyle()
        paragraph.alignment = .left
        paragraph.lineSpacing = 0
        let empId : String = UserDefaults.standard.string(forKey: "EmployeeID")!
        var startString : String = "You "
        if(empId != rejectedDB[indexPath.row].Employee_ID){
            startString =  rejectedDB[indexPath.row].Employee_Name! + "-" +   String(rejectedDB[indexPath.row].Employee_ID!) + " "
        }
        let  mBuilder = startString  + "submitted hazard of type " + self.rejectedDB[indexPath.row].hazard_Type_Name! + " for location " +  self.rejectedDB[indexPath.row].Zone_Name! + " area " + self.rejectedDB[indexPath.row].Unit_Name! +
            " subarea " + self.rejectedDB[indexPath.row].area_Name! + " department " +  self.rejectedDB[indexPath.row].subarea_Name!
        
        let agreeAttributedString = NSMutableAttributedString(string: mBuilder, attributes: [NSAttributedStringKey.foregroundColor: UIColor.black, NSAttributedStringKey.font: UIFont.systemFont(ofSize: CGFloat(15.0))])
        
        
        //Submitted
        let SubmittedAttributedString = NSAttributedString(string:"submitted hazard of type ", attributes: [NSAttributedStringKey.foregroundColor: UIColor.gray, NSAttributedStringKey.font: UIFont.systemFont(ofSize: 15.0)])
        
        let range: NSRange = (agreeAttributedString.string as NSString).range(of: "submitted hazard of type ")
        if range.location != NSNotFound {
            agreeAttributedString.replaceCharacters(in: range, with: SubmittedAttributedString)
        }
        
        //for
        let forAttributedString = NSAttributedString(string:"for location ", attributes: [NSAttributedStringKey.foregroundColor: UIColor.gray, NSAttributedStringKey.font: UIFont.systemFont(ofSize: 15.0)])
        
        let forRange: NSRange = (agreeAttributedString.string as NSString).range(of: "for location ")
        if forRange.location != NSNotFound {
            agreeAttributedString.replaceCharacters(in: forRange, with: forAttributedString)
        }
        
        let areaAttributedString = NSAttributedString(string:"area", attributes: [NSAttributedStringKey.foregroundColor: UIColor.gray, NSAttributedStringKey.font: UIFont.systemFont(ofSize: 15.0)])
        
        let areaRange: NSRange = (agreeAttributedString.string as NSString).range(of: "area")
        if areaRange.location != NSNotFound {
            agreeAttributedString.replaceCharacters(in: areaRange, with: areaAttributedString)
        }
        
        
        
        
        let subAreaAttributedString = NSAttributedString(string:"subarea", attributes: [NSAttributedStringKey.foregroundColor: UIColor.gray, NSAttributedStringKey.font: UIFont.systemFont(ofSize: 15.0)])
        let subAreaRange: NSRange = (agreeAttributedString.string as NSString).range(of: "subarea")
        if subAreaRange.location != NSNotFound {
            agreeAttributedString.replaceCharacters(in: subAreaRange, with: subAreaAttributedString)
        }
        
        let deptAttributedString = NSAttributedString(string:"department", attributes: [NSAttributedStringKey.foregroundColor: UIColor.gray, NSAttributedStringKey.font: UIFont.systemFont(ofSize: 15.0)])
        
        let deptRange: NSRange = (agreeAttributedString.string as NSString).range(of: "department")
        if deptRange.location != NSNotFound {
            agreeAttributedString.replaceCharacters(in: deptRange, with: deptAttributedString)
        }
        
        agreeAttributedString.addAttribute(NSAttributedStringKey.paragraphStyle, value: paragraph, range: NSRange(location: 0, length: agreeAttributedString.length))
        
        
        cell.statusTextView.attributedText = agreeAttributedString
        cell.statusTextView.font = UIFont.systemFont(ofSize: 15.0)
        self.data = self.rejectedDB[indexPath.row].descriptionn!
        self.lastObject = self.rejectedDB[indexPath.row].descriptionn!
        
        
        let imageViewDataCell = UIImageView()
        imageViewDataCell.image = UIImage(named : "placed")
        let postImageTapGesture = UITapGestureRecognizer(target: self, action: #selector(self.ImageViewTapped(_:)))
        if(cell.imageFile != nil) {
            cell.imageFile.addGestureRecognizer(postImageTapGesture)
            cell.imageFile.isUserInteractionEnabled = true
            postImageTapGesture.numberOfTapsRequired = 1
        }
       
        
        cell.statusTextView.isEditable = false
        cell.statusTextView.isSelectable = false
        cell.statusTextView.isScrollEnabled = false
        cell.titleTextView.isEditable = false
        cell.titleTextView.isSelectable = false
        cell.titleTextView.isScrollEnabled = false
        cell.titleTextView.textContainer.maximumNumberOfLines = 3
        cell.titleTextView.textContainer.lineBreakMode = .byTruncatingTail
        cell.titleTextView.textColor = UIColor(hexString: "2c3e50", alpha: 1.0)
        cell.selectionStyle = .none
        
      
        let urlString = self.rejectedDB[indexPath.row].image_path!
       
        let imageView = UIImageView()
        
        let urlShow = urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
        if let url = NSURL(string: self.rejectedDB[indexPath.row].image_path!) {
            
            SDWebImageManager.shared().downloadImage(with: NSURL(string: urlShow!) as URL!, options: .continueInBackground, progress: {
                (receivedSize :Int, ExpectedSize :Int) in
                
            }, completed: {
                (image : UIImage?, error : Error?, cacheType : SDImageCacheType, finished : Bool, url : URL?) in
                if(image != nil) {
                    let size = CGSize(width: 120, height: 100)
                    let imageCell = self.imageResize(image: image!, sizeChange: size)
                    cell.imageFile.image = imageCell
                    //  cell.viewImageHold.backgroundColor = UIColor(patternImage: imageCell)
                    
                }else{
                    imageView.image =  UIImage(named: "placed")
                    let size = CGSize(width: 120, height: 100)
                    let imageCell = self.imageResize(image: imageView.image!, sizeChange: size)
                    cell.imageFile.image = imageCell
                    // cell.viewImageHold.backgroundColor = UIColor(patternImage: imageCell)
                }
            })
        }else{
            imageView.image =  UIImage(named: "placed")
            let size = CGSize(width: 120, height: 100)
            let imageCell = imageResize(image: imageView.image!, sizeChange: size)
            cell.imageFile.image = imageCell
            //cell.viewImageHold.backgroundColor = UIColor(patternImage: imageCell)
        }
        
        //   cell.heightImage.constant = cell.statusTextView.contentSize.height + cell.titleTextView.contentSize.height
        
        let cal : Int = Int(self.rejectedDB[indexPath.row].risk_level!)
        

        
        if(cal == 0){
            cell.viewRiskLevel.backgroundColor = UIColor.clear
        }
        else if(cal <= 4 && cal > 0) {
            
            cell.viewRiskLevel.backgroundColor = UIColor.colorwithHexString("87ceeb", alpha: 1.0)
        }else if(cal <= 9 && cal > 4) {
            
            cell.viewRiskLevel.backgroundColor = UIColor.colorwithHexString("CCCC00", alpha: 1.0)
        } else if(cal <= 16 && cal > 9 ) {
            
            cell.viewRiskLevel.backgroundColor = UIColor.colorwithHexString("ffa500", alpha: 1.0)
        } else if(cal <= 25 && cal > 16 ) {
            
            cell.viewRiskLevel.backgroundColor = UIColor.colorwithHexString("FF0000", alpha: 1.0)
        }
        cell.separatorInset = UIEdgeInsetsMake(0, 10000, 0, 0);
        
        if ( self.data ==  self.lastObject && indexPath.row == self.rejectedDB.count - 1)
        {
            //print(String(self.rejectedDB[indexPath.row].ID))
            self.callNSearchHazardResultLoadMore(Id: String(self.rejectedDB[indexPath.row].ID))
            
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    var para = [String:String]()
    
    @objc func callNSearchHazardResult() {
       viewContentShape()
        let parameter = ["Search":FilterGraphStruct.serachStr
            , "FromDate":FilterGraphStruct.fromDate,
              "ToDate":FilterGraphStruct.toDate,
              "Hazard_Type_ID":FilterGraphStruct.hazard_Name_ID ,
              
              "Area_ID":FilterGraphStruct.area_ID,
              "Sub_Area_ID":FilterGraphStruct.sub_area_ID,
              "Unit_ID":FilterGraphStruct.unit_Id,
              "Zone_ID":FilterGraphStruct.location_ID]
       
        para = parameter.filter { $0.value != ""}
        print("para",para)
        self.rejectedAPI.serviceCalling(obj: self, parameter: para) { (dict) in
            
            self.rejectedDB = [NSearchHazardResultModel]()
            self.rejectedDB = dict as! [NSearchHazardResultModel]
            self.tableView.reloadData()
        }
        
    }
    
    @objc func callNSearchHazardResultLoadMore(Id:String) {
        
       
        
        let parameter = ["Search":FilterGraphStruct.serachStr
            ,"ID":Id
            , "FromDate":FilterGraphStruct.fromDate,
              "ToDate":FilterGraphStruct.toDate,
              "Hazard_Type_ID":FilterGraphStruct.hazard_Name_ID ,
            
            "Area_ID":FilterGraphStruct.area_ID,
            "Sub_Area_ID":FilterGraphStruct.sub_area_ID,
            "Unit_ID":FilterGraphStruct.unit_Id,
            "Zone_ID":FilterGraphStruct.location_ID]
        
        para = parameter.filter { $0.value != ""}
        print("para",para)
        
        self.rejectedLoadMoreAPI.serviceCalling(obj: self, parameter: para) { (dict) in
            
            self.rejectedLoadMoreDB = [NSearchHazardResultModel]()
            self.rejectedLoadMoreDB = dict as! [NSearchHazardResultModel]
            
            switch self.rejectedLoadMoreDB.count {
            case 0:
                break;
            default:
                self.rejectedDB.append(contentsOf: self.rejectedLoadMoreDB)
                self.tableView.reloadData()
                break;
            }
            
        }
        
    }
    
    @objc func ImageViewTapped(_ sender: UITapGestureRecognizer) {
        
        let buttonPosition:CGPoint = sender.view!.convert(.zero, to:self.tableView)
        let indexPath = self.tableView.indexPathForRow(at: buttonPosition)
        
        if rejectedDB[(indexPath?.row)!].image_path! != "" {
            let urlString = self.rejectedDB[(indexPath?.row)!].image_path!
            let urlShow = urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
            let storyBoard = UIStoryboard(name: "ThemeSafety", bundle: nil)
            let ZIVC = storyBoard.instantiateViewController(withIdentifier: "ImageZoom") as! ImageZoomViewController
            ZIVC.zoomImageUrl = urlShow!
            self.navigationController?.pushViewController(ZIVC, animated: true)
            
        }
        
        
    }
    
    @objc func tapCell(_ sender: UITapGestureRecognizer) {
        

        
        let buttonPosition:CGPoint = sender.view!.convert(.zero, to:self.tableView)
        let indexPath = self.tableView.indexPathForRow(at: buttonPosition)
        
        UserDefaults.standard.set("search", forKey: "Status")
         let storyBoard = UIStoryboard(name: "ThemeSafety", bundle: nil)
        let ZIVC = storyBoard.instantiateViewController(withIdentifier: "HazardDetailsViewController") as! HazardDetailsViewController
        ZIVC.hazardID = String(describing: rejectedDB[(indexPath?.row)!].ID)
        ZIVC.empId = rejectedDB[(indexPath?.row)!].Employee_ID!
        self.navigationController?.pushViewController(ZIVC, animated: true)
        
    }
    
    @IBAction func filterOfDashboardTypeHazardAction(_ sender: Any) {
         let storyBoard = UIStoryboard(name: "ThemeSafety", bundle: nil)
        let SearchFilterVC = storyBoard.instantiateViewController(withIdentifier: "SearchFilterViewController") as! SearchFilterViewController
        SearchFilterVC.identifierStr = "SearchHazard"
        self.navigationController?.pushViewController(SearchFilterVC, animated: true)
        
    }
    
    
}

