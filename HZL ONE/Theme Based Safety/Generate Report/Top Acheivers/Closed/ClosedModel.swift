import Foundation
import Reachability



class TopCloserModel: NSObject {
    
    
    
    var Employee_ID = Double()
    var Total_Count = Int()
    var Employee_Name = ""
    
    var SrNo = Int()
    
    
    func setDataInModel(str:[String:AnyObject])
    {
        if str["Employee_ID"] is NSNull{
            self.Employee_ID = 0
        }else{
            self.Employee_ID = ((str["Employee_ID"] as? NSString)?.doubleValue)!
        }
        if str["Employee_Name"] is NSNull{
            self.Employee_Name = ""
        }else{
            self.Employee_Name = (str["Employee_Name"] as? String)!
        }
        if str["Total_Count"] is NSNull{
            
            self.Total_Count = 0
        }else{
            
            self.Total_Count = (str["Total_Count"] as? Int)!
        }
        if str["R"] is NSNull{
            
            self.SrNo = 0
        }else{
            
            self.SrNo = (str["R"] as? Int)!
        }
//        if str["Sbu_Department"] is NSNull{
//
//            self.Sbu_Department = ""
//        }else{
//
//            self.Sbu_Department = (str["Sbu_Department"] as? String)!
//        }
        
        
    }
}


class TopCloserAPI
{
    
    var reachablty = Reachability()!
    
    
    func serviceCalling(obj:TopTenCloserViewController,Url:String,parameter:[String:String], success:@escaping (AnyObject)-> Void)
    {
        
       
        
        if(reachablty.connection != .none)
        {
            
            WebServices.sharedInstances.sendPostRequest(url: Url, parameters: parameter, successHandler: { (dict) in
                
                if let response = dict["response"]{
                    
                    let statusString : String = response["status"] as! String
                    let objectmsg = MessageCallServerModel()
                    let msg = objectmsg.sealizeMessage(cell: response as! [String : AnyObject])
                    if(statusString == "success")
                    {
                        
                        obj.tableView.isHidden = false
                        obj.noDataLabel(text: "" )
                        obj.label.isHidden = true
                        
                        if let data = dict["data"] as? [AnyObject]
                        {
                            
                            var dataArray : [TopCloserModel] = []
                            
                            for i in 0..<data.count
                            {
                                
                                if let cell = data[i] as? [String:AnyObject]
                                {
                                    let object = TopCloserModel()
                                    object.setDataInModel(str: cell)
                                    dataArray.append(object)
                                }
                                
                            }
                            
                            success(dataArray as AnyObject)
                            
                            
                        }
                        
                        obj.refresh.endRefreshing()
                       
                    }
                    else
                    {
                        
                        
                        print("DATA:fail")
                        obj.tableView.isHidden = true
                         obj.label.isHidden = false
                        obj.noDataLabel(text: msg )
                        obj.refresh.endRefreshing()
                       
                        
                    }
                    
                    
                }
                
                
                
            }, failureHandler: { (error) in
                let errorStr : String = error.description
                
                print("DATA:error",errorStr)
                
              //  obj.errorChecking(error: error)
               
            })
            
            
        }
        else{
            obj.noDataLabel(text:  "No internet connection found. Check your internet connection and try again.")
            obj.label.isHidden = false
            obj.tableView.isHidden = true
            obj.refresh.endRefreshing()
            
        }
        
        
    }
    
    
}

class TopCloserLoadMoreAPI
{
    
    var reachablty = Reachability()!
    
    
    func serviceCalling(obj:TopTenCloserViewController,Url:String,parameter:[String:String], success:@escaping (AnyObject)-> Void)
    {
        
        
        var statusString = String()
        if(reachablty.connection != .none)
        {
            obj.startLoading(view: obj.tableView)
            WebServices.sharedInstances.sendPostRequest(url: Url, parameters: parameter, successHandler: { (dict) in
                
                if let response = dict["response"]{
                    
                     statusString  = response["status"] as! String
                    
                    if(statusString == "success")
                    {
                        
//                        obj.tableView.isHidden = false
//                        obj.noDataLabel(text: "" )
                        
                        if let data = dict["data"] as? [AnyObject]
                        {
                            
                            var dataArray : [TopCloserModel] = []
                            
                            for i in 0..<data.count
                            {
                                
                                if let cell = data[i] as? [String:AnyObject]
                                {
                                    let object = TopCloserModel()
                                    object.setDataInModel(str: cell)
                                    dataArray.append(object)
                                }
                                
                            }
                            
                            success(dataArray as AnyObject)
                            
                            
                        }
                           statusString = "success"
                        obj.refresh.endRefreshing()
                          obj.stopLoading(view: obj.tableView)
                    }
                    else
                    {
                        
                        
                        print("DATA:fail")
//                        obj.tableView.isHidden = true
//                        obj.noDataLabel(text: "No data found" )
                        obj.refresh.endRefreshing()
                         obj.stopLoading(view: obj.tableView)
                        statusString = "success"
                    }
                    
                    
                }
                
                
                
            }, failureHandler: { (error) in
                let errorStr : String = error.description
                
                print("DATA:error",errorStr)
                statusString = "fail"
                //  obj.errorChecking(error: error)
                
            })
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
                if(statusString != "success") {
                    obj.noDataLabel(text:  "No internet connection found. Check your internet connection and try again.")
                    obj.label.isHidden = false
                    obj.tableView.isHidden = true
                    obj.refresh.endRefreshing()
                    obj.stopLoading(view: obj.tableView)
                }
            }
            
            
        }
        else{
//            obj.noDataLabel(text:  "No internet connection found. Check your internet connection and try again.")
//            obj.tableView.isHidden = true
            obj.refresh.endRefreshing()
          
        }
        
        
    }
    
    
}




