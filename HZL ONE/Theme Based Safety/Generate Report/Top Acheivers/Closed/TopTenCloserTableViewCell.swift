//
//  TopTenCloserTableViewCell.swift
//  Balco Sustainability Plus
//
//  Created by sudheer-kumar on 07/02/18.
//  Copyright © 2018 safiqul islam. All rights reserved.
//

import UIKit

class TopTenCloserTableViewCell: UITableViewCell {

    @IBOutlet weak var employeeName: UILabel!
    
    @IBOutlet weak var SBUDepartment: UILabel!
    
    @IBOutlet weak var totalHazard: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
