//
//  CloseTableViewCell.swift
//  sesagoa
//
//  Created by SARVANG INFOTCH on 04/12/18.
//  Copyright © 2018 Bunga Maheshwar Rao. All rights reserved.
//

import UIKit
import Alamofire
import MobileCoreServices
import CoreData

class CloseTableViewCell: UITableViewCell {
    
    @IBOutlet weak var textViewData: UITextView!
    
    @IBOutlet weak var heightImageDrop: NSLayoutConstraint!
    
    @IBOutlet weak var closeConst: NSLayoutConstraint!
    @IBOutlet weak var imageConst: NSLayoutConstraint!
    @IBOutlet weak var viewStatus: UIView!
    @IBOutlet weak var processBTN: UIButton!
    @IBOutlet weak var cameraBtn: UIButton!
    @IBOutlet weak var lblReason: UILabel!
    @IBOutlet weak var lblAttachPhoto: UILabel!
    @IBOutlet weak var CloseBtn: UIButton!
    var hazardID = String()
    @IBOutlet weak var imgViewDrop: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code\
        
        textViewData.layer.borderWidth = 1.0
        
        textViewData.layer.cornerRadius = 10.0
        textViewData.layer.borderColor = UIColor.black.cgColor
        textViewData.delegate = self
        self.textViewData.returnKeyType = UIReturnKeyType.done
        heightImageDrop.constant = 0
        
        CloseBtn.isHidden = true
    }
    
    
    
    
    
    func imageResize (image:UIImage, sizeChange:CGSize)-> UIImage{
        
        let hasAlpha = true
        let scale: CGFloat = 0.0 // Use scale factor of main screen
        
        UIGraphicsBeginImageContextWithOptions(sizeChange, !hasAlpha, scale)
        image.draw(in: CGRect(origin: CGPoint.zero, size: sizeChange))
        
        let scaledImage = UIGraphicsGetImageFromCurrentImageContext()
        return scaledImage!
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
extension CloseTableViewCell : UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView)
    {
        if (textView.text == "Type reason here...")
        {
            textView.text = ""
            textView.textColor = .black
        }
        
        
    }
    
    func textViewDidEndEditing(_ textView: UITextView)
    {
        if (textView.text == "")
        {
            textView.text = "Type reason here..."
            textView.textColor = .lightGray
        }
        textViewData.endEditing(true)
        
    }
}
