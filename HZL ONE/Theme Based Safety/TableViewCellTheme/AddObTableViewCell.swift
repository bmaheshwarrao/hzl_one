//
//  AddObTableViewCell.swift
//  sesagoa
//
//  Created by SARVANG INFOTCH on 14/12/18.
//  Copyright © 2018 Bunga Maheshwar Rao. All rights reserved.
//

import UIKit

class AddObTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    @IBOutlet weak var btnBottom: NSLayoutConstraint!
    @IBOutlet weak var imgBottom: NSLayoutConstraint!
    @IBOutlet weak var btnProcess: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var imageDrop: UIImageView!
    @IBOutlet weak var btnCamera: UIButton!
    @IBOutlet weak var viewData: UIView!
    @IBOutlet weak var lblAttachPhoto: UILabel!
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
