//
//  ReportHazardThemeTableViewCell.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 12/02/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit

class ReportHazardThemeTableViewCell: UITableViewCell {
    @IBOutlet weak var viewZone: UIView!
    @IBOutlet weak var btnZone: UIButton!
    @IBOutlet weak var lblZone: UILabel!
    @IBOutlet weak var viewUnit: UIView!
    @IBOutlet weak var btnUnit: UIButton!
    @IBOutlet weak var lblUnit: UILabel!
    @IBOutlet weak var viewArea: UIView!
    @IBOutlet weak var btnArea: UIButton!
    @IBOutlet weak var lblArea: UILabel!
    
    @IBOutlet weak var viewSubArea: UIView!
    @IBOutlet weak var btnSubArea: UIButton!
    @IBOutlet weak var lblSubArea: UILabel!
    @IBOutlet weak var viewhazard: UIView!
    @IBOutlet weak var btnhazard: UIButton!
    @IBOutlet weak var lblhazard: UILabel!
    
    @IBOutlet weak var viewTheme: UIView!
    @IBOutlet weak var btnTheme: UIButton!
    @IBOutlet weak var lblTheme: UILabel!
    
    
    @IBOutlet weak var btnCloseSelfie: UIButton!
    @IBOutlet weak var imageViewSelfie: UIImageView!
    @IBOutlet weak var btnCloseHazard: UIButton!
    @IBOutlet weak var imageViewHazard: UIImageView!
    @IBOutlet weak var textViewCaption: UITextView!
    @IBOutlet weak var textViewDesc: UITextView!
    
    
    @IBOutlet weak var viewHazardClose: UIView!
    @IBOutlet weak var viewSelfieClose: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        viewArea.layer.borderWidth = 1.0
        viewArea.layer.borderColor = UIColor.black.cgColor
        viewArea.layer.cornerRadius = 10.0
        viewUnit.layer.borderWidth = 1.0
        viewUnit.layer.borderColor = UIColor.black.cgColor
        viewUnit.layer.cornerRadius = 10.0
        viewZone.layer.borderWidth = 1.0
        viewZone.layer.borderColor = UIColor.black.cgColor
        viewZone.layer.cornerRadius = 10.0
        
        viewSubArea.layer.borderWidth = 1.0
        viewSubArea.layer.borderColor = UIColor.black.cgColor
        viewSubArea.layer.cornerRadius = 10.0
        
        viewTheme.layer.borderWidth = 1.0
        viewTheme.layer.borderColor = UIColor.black.cgColor
        viewTheme.layer.cornerRadius = 10.0
        viewhazard.layer.borderWidth = 1.0
        viewhazard.layer.borderColor = UIColor.black.cgColor
        viewhazard.layer.cornerRadius = 10.0
        viewZone.layer.borderWidth = 1.0
        viewZone.layer.borderColor = UIColor.black.cgColor
        viewZone.layer.cornerRadius = 10.0
        
       
        textViewCaption.layer.borderWidth = 1.0
        textViewCaption.layer.borderColor = UIColor.black.cgColor
        textViewCaption.layer.cornerRadius = 10.0
        
        textViewDesc.layer.borderWidth = 1.0
        textViewDesc.layer.borderColor = UIColor.black.cgColor
        textViewDesc.layer.cornerRadius = 10.0
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
