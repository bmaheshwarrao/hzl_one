//
//  GraphViewController.swift
//  VallSafety
//
//  Created by SARVANG INFOTCH on 19/10/18.
//  Copyright © 2018 Bunga Maheshwar Rao. All rights reserved.
//

import UIKit
import Charts
import CoreData
class GraphViewController: CommonVSClass {
    
    @IBOutlet weak var areaBtn: UIButton!
    @IBOutlet weak var subAreaBtn: UIButton!
    @IBOutlet weak var locationBtn: UIButton!
    @IBOutlet weak var departmentBtn: UIButton!
    
  
    
    @IBOutlet weak var lblHazardType: UILabel!
    
    var AreaIDVal = String()
    
    var SubAreaIDVal = String()
    
    var Area_NameArray = [String]()
    var Sub_Area_NameArray = [String]()
    
    @IBOutlet weak var viewShow: UIView!
 
    @IBOutlet weak var subAreaView: UIView!
    @IBOutlet weak var areaView: UIView!
     @IBOutlet weak var locationView: UIView!
    @IBOutlet weak var areaCrosBtn: UIButton!
    @IBOutlet weak var subAreaCrosBtn: UIButton!
    @IBOutlet weak var locationCrosBtn: UIButton!
    @IBOutlet weak var departmentCrosBtn: UIButton!
     @IBOutlet weak var departmentView: UIView!
    var HazardType = String()
    @IBAction func areaAction(_ sender: UIButton) {
        areaData()
       
    }
    @IBAction func locationAction(_ sender: UIButton) {
       locationData()
        
    }
    @IBAction func subAreaAction(_ sender: UIButton) {
        subAreaData()
        
    }
    @IBAction func departmentAction(_ sender: UIButton) {
        departmentData()
        
    }
    var viewData = UIView()
//    func barSetup() {
//                 viewData = UIView(frame: CGRect(x: 0, y: 0, width: 5 , height: 5))
//                view.backgroundColor = UIColor.clear
//
//                let barButtonItem = UIBarButtonItem(customView: viewData)
//                self.navigationItem.rightBarButtonItems = [barButtonItem]
//
//
//            }
    @IBAction func infoBarButtonClicked(_ sender: UIBarButtonItem) {
        
        var arrayList = ["Filter" , "Clear"]
        
        DispatchQueue.main.async(execute: {() -> Void in
            let popup = PopUpTableViewController()
            
            popup.itemsArray = arrayList
            
            popup.sourceView = self.menuBarBtn
            popup.isScroll = true
            popup.backgroundColor = UIColor.white
            if popup.itemsArray.count > 5{
                popup.popUpHeight = 200
            }
            else{
                popup.popUpHeight = CGFloat(popup.itemsArray.count)*45
            }
            popup.popUpWidth = UIScreen.main.bounds.size.width-60
            popup.backgroundImage = nil
            popup.itemTitleColor = UIColor.white
            popup.itemSelectionColor = UIColor.lightGray
            popup.arrowDirections = .any
            popup.arrowColor = UIColor.white
            popup.popCellBlock = {(_ popupVC: PopUpTableViewController, _ popupCell: UITableViewCell, _ row: Int, _ section: Int) -> Void in
                
                if(arrayList[row] == "Filter") {
                    self.getDataGraphOverallStats()
                    
                } else {
                    FilterGraphStruct.location_Name = "Select Location"
                    FilterGraphStruct.area_Name = "Select Area"
                    FilterGraphStruct.sub_area_Name = "Select Sub-Area"
                    
                    
                    self.locationBtn.setTitle( FilterGraphStruct.location_Name, for: .normal)
                    
                    
                    
                    self.locationCrosBtn.setBackgroundImage(UIImage(named: "icons8-sort"), for: .normal)
                    
                    
                    
                    
                    self.locationBtn.setTitleColor(UIColor.darkGray, for: .normal)
                    
                    FilterGraphStruct.location_ID = String()
                    
                    FilterGraphStruct.location_Name = String()
                    FilterGraphStruct.area_Name = "Select Area"
                    FilterGraphStruct.sub_area_Name = "Select Sub-Area"
                    
                    self.areaBtn.setTitle( FilterGraphStruct.area_Name, for: .normal)
                    self.subAreaBtn.setTitle( FilterGraphStruct.sub_area_Name, for: .normal)
                    
                    //    self.businessCrosBtn.setBackgroundImage(UIImage(named: "cross"), for: .normal)
                    //   self.locationCrosBtn.setBackgroundImage(UIImage(named: "cross"), for: .normal)
                    self.areaCrosBtn.setBackgroundImage(UIImage(named: "icons8-sort"), for: .normal)
                    self.subAreaCrosBtn.setBackgroundImage(UIImage(named: "icons8-sort"), for: .normal)
                    
                    //     self.businessBtn.setTitleColor(UIColor.black, for: .normal)
                    
                    //    self.locationBtn.setTitleColor(UIColor.black, for: .normal)
                    
                    self.areaBtn.setTitleColor(UIColor.darkGray, for: .normal)
                    
                    self.subAreaBtn.setTitleColor(UIColor.darkGray, for: .normal)
                    
                    FilterGraphStruct.area_ID = String()
                    FilterGraphStruct.sub_area_ID = String()
                    FilterGraphStruct.department_Name = "Select Department"
                    self.departmentBtn.setTitle( FilterGraphStruct.department_Name, for: .normal)
                    
                    
                    
                    
                    FilterGraphStruct.department_NameID = String()
                    
                    
                    
                    
                    self.departmentBtn.setTitleColor(UIColor.darkGray, for: .normal)
                    self.departmentCrosBtn.setBackgroundImage(UIImage(named: "icons8-sort"), for: .normal)
                    
                    FilterDataFromServer.sub_Area_Id = Int()
                    FilterDataFromServer.sub_Area_Name = String()
                    FilterDataFromServer.area_Id = Int()
                    FilterDataFromServer.area_Name = String()
                    FilterDataFromServer.unit_Id = Int()
                    FilterDataFromServer.unit_Name = String()
                    FilterDataFromServer.sub_Area_Id = Int()
                    FilterDataFromServer.sub_Area_Name = String()
                    FilterDataFromServer.location_name = String()
                    FilterDataFromServer.location_id = Int()
                    self.locationUpdate()
                    
                    self.getDataGraphOverallStats()
                }
      
                
                
                popupVC.dismiss(animated: false, completion: nil)
                
            }
            self.present(popup, animated: true, completion: {() -> Void in
            })
            
            
        })
        
    }
    var GrapListOverallStatsAPI = GraphDataOverallAPI()
    var GrapListOverallStatsDB:[MyGraphModel] = []
    var OverallStatsTotalCount : [Int] = []
    var OverallStatsStatus : [String] = []
    var OverallStatsStatusShow : [String] = []
    var OverallStatsColor : [String] = []
    var para = [String:String]()
    @objc func getDataGraphOverallStats(){
        var filterHazard = String()
      
        let parameter = [
         "Unit":String(FilterGraphStruct.unit_Id)
             ,"Zone":String(FilterGraphStruct.location_ID)
            ,"Area":String(FilterGraphStruct.area_ID)
            ,"Sub_Area":String(FilterGraphStruct.sub_area_ID)]
        
        para = parameter.filter { $0.value != ""}
        print("para",para)
        self.GrapListOverallStatsDB = []
       
        self.OverallStatsTotalCount = []
        self.OverallStatsStatus = []
        self.OverallStatsStatusShow = []
        self.OverallStatsColor = []
        GrapListOverallStatsAPI.serviceCallingOverall(obj: self ,parameters: para ) { (dict) in
            //
            
            self.GrapListOverallStatsDB = [MyGraphModel]()
            self.GrapListOverallStatsDB = dict as! [MyGraphModel]
            
             
            
        }
    }
    func setChart2(dataPoints: [String], values: [Int] , colorCode : [String] ) {
        
        
        
        var dataEntries1: [PieChartDataEntry] = []
        
        
        
        for (index1,_) in dataPoints.enumerated()
        {
            
            
            let pieDataEntry1 = PieChartDataEntry(value: Double(Int(values[index1])), label: dataPoints[index1],data:dataPoints[index1] as AnyObject?)
            
            dataEntries1.append(pieDataEntry1)
            
        }
        
        
        print("\(dataEntries1)")
        
        var colors2  = [UIColor]()
        
        for index2 in colorCode
        {
            
            let someColor = UIColor(hexString: index2, alpha: 1.0)
            
            colors2.append(someColor!)
            
        }
        
        print("colors\(colors2)")
        
        let chartDataSet = PieChartDataSet(values: dataEntries1, label: "")
        
      
        let chartData = PieChartData()
       
        
        chartData.addDataSet(chartDataSet)
        
        chartDataSet.colors = colors2
        let format = NumberFormatter()
        format.numberStyle = .none
        let formatter = DefaultValueFormatter(formatter: format)
        chartData.setValueFormatter(formatter)
        PieChart.data = chartData
        
        //        PieChartOverallStats.drawEntryLabelsEnabled = true
        //        PieChartOverallStats.drawCenterTextEnabled = true
        //
        //        PieChartOverallStats.contentScaleFactor = 5.0
        //
        //        chartDataSet.sliceSpace = 2
        //        chartDataSet.selectionShift = 5
        //PieChartOverallStats.drawSlicesUnderHoleEnabled = true
        
    }
    @IBOutlet weak var infoBarButton: UIBarButtonItem!
    let menuBarBtn = UIButton(type: .custom)
    var menuBarItemBtn = UIBarButtonItem()
    override func viewDidLoad() {
        super.viewDidLoad()
      
      
        self.menuBarBtn.setImage(UIImage(named: "info"), for: .normal)
        self.menuBarBtn.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        self.menuBarBtn.addTarget(self, action: #selector(GraphViewController.infoBarButtonClicked), for: .touchUpInside)
        self.menuBarItemBtn = UIBarButtonItem(customView: self.menuBarBtn)
        self.navigationItem.setRightBarButtonItems([self.menuBarItemBtn], animated: true)
        
        
        areaView.layer.cornerRadius = 10
        areaView.layer.borderWidth = 0.5
        areaView.layer.borderColor = UIColor.black.cgColor
        
        subAreaView.layer.cornerRadius = 10
        subAreaView.layer.borderWidth = 0.5
        subAreaView.layer.borderColor = UIColor.black.cgColor
        locationView.layer.cornerRadius = 10
        locationView.layer.borderWidth = 0.5
        locationView.layer.borderColor = UIColor.black.cgColor
        departmentView.layer.cornerRadius = 10
        departmentView.layer.borderWidth = 0.5
        departmentView.layer.borderColor = UIColor.black.cgColor
        PieChart.animate(xAxisDuration: 2.0, yAxisDuration: 3.0)
        PieChart.drawHoleEnabled = false
        
        PieChart.drawEntryLabelsEnabled = false
//        PieChart.layer.cornerRadius = 4.0
//        PieChart.layer.borderWidth = 1.0
        PieChart.chartDescription?.text = ""
        PieChart.delegate = self
        //PieChart.layer.borderColor = UIColor.lightGray.cgColor
       
    }
    
    
    @objc func locationUpdate(){
        
        
        if  FilterGraphStruct.area_ID == "" && FilterGraphStruct.sub_area_ID == "" {
            //            FilterGraphStruct.filterBylocation_Name = String()
            //            FilterGraphStruct.filterBylocation_ID = String()
        }else{
            //
        }
        
        
        
        switch FilterDataFromServer.unit_Name == "" {
        case true:
            self.areaBtn.setTitleColor(UIColor.darkGray, for: .normal)
            self.areaCrosBtn.setBackgroundImage(UIImage(named: "icons8-sort"), for: .normal)
            self.areaBtn.setTitle("Select Unit", for: .normal)
            break;
        default:
            self.areaBtn.setTitleColor(UIColor.black, for: .normal)
            self.areaCrosBtn.setBackgroundImage(UIImage(named: "cross"), for: .normal)
            self.areaBtn.setTitle(FilterDataFromServer.unit_Name, for: .normal)
            break;
        }
        
        switch FilterDataFromServer.area_Name == "" {
        case true:
            self.subAreaBtn.setTitleColor(UIColor.darkGray, for: .normal)
            self.subAreaCrosBtn.setBackgroundImage(UIImage(named: "icons8-sort"), for: .normal)
            self.subAreaBtn.setTitle("Select Area", for: .normal)
            break;
        default:
            self.subAreaBtn.setTitleColor(UIColor.black, for: .normal)
            self.subAreaCrosBtn.setBackgroundImage(UIImage(named: "cross"), for: .normal)
            self.subAreaBtn.setTitle(FilterDataFromServer.area_Name, for: .normal)
            break;
        }
     
        
        
        switch FilterDataFromServer.location_name == "" {
        case true:
            self.locationBtn.setTitleColor(UIColor.darkGray, for: .normal)
            self.locationCrosBtn.setBackgroundImage(UIImage(named: "icons8-sort"), for: .normal)
            self.locationBtn.setTitle("Select Zone", for: .normal)
            break;
        default:
            self.locationBtn.setTitleColor(UIColor.black, for: .normal)
            self.locationCrosBtn.setBackgroundImage(UIImage(named: "cross"), for: .normal)
            self.locationBtn.setTitle(FilterDataFromServer.location_name, for: .normal)
            break;
        }
        
        
        switch FilterDataFromServer.sub_Area_Name == "" {
        case true:
            self.departmentBtn.setTitleColor(UIColor.darkGray, for: .normal)
            self.departmentCrosBtn.setBackgroundImage(UIImage(named: "icons8-sort"), for: .normal)
            self.departmentBtn.setTitle("Select Area Location", for: .normal)
            break;
        default:
            self.departmentBtn.setTitleColor(UIColor.black, for: .normal)
            self.departmentCrosBtn.setBackgroundImage(UIImage(named: "cross"), for: .normal)
            self.departmentBtn.setTitle(FilterDataFromServer.sub_Area_Name, for: .normal)
            break;
        }
        
        
        
    }
    @IBAction func areaTap(_ sender: Any) {
        
        
        if (self.areaCrosBtn!.currentBackgroundImage?.isEqual(UIImage(named: "icons8-sort")))! {
            areaData()
            
            
        }else{
            
            
            //   FilterGraphStruct.locationType = "Location : "+FilterGraphStruct.location_Name
            FilterGraphStruct.unit_Name = "Select Unit"
            FilterGraphStruct.area_Name = "Select Area"
            
            self.areaBtn.setTitle( FilterGraphStruct.unit_Name, for: .normal)
            self.subAreaBtn.setTitle( FilterGraphStruct.area_Name, for: .normal)
            
            //    self.businessCrosBtn.setBackgroundImage(UIImage(named: "cross"), for: .normal)
            //   self.locationCrosBtn.setBackgroundImage(UIImage(named: "cross"), for: .normal)
            self.areaCrosBtn.setBackgroundImage(UIImage(named: "icons8-sort"), for: .normal)
            self.subAreaCrosBtn.setBackgroundImage(UIImage(named: "icons8-sort"), for: .normal)
            
            //     self.businessBtn.setTitleColor(UIColor.black, for: .normal)
            
            //    self.locationBtn.setTitleColor(UIColor.black, for: .normal)
            
            self.areaBtn.setTitleColor(UIColor.darkGray, for: .normal)
            
            self.subAreaBtn.setTitleColor(UIColor.darkGray, for: .normal)
            FilterGraphStruct.unit_Id = String()
            FilterGraphStruct.unit_Name = "Select Unit"
            FilterGraphStruct.area_ID = String()
            FilterGraphStruct.sub_area_ID = String()
            FilterGraphStruct.sub_area_Name = "Select Area Location"
            self.departmentBtn.setTitle( FilterGraphStruct.sub_area_Name, for: .normal)
            
            
            
            
          
            
            
            
            
            self.departmentBtn.setTitleColor(UIColor.darkGray, for: .normal)
            self.departmentCrosBtn.setBackgroundImage(UIImage(named: "icons8-sort"), for: .normal)
            
            FilterDataFromServer.unit_Id = Int()
            FilterDataFromServer.unit_Name = String()
            FilterDataFromServer.area_Id = Int()
            FilterDataFromServer.area_Name = String()
            FilterDataFromServer.sub_Area_Id = Int()
            FilterDataFromServer.sub_Area_Name = String()
            locationUpdate()
            
            getDataGraphOverallStats()
            
            
            
        }
        
    }
    
    @IBAction func subAreaTap(_ sender: Any) {
        
        
        if (self.subAreaCrosBtn!.currentBackgroundImage?.isEqual(UIImage(named: "icons8-sort")))! {
            subAreaData()
            
            
        }else{
            
            
            FilterGraphStruct.locationType = "Area : "+FilterGraphStruct.area_Name
            FilterGraphStruct.area_Name = "Select Area"
            
            self.subAreaBtn.setTitle( FilterGraphStruct.area_Name, for: .normal)
            
            //            self.businessCrosBtn.setBackgroundImage(UIImage(named: "cross"), for: .normal)
            //            self.locationCrosBtn.setBackgroundImage(UIImage(named: "cross"), for: .normal)
            self.areaCrosBtn.setBackgroundImage(UIImage(named: "cross"), for: .normal)
            self.subAreaCrosBtn.setBackgroundImage(UIImage(named: "icons8-sort"), for: .normal)
            
            //            self.businessBtn.setTitleColor(UIColor.black, for: .normal)
            //
            //            self.locationBtn.setTitleColor(UIColor.black, for: .normal)
            
            self.areaBtn.setTitleColor(UIColor.black, for: .normal)
            
            self.subAreaBtn.setTitleColor(UIColor.darkGray, for: .normal)
            
            
            
            
            FilterGraphStruct.sub_area_ID = String()
            
            FilterGraphStruct.sub_area_Name = "Select Area Location"
            self.departmentBtn.setTitle( FilterGraphStruct.sub_area_Name, for: .normal)
            
            
            
            
       
            
            
            
            
            self.departmentBtn.setTitleColor(UIColor.darkGray, for: .normal)
            self.departmentCrosBtn.setBackgroundImage(UIImage(named: "icons8-sort"), for: .normal)
            
            FilterDataFromServer.sub_Area_Id = Int()
            FilterDataFromServer.sub_Area_Name = String()
            
            FilterDataFromServer.area_Id = Int()
            FilterDataFromServer.area_Name = String()
            locationUpdate()
            getDataGraphOverallStats()
            
            
        }
        
    }
    
    
    @IBAction func departmentCrossAction(_ sender: Any) {
        
        
        if (departmentCrosBtn!.currentBackgroundImage?.isEqual(UIImage(named: "icons8-sort")))! {
            departmentData()
            
        }else{
            
            
            
            
            FilterGraphStruct.sub_area_Name = "Select Area Location"
            self.departmentBtn.setTitle( FilterGraphStruct.sub_area_Name, for: .normal)
            
            
            
            
            FilterGraphStruct.department_NameID = String()
            
            
            
            
            self.departmentBtn.setTitleColor(UIColor.darkGray, for: .normal)
            self.departmentCrosBtn.setBackgroundImage(UIImage(named: "icons8-sort"), for: .normal)
            
            FilterDataFromServer.sub_Area_Id = Int()
            FilterDataFromServer.sub_Area_Name = String()
            locationUpdate()
            getDataGraphOverallStats()
            //            self.locationBtn.setTitleColor(UIColor.darkGray, for: .normal)
            //            self.locationCrosBtn.setBackgroundImage(UIImage(named: "icons8-sort"), for: .normal)
            //
            //
            //            self.areaBtn.setTitleColor(UIColor.darkGray, for: .normal)
            //            self.areaCrosBtn.setBackgroundImage(UIImage(named: "icons8-sort"), for: .normal)
            //
            //
            //            self.subAreaBtn.setTitleColor(UIColor.darkGray, for: .normal)
            //            self.subAreaCrosBtn.setBackgroundImage(UIImage(named: "icons8-sort"), for: .normal)
            //
            //            FilterGraphStruct.locationType = String()
            
            
        }
        //
    }
    
    @IBAction func locationCrossAction(_ sender: Any) {
        
        
        if (self.locationCrosBtn!.currentBackgroundImage?.isEqual(UIImage(named: "icons8-sort")))! {
            locationData()
            
            
        }else{
            
            
            
            FilterGraphStruct.location_Name = "Select Location"
            FilterGraphStruct.area_Name = "Select Area"
            FilterGraphStruct.sub_area_Name = "Select Sub-Area"
            
            
            self.locationBtn.setTitle( FilterGraphStruct.location_Name, for: .normal)
            
            
            
            self.locationCrosBtn.setBackgroundImage(UIImage(named: "icons8-sort"), for: .normal)
            
            
            
            
            self.locationBtn.setTitleColor(UIColor.darkGray, for: .normal)
            
            FilterGraphStruct.location_ID = String()
            
            FilterGraphStruct.location_Name = String()
            FilterGraphStruct.area_Name = "Select Area"
            FilterGraphStruct.sub_area_Name = "Select Sub-Area"
            
            self.areaBtn.setTitle( FilterGraphStruct.area_Name, for: .normal)
            self.subAreaBtn.setTitle( FilterGraphStruct.sub_area_Name, for: .normal)
            
            //    self.businessCrosBtn.setBackgroundImage(UIImage(named: "cross"), for: .normal)
            //   self.locationCrosBtn.setBackgroundImage(UIImage(named: "cross"), for: .normal)
            self.areaCrosBtn.setBackgroundImage(UIImage(named: "icons8-sort"), for: .normal)
            self.subAreaCrosBtn.setBackgroundImage(UIImage(named: "icons8-sort"), for: .normal)
            
            //     self.businessBtn.setTitleColor(UIColor.black, for: .normal)
            
            //    self.locationBtn.setTitleColor(UIColor.black, for: .normal)
            
            self.areaBtn.setTitleColor(UIColor.darkGray, for: .normal)
            
            self.subAreaBtn.setTitleColor(UIColor.darkGray, for: .normal)
            
            FilterGraphStruct.area_ID = String()
            FilterGraphStruct.sub_area_ID = String()
            FilterGraphStruct.unit_Id = String()
            FilterGraphStruct.unit_Name = "Select Unit"
            FilterGraphStruct.sub_area_Name = "Select Area Location"
            self.departmentBtn.setTitle( FilterGraphStruct.sub_area_Name, for: .normal)
            
            
            
            
           
            
            
            
            
            self.departmentBtn.setTitleColor(UIColor.darkGray, for: .normal)
            self.departmentCrosBtn.setBackgroundImage(UIImage(named: "icons8-sort"), for: .normal)
            FilterDataFromServer.unit_Id = Int()
            FilterDataFromServer.unit_Name = String()
            FilterDataFromServer.sub_Area_Id = Int()
            FilterDataFromServer.sub_Area_Name = String()
            FilterDataFromServer.area_Id = Int()
            FilterDataFromServer.area_Name = String()
            
            FilterDataFromServer.location_name = String()
            FilterDataFromServer.location_id = Int()
            locationUpdate()
            getDataGraphOverallStats()
        }
        
    }
    @IBAction func areaCrossAction(_ sender: Any) {
        
        
        if (self.areaCrosBtn!.currentBackgroundImage?.isEqual(UIImage(named: "icons8-sort")))! {
            areaData()
            
            
        }else{
            
            
            //   FilterGraphStruct.locationType = "Location : "+FilterGraphStruct.location_Name
            FilterGraphStruct.area_Name = "Select Area"
            FilterGraphStruct.sub_area_Name = "Select Sub-Area"
            
            self.areaBtn.setTitle( FilterGraphStruct.area_Name, for: .normal)
            self.subAreaBtn.setTitle( FilterGraphStruct.sub_area_Name, for: .normal)
            
            //    self.businessCrosBtn.setBackgroundImage(UIImage(named: "cross"), for: .normal)
            //   self.locationCrosBtn.setBackgroundImage(UIImage(named: "cross"), for: .normal)
            self.areaCrosBtn.setBackgroundImage(UIImage(named: "icons8-sort"), for: .normal)
            self.subAreaCrosBtn.setBackgroundImage(UIImage(named: "icons8-sort"), for: .normal)
            
            //     self.businessBtn.setTitleColor(UIColor.black, for: .normal)
            
            //    self.locationBtn.setTitleColor(UIColor.black, for: .normal)
            
            self.areaBtn.setTitleColor(UIColor.darkGray, for: .normal)
            
            self.subAreaBtn.setTitleColor(UIColor.darkGray, for: .normal)
            
            FilterGraphStruct.area_ID = String()
            FilterGraphStruct.sub_area_ID = String()
            FilterGraphStruct.sub_area_Name = "Select Area Location"
            self.departmentBtn.setTitle( FilterGraphStruct.sub_area_Name, for: .normal)
            
            
            
            
            FilterGraphStruct.department_NameID = String()
            
            
            
            
            self.departmentBtn.setTitleColor(UIColor.darkGray, for: .normal)
            self.departmentCrosBtn.setBackgroundImage(UIImage(named: "icons8-sort"), for: .normal)
            
            FilterDataFromServer.sub_Area_Id = Int()
            FilterDataFromServer.sub_Area_Name = String()
            FilterDataFromServer.area_Id = Int()
            FilterDataFromServer.area_Name = String()
            FilterDataFromServer.sub_Area_Id = Int()
            FilterDataFromServer.sub_Area_Name = String()
            locationUpdate()
            getDataGraphOverallStats()
        }
        
    }
    @IBAction func subAreaCrossAction(_ sender: Any) {
        
        
        if (self.subAreaCrosBtn!.currentBackgroundImage?.isEqual(UIImage(named: "icons8-sort")))! {
            subAreaData()
            
            
        }else{
            
            FilterGraphStruct.locationType = "Area : "+FilterGraphStruct.area_Name
            FilterGraphStruct.sub_area_Name = "Select Sub-Area"
            
            self.subAreaBtn.setTitle( FilterGraphStruct.sub_area_Name, for: .normal)
            
            //            self.businessCrosBtn.setBackgroundImage(UIImage(named: "cross"), for: .normal)
            //            self.locationCrosBtn.setBackgroundImage(UIImage(named: "cross"), for: .normal)
            self.areaCrosBtn.setBackgroundImage(UIImage(named: "cross"), for: .normal)
            self.subAreaCrosBtn.setBackgroundImage(UIImage(named: "icons8-sort"), for: .normal)
            
            //            self.businessBtn.setTitleColor(UIColor.black, for: .normal)
            //
            //            self.locationBtn.setTitleColor(UIColor.black, for: .normal)
            
            self.areaBtn.setTitleColor(UIColor.black, for: .normal)
            
            self.subAreaBtn.setTitleColor(UIColor.darkGray, for: .normal)
            
            
            
            
            FilterGraphStruct.sub_area_ID = String()
            
            FilterGraphStruct.sub_area_Name = "Select Area Location"
            self.departmentBtn.setTitle( FilterGraphStruct.sub_area_Name, for: .normal)
            
            
            
            
         
            
            
            
            
            self.departmentBtn.setTitleColor(UIColor.darkGray, for: .normal)
            self.departmentCrosBtn.setBackgroundImage(UIImage(named: "icons8-sort"), for: .normal)
            
            FilterDataFromServer.sub_Area_Id = Int()
            FilterDataFromServer.sub_Area_Name = String()
            
            FilterDataFromServer.sub_Area_Id = Int()
            FilterDataFromServer.sub_Area_Name = String()
            locationUpdate()
            getDataGraphOverallStats()
        }
        
    }
    
    
    
    
    func departmentData(){
        
        
        if(FilterDataFromServer.unit_Name != "" && FilterDataFromServer.area_Name != "" && FilterDataFromServer.location_name != "" ) {
            FilterDataFromServer.filterType = "Sub Area"
            
            
            FilterDataFromServer.sub_Area_Id = Int()
            FilterDataFromServer.sub_Area_Name = String()
            updateData()
            let storyBoard = UIStoryboard(name: "ThemeSafety", bundle: nil)
            let ZIVC = storyBoard.instantiateViewController(withIdentifier: "ThemeBasedASUZViewController") as! ThemeBasedASUZViewController
            ZIVC.titleStr = "Select Area Location"
            self.navigationController?.pushViewController(ZIVC, animated: true)
        }
        
        
        
        
        
        
        
    }
    //
    func locationData(){
        
       
        FilterDataFromServer.filterType = "Location"
        FilterDataFromServer.unit_Id = Int()
        FilterDataFromServer.unit_Name = String()
        FilterDataFromServer.area_Id = Int()
        FilterDataFromServer.area_Name = String()
        FilterDataFromServer.sub_Area_Id = Int()
        FilterDataFromServer.sub_Area_Name = String()
        FilterDataFromServer.location_name = String()
        FilterDataFromServer.location_id = Int()
        updateData()
        
        
        let storyBoard = UIStoryboard(name: "ThemeSafety", bundle: nil)
        let ZIVC = storyBoard.instantiateViewController(withIdentifier: "ThemeBasedASUZViewController") as! ThemeBasedASUZViewController
        ZIVC.titleStr = "Select Zone"
        self.navigationController?.pushViewController(ZIVC, animated: true)
        
    }
    
    func areaData(){
        
        
        
        if(FilterDataFromServer.location_name != "") {
            FilterDataFromServer.filterType = "Unit"
            FilterDataFromServer.unit_Id = Int()
            FilterDataFromServer.unit_Name = String()
            FilterDataFromServer.area_Id = Int()
            FilterDataFromServer.area_Name = String()
            FilterDataFromServer.sub_Area_Id = Int()
            FilterDataFromServer.sub_Area_Name = String()
            updateData()
            
            
            let storyBoard = UIStoryboard(name: "ThemeSafety", bundle: nil)
            let ZIVC = storyBoard.instantiateViewController(withIdentifier: "ThemeBasedASUZViewController") as! ThemeBasedASUZViewController
            ZIVC.titleStr = "Select Unit"
            self.navigationController?.pushViewController(ZIVC, animated: true)
        }
        
    }
    
    func subAreaData(){
        
        
        if(FilterDataFromServer.unit_Name != "" && FilterDataFromServer.location_name != "") {
            
            
            FilterDataFromServer.filterType = "Area"
            FilterDataFromServer.area_Id = Int()
            FilterDataFromServer.area_Name = String()
            
            FilterDataFromServer.sub_Area_Id = Int()
            FilterDataFromServer.sub_Area_Name = String()
            updateData()
            
            let storyBoard = UIStoryboard(name: "ThemeSafety", bundle: nil)
            let ZIVC = storyBoard.instantiateViewController(withIdentifier: "ThemeBasedASUZViewController") as! ThemeBasedASUZViewController
            ZIVC.titleStr = "Select Area"
            self.navigationController?.pushViewController(ZIVC, animated: true)
            
            
            
            
        }
        
    }
    @objc func updateData(){
        if(FilterDataFromServer.location_name != "") {
            FilterGraphStruct.location_Name = FilterDataFromServer.location_name
            FilterGraphStruct.location_ID = String(FilterDataFromServer.location_id)
        } else {
            FilterGraphStruct.location_Name = "Select Location"
            FilterGraphStruct.location_ID = String()
        }
        if(FilterDataFromServer.location_name != "" && FilterDataFromServer.unit_Name != "") {
            FilterGraphStruct.unit_Name = FilterDataFromServer.unit_Name
            FilterGraphStruct.unit_Id = String(FilterDataFromServer.unit_Id)
        } else {
            
            FilterGraphStruct.unit_Name = "Select Unit"
            FilterGraphStruct.unit_Id = String()
        }
        if(FilterDataFromServer.location_name != "" && FilterDataFromServer.unit_Name != "" && FilterDataFromServer.area_Name != "") {
            FilterGraphStruct.area_Name = FilterDataFromServer.area_Name
            FilterGraphStruct.area_ID = String(FilterDataFromServer.area_Id)
        } else {
            
            FilterGraphStruct.area_Name = "Select Area"
            FilterGraphStruct.area_ID = String()
        }
        if(FilterDataFromServer.location_name != "" && FilterDataFromServer.area_Name != "" && FilterDataFromServer.unit_Name != "" && FilterDataFromServer.sub_Area_Name != "") {
            FilterGraphStruct.sub_area_Name = FilterDataFromServer.sub_Area_Name
            FilterGraphStruct.sub_area_ID = String(FilterDataFromServer.sub_Area_Id)
        } else {
            
            FilterGraphStruct.department_Name = "Select Area Location"
            FilterGraphStruct.department_NameID = String()
        }
        if(FilterDataFromServer.hazard_Name != "") {
            
            FilterGraphStruct.hazard_Name = FilterDataFromServer.hazard_Name
            FilterGraphStruct.hazard_Name_ID = String(FilterDataFromServer.hazard_Id)
        } else {
            
            
            FilterGraphStruct.hazard_Name = "Select Hazard Type"
            FilterGraphStruct.hazard_Name_ID = String()
        }
    }
    
    
    func hazardData(){
        
    
        FilterDataFromServer.filterType = "Hazard"
        FilterDataFromServer.hazard_Id = Int()
        FilterDataFromServer.hazard_Name = String()
        
        
        updateData()
        let storyBoard = UIStoryboard(name: "ThemeSafety", bundle: nil)
        let ZIVC = storyBoard.instantiateViewController(withIdentifier: "ThemeBasedASUZViewController") as! ThemeBasedASUZViewController
        ZIVC.titleStr = "Select Hazard Type"
        self.navigationController?.pushViewController(ZIVC, animated: true)
        
    }
    
   
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        
        self.tabBarController?.tabBar.isHidden = true
        self.title = "Filter"
        let nav = self.navigationController?.navigationBar
        nav?.tintColor = UIColor.white
       nav?.barTintColor = UIColor(hexString: "2c3e50", alpha: 1.0)
        nav?.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        locationUpdate()
        updateData()
        getDataGraphOverallStats()
        
    }
    @IBOutlet weak var PieChart: PieChartView!
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBOutlet weak var navItem: UINavigationItem!
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
extension GraphViewController: ChartViewDelegate {
    func chartValueSelected(_ chartView: ChartViewBase, entry: ChartDataEntry, highlight: Highlight) {


    
        
        if let dataSet = chartView.data?.dataSets[ highlight.dataSetIndex] {
            
            let sliceIndex: Int = dataSet.entryIndex( entry: entry)
            print( "Selected slice index: \( sliceIndex)")
            
            let sta = self.OverallStatsStatusShow[sliceIndex]
            
            UserDefaults.standard.set(6, forKey: "hazard")
             UserDefaults.standard.set(sta, forKey: "Status")
              let storyBoard = UIStoryboard(name: "ThemeSafety", bundle: nil)
            let ZIVC = storyBoard.instantiateViewController(withIdentifier: "ReportedHazardViewController") as! ReportedHazardViewController
            
             //ZIVC.hazardtypeId = String(DashboardDB[(indexPath?.row)!].Type_ID)
            
            ZIVC.statusStr = sta
            ZIVC.locationStr = FilterGraphStruct.location_ID
            ZIVC.unitStr = FilterGraphStruct.area_ID
            ZIVC.AreaStr = FilterGraphStruct.sub_area_ID
            ZIVC.departmentStr = FilterGraphStruct.department_NameID
            
            self.navigationController?.pushViewController(ZIVC, animated: true)
            
//            let ReportedHazardVC = self.storyboard?.instantiateViewController(withIdentifier: "ReportedHazardViewController") as! ReportedHazardViewController
//
//            self.navigationController?.pushViewController(ReportedHazardVC, animated: true)
        }
        
        
        
        
        
        
        
        
        
        
        
    }
}
