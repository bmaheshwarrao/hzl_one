//
//  HazardDetailsModel.swift
//  VallSafety
//
//  Created by SARVANG INFOTCH on 13/10/18.
//  Copyright © 2018 Bunga Maheshwar Rao. All rights reserved.
//

import Foundation
import CoreData

/// Graph


var graphDB:[Graph] = []

func saveGraphData (status:String,color_Code:String,total_Count:Int64,privateContext:NSManagedObjectContext){
    
    //let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    let tasks = Graph(context: privateContext)
    
    tasks.status = status
    tasks.total_Count = total_Count
    tasks.color_Code = color_Code
    
    do {
        try privateContext.save()
    }catch{
        
    }
    
    
}

func deleteGraphData()
{
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    let req = NSFetchRequest<NSFetchRequestResult>.init(entityName: "Graph")
    let batchDeleteRequest = NSBatchDeleteRequest(fetchRequest: req)
    
    do {
        try context.execute(batchDeleteRequest)
        
    } catch {
        // Error Handling
    }
}

func getMainGraphData() {
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    graphDB = [Graph]()
    do {
        
        graphDB = try context.fetch(Graph.fetchRequest())
        
    } catch {
        print("Fetching Failed")
    }
}


class MyGraphModel: NSObject {
    
    
    

    
    var Total_Count : Int?
    var Status : String?
    var Color_Code : String?
    
    
    
    
    
    
    func setDataInModel(cell:[String:AnyObject])
    {
       
        if cell["Total_Count"] is NSNull || cell["Total_Count"] == nil{
            self.Total_Count = 0
        }else{
            
            
            let emp1 = cell["Total_Count"]
            let Total_cnt = Int((emp1?.description)!)
            self.Total_Count = Total_cnt
            
        }
        
        if cell["Status"] is NSNull || cell["Status"] == nil{
            self.Status = ""
        }else{
            let emp1 = cell["Status"]
            
            self.Status = (emp1?.description)!
            
        }
        if cell["Color_Code"] is NSNull || cell["Color_Code"] == nil{
            self.Color_Code = ""
        }else{
            let emp1 = cell["Color_Code"]
            
            self.Color_Code = (emp1?.description)!
            
        }
    }
}



class GraphDataAPI
{
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    var reachablty = Reachability()!
    var Color_Code = String()
    var status = String()
    var Total_Count = Int()
    
    func serviceCalling(obj:DashBoardViewController,parameters : [String:String] , success:@escaping (AnyObject)-> Void)
    {
        //obj.startLoading()
        
        if(reachablty.connection != .none)
        {
            
            
            WebServices.sharedInstances.sendPostRequest(url: URLConstants.Hazard_Graph, parameters: parameters, successHandler: { (dict) in
                
                if let response = dict["response"]{
                    
                    let statusString : String = response["status"] as! String
                    
                    if(statusString == "success")
                    {
                        
                        
                        if let data = dict["data"] as? [AnyObject]
                        {
                           
                            for i in 0..<data.count
                            {
                                
                                if let cell = data[i] as? [String:AnyObject]
                                {
                                var dataArray : [MyGraphModel] = []
                                
                                
                                let object = MyGraphModel()
                                    object.setDataInModel(cell: cell)
                                dataArray.append(object)
                                
                                
                                success(dataArray as AnyObject)
                                
                            }
                            }
                        }
                            
                            
                        
                            
                                
//                                DispatchQueue.main.async {
//                                    switch graphDB.count {
//                                    case 0:
//                                        NotificationCenter.default.post(name: NSNotification.Name.init("graphDataUpdate"), object: nil)
//                                        break;
//                                    default:
//                                        break;
//                                    }
//                                    
//                                }
                            
                       // obj.stopLoading()
                        }
                    else
                    {
                        print("DATA:fail")
                        // obj.tableView.isHidden = true
                        //obj.noDataLabel(text: "No Data Found!" )
                        // obj.refreshControl.endRefreshing()
                       // obj.stopLoading()
                        
                    }
                        
                        // obj.refreshControl.endRefreshing()
                        
                    
                    
                
                    
                    
                }
                
                
                
                
            })
            { (error) in
                
                let errorStr : String = error.description
                
                print("DATA:error",errorStr)
                
              //  obj.errorChecking(error: error)
                //obj.stopLoading()
            }
            
            
        }
        else{
          //  obj.stopLoading()
            
        }
        
        
    }
    
    
}


class GraphDataOverallAPI
{
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    var reachablty = Reachability()!
    var Color_Code = String()
    var status = String()
    var Total_Count = Int()
    
    func serviceCalling(obj:GenerateReportFrontViewController,parameters : [String:String] , success:@escaping (AnyObject)-> Void)
    {
        //obj.startLoading()
        
        if(reachablty.connection != .none)
        {
            
            
            WebServices.sharedInstances.sendPostRequest(url: URLConstants.GraphOverall, parameters: parameters, successHandler: { (dict) in
                
                if let response = dict["response"]{
                    
                    let statusString : String = response["status"] as! String
                    let objectmsg = MessageCallServerModel()
            let msg = objectmsg.sealizeMessage(cell: response as! [String : AnyObject])
                    print(response)
                    if(statusString == "success")
                    {
                        
                        
                        if let data = dict["data"] as? [AnyObject]
                        {
                            
                            for i in 0..<data.count
                            {
                                
                                if let cell = data[i] as? [String:AnyObject]
                                {
                                    var dataArray : [MyGraphModel] = []
                                    
                                    
                                    let object = MyGraphModel()
                                    object.setDataInModel(cell: cell)
                                    dataArray.append(object)
                                    
                                    
                                    success(dataArray as AnyObject)
                                    
                                }
                            }
                        }
                        
                        
                        
                        
                        
                        //                                DispatchQueue.main.async {
                        //                                    switch graphDB.count {
                        //                                    case 0:
                        //                                        NotificationCenter.default.post(name: NSNotification.Name.init("graphDataUpdate"), object: nil)
                        //                                        break;
                        //                                    default:
                        //                                        break;
                        //                                    }
                        //
                        //                                }
                        
                        // obj.stopLoading()
                    }
                    else
                    {
                        print("DATA:fail")
                        // obj.tableView.isHidden = true
                        //obj.noDataLabel(text: "No Data Found!" )
                        // obj.refreshControl.endRefreshing()
                        // obj.stopLoading()
                        
                    }
                    
                    // obj.refreshControl.endRefreshing()
                    
                    
                    
                    
                    
                    
                }
                
                
                
                
            })
            { (error) in
                
                let errorStr : String = error.description
                
                print("DATA:error",errorStr)
                
                //  obj.errorChecking(error: error)
                //obj.stopLoading()
            }
            
            
        }
        else{
            //  obj.stopLoading()
            
        }
        
        
    }
    
    
    
    func serviceCallingOverall(obj:GraphViewController,parameters : [String:String] , success:@escaping (AnyObject)-> Void)
    {
        obj.startLoading()
        
        if(reachablty.connection != .none)
        {
            
            
            WebServices.sharedInstances.sendPostRequest(url: URLConstants.GraphOverall, parameters: parameters, successHandler: { (dict) in
                
                if let response = dict["response"]{
                    
                    let statusString : String = response["status"] as! String
                    let objectmsg = MessageCallServerModel()
            let msg = objectmsg.sealizeMessage(cell: response as! [String : AnyObject])
                    print(response)
                    if(statusString == "success")
                    {
                        obj.stopLoading()
                        
                        if let data = dict["data"] as? [AnyObject]
                        {
                            var dataArray : [MyGraphModel] = []
                            for i in 0..<data.count
                            {
                                
                                if let cell = data[i] as? [String:AnyObject]
                                {
                                   //  dataArray  = []
                                    
                                    
                                    let object = MyGraphModel()
                                    object.setDataInModel(cell: cell)
                                    dataArray.append(object)
                                    
                                   
                                    
                                    
                                    
                                    
                                    success(dataArray as AnyObject)
                                    
                                }
                            }
                            
                            if(dataArray.count > 0) {
                                for i in 0..<dataArray.count {
                                    let tot : Int = dataArray[i].Total_Count!
                                    let total : String = String(tot)
                                    obj.OverallStatsTotalCount.append(tot)
                                    obj.OverallStatsStatus.append(dataArray[i].Status! + "-" + String(total))
                                    obj.OverallStatsColor.append(dataArray[i].Color_Code!)
                                    obj.OverallStatsStatusShow.append(dataArray[i].Status!)
                                    
                                }
                                obj.PieChart.drawCenterTextEnabled = false
                                obj.PieChart.isHidden = false;
                                obj.label.isHidden = true
                                obj.setChart2(dataPoints: obj.OverallStatsStatus, values: obj.OverallStatsTotalCount , colorCode :obj.OverallStatsColor)
                            }else{
                                obj.OverallStatsStatus = []
                                obj.OverallStatsTotalCount = []
                                obj.OverallStatsColor = []
                                  obj.setChart2(dataPoints: obj.OverallStatsStatus, values: obj.OverallStatsTotalCount , colorCode :obj.OverallStatsColor)
                              obj.PieChart.centerText = "No Chart Data Found"
                                obj.PieChart.drawCenterTextEnabled = true
                              
                            }
                            
                            
                        }
                        
                        
                        
                        
                        
                        //                                DispatchQueue.main.async {
                        //                                    switch graphDB.count {
                        //                                    case 0:
                        //                                        NotificationCenter.default.post(name: NSNotification.Name.init("graphDataUpdate"), object: nil)
                        //                                        break;
                        //                                    default:
                        //                                        break;
                        //                                    }
                        //
                        //                                }
                        
                        // obj.stopLoading()
                    }
                    else
                    {
                        print("DATA:fail")
                        // obj.tableView.isHidden = true
                      //  obj.noDataLabel(text: "No Data Found!" )
                        
                         obj.stopLoading()
                        obj.OverallStatsStatus = []
                        obj.OverallStatsTotalCount = []
                        obj.OverallStatsColor = []
                         obj.setChart2(dataPoints: obj.OverallStatsStatus, values: obj.OverallStatsTotalCount , colorCode :obj.OverallStatsColor)
                        obj.PieChart.centerText = "No Chart Data Found"
                        obj.PieChart.drawCenterTextEnabled = true
                        // obj.refreshControl.endRefreshing()
                        // obj.stopLoading()
                        
                    }
                    
                    // obj.refreshControl.endRefreshing()
                    
                    
                    
                    
                    
                    
                }
                
                
                
                
            })
            { (error) in
                
                let errorStr : String = error.description
                
                print("DATA:error",errorStr)
                
                //  obj.errorChecking(error: error)
                obj.stopLoading()
            }
            
            
        }
        else{
              obj.stopLoading()
            obj.OverallStatsStatus = []
            obj.OverallStatsTotalCount = []
            obj.OverallStatsColor = []
            obj.setChart2(dataPoints: obj.OverallStatsStatus, values: obj.OverallStatsTotalCount , colorCode :obj.OverallStatsColor)
            obj.PieChart.centerText = "No Chart Data Found"
            obj.PieChart.drawCenterTextEnabled = true
        }
        
        
    }
    
    
    
    
    
    
    
}


class GraphDataForMoreAPI
{
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    var reachablty = Reachability()!
    var Color_Code = String()
    var status = String()
    var Total_Count = Int()
    
    func serviceCalling(obj:GraphViewController,parameters : [String:String] , success:@escaping (AnyObject)-> Void)
    {
        obj.startLoading()
        
        if(reachablty.connection != .none)
        {
            
            
            WebServices.sharedInstances.sendPostRequest(url: URLConstants.Hazard_Graph, parameters: parameters, successHandler: { (dict) in
                
                if let response = dict["response"]{
                    
                    let statusString : String = response["status"] as! String
                    let objectmsg = MessageCallServerModel()
            let msg = objectmsg.sealizeMessage(cell: response as! [String : AnyObject])
                    print(response)
                    if(statusString == "success")
                    {
                        
                        
                        if let data = dict["data"] as? [AnyObject]
                        {
                            
                            for i in 0..<data.count
                            {
                                
                                if let cell = data[i] as? [String:AnyObject]
                                {
                                    var dataArray : [MyGraphModel] = []
                                    
                                    
                                    let object = MyGraphModel()
                                    object.setDataInModel(cell: cell)
                                    dataArray.append(object)
                                    
                                    
                                    success(dataArray as AnyObject)
                                    
                                }
                            }
                        }
                        
                        
                        
                        
                        
                        //                                DispatchQueue.main.async {
                        //                                    switch graphDB.count {
                        //                                    case 0:
                        //                                        NotificationCenter.default.post(name: NSNotification.Name.init("graphDataUpdate"), object: nil)
                        //                                        break;
                        //                                    default:
                        //                                        break;
                        //                                    }
                        //
                        //                                }
                        
                        obj.stopLoading()
                    }
                    else
                    {
                        print("DATA:fail")
                        obj.PieChart.isHidden = true;
                        obj.label.isHidden = false
                        obj.noDataLabel(text: msg)
                        
                        // obj.tableView.isHidden = true
                        //obj.noDataLabel(text: "No Data Found!" )
                        // obj.refreshControl.endRefreshing()
                        obj.stopLoading()
                        
                    }
                    
                    // obj.refreshControl.endRefreshing()
                    
                    
                    
                    
                    
                    
                }
                
                
                
                
            })
            { (error) in
                
                let errorStr : String = error.description
                
                print("DATA:error",errorStr)
                
                //  obj.errorChecking(error: error)
                obj.stopLoading()
            }
            
            
        }
        else{
            obj.stopLoading()
            
        }
        
        
    }
    
    
}

















class MyReportedHazardDetailsDataModel: NSObject {
    
    
    
  
    
    var areaId : String?
    var area_Name : String?
    var subareaId : String?
    var subarea_Name : String?
    var Assign_Employee_ID : String?
    var Assign_Employee_Name : String?
    var ID = Int()
    var Consequence  : Int?
    var Date_Time : String?
    var Description : String?
    var Employee_ID : String?
    var Employee_Name : String?
    var Hazard_Name : String?
    var Hazard_Type_ID : String?
    var Image_path : String?
    
    var Likelihood : Int?
    var Risk_Level : Int?
    var Status : String?
    
   
    
    var Status_Employee_ID : String?
    var Status_Employee_Name : String?
   
    var Unit_ID : String?
    var Zone_ID : String?
    var Unit_Name : String?
    var Zone_Name : String?
    
    
    
    func setDataInModel(cell:[String:AnyObject])
    {
        
        if cell["Assign_Employee_ID"] is NSNull || cell["Assign_Employee_ID"] == nil{
             self.Assign_Employee_ID = ""
        }else{
         
            let emp1 = cell["Assign_Employee_ID"]
            
            self.Assign_Employee_ID = (emp1?.description)!
        }
        if cell["Assign_Employee_Name"] is NSNull || cell["Assign_Employee_Name"] == nil{
            self.Assign_Employee_Name = ""
        }else{
      
            let emp1 = cell["Assign_Employee_Name"]
            
            self.Assign_Employee_Name = (emp1?.description)!
        }
        if cell["ID"] is NSNull || cell["ID"] == nil{
        
        }else{
            let emp1 = cell["ID"]
            let id = Int((emp1?.description)!)
            self.ID = id!
        }
        if cell["Consequence"] is NSNull || cell["Consequence"] == nil{
           self.Consequence = 0
        }else{
            let emp1 = cell["Zone_Name"]
            let cons = Int((emp1?.description)!)
            self.Consequence = cons
        }
        
        if cell["Date_Time"] is NSNull || cell["Date_Time"] == nil{
            self.Date_Time = ""
        }else{
           
            let emp1 = cell["Date_Time"]
            
            self.Date_Time = (emp1?.description)!
        }
        
        if cell["Description"] is NSNull || cell["Description"] == nil{
            self.Description = ""
        }else{
           
            let emp1 = cell["Description"]
            
            self.Description = (emp1?.description)!
        }
        if cell["Employee_ID"] is NSNull || cell["Employee_ID"] == nil{
             self.Employee_ID = ""
        }else{
            
            let emp1 = cell["Employee_ID"]
            
            self.Employee_ID = (emp1?.description)!
        }
        if cell["Employee_Name"] is NSNull || cell["Employee_Name"] == nil{
            self.Employee_Name = ""
        }else{
       
            let emp1 = cell["Employee_Name"]
            
            self.Employee_Name = (emp1?.description)!
        }
        
        if cell["Hazard_Name"] is NSNull || cell["Hazard_Name"] == nil{
            self.Hazard_Name = ""
        }else{
        
            
            let emp1 = cell["Hazard_Name"]
            
            self.Hazard_Name = (emp1?.description)!
        }
        if cell["Hazard_Type_ID"] is NSNull || cell["Hazard_Type_ID"] == nil{
              self.Hazard_Type_ID = ""
        }else{
          
            let emp1 = cell["Hazard_Type_ID"]
            
            self.Hazard_Type_ID = (emp1?.description)!
          
        }
        if cell["Image_path"] is NSNull || cell["Image_path"] == nil{
            self.Image_path = ""
        }else{
            let emp1 = cell["Image_path"]
            
            self.Image_path = (emp1?.description)!
        }
        
        if cell["Likelihood"] is NSNull || cell["Likelihood"] == nil{
            self.Likelihood = 0
        }else{
            let emp1 = cell["Likelihood"]
            let like = Int((emp1?.description)!)
            self.Likelihood = like
        }
        if cell["Risk_Level"] is NSNull || cell["Risk_Level"] == nil{
            self.Risk_Level = 0
        }else{
            let emp1 = cell["Risk_Level"]
            let risk = Int((emp1?.description)!)
            self.Risk_Level = risk
        }
        
        
        if cell["Status"] is NSNull || cell["Status"] == nil{
            self.Status = ""
        }else{
            let emp1 = cell["Status"]
            
            self.Status = (emp1?.description)!
        }
        if cell["Status_Employee_ID"] is NSNull  || cell["Status_Employee_ID"] == nil{
            self.Status_Employee_ID = ""
        }else{
           
            let emp1 = cell["Status_Employee_ID"]
            
            self.Status_Employee_ID = (emp1?.description)!
        }
        if cell["Status_Employee_Name"] is NSNull || cell["Status_Employee_Name"] == nil{
            self.Status_Employee_Name = ""
        }else{
            let emp1 = cell["Status_Employee_Name"]
            
            self.Status_Employee_Name = (emp1?.description)!
        }
        if cell["Area_ID"] is NSNull || cell["Area_ID"] == nil{
              self.areaId = "0"
        }else{
          
            let emp1 = cell["Area_ID"]
            
            self.areaId = (emp1?.description)!
        }
        if cell["Area_Name"] is NSNull || cell["Area_Name"] == nil{
            self.area_Name = ""
        }else{
        
            let emp1 = cell["Area_Name"]
            
            self.area_Name = (emp1?.description)!
        }
        
        
        if cell["Sub_Area_ID"] is NSNull || cell["Sub_Area_ID"] == nil{
            self.subareaId = "0"
        }else{
        
            
            let emp1 = cell["Sub_Area_ID"]
            
            self.subareaId = (emp1?.description)!
        }
        if cell["SubArea_Name"] is NSNull || cell["SubArea_Name"] == nil{
            self.subarea_Name = ""
        }else{
        
            
            
            let emp1 = cell["SubArea_Name"]
            
            self.subarea_Name = (emp1?.description)!
        }
        
        
        if cell["Unit_ID"] is NSNull || cell["Unit_ID"] == nil{
            self.Unit_ID = "0"
        }else{
           
            
            
            let emp1 = cell["Unit_ID"]
            
            self.Unit_ID = (emp1?.description)!
        }
        if cell["Unit_Name"] is NSNull || cell["Unit_Name"] == nil{
            self.Unit_Name = ""
        }else{
           
            let emp1 = cell["Unit_Name"]
            
            self.Unit_Name = (emp1?.description)!
        }
        if cell["Zone_ID"] is NSNull || cell["Zone_ID"] == nil{
            self.Zone_ID = "0"
        }else{
            let emp1 = cell["Zone_ID"]
            
            self.Zone_ID = (emp1?.description)!
        }
        if cell["Zone_Name"] is NSNull || cell["Zone_Name"] == nil{
            self.Zone_Name = ""
        }else{
          
            let emp1 = cell["Zone_Name"]
            
            self.Zone_Name = (emp1?.description)!
            
        }
        
        
        
        
    }
}


class MyReportedHazardDetailsDataAPI
{
    
    var reachablty = Reachability()!
    
    
    func serviceCalling(obj:HazardDetailsViewController,hazardID:String,pNo:String, success:@escaping (AnyObject)-> Void)
    {
        obj.startLoading()
        
        if(reachablty.connection != .none)
        {
            
            WebServices.sharedInstances.sendPostRequest(url: URLConstants.Hazard_Details, parameters: ["Hazard_ID":hazardID], successHandler: { (dict) in
                
                obj.tableView.isHidden = false
                obj.noDataLabel(text: "" )
                obj.label.isHidden = true
                if let response = dict["response"]{
                    let objectmsg = MessageCallServerModel()
            let msg = objectmsg.sealizeMessage(cell: response as! [String : AnyObject])
                    print(response)
                    let statusString : String = response["status"] as! String
                    
                    if(statusString == "success")
                    {
                        
                        if let data = dict["data"] as? [String:AnyObject]
                        {
                            var dataArray : [MyReportedHazardDetailsDataModel] = []
                            
                            
                            let object = MyReportedHazardDetailsDataModel()
                            object.setDataInModel(cell: data)
                            dataArray.append(object)
                            
                            
                            success(dataArray as AnyObject)
                            
                        }
                        
                        obj.refresh.endRefreshing()
                       
                        obj.stopLoading()
                        //obj.tableView.reloadData()
                    }
                    else
                    {
                        obj.tableView.isHidden = true
                        obj.noDataLabel(text: msg )
                        obj.label.isHidden = false
                        // obj.tableView.isHidden = true
                        //obj.noDataLabel(text: "No Data Found!" )
                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                        
                    }
                    
                    
                }
                
                
                
                
            })
            { (error) in
                
                let errorStr : String = error.description
                
                print("DATA:error",errorStr)
                  obj.label.isHidden = true
               // obj.errorChecking(error: error)
                obj.refresh.endRefreshing()
                obj.stopLoading()
            }
            
            
        }
        else{
             obj.label.isHidden = true
            obj.refresh.endRefreshing()
             obj.stopLoading()
        }
        
        
    }
    
    
}



class HazardDetailsDataAPICLOSE
{
    
    var reachablty = Reachability()!
    
    
    func serviceCalling(obj:CloseViewController,hazardID:String,pNo:String, success:@escaping (AnyObject)-> Void)
    {
        obj.startLoading()
        
        if(reachablty.connection != .none)
        {
            
            WebServices.sharedInstances.sendPostRequest(url: URLConstants.Hazard_Details, parameters: ["Hazard_ID":hazardID], successHandler: { (dict) in
                
                obj.tableView.isHidden = false
                obj.noDataLabel(text: "" )
                obj.label.isHidden = true
                if let response = dict["response"]{
                    
                    let statusString : String = response["status"] as! String
                    let objectmsg = MessageCallServerModel()
            let msg = objectmsg.sealizeMessage(cell: response as! [String : AnyObject])
                    print(response)
                    if(statusString == "success")
                    {
                        
                        if let data = dict["data"] as? [String:AnyObject]
                        {
                            var dataArray : [MyReportedHazardDetailsDataModel] = []
                            
                            
                            let object = MyReportedHazardDetailsDataModel()
                            object.setDataInModel(cell: data)
                            dataArray.append(object)
                            
                            
                            success(dataArray as AnyObject)
                            
                        }
                        
                        obj.refresh.endRefreshing()
                        
                        obj.stopLoading()
                        //obj.tableView.reloadData()
                    }
                    else
                    {
                        
                         obj.tableView.isHidden = true
                        obj.noDataLabel(text: msg )
                        obj.label.isHidden = false
                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                        
                    }
                    
                    
                }
                
                
                
                
            })
            { (error) in
                
                let errorStr : String = error.description
                
                print("DATA:error",errorStr)
                
                // obj.errorChecking(error: error)
                obj.refresh.endRefreshing()
                obj.stopLoading()
            }
            
            
        }
        else{
            obj.refresh.endRefreshing()
            obj.stopLoading()
        }
        
        
    }
    
    
}


class HazardDetailsDataAPIAssign
{
    
    var reachablty = Reachability()!
    
    
    func serviceCalling(obj:AssignSavedViewController,hazardID:String,pNo:String, success:@escaping (AnyObject)-> Void)
    {
        obj.startLoading()
        
        if(reachablty.connection != .none)
        {
            
            WebServices.sharedInstances.sendPostRequest(url: URLConstants.Hazard_Details, parameters: ["Hazard_ID":hazardID], successHandler: { (dict) in
                
                
                if let response = dict["response"]{
                    
                    let statusString : String = response["status"] as! String
                    
                    if(statusString == "success")
                    {
                        
                        if let data = dict["data"] as? [String:AnyObject]
                        {
                            var dataArray : [MyReportedHazardDetailsDataModel] = []
                            
                            
                            let object = MyReportedHazardDetailsDataModel()
                            object.setDataInModel(cell: data)
                            dataArray.append(object)
                            
                            
                            success(dataArray as AnyObject)
                            
                        }
                        
                        obj.refresh.endRefreshing()
                        
                        obj.stopLoading()
                        //obj.tableView.reloadData()
                    }
                    else
                    {
                        
                        // obj.tableView.isHidden = true
                        //obj.noDataLabel(text: "No Data Found!" )
                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                        
                    }
                    
                    
                }
                
                
                
                
            })
            { (error) in
                
                let errorStr : String = error.description
                
                print("DATA:error",errorStr)
                
                // obj.errorChecking(error: error)
                obj.refresh.endRefreshing()
                obj.stopLoading()
            }
            
            
        }
        else{
            obj.refresh.endRefreshing()
            obj.stopLoading()
        }
        
        
    }
    
    
}


class HazardDetailsDataAPIForward
{
    
    var reachablty = Reachability()!
    
    
    func serviceCalling(obj:ForwardViewController,hazardID:String,pNo:String, success:@escaping (AnyObject)-> Void)
    {
        obj.startLoading()
        
        if(reachablty.connection != .none)
        {
            
            WebServices.sharedInstances.sendPostRequest(url: URLConstants.Hazard_Details, parameters: ["Hazard_ID":hazardID], successHandler: { (dict) in
                
                
                if let response = dict["response"]{
                    
                    let statusString : String = response["status"] as! String
                    
                    if(statusString == "success")
                    {
                        
                        if let data = dict["data"] as? [String:AnyObject]
                        {
                            var dataArray : [MyReportedHazardDetailsDataModel] = []
                            
                            
                            let object = MyReportedHazardDetailsDataModel()
                            object.setDataInModel(cell: data)
                            dataArray.append(object)
                            
                            
                            success(dataArray as AnyObject)
                            
                        }
                        
                        obj.refresh.endRefreshing()
                        
                        obj.stopLoading()
                        //obj.tableView.reloadData()
                    }
                    else
                    {
                        
                        // obj.tableView.isHidden = true
                        //obj.noDataLabel(text: "No Data Found!" )
                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                        
                    }
                    
                    
                }
                
                
                
                
            })
            { (error) in
                
                let errorStr : String = error.description
                
                print("DATA:error",errorStr)
                
                // obj.errorChecking(error: error)
                obj.refresh.endRefreshing()
                obj.stopLoading()
            }
            
            
        }
        else{
            obj.refresh.endRefreshing()
            obj.stopLoading()
        }
        
        
    }
    
    
}

class HazardDetailsDataAPIReject
{
    
    var reachablty = Reachability()!
    
    
    func serviceCalling(obj:RejectViewController,hazardID:String,pNo:String, success:@escaping (AnyObject)-> Void)
    {
        obj.startLoading()
        
        if(reachablty.connection != .none)
        {
            
            WebServices.sharedInstances.sendPostRequest(url: URLConstants.Hazard_Details, parameters: ["Hazard_ID":hazardID], successHandler: { (dict) in
                
                
                if let response = dict["response"]{
                    
                    let statusString : String = response["status"] as! String
                    
                    if(statusString == "success")
                    {
                        
                        if let data = dict["data"] as? [String:AnyObject]
                        {
                            var dataArray : [MyReportedHazardDetailsDataModel] = []
                            
                            
                            let object = MyReportedHazardDetailsDataModel()
                            object.setDataInModel(cell: data)
                            dataArray.append(object)
                            
                            
                            success(dataArray as AnyObject)
                            
                        }
                        
                        obj.refresh.endRefreshing()
                        
                        obj.stopLoading()
                        //obj.tableView.reloadData()
                    }
                    else
                    {
                        
                        // obj.tableView.isHidden = true
                        //obj.noDataLabel(text: "No Data Found!" )
                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                        
                    }
                    
                    
                }
                
                
                
                
            })
            { (error) in
                
                let errorStr : String = error.description
                
                print("DATA:error",errorStr)
                
                // obj.errorChecking(error: error)
                obj.refresh.endRefreshing()
                obj.stopLoading()
            }
            
            
        }
        else{
            obj.refresh.endRefreshing()
            obj.stopLoading()
        }
        
        
    }
    
    
}


class AreaManagersDataListDataInDetailsModel: NSObject {
    
    var name = String()
    var mobileNo = String()
    var email = String()
    var Employee_ID = String()
    
    func setDataInModel(str:[String:AnyObject])
    {
        
        if str["Employee_Name"] is NSNull || str["Employee_Name"] == nil {
            self.name = ""
        }else{
            
            
            let emp1 = str["Employee_Name"]
            
            self.name = (emp1?.description)!
        }
        if str["Mobile_Number"] is NSNull || str["Mobile_Number"] == nil{
            self.mobileNo = ""
        }else{
           
            let emp1 = str["Mobile_Number"]
            
            self.mobileNo = (emp1?.description)!
        }
        if str["Employee_ID"] is NSNull || str["Employee_ID"] == nil{
            self.Employee_ID = ""
        }else{
       
            let emp1 = str["Employee_ID"]
            
            self.Employee_ID = (emp1?.description)!
        }
        if str["Email_ID"] is NSNull || str["Email_ID"] == nil{
            self.email = ""
        }else{
       
            let emp1 = str["Email_ID"]
            
            self.email = (emp1?.description)!
        }
        
       
    }
}




class AreaManagersDataListDataInDetailsAPI
{
    
    var reachablty = Reachability()!
    
    
    func serviceCalling(obj:HazardDetailsViewController, hazard_ID:String,pNo:String, success:@escaping (AnyObject)-> Void)
    {
        
        obj.startLoading()
       
        
        if(reachablty.connection != .none)
        {
            
            WebServices.sharedInstances.sendPostRequest(url: URLConstants.Area_Managers_List, parameters: ["Hazard_ID":hazard_ID,"Name":"Area_Manager"], successHandler: { (dict) in
                
                
                if let response = dict["response"]{
                    
                    let statusString : String = response["status"] as! String
                    
                    if(statusString == "success")
                    {
                        
                        //                        obj.tableView.isHidden = false
                        //                        obj.noDataLabel(text: "" )
                        
                        if let data = dict["data"] as? [AnyObject]
                        {
                            
                            var dataArray : [AreaManagersListDataInDetailsModel] = []
                            
                            for i in 0..<data.count
                            {
                                
                                if let cell = data[i] as? [String:AnyObject]
                                {
                                    let object = AreaManagersListDataInDetailsModel()
                                    object.setDataInModel(str: cell)
                                    dataArray.append(object)
                                    
                                }
                                
                            }
                            obj.lblInt = 0
                            obj.tableView.reloadData()
                            success(dataArray as AnyObject)
                            
                            
                        }
                        
                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                    }
                    else
                    {
                        obj.lblInt = 1
                        obj.tableView.reloadData()
                        print("DATA:fail")
                        //                        obj.tableView.isHidden = true
                        //                        obj.noDataLabel(text: "Sorry! No area manager found with name "+name)
                        obj.refresh.endRefreshing()
                         obj.stopLoading()
                        
                    }
                    
                    
                }
                
                
                
            }, failureHandler: { (error) in
                let errorStr : String = error.description
                obj.lblInt = 1
                obj.tableView.reloadData()
                print("DATA:error",errorStr)
                
              //  obj.errorChecking(error: error)
                 obj.stopLoading()
            })
            
            
        }
        else{
            //            obj.noDataLabel(text:  "No internet connection found. Check your internet connection and try again.")
            //            obj.tableView.isHidden = true
            obj.refresh.endRefreshing()
             obj.stopLoading()
            obj.lblInt = 1
            obj.tableView.reloadData()
        }
        
        
    }
    
    
}

