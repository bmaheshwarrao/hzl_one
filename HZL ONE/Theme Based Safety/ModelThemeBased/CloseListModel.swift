//
//  CloseListModel.swift
//  sesagoa
//
//  Created by SARVANG INFOTCH on 19/12/18.
//  Copyright © 2018 Bunga Maheshwar Rao. All rights reserved.
//

//
//  LeadershipModel.swift
//  sesagoa
//
//  Created by SARVANG INFOTCH on 15/12/18.
//  Copyright © 2018 Bunga Maheshwar Rao. All rights reserved.
//

import Foundation


class CloseList: NSObject {
    
    
    

    var Employee_Name : String?
    var ID  = Int()
    
    var Hazard_ID : String?
    var Date_Time : String?
    var Remark : String?
    var image_path : String?
    
    var Employee_ID : String?
    
    var status : String?
    
 
    
    
    func setDataInModel(cell:[String:AnyObject])
    {
        
        
        
        
        
     
        
        
        
        
        
        
        
        
        if cell["ID"] is NSNull{
            
        }else{
            self.ID = (cell["ID"] as? Int)!
        }
        if cell["Employee_ID"] is NSNull{
            self.Employee_ID = ""
        }else{
            self.Employee_ID = (cell["Employee_ID"] as? String)!
        }
        if cell["Hazard_ID"] is NSNull{
            self.Hazard_ID = ""
        }else{
            self.Hazard_ID = (cell["Hazard_ID"] as? String)!
        }
        if cell["Employee_Name"] is NSNull{
            self.Employee_Name = ""
        }else{
            self.Employee_Name = (cell["Employee_Name"] as? String)!
        }
        
        if cell["Status"] is NSNull{
            
        }else{
            self.status = (cell["Status"] as? String)!
        }
       
        if cell["Remark"] is NSNull{
            self.Remark = ""
        }else{
            self.Remark = (cell["Remark"] as? String)!
        }
        if cell["Date_Time"] is NSNull{
            self.Date_Time = ""
        }else{
            self.Date_Time = (cell["Date_Time"] as? String)!
        }
        if cell["Image"] is NSNull{
            self.image_path = ""
        }else{
            self.image_path = (cell["Image"] as? String)!
        }
        
        
        
        
        
        
        
        
        
        
        
    }
}


class CloseListData
{
    
    var reachablty = Reachability()!
    
    
    func serviceCalling(obj:CloseListViewController,parameter:[String:String], success:@escaping (AnyObject)-> Void)
    {
     
        obj.startLoading(view: obj.tableView)
        
        if(reachablty.connection != .none)
        {
            
            WebServices.sharedInstances.sendPostRequest(url: URLConstants.Hazard_Process_Details, parameters: parameter, successHandler: { (dict) in
                
                if let response = dict["response"]{
                    
                    
                    let statusString : String = response["status"] as! String
                    let objectmsg = MessageCallServerModel()
            let msg = objectmsg.sealizeMessage(cell: response as! [String : AnyObject])
                    print(response)
                    if(statusString == "success")
                    {
                        
                        obj.noDataLabel(text:  "")
                        obj.label.isHidden = true
                        
                        obj.stopLoading(view: obj.tableView)
                        if let data = dict["data"] as? [AnyObject]
                        {
                            
                            var dataArray : [CloseList] = []
                            
                            for i in 0..<data.count
                            {
                                
                                if let celll = data[i] as? [String:AnyObject]
                                {
                                    let object = CloseList()
                                    object.setDataInModel(cell: celll)
                                    dataArray.append(object)
                                }
                                
                            }
                            
                            success(dataArray as AnyObject)
                            
                            
                        }
                        
                        obj.refresh.endRefreshing()
                        obj.stopLoading(view: obj.tableView)
                    }
                    else
                    {
                        
                        
                        print("DATA:fail")
                        
                        
                        obj.noDataLabel(text:  msg)
                        obj.label.isHidden = false
                        obj.refresh.endRefreshing()
                        obj.stopLoading(view: obj.tableView)
                        //  obj.stopLoading()
                        
                    }
                    
                    
                }
                
                
                
            }, failureHandler: { (error) in
                let errorStr : String = error.description
                
                print("DATA:error",errorStr)
                
                //obj.errorChecking(error: error)
                  obj.stopLoading()
            })
            
            
        }
        else{
            
              obj.noDataLabel(text:  "No internet connection found. Check your internet connection and try again.")
            obj.label.isHidden = false
            obj.refresh.endRefreshing()
            // obj.stopLoading()
        }
        
        
    }
    
    
}



import Foundation
import Reachability

class SelfieListDataModel: NSObject {
    
    var Employee_ID = String()
    var Employees_Name = String()
    var Hazard_ID = String()
    var ImageURL = String()
    var ImageCaption = String()
    var Date_Time = String()
    var ID = String()
    
    
    
    
    func setDataInModel(str:[String:AnyObject])
    {
        if str["ID"] is NSNull || str["ID"] == nil {
            self.ID = ""
        }else{
            let emp1 = str["ID"]
            
            self.ID = (emp1?.description)!
        }
        if str["Employee_ID"] is NSNull || str["Employee_ID"] == nil{
            self.Employee_ID = ""
        }else{
            let emp1 = str["Employee_ID"]
            
            self.Employee_ID = (emp1?.description)!
        }
        if str["Employees_Name"] is NSNull || str["Employees_Name"] == nil{
            self.Employees_Name = ""
        }else{
            let emp1 = str["Employees_Name"]
            
            self.Employees_Name = (emp1?.description)!
        }
        if str["Hazard_ID"] is NSNull || str["Hazard_ID"] == nil{
            self.Hazard_ID = ""
        }else{
            let emp1 = str["Hazard_ID"]
            
            self.Hazard_ID = (emp1?.description)!
        }
        if str["ImageURL"] is NSNull || str["ImageURL"] == nil{
            self.ImageURL = ""
        }else{
            let emp1 = str["ImageURL"]
            
            self.ImageURL = (emp1?.description)!
        }
        if str["ImageCaption"] is NSNull || str["ImageCaption"] == nil{
            self.ImageCaption = ""
        }else{
            let emp1 = str["ImageCaption"]
            
            self.ImageCaption = (emp1?.description)!
        }
        if str["Date_Time"] is NSNull || str["Date_Time"] == nil{
            self.Date_Time = ""
        }else{
            let emp1 = str["Date_Time"]
            
            self.Date_Time = (emp1?.description)!
        }
        
    }
}

class SelfieListDataAPI
{
    
    var reachablty = Reachability()!
    
    
    func serviceCalling(obj:SelfieListViewController, parameter : [String:String], success:@escaping (AnyObject)-> Void)
    {
        
        obj.startLoading()
        
        if(reachablty.connection != .none)
        {
            WebServices.sharedInstances.sendPostRequest(url: URLConstants.Selfi_Time_Line, parameters: parameter, successHandler: { (dict) in
                
                if let response = dict["response"]{
                    
                    let statusString : String = response["status"] as! String
                    let objectmsg = MessageCallServerModel()
            let msg = objectmsg.sealizeMessage(cell: response as! [String : AnyObject])
                    print(response)
                    if(statusString == "success")
                    {
                        
                        obj.tableView.isHidden = false
                        obj.noDataLabel(text: "" )
                        obj.label.isHidden = true
                        if let data = dict["data"] as? [AnyObject]
                        {
                            
                            var dataArray : [SelfieListDataModel] = []
                            
                            for i in 0..<data.count
                            {
                                
                                if let cell = data[i] as? [String:AnyObject]
                                {
                                    let object = SelfieListDataModel()
                                    object.setDataInModel(str: cell)
                                    dataArray.append(object)
                                }
                                
                            }
                            
                            success(dataArray as AnyObject)
                            
                            
                        }
                        
                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                    }
                    else
                    {
                        
                        print("DATA:fail")
                        obj.tableView.isHidden = true
                        obj.noDataLabel(text: msg)
                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                        obj.label.isHidden = false
                        
                    }
                    
                    
                }
                
                
                
            }, failureHandler: { (error) in
                let errorStr : String = error.description
                
                print("DATA:error",errorStr)
                
                // obj.errorChecking(error: error)
                obj.stopLoading()
            })
            
            
        }
        else{
            obj.noDataLabel(text:  "No internet connection found. Check your internet connection and try again.")
            obj.tableView.isHidden = true
            obj.refresh.endRefreshing()
            obj.stopLoading()
            obj.label.isHidden = false
        }
        
        
    }
    
    
}

class SelfieListDataLoadMoreAPI
{
    
    var reachablty = Reachability()!
    
    
    func serviceCalling(obj:SelfieListViewController,parameter:[String:String], success:@escaping (AnyObject)-> Void)
    {
        
        
        
        if(reachablty.connection != .none)
        {
            
            WebServices.sharedInstances.sendPostRequest(url: URLConstants.Selfi_Time_Line, parameters: parameter, successHandler: { (dict) in
                
                if let response = dict["response"]{
                    
                    let statusString : String = response["status"] as! String
                    
                    if(statusString == "success")
                    {
                        
                        //                        obj.tableView.isHidden = false
                        //                        obj.noDataLabel(text: "" )
                        
                        if let data = dict["data"] as? [AnyObject]
                        {
                            
                            var dataArray : [SelfieListDataModel] = []
                            
                            for i in 0..<data.count
                            {
                                
                                if let cell = data[i] as? [String:AnyObject]
                                {
                                    let object = SelfieListDataModel()
                                    object.setDataInModel(str: cell)
                                    dataArray.append(object)
                                }
                                
                            }
                            
                            success(dataArray as AnyObject)
                            
                            
                        }
                        
                        obj.refresh.endRefreshing()
                        
                    }
                    else
                    {
                        
                        
                        print("DATA:fail")
                        //                        obj.tableView.isHidden = true
                        //                        obj.noDataLabel(text: "No data found" )
                        obj.refresh.endRefreshing()
                        
                        
                    }
                    
                    
                }
                
                
                
            }, failureHandler: { (error) in
                let errorStr : String = error.description
                
                print("DATA:error",errorStr)
                
                //  obj.errorChecking(error: error)
                
            })
            
            
        }
        else{
            //            obj.noDataLabel(text:  "No internet connection found. Check your internet connection and try again.")
            //            obj.tableView.isHidden = true
            obj.refresh.endRefreshing()
            
        }
        
        
    }
    
    
}

