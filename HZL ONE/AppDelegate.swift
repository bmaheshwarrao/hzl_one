//
//  AppDelegate.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 17/11/18.
//  Copyright © 2018 SARVANG INFOTCH. All rights reserved.
//

import UIKit
import CoreData
import IQKeyboardManagerSwift
import AudioToolbox.AudioServices
import AVFoundation

import BRYXBanner
import Fabric
import Crashlytics
//import Google
import Firebase

import UserNotifications
import FirebaseInstanceID
import FirebaseMessaging
import Foundation
import ESTabBarController_swift
var ProjectName = "HZL ONE"
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate , NSFetchedResultsControllerDelegate,UNUserNotificationCenterDelegate,MessagingDelegate  {

    var window: UIWindow?
    
    
    
    
    var connectedToGCM = false
    var subscribedToTopic = false
    var gcmSenderID: String?
    var registrationToken: InstanceIDResult?
    var registrationOptions = [String: AnyObject]()
    
    let registrationKey = "onRegistrationCompleted"
    let messageKey = "onMessageReceived"
    let subscriptionTopic = "/topics/global"
    
   
    
    var player: AVAudioPlayer?
  
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
       
       
        InstanceID.instanceID().instanceID(handler: registrationHandler)
        
       
        Fabric.with([Crashlytics.self])
        
        UINavigationBar.appearance().barTintColor = .black
        UINavigationBar.appearance().tintColor = .white
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        UINavigationBar.appearance().isTranslucent = false

       
        
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
            // For iOS 10 data message (sent via FCM
            Messaging.messaging().delegate = self
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        
        application.registerForRemoteNotifications()
        
        FirebaseApp.configure()
        if #available(iOS 8.0, *) {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
            application.registerForRemoteNotifications()
            Messaging.messaging().delegate = self
            
        } else {
            // Fallback
            let types: UIRemoteNotificationType = [.alert, .badge, .sound]
            application.registerForRemoteNotifications(matching: types)
        }
        
        UIApplication.shared.applicationIconBadgeNumber = 0
        
        // [END register_for_remote_notifications]
        // [START start_gcm_service]
        //        let gcmConfig = GCMConfig.default()
        //        gcmConfig?.receiverDelegate = self
        //        GCMService.sharedInstance().start(with: gcmConfig)
        
        UserDefaults.standard.set("Enable", forKey: "notificationEnable")
        UserDefaults.standard.set("Device Default", forKey: "soundEnable")
        
        UserDefaults.standard.set("notificEnable", forKey: "notificationEnableIm")
        UserDefaults.standard.set("soundEnable", forKey: "soundEnableIm")
        
        
        UserDefaults.standard.set(true, forKey: "soundSwitch")
        UserDefaults.standard.set(true, forKey: "vibrateSwitch")
        
        let systemSoundID: SystemSoundID = 1315
        UserDefaults.standard.set(systemSoundID, forKey: "soundFileEnable")
        
        
        IQKeyboardManager.shared.enable = true;
        
        
        
        
        //  UserDefaults.standard.set(false, forKey: "isWorkmanLogin")
        //  UserDefaults.standard.set(false, forKey: "isLogin")
        
        if UserDefaults.standard.bool(forKey: "isWorkmanLogin") == true {
            if UserDefaults.standard.bool(forKey: "isLogin") == true {
                
                
                self.window?.rootViewController = ExampleProvider.customBouncesStyle()
                self.window?.makeKeyAndVisible()
            }
            else
            {
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let loginVC  =  storyboard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                
                self.window?.rootViewController = loginVC
                self.window?.makeKeyAndVisible()
            }
        } else {
            if UserDefaults.standard.bool(forKey: "isLogin") == true {
                
                
              //  let storyboard = UIStoryboard(name: "Main", bundle: nil)
                self.window?.rootViewController = ExampleProvider.customBouncesStyle()
                self.window?.makeKeyAndVisible()
            }
            else
            {
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let loginVC  =  storyboard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                
                self.window?.rootViewController = loginVC
                self.window?.makeKeyAndVisible()
                
            }
        }
        
        
        return true
    }
    var backgroundSessionCompletionHandler: (() -> Void)?
    func application(_ application: UIApplication, handleEventsForBackgroundURLSession identifier: String, completionHandler: @escaping () -> Void) {
        backgroundSessionCompletionHandler = completionHandler
    }
    
    func application( _ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken
        deviceToken: Data ) {
        
       
        InstanceID.instanceID().instanceID(handler: registrationHandler)
        
        let deviceTokenString = deviceToken.reduce("", {$0 + String(format: "%02X", $1)})
       print(deviceTokenString)
    }
    
    func application( _ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError
        error: Error ) {
        print("Registration for remote notification failed with error: \(error.localizedDescription)")
        // [END receive_apns_token_error]
        let userInfo = ["error": error.localizedDescription]
        NotificationCenter.default.post(
            name: Notification.Name(rawValue: registrationKey), object: nil, userInfo: userInfo)
    }
    //Speak notification.....received
    func application( _ application: UIApplication,
                      didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                      fetchCompletionHandler handler: @escaping (UIBackgroundFetchResult) -> Void) {
        Messaging.messaging().appDidReceiveMessage(userInfo)
        print("Notification received: \(userInfo)")
//        guard let message = userInfo["gcm.notification.message"] else {
//            // print("Message ID: \(messageID)")
//            return
//        }
//        guard let action = userInfo["gcm.notification.action"] else {
//            // print("Message ID: \(messageID)")
//            return
//        }
//        guard let senderName = userInfo["gcm.notification.senderName"] else {
//            return
//            // print("Message ID: \(messageID)")
//        }
//        guard let actionData = userInfo["gcm.notification.actionData"] else {
//            return
//            // print("Message ID: \(messageID)")
//        }
//        guard let Receiver_Name = userInfo["gcm.notification.Receiver_Name"] else {
//            return
//            // print("Message ID: \(messageID)")
//        }
//        guard let Action_Type = userInfo["gcm.notification.Action_Type"] else {
//            return
//            // print("Message ID: \(messageID)")
//        }
//
//        // Print full message.
//        print(userInfo)
//
//        handler(UIBackgroundFetchResult.newData)
//
//
//        //        GCMService.sharedInstance().appDidReceiveMessage(userInfo)
//        //        NotificationCenter.default.post(name: Notification.Name(rawValue: messageKey), object: nil,
//        //                                        userInfo: userInfo)
//        //        handler(UIBackgroundFetchResult.noData)
//        //
//        switch application.applicationState {
//
//        case .active:
//
//            notificationRedirect(action: action as! String, actionType: Action_Type as! String, Action_ID: actionData as! String, Receiver_Name: Receiver_Name as! String, message: message as! String, senderName: senderName as! String)
//            break
//        case .inactive:
//
//            notificationRedirect(action: action as! String, actionType: Action_Type as! String, Action_ID: actionData as! String, Receiver_Name: Receiver_Name as! String, message: message as! String, senderName: senderName as! String)
//            break
//        case .background:
//            notificationRedirect(action: action as! String, actionType: Action_Type as! String, Action_ID: actionData as! String, Receiver_Name: Receiver_Name as! String, message: message as! String, senderName: senderName as! String)
//
//            break
//        }
        
        
    }
    func notificationRedirect (action : String , actionType : String , Action_ID : String,Receiver_Name : String,message : String,senderName : String){
        
        if UserDefaults.standard.value(forKey:"notificationEnable") as? String == "Enable"
        {
            
            if UserDefaults.standard.bool(forKey: "soundSwitch") == true{
                if UserDefaults.standard.value(forKey: "soundEnable") as! String == "Device Default"{
                    let systemSoundID: SystemSoundID = 1315
                    AudioServicesPlaySystemSound (systemSoundID)
                }else{
                    playSound(soundName:UserDefaults.standard.value(forKey: "soundFileEnable") as! String)
                }
            }
            if UserDefaults.standard.bool(forKey: "vibrateSwitch") == true{
                AudioServicesPlaySystemSound(kSystemSoundID_Vibrate)
            }
            
            
            AudioServicesPlaySystemSound(kSystemSoundID_Vibrate)
            
        
            _ = "HZL ONE"
            let body = message
            let title = Receiver_Name
            
            
            
//            let banner = Banner(title: title, subtitle: body, image: UIImage(named: "appiconimage"), backgroundColor: UIColor(red:48.00/255.0, green:174.0/255.0, blue:51.5/255.0, alpha:1.000), didTapBlock: {
//                var updateType = UserDefaults.standard.string(forKey: "updateType")
//
//                if(updateType != "Mandatory") {
//                    if UserDefaults.standard.bool(forKey: "isWorkmanLogin") == true {
//
//                        if  UserDefaults.standard.bool(forKey: "isLogin") == true {
//
//                            let storyboard = UIStoryboard(name: "Main", bundle: nil)
//                            let dashBoardVC = storyboard.instantiateViewController(withIdentifier: "DashBoardViewController") as! DashBoardViewController
//                            dashBoardVC.actionType = actionType
//                            dashBoardVC.notify = true
//                            dashBoardVC.action = action
//                            dashBoardVC.Action_ID = Action_ID
//                            dashBoardVC.message = message
//                            let navigationVC = UINavigationController(rootViewController: dashBoardVC)
//                            navigationVC.navigationBar.barTintColor =  UIColor(red: 41/255, green: 88/255, blue: 144/255, alpha: 1.0)
//                            navigationVC.navigationBar.isTranslucent = false;
//                            self.window?.rootViewController = navigationVC
//                            self.window?.makeKeyAndVisible()
//
//
//
//
//
//
//
//
//
//                        }
//                    }else{
//
//                        if  UserDefaults.standard.bool(forKey: "isLogin") == true {
//                            let storyboard = UIStoryboard(name: "Main", bundle: nil)
//                            let dashBoardVC  =  storyboard.instantiateViewController(withIdentifier: "Home") as! HomeViewController
//                            dashBoardVC.actionType = actionType
//                            dashBoardVC.notify = true
//                            dashBoardVC.action = action
//                            dashBoardVC.Action_ID = Action_ID
//                            dashBoardVC.message = message
//                            let navigationVC = UINavigationController(rootViewController: dashBoardVC)
//                            navigationVC.navigationBar.barTintColor =  UIColor(red: 41/255, green: 88/255, blue: 144/255, alpha: 1.0)
//                            navigationVC.navigationBar.isTranslucent = false;
//                            self.window?.rootViewController = navigationVC
//                            self.window?.makeKeyAndVisible()
//                        }
//                    }
//                }
//            })
//            banner.show(duration: 2.0)
            
            
        }
        
    }
    func playSound (soundName:String) {
        
        guard let url = Bundle.main.url(forResource: soundName, withExtension: "mp3") else { return }
        
        do {
            try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback)
            try AVAudioSession.sharedInstance().setActive(true)
            
            player = try AVAudioPlayer(contentsOf: url)
            guard let player = player else { return }
            
            player.play()
        } catch let error {
            print(error.localizedDescription)
        }
    }
    
    
    // [END ack_message_reception]
    func registrationHandler(_ registrationToken: InstanceIDResult?, error: Error?) {
        
        
        if let registrationToken = registrationToken {
            self.registrationToken = registrationToken
            
         //   print("Registration Token: \(registrationToken)")
          //  self.subscribeToTopic()
            let userInfo = ["registrationToken": registrationToken]
            NotificationCenter.default.post(
                name: Notification.Name(rawValue: self.registrationKey), object: nil, userInfo: userInfo)
            let userDefaults = UserDefaults.standard
            print(registrationToken.token)
          // print( Messaging.messaging().fcmToken)
            //  UserDefaults.standard.set(false, forKey: "GCMRegister")
            userDefaults.set(registrationToken.token, forKey: "Token")
            
        } else if let error = error {
            print("Registration to GCM failed with error: \(error.localizedDescription)")
            let userInfo = ["error": error.localizedDescription]
            NotificationCenter.default.post(
                name: Notification.Name(rawValue: self.registrationKey), object: nil, userInfo: userInfo)
        }
    }
    
    
    // [START on_token_refresh]
    func onTokenRefresh() {
        // A rotation of the registration tokens is happening, so the app needs to request a new token.
        print("The GCM registration token needs to be changed.")
        // GGLInstanceID.sharedInstance().token(withAuthorizedEntity: gcmSenderID,
        //scope: kGGLInstanceIDScopeGCM, options: registrationOptions, handler: registrationHandler)
    }
    // [END on_token_refresh]
    // [START upstream_callbacks]
    func willSendDataMessage(withID messageID: String!, error: Error!) {
        if error != nil {
            // Failed to send the message.
        } else {
            // Will send message, you can save the messageID to track the message
        }
    }
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        self.saveContext()
    }

    // MARK: - Core Data stack

    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
        */
       //  deleteZoneData()
        let container = NSPersistentContainer(name: "HZL_ONE")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                 
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()

    // MARK: - Core Data Saving support

    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }

}

