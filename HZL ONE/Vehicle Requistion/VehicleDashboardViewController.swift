//
//  VehicleDashboardViewController.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 25/01/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit

class VehicleDashboardViewController: UIViewController {
var VehicleStrNav = String()
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        if(MoveStruct.isMove == true){
            MoveStruct.isMove = false
            self.view.makeToast(MoveStruct.message)
        }
        self.title = "Vehicle Requistion"
        self.tabBarController?.tabBar.isHidden = true
        let nav = self.navigationController?.navigationBar
        nav?.tintColor = UIColor.white
       nav?.barTintColor = UIColor(hexString: "2c3e50", alpha: 1.0)
        nav?.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        
        
    }
    @IBAction func btnVehicleOutStationClicked(_ sender: UIButton) {
        FilterDataFromServer.Authority_Name  = String()
        FilterDataFromServer.Authority_Id = String()
        let storyBoard = UIStoryboard(name: "VehicleRequistion", bundle: nil)
        let ZIVC = storyBoard.instantiateViewController(withIdentifier: "VehicleOutStationViewController") as! VehicleOutStationViewController
        
        self.navigationController?.pushViewController(ZIVC, animated: true)
    }
    @IBAction func btnVehicleLocalClicked(_ sender: UIButton) {
        FilterDataFromServer.Authority_Name  = String()
        FilterDataFromServer.Authority_Id = String()
        let storyBoard = UIStoryboard(name: "VehicleRequistion", bundle: nil)
        let ZIVC = storyBoard.instantiateViewController(withIdentifier: "VehicleLocalViewController") as! VehicleLocalViewController
       
        self.navigationController?.pushViewController(ZIVC, animated: true)
    }
    
    @IBAction func btnVehicleTourClicked(_ sender: UIButton) {
        FilterDataFromServer.Authority_Name  = String()
        FilterDataFromServer.Authority_Id = String()
        dataPassenger = []
        dataJourney = []
        let storyBoard = UIStoryboard(name: "VehicleRequistion", bundle: nil)
        let ZIVC = storyBoard.instantiateViewController(withIdentifier: "VehicleTourViewController") as! VehicleTourViewController
        
        self.navigationController?.pushViewController(ZIVC, animated: true)
    }
    @IBAction func btnVehicleMyrequestClicked(_ sender: UIButton) {
      
        let storyBoard = UIStoryboard(name: "VehicleRequistion", bundle: nil)
        let ZIVC = storyBoard.instantiateViewController(withIdentifier: "MyRequestVehicleViewController") as! MyRequestVehicleViewController
        
        self.navigationController?.pushViewController(ZIVC, animated: true)
    }
    @IBAction func btnVehiclePendingBookingClicked(_ sender: UIButton) {
        
        let storyBoard = UIStoryboard(name: "VehicleRequistion", bundle: nil)
        let ZIVC = storyBoard.instantiateViewController(withIdentifier: "VehicleDisplayPendingApprovalViewController") as! VehicleDisplayPendingApprovalViewController
        ZIVC.statusStr = "Pending Booking"
        self.navigationController?.pushViewController(ZIVC, animated: true)
    }
    @IBAction func btnVehiclePendingApprovalClicked(_ sender: UIButton) {
        
        let storyBoard = UIStoryboard(name: "VehicleRequistion", bundle: nil)
        let ZIVC = storyBoard.instantiateViewController(withIdentifier: "VehicleDisplayPendingApprovalViewController") as! VehicleDisplayPendingApprovalViewController
        ZIVC.statusStr = "Pending Approval"
        
        self.navigationController?.pushViewController(ZIVC, animated: true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
