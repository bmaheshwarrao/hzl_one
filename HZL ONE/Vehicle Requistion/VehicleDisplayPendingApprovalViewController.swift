//
//  VehicleDisplayPendingApprovalViewController.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 04/03/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit

class VehicleDisplayPendingApprovalViewController:CommonVSClass {
    var statusStr = String()
    @IBOutlet weak var tableDisplayHazAction: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        //barSetup()
        tableDisplayHazAction.rowHeight = 70
        tableDisplayHazAction.delegate = self;
        tableDisplayHazAction.dataSource = self;
        refresh.addTarget(self, action: #selector(Reportdata), for: .valueChanged)
        
        self.tableDisplayHazAction.addSubview(refresh)
        
        tableDisplayHazAction.tableFooterView = UIView()
        
        // Do any additional setup after loading the view.
    }
    
    func setUp() {
        
        self.title = statusStr
        
        self.tabBarController?.tabBar.isHidden = true
        let nav = self.navigationController?.navigationBar
        nav?.tintColor = UIColor.white
        nav?.barTintColor = UIColor(hexString: "2c3e50", alpha: 1.0)
        nav?.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        Reportdata()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        setUp()
    }
    override func viewWillDisappear(_ animated: Bool) {
        
    }
    var vehicleDisplay : [vehicleDataDisplay] = []
    
    
    
    @objc func tapBack(_ sender : UIButton) {
        dismiss(animated: true, completion: nil)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    let reachability = Reachability()!
    var DataAPI = PendingApprovalDataAPI()
    var ReportedDB : [PendingApprovalDataModel] = []
    @objc func Reportdata() {
        
        
        
        
        
        let empId = UserDefaults.standard.string(forKey: "EmployeeID")!
        let Profile_AuthKey = UserDefaults.standard.string(forKey: "Profile_AuthKey")!
        let paramm  : [String:String] = ["UserID": empId , "AuthKey" :Profile_AuthKey ]
        
        
        
        
        self.DataAPI.serviceCalling(obj: self, param: paramm ) { (dict) in
            
            self.ReportedDB = [PendingApprovalDataModel]()
            self.ReportedDB = dict as! [PendingApprovalDataModel]
            self.tableDisplayHazAction.reloadData()
        }
        
    }
   
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
}
extension VehicleDisplayPendingApprovalViewController : UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        
        
        
    
        
        
        
    }
}
extension VehicleDisplayPendingApprovalViewController : UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.ReportedDB.count
        
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! DisplayHazardTableViewCell
        cell.selectionStyle = UITableViewCellSelectionStyle.none
       
                cell.lblShowHazAction.textColor = UIColor.darkGray
                cell.lblShowHazAction.text = self.ReportedDB[indexPath.row].Action_Text
                cell.lblShowHazAction.font =  UIFont.systemFont(ofSize: 16.0, weight: .medium)
                cell.lblAddData.text = self.ReportedDB[indexPath.row].Count_Record
                
            return cell
        }
    
    
    
}

class vehicleDataDisplay :  NSObject {
    
    
    var Appr_Locals = String()
    var Appr_Outs = String()
    var Appr_Tours = String()
    var Admin_Locals = String()
    var Admin_Outs = String()
    var Admin_Tours = String()
    
}
