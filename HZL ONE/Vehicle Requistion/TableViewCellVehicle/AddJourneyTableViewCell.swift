//
//  AddJourneyTableViewCell.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 26/02/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit

class AddJourneyTableViewCell: UITableViewCell {

    
    @IBOutlet weak var viewTo: UIView!
    @IBOutlet weak var txtTo: UITextField!
    @IBOutlet weak var viewFrom: UIView!
    @IBOutlet weak var txtFrom: UITextField!
    @IBOutlet weak var viewTransport: UIView!
    @IBOutlet weak var lblTransport: UILabel!
    @IBOutlet weak var btnTransport: UIButton!
    
    @IBOutlet weak var viewJourneyDate: UIView!
    @IBOutlet weak var lblJourneyDate: UILabel!
    @IBOutlet weak var btnJourneyDate: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        viewJourneyDate.layer.borderWidth = 1.0
        viewJourneyDate.layer.borderColor = UIColor.black.cgColor
        viewJourneyDate.layer.cornerRadius = 10.0
        
        viewTransport.layer.borderWidth = 1.0
        viewTransport.layer.borderColor = UIColor.black.cgColor
        viewTransport.layer.cornerRadius = 10.0
        viewFrom.layer.borderWidth = 1.0
        viewFrom.layer.borderColor = UIColor.black.cgColor
        viewFrom.layer.cornerRadius = 10.0
        viewTo.layer.borderWidth = 1.0
        viewTo.layer.borderColor = UIColor.black.cgColor
        viewTo.layer.cornerRadius = 10.0
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
