//
//  PassengerTableViewCell.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 26/02/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit

class PassengerTableViewCell: UITableViewCell {
@IBOutlet weak var lblNo: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblPassengerType: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var btnClose: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
