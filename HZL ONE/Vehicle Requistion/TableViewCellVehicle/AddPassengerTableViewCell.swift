//
//  AddPassengerTableViewCell.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 26/02/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit

class AddPassengerTableViewCell: UITableViewCell {

    @IBOutlet weak var btnPassengerType: UIButton!
    @IBOutlet weak var lblPassengerType: UILabel!
    @IBOutlet weak var viewPassengerType: UIView!
    
    
    @IBOutlet weak var viewEmpId: UIView!
    @IBOutlet weak var txtEmpId: UITextField!
    
    
    @IBOutlet weak var viewName: UIView!
    @IBOutlet weak var txtName: UITextField!
    
    @IBOutlet weak var viewContact: UIView!
    @IBOutlet weak var txtContact: UITextField!
    @IBOutlet weak var viewEmail: UIView!
    @IBOutlet weak var txtEmail: UITextField!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        viewPassengerType.layer.borderWidth = 1.0
        viewPassengerType.layer.borderColor = UIColor.black.cgColor
        viewPassengerType.layer.cornerRadius = 10.0
        
        viewEmpId.layer.borderWidth = 1.0
        viewEmpId.layer.borderColor = UIColor.black.cgColor
        viewEmpId.layer.cornerRadius = 10.0
        viewName.layer.borderWidth = 1.0
        viewName.layer.borderColor = UIColor.black.cgColor
        viewName.layer.cornerRadius = 10.0
        viewContact.layer.borderWidth = 1.0
        viewContact.layer.borderColor = UIColor.black.cgColor
        viewContact.layer.cornerRadius = 10.0
        viewEmail.layer.borderWidth = 1.0
        viewEmail.layer.borderColor = UIColor.black.cgColor
        viewEmail.layer.cornerRadius = 10.0
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
