//
//  VehicleLocalTableViewCell.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 25/02/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit
import DLRadioButton
class VehicleLocalTableViewCell: UITableViewCell {

    @IBOutlet weak var btnSelf: DLRadioButton!
    @IBOutlet weak var btnGuest: DLRadioButton!
    
    @IBOutlet weak var viewName: UIView!
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var txtContact: UITextField!
    @IBOutlet weak var viewContact: UIView!
    
    
    @IBOutlet weak var btnPurpose: UIButton!
    @IBOutlet weak var lblPurpose: UILabel!
    @IBOutlet weak var viewPurpose: UIView!
    
    @IBOutlet weak var btnPerson: UIButton!
    @IBOutlet weak var lblPerson: UILabel!
    @IBOutlet weak var viewPerson: UIView!
    
    @IBOutlet weak var btnJourneyDate: UIButton!
    @IBOutlet weak var lblJourneyDate: UILabel!
    @IBOutlet weak var viewJourneyDate: UIView!
    
    @IBOutlet weak var btnFromTime: UIButton!
    @IBOutlet weak var lblFromTime: UILabel!
    @IBOutlet weak var viewFromTime: UIView!
    
    @IBOutlet weak var btnToTime: UIButton!
    @IBOutlet weak var lblToTime: UILabel!
    @IBOutlet weak var viewToTime: UIView!
    
    
    @IBOutlet weak var textViewPickAddress: UITextView!
    @IBOutlet weak var viewPickAddress: UIView!
    
    @IBOutlet weak var textViewDropAddress: UITextView!
    @IBOutlet weak var viewDropAddress: UIView!
    
    @IBOutlet weak var btnApprovalAuth: UIButton!
    @IBOutlet weak var lblApprovalAuth: UILabel!
    @IBOutlet weak var viewApprovalAuth: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        viewApprovalAuth.layer.borderWidth = 1.0
        viewApprovalAuth.layer.borderColor = UIColor.black.cgColor
        viewApprovalAuth.layer.cornerRadius = 10.0
        
        viewPickAddress.layer.borderWidth = 1.0
        viewPickAddress.layer.borderColor = UIColor.black.cgColor
        viewPickAddress.layer.cornerRadius = 10.0
        
        viewDropAddress.layer.borderWidth = 1.0
        viewDropAddress.layer.borderColor = UIColor.black.cgColor
        viewDropAddress.layer.cornerRadius = 10.0
        
        viewFromTime.layer.borderWidth = 1.0
        viewFromTime.layer.borderColor = UIColor.black.cgColor
        viewFromTime.layer.cornerRadius = 10.0
        
        viewToTime.layer.borderWidth = 1.0
        viewToTime.layer.borderColor = UIColor.black.cgColor
        viewToTime.layer.cornerRadius = 10.0
        
        
        viewName.layer.borderWidth = 1.0
        viewName.layer.borderColor = UIColor.black.cgColor
        viewName.layer.cornerRadius = 10.0
        
        viewContact.layer.borderWidth = 1.0
        viewContact.layer.borderColor = UIColor.black.cgColor
        viewContact.layer.cornerRadius = 10.0
        
        viewPurpose.layer.borderWidth = 1.0
        viewPurpose.layer.borderColor = UIColor.black.cgColor
        viewPurpose.layer.cornerRadius = 10.0
        
        viewPerson.layer.borderWidth = 1.0
        viewPerson.layer.borderColor = UIColor.black.cgColor
        viewPerson.layer.cornerRadius = 10.0
        
        viewJourneyDate.layer.borderWidth = 1.0
        viewJourneyDate.layer.borderColor = UIColor.black.cgColor
        viewJourneyDate.layer.cornerRadius = 10.0
        txtContact.keyboardType = UIKeyboardType.numberPad
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
