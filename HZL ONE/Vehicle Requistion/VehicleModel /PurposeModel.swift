//
//  PurposeModel.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 25/02/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import Foundation
class PurposeDataModel: NSObject {
    
    
    
    var ID: String?
    var Name : String?

    
    func setDataInModel(cell:[String:AnyObject])
    {
        
        if cell["ID"] is NSNull || cell["ID"] == nil {
            self.ID = ""
        }else{
            let app = cell["ID"]
            self.ID = (app?.description)!
        }
        if cell["Name"] is NSNull || cell["Name"] == nil {
            self.Name = ""
        }else{
            let app = cell["Name"]
            self.Name = (app?.description)!
        }
       
        
        
    }
}


class PurposeDataAPI
{
    
    var reachablty = Reachability()!
    
    
    func serviceCalling(obj:VehicleLocalViewController,param : [String:String], success:@escaping (AnyObject)-> Void)
    {
        
        obj.startLoading()
        
        if(reachablty.connection != .none)
        {
            
            WebServices.sharedInstances.sendPostRequest(url: URLConstants.GetPurposeOfJourney, parameters: param, successHandler: { (dict) in
                
                if let response = dict["response"]{
                    
                    let statusString : String = response["status"] as! String
                    
                    if(statusString == "ok")
                    {
                        
                        
                        
                        
                        if let data = dict["data"] as? [AnyObject]
                        {
                            
                            var dataArray: [PurposeDataModel] = []
                            
                            for i in 0..<data.count
                            {
                                
                                if let cell = data[i] as? [String:AnyObject]
                                {
                                    let object = PurposeDataModel()
                                    object.setDataInModel(cell: cell)
                                    dataArray.append(object)
                                }
                                
                            }
                            
                            success(dataArray as AnyObject)
                            
                            
                        }
                        
                        
                        
                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                    }
                    else
                    {
                      
                        obj.refresh.endRefreshing()
                        
                        obj.stopLoading()
                        
                    }
                    
                    
                }
                
                
                
            }, failureHandler: { (error) in
                let errorStr : String = error.description
                
                print("DATA:error",errorStr)
                
                // obj.errorChecking(error: error)
                obj.stopLoading()
            })
            
            
        }
        else{
            // obj.noDataLabel(text:  "No internet connection found. Check your internet connection and try again.")
         
            obj.refresh.endRefreshing()
            obj.stopLoading()
        }
        
        
    }
    
    
}



class ApprovalAuthDataListModel: NSObject {
    
    var name = String()
    var id = String()
    func setDataInModel(cell:[String:AnyObject])
    {
        
        if cell["ID"] is NSNull || cell["ID"] == nil {
            self.id = ""
        }else{
            let app = cell["ID"]
            self.id = (app?.description)!
        }
        if cell["First_Name"] is NSNull || cell["First_Name"] == nil {
            self.name = ""
        }else{
            let app = cell["First_Name"]
            
            
            self.name = (app?.description)!
        }
        
        
        
    }
    
}





class ApprovalAuthorityListApi
{
    var reachablty = Reachability()!
    func serviceCalling(obj:ApprovalAuthorityViewController,param : [String:String], success:@escaping (AnyObject)-> Void)
    {
        
        obj.startLoading(view: obj.tableViewASUZ)
    
        
        if(reachablty.connection != .none)
        {
            
           
            
       
            WebServices.sharedInstances.sendPostRequest(url: URLConstants.GetApproverListAuth, parameters: param, successHandler: { (dict) in
                
                if let response = dict["response"]{
                    
                    var statusString : String = response["status"] as! String
                    
                    if(statusString == "ok")
                    {
                        
                        if let data = dict["data"] as? [AnyObject]
                        {
                            
                            var dataArray: [ApprovalAuthDataListModel] = []
                            
                            for i in 0..<data.count
                            {
                                
                                if let cell = data[i] as? [String:AnyObject]
                                {
                                    let object = ApprovalAuthDataListModel()
                                    object.setDataInModel(cell: cell)
                                    dataArray.append(object)
                                }
                                
                            }
                            
                            success(dataArray as AnyObject)
                            
                            
                        }
                        
                        
                       
                        
                            
                            obj.tableViewASUZ.reloadData()
                        
                            
                            
                        
                        
                        obj.refresh.endRefreshing()
                        obj.stopLoading(view: obj.tableViewASUZ)
                    }
                    else
                    {
                       
                        obj.tableViewASUZ.reloadData()
                        obj.label.isHidden = false
                        obj.noDataLabel(text: "No Data Found" )
                        print("DATA:fail")
                        //                        obj.tableView.isHidden = true
                        //                        obj.noDataLabel(text: "Sorry! No area manager found with name "+name)
                        obj.refresh.endRefreshing()
                        obj.stopLoading(view: obj.tableViewASUZ)
                        
                    }
                    
                    
                }
                
                
                
            }, failureHandler: { (error) in
                let errorStr : String = error.description
               
                print("DATA:error",errorStr)
                
                //  obj.errorChecking(error: error)
                
            })
            
            
        }
        else{
            obj.noDataLabel(text:  "No internet connection found. Check your internet connection and try again.")
            obj.tableViewASUZ.isHidden = true
            obj.label.isHidden = false
            obj.refresh.endRefreshing()
            obj.stopLoading()
            
            obj.tableViewASUZ.reloadData()
        }
        
        
    }
}




class TransportDataModel: NSObject {
    
    
    
    var ID: String?
    var Name : String?
    
    
    func setDataInModel(cell:[String:AnyObject])
    {
        
        if cell["ID"] is NSNull || cell["ID"] == nil {
            self.ID = ""
        }else{
            let app = cell["ID"]
            self.ID = (app?.description)!
        }
        if cell["Name"] is NSNull || cell["Name"] == nil {
            self.Name = ""
        }else{
            let app = cell["Name"]
            self.Name = (app?.description)!
        }
        
        
        
    }
}


class TransportDataAPI
{
    
    var reachablty = Reachability()!
    
    
    func serviceCalling(obj:AddJourneyViewController,param : [String:String], success:@escaping (AnyObject)-> Void)
    {
        
        obj.startLoading()
        
        if(reachablty.connection != .none)
        {
            
            WebServices.sharedInstances.sendPostRequest(url: URLConstants.GetTravelType, parameters: param, successHandler: { (dict) in
                
                if let response = dict["response"]{
                    
                    let statusString : String = response["status"] as! String
                    
                    if(statusString == "ok")
                    {
                        
                        
                        
                        
                        if let data = dict["data"] as? [AnyObject]
                        {
                            
                            var dataArray: [TransportDataModel] = []
                            
                            for i in 0..<data.count
                            {
                                
                                if let cell = data[i] as? [String:AnyObject]
                                {
                                    let object = TransportDataModel()
                                    object.setDataInModel(cell: cell)
                                    dataArray.append(object)
                                }
                                
                            }
                            
                            success(dataArray as AnyObject)
                            
                            
                        }
                        
                        
                        
                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                    }
                    else
                    {
                        
                        obj.refresh.endRefreshing()
                        
                        obj.stopLoading()
                        
                    }
                    
                    
                }
                
                
                
            }, failureHandler: { (error) in
                let errorStr : String = error.description
                
                print("DATA:error",errorStr)
                
                // obj.errorChecking(error: error)
                obj.stopLoading()
            })
            
            
        }
        else{
            // obj.noDataLabel(text:  "No internet connection found. Check your internet connection and try again.")
            
            obj.refresh.endRefreshing()
            obj.stopLoading()
        }
        
        
    }
    
    
}

class PlacesDataModel: NSObject {
    
    
    
    var ID: String?
    var Display_Name : String?
    
    
    func setDataInModel(cell:[String:AnyObject])
    {
        
        if cell["ID"] is NSNull || cell["ID"] == nil {
            self.ID = ""
        }else{
            let app = cell["ID"]
            self.ID = (app?.description)!
        }
        if cell["Display_Name"] is NSNull || cell["Display_Name"] == nil {
            self.Display_Name = ""
        }else{
            let app = cell["Display_Name"]
            self.Display_Name = (app?.description)!
        }
        
        
        
    }
}

class PlacesDataAPI
{
    
    var reachablty = Reachability()!
    
    
    func serviceCalling(obj:AddJourneyViewController,param : [String:String], success:@escaping (AnyObject)-> Void)
    {
        
        obj.startLoading()
        
        if(reachablty.connection != .none)
        {
            
            WebServices.sharedInstances.sendPostRequest(url: URLConstants.GetPlaces, parameters: param, successHandler: { (dict) in
                
                if let response = dict["response"]{
                    
                    let statusString : String = response["status"] as! String
                    
                    if(statusString == "ok")
                    {
                        
                        
                        
                        
                        if let data = dict["data"] as? [AnyObject]
                        {
                            
                            var dataArray: [PlacesDataModel] = []
                            
                            for i in 0..<data.count
                            {
                                
                                if let cell = data[i] as? [String:AnyObject]
                                {
                                   
                                    let object = PlacesDataModel()
                                    object.setDataInModel(cell: cell)
                                    dataArray.append(object)
                                }
                                
                            }
                            
                            success(dataArray as AnyObject)
                            
                            
                        }
                        
                        
                        
                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                    }
                    else
                    {
                        
                        obj.refresh.endRefreshing()
                        
                        obj.stopLoading()
                        
                    }
                    
                    
                }
                
                
                
            }, failureHandler: { (error) in
                let errorStr : String = error.description
                
                print("DATA:error",errorStr)
                
                // obj.errorChecking(error: error)
                obj.stopLoading()
            })
            
            
        }
        else{
            // obj.noDataLabel(text:  "No internet connection found. Check your internet connection and try again.")
            
            obj.refresh.endRefreshing()
            obj.stopLoading()
        }
        
        
    }
    
    
}




