//
//  ViewAuditHeaderTableViewCell.swift
//  5s Audit
//
//  Created by SARVANG INFOTCH on 01/12/18.
//  Copyright © 2018 SARVANG INFOTCH. All rights reserved.
//



import UIKit

protocol ViewAuditHeaderTableViewHeaderDelegate {
    func toggleSection(_ header: ViewAuditHeaderTableViewHeader, section: Int)
}

class ViewAuditHeaderTableViewHeader: UITableViewHeaderFooterView {
    
    var delegate: ViewAuditHeaderTableViewHeaderDelegate?
    var section: Int = 0
   
    let titleLabel = UILabel()
    let titleView = UIViewX()
    let arrowLabel = UIImageView()
     let stackView   = UIStackView()
    override init(reuseIdentifier: String?) {
        super.init(reuseIdentifier: reuseIdentifier)
        
        // Content View
        contentView.backgroundColor = UIColor.clear
        
        let marginGuide = contentView.layoutMarginsGuide
      //  contentView.addSubview(titleView)
        titleView.cornerRadius = 10
       // contentView.addSubview(titleLabel)
        titleView.cornerRadius = 10.0
        titleView.backgroundColor = UIColor.white
        
        titleLabel.textColor = UIColor.black
        titleLabel.font = UIFont.systemFont(ofSize: 15.0, weight: .medium)
        titleLabel.textAlignment = NSTextAlignment.left
       titleLabel.translatesAutoresizingMaskIntoConstraints = false
        titleView.heightAnchor.constraint(equalToConstant: 20.0).isActive = true
        titleView.widthAnchor.constraint(equalToConstant: 20.0).isActive = true
        
        
        
        titleLabel.heightAnchor.constraint(equalToConstant: 20.0).isActive = true
        
      
        
        //Stack View
       
        stackView.axis  = UILayoutConstraintAxis.horizontal
        stackView.distribution  = UIStackViewDistribution.fill
        stackView.alignment = UIStackViewAlignment.center
        stackView.spacing   = 10.0
        
        stackView.addArrangedSubview(titleView)
        stackView.addArrangedSubview(titleLabel)
        stackView.translatesAutoresizingMaskIntoConstraints = false
        
        contentView.addSubview(stackView)
        
        //Constraints
        stackView.centerXAnchor.constraint(equalTo: marginGuide.centerXAnchor).isActive = true
        stackView.centerYAnchor.constraint(equalTo: marginGuide.centerYAnchor).isActive = true
         titleLabel.leadingAnchor.constraint(equalTo: stackView.leadingAnchor, constant: 0.0)
        titleLabel.trailingAnchor.constraint(equalTo: stackView.trailingAnchor, constant: 0.0)
   
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //
    // Trigger toggle section when tapping on the header
    //
   
    
}

