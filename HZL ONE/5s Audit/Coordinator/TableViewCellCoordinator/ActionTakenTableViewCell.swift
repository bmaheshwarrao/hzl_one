//
//  ActionTakenTableViewCell.swift
//  5s Audit
//
//  Created by SARVANG INFOTCH on 26/11/18.
//  Copyright © 2018 SARVANG INFOTCH. All rights reserved.
//

import UIKit

class ActionTakenTableViewCell: UITableViewCell {

    @IBOutlet weak var actionCommentLbl: UILabel!
    @IBOutlet weak var containerView: UIViewX!
    @IBOutlet weak var StatusLbl: UILabel!
   
    @IBOutlet weak var imageFile: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
