//
//  CoordinatorFirstScreenViewController.swift
//  5s Audit
//
//  Created by SARVANG INFOTCH on 19/11/18.
//  Copyright © 2018 SARVANG INFOTCH. All rights reserved.
//

import UIKit

class CoordinatorFirstScreenViewController: UIViewController {
    var arrayCoodinatorTitle : [String] = []
    var arrayCoodinatorTitleDesc : [String] = []
    var arrayCoodinatorColor : [String] = []
    @IBOutlet weak var tableView: UITableView!

    var screen = UserDefaults.standard.string(forKey: "screen")!
    override func viewDidLoad() {
        super.viewDidLoad()
        if(screen == "HOD"){
            arrayCoodinatorTitle = ["Pending Observation","Upcoming Audits","Completed Audits","Missed Audits"]
        }else{
            arrayCoodinatorTitle = ["Pending Observation","Upcoming Audits","Completed Audits","Missed Audits"]
        }
        arrayCoodinatorTitleDesc = ["Audits which has open observations","Next Audits","Audits which has all closed observations","Audits i missed"]
        arrayCoodinatorColor = ["F38B34","48c6e7","8bc24b","da3b38"]
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
        tableView.tableFooterView?.backgroundColor = UIColor.groupTableViewBackground

        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
     screen = UserDefaults.standard.string(forKey: "screen")!
         if(screen == "HOD"){
            self.title = "HOD"
         }else{
            self.title = "Coordinator"
        }
        
        
        self.tabBarController?.tabBar.isHidden = true
        let nav = self.navigationController?.navigationBar
        nav?.barStyle = UIBarStyle.black
        nav?.tintColor = UIColor.white
        nav?.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @objc func tapCell(_ sender: UITapGestureRecognizer) {
                let buttonPosition:CGPoint = sender.view!.convert(.zero, to:self.tableView)
                let indexPath = self.tableView.indexPathForRow(at: buttonPosition)
           let storyboard = UIStoryboard(name: "Audit", bundle: nil)
        if(arrayCoodinatorTitle[(indexPath?.row)!] == "Pending Observation"){
            let ZIVC = storyboard.instantiateViewController(withIdentifier: "PendingObservationCoordinatorViewController") as! PendingObservationCoordinatorViewController

            UserDefaults.standard.set("Pending", forKey: "screenStatus")
                    self.navigationController?.pushViewController(ZIVC, animated: true)
            
        } else if(arrayCoodinatorTitle[(indexPath?.row)!] == "Upcoming Audits"){
            let ZIVC = storyboard.instantiateViewController(withIdentifier: "UpcomingCompletedMissedViewController") as! UpcomingCompletedMissedViewController
       
            UserDefaults.standard.set("Upcoming", forKey: "screenStatus")
            self.navigationController?.pushViewController(ZIVC, animated: true)
            
        } else if(arrayCoodinatorTitle[(indexPath?.row)!] == "Completed Audits"){
             let ZIVC = storyboard.instantiateViewController(withIdentifier: "UpcomingCompletedMissedViewController") as! UpcomingCompletedMissedViewController
     
            UserDefaults.standard.set("Completed", forKey: "screenStatus")
            self.navigationController?.pushViewController(ZIVC, animated: true)
            
        } else if(arrayCoodinatorTitle[(indexPath?.row)!] == "Missed Audits"){
            let ZIVC = storyboard.instantiateViewController(withIdentifier: "UpcomingCompletedMissedViewController") as! UpcomingCompletedMissedViewController
             UserDefaults.standard.set("Missed", forKey: "screenStatus")
            self.navigationController?.pushViewController(ZIVC, animated: true)
            
        }else {
            
        }
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
extension CoordinatorFirstScreenViewController : UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
        // return 200
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayCoodinatorTitle.count
    }
    
    
}

extension CoordinatorFirstScreenViewController : UITableViewDataSource {
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! CoordinatefIrstTableViewCell
        
        
        
        cell.containerView.layer.cornerRadius = 8
        cell.lblTitle.text = arrayCoodinatorTitle[indexPath.row]
        
       cell.lblTitleDesc.text = arrayCoodinatorTitleDesc[indexPath.row]
        
        cell.containerView.backgroundColor = UIColor.colorwithHexString(arrayCoodinatorColor[indexPath.row], alpha: 1.0)
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tapCell(_:)))
        cell.isUserInteractionEnabled = true;
        cell.addGestureRecognizer(tapGesture)
        
        
        
        return cell;
    }
}
