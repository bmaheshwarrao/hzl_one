//
//  FinalSubmitViewController.swift
//  5s Audit
//
//  Created by SARVANG INFOTCH on 29/11/18.
//  Copyright © 2018 SARVANG INFOTCH. All rights reserved.
//

import UIKit
var textComment = String()
class FinalSubmitViewController: UIViewController {
    var score = String()
    static func instantiate() -> FinalSubmitViewController? {
        return UIStoryboard(name: "Audit", bundle: nil).instantiateViewController(withIdentifier: "\(FinalSubmitViewController.self)") as? FinalSubmitViewController
    }
      var PlaceHolderText = "Type Comment here."
    @IBOutlet weak var textViewComment: UITextView!
    override func viewDidLoad() {
        super.viewDidLoad()
        totalScoreLbl.text = "Total Score is : " + score
        textViewComment.text = ""
        textViewComment.layer.cornerRadius = 10.0
        textViewComment.layer.borderWidth = 1.0
        textViewComment.layer.borderColor = UIColor.black.cgColor
        textViewComment.delegate = self
            textViewComment.text = PlaceHolderText
            textViewComment.textColor = .lightGray
       
        
        
        
        // Do any additional setup after loading the view.
    }

    @IBOutlet weak var totalScoreLbl: UILabel!
    @IBAction func btnFinalSubmitClicked(_ sender: UIButton) {
        if(textViewComment.text == "" || textViewComment.text == PlaceHolderText){
            self.view.makeToast("Please Write Comment")
        }else{
            textComment = textViewComment.text
          NotificationCenter.default.post(name:NSNotification.Name.init("finalSubmit"), object: nil)
            dismiss(animated: true, completion: nil)
        
        }
    }
    @IBAction func btnReobserveClicked(_ sender: UIButton) {
         dismiss(animated: true, completion: nil)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
extension FinalSubmitViewController : UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView)
    {
        if (textView.text == PlaceHolderText)
        {
            textView.text = ""
            textView.textColor = .black
        }
        
        
    }
    
    func textViewDidEndEditing(_ textView: UITextView)
    {
        if (textView.text == "")
        {
            textView.text = PlaceHolderText
            textView.textColor = .lightGray
        }
        self.textViewComment.endEditing(true)
        
    }
}
struct FinalMove {
     static var isMove =  Bool()
}
