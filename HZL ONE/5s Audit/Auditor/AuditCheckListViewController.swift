//
//  AuditCheckListViewController.swift
//  5s Audit
//
//  Created by SARVANG INFOTCH on 17/11/18.
//  Copyright © 2018 SARVANG INFOTCH. All rights reserved.
//

import UIKit

class AuditCheckListViewController: CommonVSClass {
    
    var AuditListDB:[AuditCheckListModel] = []
    
    var AuditListAPI = AuditCheckListAPI()
    
    var auditId = String()
    var auditType = String()
    var data: String?
    var lastObject: String?
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.isHidden = true
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
        tableView.tableFooterView?.backgroundColor = UIColor.groupTableViewBackground
       // cellButton.btnNextHome.isHidden = true
        // Do any additional setup after loading the view.
        refresh.addTarget(self, action: #selector(AuditList), for: .valueChanged)
        let swipe = UISwipeGestureRecognizer(target: self, action: #selector(self.swipeRecognizer))
        swipe.direction = ([.down])
        self.view.addGestureRecognizer(swipe)
        
        
        self.tableView.addSubview(refresh)
        
        
        
    }
    var reachability = Reachability()!
    @objc func swipeRecognizer(_ sender: UISwipeGestureRecognizer) {
        if (sender.direction == .down) {
            
            if(self.reachability.connection != .none)
            {
                
                self.AuditList()
            }
            else
            {
                self.startLoading(view: self.view)
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                    self.noDataLabel(text: "No internet connection found. Check your internet connection and try again.")
                    self.stopLoading(view: self.view)
                }
            }
            
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
  
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        if(MoveStruct.isMove == true){
            self.navigationController?.popViewController(animated: false)
        }
        
        self.title = self.auditType
        self.tabBarController?.tabBar.isHidden = true
        let nav = self.navigationController?.navigationBar
        nav?.barStyle = UIBarStyle.black
        nav?.tintColor = UIColor.white
        nav?.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        valStatus = 0
        AuditList()
    }
    @objc func AuditList() {
        var para = [String:String]()
    
        let parameter = ["Audit_ID":self.auditId ,"Audit_Type":self.auditType  ]
   
        para = parameter.filter { $0.value != ""}
        print("para",para)
        self.AuditListAPI.serviceCalling(obj: self, parameter: para) { (dict) in
            self.cellButton.btnNextHome.isHidden = true
            self.cellButton.isHidden = true
            self.AuditListDB = [AuditCheckListModel]()
            self.AuditListDB = dict as! [AuditCheckListModel]
      
            self.tableView.reloadData()
            self.tableView.isHidden = false
        }
        
    }
    
     var FiveListAPI = FivesTypeListAPI()
    var FivesTypeListDB:[FivesTypeListModel] = []

    var totalQuestion = Int()
    var totalAttempted = Int()
    @IBAction func btnNextHomeClicked(_ sender: UIButton) {
        if(cellButton.btnNextHome.titleLabel?.text == "Home"){
            valStatus = 0
            navigationController?.popViewController(animated: true)
        }else{
        var para = [String:String]()
        let parameter = ["AuditID":auditId]
        
        para = parameter.filter { $0.value != ""}
        print("para",para)
        self.FiveListAPI.serviceCallingList(obj: self, parameter: para) { (dict) in
            var ival = 0
            self.totalQuestion = 0
            self.totalAttempted = 0
            self.FivesTypeListDB = [FivesTypeListModel]()
            self.FivesTypeListDB = dict as! [FivesTypeListModel]
            if(self.FivesTypeListDB.count > 0){
                for i in 0...self.FivesTypeListDB.count - 1{
                    
                    ival = i
                    
                    self.totalQuestion = Int(self.FivesTypeListDB[i].TotalQuestion!)!
                    self.totalAttempted = Int(self.FivesTypeListDB[i].Attempt!)!
                    self.auditType = self.FivesTypeListDB[i].CL_5S_Type!
                    if(self.totalQuestion != 0 && self.totalAttempted != 0){
                        if(self.totalQuestion == self.totalAttempted){
                            if(ival == self.FivesTypeListDB.count - 1){
                                 self.valStatus = 0
                                 self.cellButton.isHidden = false
                                self.cellButton.btnNextHome.setTitle("Home", for: .normal)
                            }
                        }else{
                            self.title = self.auditType
                            self.AuditList()
                            self.valStatus = 1
                            self.cellButton.btnNextHome.isHidden = true
                            self.cellButton.isHidden = true
                            break
                        }
                    }else{
                        self.title = self.auditType
                        self.AuditList()
                        self.valStatus = 1
                        self.cellButton.btnNextHome.isHidden = true
                        self.cellButton.isHidden = true
                        break
                    }
                }
            }
            
            
            
        }
        }
        
        
    }
    @IBAction func tapCellButton(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "Audit", bundle: nil)
        let ZIVC = storyboard.instantiateViewController(withIdentifier: "SubmitObservationAuditViewController") as! SubmitObservationAuditViewController
        

        
        print(AuditListDB[sender.tag].flag!)
        if(AuditListDB[sender.tag].flag! == "not submitted"){
            ZIVC.score = String()
            ZIVC.textMessage = String()
            ZIVC.status = String()
        }else{
            print((AuditListDB[sender.tag].Image))
            ZIVC.ImageCollectFromServer = (AuditListDB[sender.tag].Image)
            ZIVC.score = AuditListDB[sender.tag].Score!
            ZIVC.textMessage = AuditListDB[sender.tag].Observation!
            ZIVC.status = AuditListDB[sender.tag].Report_Status!
        }
        ZIVC.pointId = AuditListDB[sender.tag].CL_Point_ID!
        ZIVC.auditId = AuditListDB[sender.tag].Audit_ID!
        
        self.navigationController?.pushViewController(ZIVC, animated: true)
        
        
    }
    @objc func tapCell(_ sender: UIButton) {
//                let buttonPosition:CGPoint = sender.view!.convert(.zero, to:self.tableView)
//                let indexPath = self.tableView.indexPathForRow(at: buttonPosition)
//
//
//
//
//                let storyboard = UIStoryboard(name: "Audit", bundle: nil)
//                let ZIVC = storyboard.instantiateViewController(withIdentifier: "SubmitObservationViewController") as! SubmitObservationViewController
//
//        print(indexPath?.row)
//
//        print(AuditListDB[(indexPath?.row)!].flag!)
//        if(AuditListDB[(indexPath?.row)!].flag! == "not submitted"){
//            ZIVC.score = String()
//            ZIVC.textMessage = String()
//            ZIVC.status = String()
//        }else{
//            print((AuditListDB[(indexPath?.row)!].Image))
//            ZIVC.ImageCollectFromServer = (AuditListDB[(indexPath?.row)!].Image)
//                ZIVC.score = AuditListDB[(indexPath?.row)!].Score!
//         ZIVC.textMessage = AuditListDB[(indexPath?.row)!].Observation!
//         ZIVC.status = AuditListDB[(indexPath?.row)!].Report_Status!
//        }
//        ZIVC.pointId = AuditListDB[(indexPath?.row)!].CL_Point_ID!
//        ZIVC.auditId = AuditListDB[(indexPath?.row)!].Audit_ID!
//
//                self.navigationController?.pushViewController(ZIVC, animated: true)
   
        
    }
    var cellButton : ButtonAuditTableViewCell!
    var valStatus : Int = 0
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

extension AuditCheckListViewController : UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
      
//        if(indexPath.section == 0){
            return UITableViewAutomaticDimension
//        }else{
//            return 50
//        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 2
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(section == 0){
        return AuditListDB.count
        }else{
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 {
            return CGFloat.leastNormalMagnitude
        }else {
            return 10
        }
    }
}

extension AuditCheckListViewController : UITableViewDataSource {
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if(indexPath.section == 0){
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! AuditCheckListTableViewCell
        
       
        
        cell.containerView.layer.cornerRadius = 8
//        cell.PointIdLbl.text = "Point Id #" + AuditListDB[indexPath.row].CL_Point_ID!
        cell.lblQuestionNo.text = String(indexPath.row + 1)
        cell.textViewPoint.text =  AuditListDB[indexPath.row].CL_5S_Point!
        if(AuditListDB[indexPath.row].flag! == "not submitted"){
              cell.viewStatusColor.backgroundColor = UIColor.colorwithHexString("F35E44", alpha: 1.0)
            cell.viewStatusColor.shadowColor = UIColor.colorwithHexString("F35E44", alpha: 1.0)
             cell.viewStatusColor.shadowOpacity = 0.5
            valStatus = 1
        }else{
              cell.viewStatusColor.backgroundColor = UIColor.colorwithHexString("C4DB5F", alpha: 1.0)
           
        }
      
        
         cell.viewStatusColor.layer.cornerRadius = 8
      
        
//        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tapCell(_:)))
//
//        cell.isUserInteractionEnabled = true;
//        cell.addGestureRecognizer(tapGesture)
        cell.btnCell.tag = indexPath.row
        
        
        return cell;
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "button", for: indexPath) as! ButtonAuditTableViewCell
        
            cell.btnNextHome.isHidden = true
            if(valStatus == 1){
                cell.btnNextHome.isHidden = true
                cell.isHidden = true
            }else{
                cell.isHidden = false
                cell.btnNextHome.isHidden = false
            }
            cellButton = cell
            return cell
        }
    }
}
