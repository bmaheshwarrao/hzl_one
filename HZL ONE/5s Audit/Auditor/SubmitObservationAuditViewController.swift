//
//  SubmitObservationAuditViewController.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 20/03/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit
import Fusuma
import Alamofire
import DLRadioButton
import SDWebImage

class SubmitObservationAuditViewController: CommonVSClass,UICollectionViewDelegate,UICollectionViewDataSource,FusumaDelegate {
    @IBOutlet weak var tableView: UITableView!
    var status = String()
    var textMessage = String()
    var score = String()
    
    var pointId = String()
    var auditId = String()
    var ImageCollectFromServer = [ImageModel]()
    
    
    
    
    
    var cellData : SubmitTableViewCell!
    
    
    
    var PlaceHolderText = "Type Observation here."
    var imageArrayLoad : [ImageObject] = []
    var imageFromSerLoad : [ImageObjectLoad] = []
    // @IBOutlet weak var viewCamera: NSLayoutConstraint!
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
        tableView.tableFooterView?.backgroundColor = UIColor.groupTableViewBackground
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        
        self.title = "Submit Observation"
        self.tabBarController?.tabBar.isHidden = true
        let nav = self.navigationController?.navigationBar
        nav?.barStyle = UIBarStyle.black
        nav?.tintColor = UIColor.white
        nav?.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func btnCrossClicked(_ sender: UIButton) {
        
        print(sender.tag)
        imageArrayLoad.remove(at: sender.tag)
        imagesData.remove(at: sender.tag)
        cellData.collectionView.reloadData()
        updateConstrains()
        
        
        
    }
    @IBAction func btnCrossClickedImageLoad(_ sender: UIButton) {
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.0) {
            
            let actionSheetController: UIAlertController = UIAlertController(title: "Alert", message: "Are you sure you want to delete?", preferredStyle: .actionSheet)
            
            let delete = UIAlertAction(title: "Delete", style: .default, handler: { (alert) in
                
                self.startLoadingPK(view: self.view)
                
                let parameter = ["ImageID":self.imageFromSerLoad[sender.tag].imageId!,
                                 "EmployeeID": UserDefaults.standard.string(forKey: "EmployeeID")!
                    
                    ] as [String:String]
                
                
                
                print("parameter",parameter)
                
                WebServices.sharedInstances.sendPostRequest(url: URLConstants.Delete_Image, parameters: parameter, successHandler: { (response:[String : AnyObject]) in
                    
                    let respon = response["response"] as! [String:AnyObject]
                    self.stopLoadingPK(view: self.view)
                    let objectmsg = MessageCallServerModel()
                    let msg = objectmsg.sealizeMessage(cell: respon as! [String : AnyObject])
                    if respon["status"] as! String == "success" {
                        self.view.makeToast(msg)
                        self.imageFromSerLoad.remove(at: sender.tag)
                        self.cellData.collectionViewLoadImage.reloadData()
                        self.updateConstrains()
                        
                    }else{
                        self.stopLoadingPK(view: self.view)
                        self.view.makeToast(msg)
                    }
                    
                }) { (err) in
                    self.stopLoadingPK(view: self.view)
                    print(err.description)
                }
                
                
                
                
            })
            
            let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: { (alert) in
                
            })
            
            actionSheetController.addAction(delete)
            
            actionSheetController.addAction(cancel)
            self.present(actionSheetController, animated: true, completion: nil)
            
            
        }
        
        
    }
    @IBAction func UploadImageTapped(_ sender: UITapGestureRecognizer) {
        let fusuma = FusumaViewController()
        
        fusuma.delegate = self
        fusuma.cropHeightRatio = 1.0
        fusuma.allowMultipleSelection = true
        
        fusumaSavesImage = true
        
        self.present(fusuma, animated: true, completion: nil)
    }
    var strAction = String()
    @IBAction func radioButtonClicked(_ sender: DLRadioButton) {
        
        if (sender.isMultipleSelectionEnabled) {
            for button in sender.selectedButtons() {
                print(String(format: "%@ is selected.\n", button.titleLabel!.text!));
            }
        } else {
            //print(String(format: "%@ is selected.\n", sender.selected()!.titleLabel!.text!));
        }
        
        if(cellData.radioOpen.isSelected == true){
            self.strAction = "Open"
            // cellData.radioOpen.isSelected = false
        }else{
            
            self.strAction = "Close"
        }
    }
    func imageResize (image:UIImage, sizeChange:CGSize)-> UIImage{
        
        let hasAlpha = true
        let scale: CGFloat = 0.0 // Use scale factor of main screen
        
        UIGraphicsBeginImageContextWithOptions(sizeChange, !hasAlpha, scale)
        image.draw(in: CGRect(origin: CGPoint.zero, size: sizeChange))
        
        let scaledImage = UIGraphicsGetImageFromCurrentImageContext()
        return scaledImage!
    }
    /// score
    
    
    
    
    @IBAction func btnScore0Clicked(_ sender: RoundShapeButton) {
        
        score = "0"
        scoreValData(val : Int(score)!)
    }
    @IBAction func btnScore1Clicked(_ sender: RoundShapeButton) {
        
        score = "1"
        scoreValData(val : Int(score)!)
    }
    @IBAction func btnScore2Clicked(_ sender: RoundShapeButton) {
        
        score = "2"
        scoreValData(val : Int(score)!)
    }
    @IBAction func btnScore3Clicked(_ sender: RoundShapeButton) {
        
        score = "3"
        scoreValData(val : Int(score)!)
    }
    @IBAction func btnScore4Clicked(_ sender: RoundShapeButton) {
        
        score = "4"
        scoreValData(val : Int(score)!)
    }
    
    func scoreValData(val : Int){
        if(val == 0){
            cellData.viewRate0.backgroundColor = UIColor.black
            cellData.viewRate1.backgroundColor = UIColor.white
            cellData.viewRate2.backgroundColor = UIColor.white
            cellData.viewRate3.backgroundColor = UIColor.white
            cellData.viewRate4.backgroundColor = UIColor.white
        }else if(val == 1){
            cellData.viewRate0.backgroundColor = UIColor.white
            cellData.viewRate1.backgroundColor = UIColor.black
            cellData.viewRate2.backgroundColor = UIColor.white
            cellData.viewRate3.backgroundColor = UIColor.white
            cellData.viewRate4.backgroundColor = UIColor.white
        }else if(val == 2){
            cellData.viewRate0.backgroundColor = UIColor.white
            cellData.viewRate1.backgroundColor = UIColor.white
            cellData.viewRate2.backgroundColor = UIColor.black
            cellData.viewRate3.backgroundColor = UIColor.white
            cellData.viewRate4.backgroundColor = UIColor.white
        }else if(val == 3){
            cellData.viewRate0.backgroundColor = UIColor.white
            cellData.viewRate1.backgroundColor = UIColor.white
            cellData.viewRate2.backgroundColor = UIColor.white
            cellData.viewRate3.backgroundColor = UIColor.black
            cellData.viewRate4.backgroundColor = UIColor.white
        }else{
            cellData.viewRate0.backgroundColor = UIColor.white
            cellData.viewRate1.backgroundColor = UIColor.white
            cellData.viewRate2.backgroundColor = UIColor.white
            cellData.viewRate3.backgroundColor = UIColor.white
            cellData.viewRate4.backgroundColor = UIColor.black
        }
    }
    
    func updateConstrains(){
        if(imageArrayLoad.count != 0){
            cellData.collectionView.isHidden = false
            cellData.heightImageBrowse.constant = 103
        }else{
            cellData.collectionView.isHidden = true
            cellData.heightImageBrowse.constant = 0
        }
        if(imageFromSerLoad.count == 0){
            cellData.collectionViewLoadImage.isHidden = true
            cellData.lblUploadImages.isHidden = true
            cellData.heightAnotherView.constant = 0
            cellData.heightAnotherCollection.constant = 0
            cellData.heightUploadedImages.constant = 0
        } else{
            cellData.collectionViewLoadImage.isHidden = false
            cellData.lblUploadImages.isHidden = false
            cellData.heightAnotherView.constant = 126
            cellData.heightAnotherCollection.constant = 103
            cellData.heightUploadedImages.constant = 18
        }
        
        if(imageArrayLoad.count > 0 && imageFromSerLoad.count > 0){
            cellData.bottomCell.constant =   1.5
        }else if(imageArrayLoad.count == 0 && imageFromSerLoad.count == 0){
            cellData.bottomCell.constant =   230.5
        }else{
            
            cellData.bottomCell.constant =  (cellData.heightAnotherView.constant + cellData.heightImageBrowse.constant + 1.5)
            cellData.cameraViewTop.constant = 19
            print(cellData.bottomCell.constant)
        }
        //tableView.reloadData()
    }
    
    
    
    /// Process Clicked
    
    var reachability = Reachability()!
    var imagedata: Data? = nil
    var imagesData : [Data] = []
    @IBAction func btnProcessClicked(_ sender: UIButton) {
        
        if reachability.connection == .none{
            
            self.view.makeToast("Internet is not available, please check your internet connection try again.")
            
        }else if(self.score == String()){
            
            self.view.makeToast("Please Select Score")
        }else if(self.strAction == String()){
            
            self.view.makeToast("Please Select Status")
        }
        else if self.cellData.textViewObservation.text == "" || self.cellData.textViewObservation.text == PlaceHolderText{
            
            self.view.makeToast("Please Write Message")
        }else{
            
            
            
            self.startLoadingPK(view: self.view)
            
            
            
            let empId : String = UserDefaults.standard.string(forKey: "EmployeeID")!
            
            let parameter =  [
                "AuditID": auditId ,
                "CL_Point_ID":pointId,
                "Observation": cellData.textViewObservation.text!,
                "Status":self.strAction,
                "Score":score
                
                
                ] as! [String:String]
            
            print(parameter)
            let jsonData: Data? = try? JSONSerialization.data(withJSONObject: parameter, options: [])
            let para: [String: Data] = ["data": jsonData!]
            
            Alamofire.upload(multipartFormData: { multipartFormData in
                
                if (self.imageArrayLoad.count > 0) {
                    var i = 0
                    for imageData in self.imageArrayLoad {
                        
                        let size = CGSize(width: 500, height: 500)
                        let res = self.imageResize(image: imageData.imageLoad!,sizeChange: size)
                        let imgNSDAta = UIImageJPEGRepresentation(res, 1.0)!
                        
                        let fileName = String(Date().timeIntervalSince1970) + String(i)
                        multipartFormData.append(imgNSDAta, withName: "\(fileName)", fileName: "file.jpg", mimeType: "image/jpeg")
                        i = i + 1
                    }
                    
                }
                
                for (key, value) in para {
                    multipartFormData.append(value, withName: key)
                    
                    
                }
                
            },
                             to:URLConstants.Submit_Observation)
            { (result) in
                switch result {
                case .success(let upload, _, _):
                    
                    upload.uploadProgress(closure: { (progress) in
                        
                    })
                    
                    
                    upload.responseJSON { response in
                        
                        
                        if let json = response.result.value
                        {
                            var dict = json as! [String: AnyObject]
                            
                            if let response = dict["response"]{
                                print(response)
                                let statusString = response["status"] as! String
                                 let objectmsg = MessageCallServerModel()
                    let msg = objectmsg.sealizeMessage(cell: response as! [String : AnyObject])
                                print("5s Audit Dict :",dict)
                                if(statusString == "success")
                                {
                                    
                                    
                                    
                                    self.view.makeToast("Successfully Submitted.")
                                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                                        self.stopLoadingPK(view: self.view)
                                        self.updateConstrains()
                                        self.navigationController?.popViewController(animated: true)
                                    }
                                    
                                    
                                    
                                }
                                else{
                                    
                                    self.stopLoadingPK(view: self.view)
                                    self.updateConstrains()
                                    
                                    
                                }
                                
                                
                                
                            }
                            
                            
                        }
                        
                        
                    }
                    
                case .failure(let encodingError):
                    
                    self.stopLoadingPK(view: self.view)
                    self.errorChecking(error: encodingError)
                    self.updateConstrains()
                    print(encodingError.localizedDescription)
                }
            }
        }
        
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if(collectionView == cellData.collectionViewLoadImage){
            return self.imageFromSerLoad.count
        }else{
            return self.imageArrayLoad.count
        }
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if(collectionView == cellData.collectionView){
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellImage", for: indexPath) as! ImageCollectionViewCell
            cell.btnCrossImage.tag = indexPath.row
            cell.contentView.frame.size.width = cell.imgCollection.frame.size.width
            cell.contentView.frame.size.height = cell.imgCollection.frame.size.height
            
            //        if(self.imageArrayLoad.count == indexPath.row){
            //        cell.btnCrossImage.isHidden = true
            //            cell.lblDelete.isHidden = true
            //            cell.imgCross.isHidden = true
            //            cell.btnNumber.isHidden = true
            //            cell.imgCollection.image =  UIImage.init(named: "imgAdd")
            //            let tapGesture = UITapGestureRecognizer(target: self, action: #selector(UploadImageTapped(_:)))
            //            cell.isUserInteractionEnabled = true;
            //            cell.addGestureRecognizer(tapGesture)
            //
            //        }else{
            cell.btnCrossImage.isHidden = false
            cell.lblDelete.isHidden = false
            cell.imgCross.isHidden = false
            let no : Int  = indexPath.row + 1
            cell.imgCollection.image =  imageArrayLoad[indexPath.row].imageLoad!
            cell.btnNumber.setTitle(String(no), for: .normal)
            cell.btnNumber.isHidden = false
            //}
            
            
            return cell
        }else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellImageData", for: indexPath) as! ImageDataCollectionViewCell
            cell.btnCrossImage.tag = indexPath.row
            cell.contentView.frame.size.width = cell.imgCollection.frame.size.width
            cell.contentView.frame.size.height = cell.imgCollection.frame.size.height
            
            cell.btnCrossImage.isHidden = false
            cell.lblDelete.isHidden = false
            cell.imgCross.isHidden = false
            let no : Int  = indexPath.row + 1
            cell.btnNumber.setTitle(String(no), for: .normal)
            print(imageFromSerLoad[indexPath.row].imageDataLoad)
            cell.imgCollection.image =  imageFromSerLoad[indexPath.row].imageDataLoad!
            
            
            
            
            return cell
        }
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        return UIEdgeInsetsMake(0, 0, 0, 0)
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        return CGSize(width: self.cellData.collectionView.bounds.size.width, height: self.cellData.collectionView.bounds.size.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        
        //print(indexPath.item)
        
        
    }
    
    func fusumaImageSelected(_ image: UIImage, source: FusumaMode) {

        switch source {

        case .camera:

            print("Image captured from Camera")

        case .library:

            print("Image selected from Camera Roll")

        default:

            print("Image selected")
        }

        let imgOb = ImageObject()
        imgOb.imageLoad = image
        imgOb.Id = String(imageArrayLoad.count + 1)
        imageArrayLoad.append(imgOb)
        self.cellData.collectionView.reloadData()
        let size = CGSize(width: 500, height: 500)
        var res = self.imageResize(image: image,sizeChange: size)
        self.imagedata = UIImageJPEGRepresentation(res, 1.0)!
        self.imagesData.append(self.imagedata!)
        updateConstrains()
    }

    func fusumaMultipleImageSelected(_ images: [UIImage], source: FusumaMode) {

        print("Number of selection images: \(images.count)")

        var count: Double = 0

        for image in images {

            //   DispatchQueue.main.asyncAfter(deadline: .now() + (3.0 * count)) {

            let imgOb = ImageObject()
            imgOb.imageLoad = image
            imgOb.Id = String(self.imageArrayLoad.count + 1)
            self.imageArrayLoad.append(imgOb)
            self.cellData.collectionView.reloadData()
            print("w: \(image.size.width) - h: \(image.size.height)")
            let size = CGSize(width: 500, height: 500)
            let res = self.imageResize(image: image,sizeChange: size)
            self.imagedata = UIImageJPEGRepresentation(res, 1.0)!
            self.imagesData.append(self.imagedata!)
            updateConstrains()
            //            if(imageFromSerLoad.count == 0){
            //                 cellData.bottomCell.constant = 127.5
            //            }else{
            //                cellData.bottomCell.constant = 1.5
            //            }

        }

        //count += 1
        //}

        //        heightImage.constant = 140
        //        collectionView.isHidden = false
        //        viewCamera.constant = 0
        //        viewPhotoUpload.isHidden = true

    }

    func fusumaImageSelected(_ image: UIImage, source: FusumaMode, metaData: ImageMetadata) {

        print("Image mediatype: \(metaData.mediaType)")
        print("Source image size: \(metaData.pixelWidth)x\(metaData.pixelHeight)")
        print("Creation date: \(String(describing: metaData.creationDate))")
        print("Modification date: \(String(describing: metaData.modificationDate))")
        print("Video duration: \(metaData.duration)")
        print("Is favourite: \(metaData.isFavourite)")
        print("Is hidden: \(metaData.isHidden)")
        print("Location: \(String(describing: metaData.location))")
    }

    func fusumaVideoCompleted(withFileURL fileURL: URL) {

        print("video completed and output to file: \(fileURL)")
        // self.fileUrlLabel.text = "file output to: \(fileURL.absoluteString)"
    }

    func fusumaDismissedWithImage(_ image: UIImage, source: FusumaMode) {

        switch source {

        case .camera:

            print("Called just after dismissed FusumaViewController using Camera")

        case .library:

            print("Called just after dismissed FusumaViewController using Camera Roll")

        default:

            print("Called just after dismissed FusumaViewController")
        }
    }

    func fusumaCameraRollUnauthorized() {

        print("Camera roll unauthorized")

        let alert = UIAlertController(title: "Access Requested",
                                      message: "Saving image needs to access your photo album",
                                      preferredStyle: .alert)

        alert.addAction(UIAlertAction(title: "Settings", style: .default) { (action) -> Void in

            if let url = URL(string:UIApplicationOpenSettingsURLString) {

                UIApplication.shared.openURL(url)
            }
        })

        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel) { (action) -> Void in

        })

        guard let vc = UIApplication.shared.delegate?.window??.rootViewController,
            let presented = vc.presentedViewController else {

                return
        }

        presented.present(alert, animated: true, completion: nil)
    }

    func fusumaClosed() {

        print("Called when the FusumaViewController disappeared")
    }

    func fusumaWillClosed() {

        print("Called when the close button is pressed")
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
extension SubmitObservationAuditViewController : UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView)
    {
        if (textView.text == PlaceHolderText)
        {
            textView.text = ""
            textView.textColor = .black
        }
        
        
    }
    
    func textViewDidEndEditing(_ textView: UITextView)
    {
        if (textView.text == "")
        {
            textView.text = PlaceHolderText
            textView.textColor = .lightGray
        }
        self.cellData.textViewObservation.endEditing(true)
        
    }
}
extension SubmitObservationAuditViewController : UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    
}

extension SubmitObservationAuditViewController : UITableViewDataSource {
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! SubmitTableViewCell
        cell.selectionStyle = .none
        cellData = cell
        cellData.collectionView.delegate = self
        cellData.collectionView.dataSource = self
        cellData.collectionViewLoadImage.delegate = self
        cellData.collectionViewLoadImage.dataSource = self
        if(ImageCollectFromServer.count > 0){
            for i in 0...self.ImageCollectFromServer.count - 1{
                let imgOb = ImageObjectLoad()
                let urlString = self.ImageCollectFromServer[i].ImageURL
                let urlShow = urlString?.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
                
                
                print(urlShow!)
                SDWebImageManager.shared().downloadImage(with: NSURL(string: urlShow!) as URL!, options: .continueInBackground, progress: {
                    (receivedSize :Int, ExpectedSize :Int) in
                    
                }, completed: {
                    (image : UIImage?, error : Error?, cacheType : SDImageCacheType, finished : Bool, url : URL?) in
                    if(image != nil){
                        imgOb.imageDataLoad = image
                    }else{
                        imgOb.imageDataLoad = UIImage(named: "placed")
                    }
                    imgOb.IdLoad = String(self.imageFromSerLoad.count + 1)
                    imgOb.imageId = self.ImageCollectFromServer[i].ImageID
                    self.imageFromSerLoad.append(imgOb)
                    self.cellData.collectionViewLoadImage.reloadData()
                    
                    
                })
                
            }
            
            cellData.heightImageBrowse.constant = 0
            cellData.bottomCell.constant = 104.5
        }else{
            
            cellData.heightUploadedImages.constant = 0
            cellData.heightAnotherView.constant = 0
            cellData.heightAnotherCollection.constant = 0
            cellData.heightImageBrowse.constant = 0
            cellData.bottomCell.constant = 231.5
        }
        
        if(score != String()){
            scoreValData(val : Int(score)!)
        }
        
        cellData.textViewObservation.layer.cornerRadius = 10.0
        cellData.textViewObservation.layer.borderWidth = 1.0
        cellData.textViewObservation.layer.borderColor = UIColor.black.cgColor
        if(textMessage == String()){
            cellData.textViewObservation.text = PlaceHolderText
            cellData.textViewObservation.textColor = .lightGray
        }else{
            cellData.textViewObservation.text = textMessage
            cellData.textViewObservation.textColor = .black
        }
        
        
        cellData.textViewObservation.delegate = self
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(UploadImageTapped(_:)))
        cellData.viewPhotoUpload.isUserInteractionEnabled = true;
        cellData.viewPhotoUpload.addGestureRecognizer(tapGesture)
        
        print(status)
        if(status == "Open"){
            cellData.radioOpen.isSelected = true
        }else{
            cellData.radioOpen.isSelected = false
        }
        cellData.radioOpen.isMultipleSelectionEnabled = true;
        cellData.radioOpen.isIconSquare = true
        strAction = status
        imageArrayLoad  = []
        // Do any additional setup after loading the view.
        
        return cell;
    }
}


class ImageObjectLoad : NSObject{
    var IdLoad : String?
    var imageId : String?
    var imageDataLoad : UIImage?
}
class ImageObject : NSObject{
    var Id : String?
    var imageLoad : UIImage?
}
