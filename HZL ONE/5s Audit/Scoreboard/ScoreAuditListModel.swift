//
//  ScoreAuditListModel.swift
//  5s Audit
//
//  Created by SARVANG INFOTCH on 17/11/18.
//  Copyright © 2018 SARVANG INFOTCH. All rights reserved.
//

import Foundation
import Reachability



class ScoreAuditListModel: NSObject {
    
    
    
    var ID = Int()
    var LocationName : String?
    var Audit_MasterID : String?
    var TotalScore : String?
    var Audit_DateTime : String?
    var Auditor_Comment : String?
    var NextLevel : String?
    
   
    
    func setDataInModel(cell:[String:AnyObject])
    {
        
        if cell["ID"] is NSNull{
            self.ID = 0
        }else{
            let idd1 : Int = (cell["ID"] as? Int)!
            self.ID = idd1
            
        }
        
        if cell["Audit_MasterID"] is NSNull{
            self.Audit_MasterID = ""
        }else{
            let idd1 : Int = (cell["Audit_MasterID"] as? Int)!
            self.Audit_MasterID = String(idd1)
            
        }
        if cell["LocationName"] is NSNull{
            self.LocationName = ""
        }else{
            self.LocationName = (cell["LocationName"] as? String)!
        }
        if cell["TotalScore"] is NSNull{
            self.TotalScore = ""
        }else{
            let idd1 : Int = (cell["TotalScore"] as? Int)!
            self.TotalScore = String(idd1)
          
        }
        if cell["NextLevel"] is NSNull{
            self.NextLevel = ""
        }else{
            self.NextLevel = (cell["NextLevel"] as? String)!
        }
        
        
        if(self.NextLevel != "NA"){
        if cell["Audit_DateTime"] is NSNull{
            self.Audit_DateTime = ""
        }else{
            self.Audit_DateTime = (cell["Audit_DateTime"] as? String)!
        }
        
        if cell["Auditor_Comment"] is NSNull{
            self.Auditor_Comment = ""
        }else{
            self.Auditor_Comment = (cell["Auditor_Comment"] as? String)!
        }
        }
        
        
    }
}


class ScoreAuditListAPI
{
    
    var reachablty = Reachability()!
    
    
    func serviceCalling(obj:ScoreboardAuditListViewController,parameter:[String:String], success:@escaping (AnyObject)-> Void)
    {
        
        obj.startLoading()
        
        if(reachablty.connection != .none)
        {
            
            WebServices.sharedInstances.sendPostRequest(url: URLConstants.Get_Scoreboard_Audit_List, parameters: parameter, successHandler: { (dict) in
                
                if let response = dict["response"]{
                    
                    let statusString : String = response["status"] as! String
                    let objectmsg = MessageCallServerModel()
            let msg = objectmsg.sealizeMessage(cell: response as! [String : AnyObject])
                    print(response)
                    if(statusString == "success")
                    {
                        
                        obj.tableView.isHidden = false
                        obj.noDataLabel(text: "" )
                        obj.label.isHidden = true
                        
                        if let data = dict["data"] as? [AnyObject]
                        {
                            
                            var dataArray : [ScoreAuditListModel] = []
                            
                            for i in 0..<data.count
                            {
                                
                                if let celll = data[i] as? [String:AnyObject]
                                {
                                    let object = ScoreAuditListModel()
                                    object.setDataInModel(cell: celll)
                                    dataArray.append(object)
                                }
                                
                            }
                            
                            success(dataArray as AnyObject)
                            
                            
                        }
                        
                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                    }
                    else
                    {
                        
                        
                        print("DATA:fail")
                        obj.tableView.isHidden = true
                        obj.noDataLabel(text: msg)
                        obj.refresh.endRefreshing()
                        obj.label.isHidden = false
                        obj.stopLoading()
                        
                    }
                    
                    
                }
                
                
                
            }, failureHandler: { (error) in
                let errorStr : String = error.description
                
                print("DATA:error",errorStr)
                
                // obj.errorChecking(error: error)
                obj.stopLoading()
            })
            
            
        }
        else{
            obj.noDataLabel(text:  "No internet connection found. Check your internet connection and try again.")
            obj.tableView.isHidden = true
            obj.label.isHidden = false
            obj.refresh.endRefreshing()
            obj.stopLoading()
        }
        
        
    }
    
    
}




