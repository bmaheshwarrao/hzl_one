//
//  ScoreListModel.swift
//  5s Audit
//
//  Created by SARVANG INFOTCH on 17/11/18.
//  Copyright © 2018 SARVANG INFOTCH. All rights reserved.
//



import Foundation
import Reachability



class ScoreListModel: NSObject {
    
    
    
  
    var Audit_MasterID : String?
    var Audit_MonthYear : String?
    var Audit_Title : String?
    var Audit_Type : String?
  
    

    
    func setDataInModel(cell:[String:AnyObject])
    {
        
        if cell["Audit_MasterID"] is NSNull{
            self.Audit_MasterID = ""
        }else{
            let idd1 : Int = (cell["Audit_MasterID"] as? Int)!
            self.Audit_MasterID = String(idd1)
            
        }
        if cell["Audit_MonthYear"] is NSNull{
            self.Audit_MonthYear = ""
        }else{
            self.Audit_MonthYear = (cell["Audit_MonthYear"] as? String)!
        }
        if cell["Audit_Title"] is NSNull{
            self.Audit_Title = ""
        }else{
            self.Audit_Title = (cell["Audit_Title"] as? String)!
        }
        if cell["Audit_Type"] is NSNull{
            self.Audit_Type = ""
        }else{
           self.Audit_Type = (cell["Audit_Type"] as? String)!
        }
       
        
        
    }
}


class ScoreListAPI
{
    
    var reachablty = Reachability()!
    
    
    func serviceCalling(obj:ScoreboardViewController,parameter:[String:String], success:@escaping (AnyObject)-> Void)
    {
        
        obj.startLoading()
        
        if(reachablty.connection != .none)
        {
            
            WebServices.sharedInstances.sendPostRequest(url: URLConstants.Get_Scoreboard_List, parameters: parameter, successHandler: { (dict) in
                
                if let response = dict["response"]{
                    
                    let statusString : String = response["status"] as! String
                    let objectmsg = MessageCallServerModel()
            let msg = objectmsg.sealizeMessage(cell: response as! [String : AnyObject])
                    print(response)
                    if(statusString == "success")
                    {
                        
                        obj.tableView.isHidden = false
                        obj.noDataLabel(text: "" )
                        obj.label.isHidden = true
                        
                        if let data = dict["data"] as? [AnyObject]
                        {
                            
                            var dataArray : [ScoreListModel] = []
                            
                            for i in 0..<data.count
                            {
                                
                                if let celll = data[i] as? [String:AnyObject]
                                {
                                    let object = ScoreListModel()
                                    object.setDataInModel(cell: celll)
                                    dataArray.append(object)
                                }
                                
                            }
                            
                            success(dataArray as AnyObject)
                            
                            
                        }
                        
                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                    }
                    else
                    {
                        
                        
                        print("DATA:fail")
                        obj.tableView.isHidden = true
                        obj.noDataLabel(text: msg)
                        obj.refresh.endRefreshing()
                        obj.label.isHidden = false
                        obj.stopLoading()
                        
                    }
                    
                    
                }
                
                
                
            }, failureHandler: { (error) in
                let errorStr : String = error.description
                
                print("DATA:error",errorStr)
                
                // obj.errorChecking(error: error)
                obj.stopLoading()
            })
            
            
        }
        else{
            obj.noDataLabel(text:  "No internet connection found. Check your internet connection and try again.")
            obj.tableView.isHidden = true
            obj.label.isHidden = false
            obj.refresh.endRefreshing()
            obj.stopLoading()
        }
        
        
    }
    
    
}




