//
//  MeetingManagerAssignTaskViewController.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 09/04/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit
import ViewPager_Swift
class MeetingManagerAssignTaskViewController: CommonVSClass ,UITableViewDelegate, UITableViewDataSource {
    var typeData = String()
    @IBOutlet weak var tableView: UITableView!
    var refreshControl = UIRefreshControl()
    
    
    var status = String()
    var task_Id = String()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.estimatedRowHeight = 65
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.tableFooterView = UIView()
        
        
        
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        if(MoveStruct.isMove == true){
            self.navigationController?.popViewController(animated: false)
        }
        self.tabBarController?.tabBar.isHidden = true
        self.title = "Assign Task"
        let nav = self.navigationController?.navigationBar
        nav?.tintColor = UIColor.white
        nav?.barTintColor = UIColor(hexString: "2c3e50", alpha: 1.0)
        nav?.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        
    }
    var reachability = Reachability()!
    

   
    @IBAction func btnProcesstoCirculate(_ sender: UIButton) {
        let storyboard1 = UIStoryboard(name: "MOM", bundle: nil)
        let auditVC = storyboard1.instantiateViewController(withIdentifier: "ProcesstoCirculateViewController") as! ProcesstoCirculateViewController
        auditVC.meetingDetailsDB =  meetingDetailsDB
         auditVC.meetingCellDB =  meetingCellDB
        self.navigationController?.pushViewController(auditVC, animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    var MeetingAssignDB = ManageMeetingModel()
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    var meetingDetailsDB = ViewMeetingManagerModel()
     var meetingCellDB = ManageMeetingModel()
    var cellData : MeetingManagerAssignTaskTableViewCell!
    var data: String?
    var lastObject: String?
    var indPath = IndexPath()
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if(section == 0 ) {
            return 10.0
        } else {
            return 0.0
        }
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 1
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if(indexPath.section == 0){
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! ManageMeetingMOMTableViewCell
            
         
            cell.lblOwner.text = "Owner : " + self.meetingCellDB.Meeting_Owner_Name + " - " + self.meetingCellDB.Meeting_Owner
            cell.lblVenue.text = "Venue : " + self.meetingCellDB.Meeting_Venue
            cell.lblDesc.text = self.meetingCellDB.Meeting_Title
            cell.containerView.layer.cornerRadius = 10
            
            let milisecondDate = self.meetingCellDB.Meeting_DateTime
            let dateVarDate = Date.init(timeIntervalSince1970: TimeInterval(milisecondDate)!/1000)
            let dateFormatterday = DateFormatter()
            dateFormatterday.dateFormat = "dd"
            let valDate =  dateFormatterday.string(from: dateVarDate)
            cell.lblDate.text = valDate
            let dateFormattermon = DateFormatter()
            dateFormattermon.dateFormat = "MMMM yy"
            let valDateMon =  dateFormattermon.string(from: dateVarDate)
            cell.lblMonYear.text = valDateMon
            
            let dateFormatterWeekDay = DateFormatter()
            dateFormatterWeekDay.dateFormat = "EEEE"
            let valDateWeekDay =  dateFormatterWeekDay.string(from: dateVarDate)
            cell.lblDay.text = valDateWeekDay
            
            cell.lblTime.text = "Time : " + self.meetingCellDB.Meeting_Time
            
            
            
            
            
            
            
            
            
            
            cell.separatorInset = UIEdgeInsetsMake(0, 10000, 0, 0);
            cell.selectionStyle = .none
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellAssign", for: indexPath) as! MeetingManagerAssignTaskTableViewCell
            cellData = cell
            let myOptions = ViewPagerOptions(viewPagerWithFrame: CGRect(x: 0, y: 0, width: cell.viewSplitter.frame.width , height: cell.viewSplitter.frame.height - 50))
            
            myOptions.tabType = ViewPagerTabType.basic
            
            
            
            
            
            
            myOptions.isTabHighlightAvailable = true
            // myOptions.is
            // If I want indicator bar to show below current page tab
            myOptions.isTabIndicatorAvailable = true
            myOptions.fitAllTabsInView = true;
            myOptions.tabViewTextFont = UIFont.systemFont(ofSize: 15.0, weight: .medium)
            // Oh! and let's change color of tab to red
            myOptions.tabIndicatorViewBackgroundColor = UIColor.yellow
            myOptions.tabViewBackgroundHighlightColor = UIColor(hexString: "2c3e50", alpha: 1.0)!
            myOptions.tabViewBackgroundDefaultColor = UIColor(hexString: "2c3e50", alpha: 1.0)!
            myOptions.tabViewTextDefaultColor = UIColor.yellow
            myOptions.tabViewTextHighlightColor = UIColor.yellow
            
            let viewPager = ViewPagerController()
            
            viewPager.options = myOptions
            viewPager.dataSource = self
            
            //Now let me add this to my viewcontroller
            self.addChildViewController(viewPager)
            
            
            
            cell.viewSplitter.addSubview(viewPager.view)
            viewPager.didMove(toParentViewController: self)
            return cell
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableViewAutomaticDimension
        
    }
    
}
extension MeetingManagerAssignTaskViewController : ViewPagerControllerDataSource {
    func numberOfPages() -> Int {
        return 2
    }
    
    func viewControllerAtPosition(position: Int) -> UIViewController {
        let storyBoard : UIStoryboard = UIStoryboard(name: "MOM",bundle : nil)
        if(position == 0) {
            
            let empVC = storyBoard.instantiateViewController(withIdentifier: "AssignTaskMOMViewController") as! AssignTaskMOMViewController
           empVC.meetingDetailsDB =  meetingDetailsDB
            return  empVC as CommonVSClass
        }else  {
            
            let empVC = storyBoard.instantiateViewController(withIdentifier: "AssignedTaskMOMViewController") as! AssignedTaskMOMViewController
           empVC.meetingDetailsDB =  meetingDetailsDB
            return  empVC as CommonVSClass
        }
      
    }
    
    func tabsForPages() -> [ViewPagerTab] {
        return [ViewPagerTab(title: "Assign Task" , image: #imageLiteral(resourceName: "BackBlack")) , ViewPagerTab(title: "Assigned Task" , image: #imageLiteral(resourceName: "BackBlack"))]
    }
    
    
}
extension MeetingManagerAssignTaskViewController : ViewPagerControllerDelegate {
    
}


