//
//  CompletedMeetingManagerViewController.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 10/04/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit

class CompletedMeetingManagerViewController: CommonVSClass ,UITableViewDelegate, UITableViewDataSource {
    var typeData = String()
    @IBOutlet weak var tableView: UITableView!
    var refreshControl = UIRefreshControl()
    
    
    var meeting_Id = String()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getMeetingData()
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.estimatedRowHeight = 65
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.tableFooterView = UIView()
        
        refreshControl.addTarget(self, action: #selector(getMeetingData), for: .valueChanged)
        
        self.tableView.addSubview(refreshControl)
        let swipe = UISwipeGestureRecognizer(target: self, action: #selector(self.swipeRecognizer))
        swipe.direction = ([.down])
        self.view.addGestureRecognizer(swipe)
        
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.tabBarController?.tabBar.isHidden = true
        self.title = "Meeting Detail"
        let nav = self.navigationController?.navigationBar
        nav?.tintColor = UIColor.white
        nav?.barTintColor = UIColor(hexString: "2c3e50", alpha: 1.0)
        nav?.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        
    }
    var reachability = Reachability()!
    @objc func swipeRecognizer(_ sender: UISwipeGestureRecognizer) {
        if (sender.direction == .down) {
            
            if(self.reachability.connection != .none)
            {
                
                self.getMeetingData()
            }
            else
            {
                self.startLoading(view: self.view)
                DispatchQueue.main.asyncAfter(deadline: .now() + 4.5) {
                    self.noDataLabel(text: "No internet connection found. Check your internet connection and try again.")
                    self.stopLoading(view: self.view)
                }
            }
            
        }
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    var ManageMeetingDB:[ViewMeetingManagerModel] = []
    var ManageMeetingAPI = ViewManageMeetingCompletedModelDataAPI()
    @objc func getMeetingData(){
        var param = [String:String]()
        
        
        param = ["Meeting_ID": meeting_Id   ]
        print(param)
        
        
        ManageMeetingAPI.serviceCalling(obj: self,  parameter: param ) { (dict) in
            
            self.ManageMeetingDB = dict as! [ViewMeetingManagerModel]
            
            self.tableView.reloadData()
            
        }
        
        self.tableView.reloadData()
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    var data: String?
    var lastObject: String?
    var indPath = IndexPath()
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        
        
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let header = tableView.dequeueReusableCell(withIdentifier: "header") as! HeaderMyMeetingTableViewCell
        if(section == 2) {
            header.lblHeader.text = "INTERNAL PARTICIPANTS"
        }else if(section == 3) {
            header.lblHeader.text = "EXTERNAL PARTICIPANTS"
        }else if(section == 4) {
            header.lblHeader.text = "TASK"
        }else{
            header.lblHeader.text = ""
        }
        
        return header
        
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if(section == 0 || section == 1) {
            return 0.0
        } else {
            return 50.0
        }
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 5
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(section == 0 || section == 1){
            return 1
        }else if (section == 2) {
            if(self.ManageMeetingDB.count > 0){
            if(self.ManageMeetingDB[0].viewInternal.count > 0) {
                return self.ManageMeetingDB[0].viewInternal.count
            }else{
                return 1
            }
            }else{
                return 1
            }
        }else if (section == 3) {
            if(self.ManageMeetingDB.count > 0){
            if(self.ManageMeetingDB[0].viewExternal.count > 0) {
                return self.ManageMeetingDB[0].viewExternal.count
            }else{
                return 1
            }
            }else{
                return 1
            }
        }else  {
            if(self.ManageMeetingDB.count > 0){
            if(self.ManageMeetingDB[0].viewTask.count > 0) {
                return self.ManageMeetingDB[0].viewTask.count
            }else{
                return 1
            }
            }else{
                return 1
            }
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if(indexPath.section == 0) {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellMeeting", for: indexPath) as! TitleMyMeetingTableViewCell
             if(self.ManageMeetingDB.count > 0){
            
            cell.lblTitle.text = self.ManageMeetingDB[0].viewMeet[indexPath.row].Meeting_Title
            cell.textViewAgenda.text = self.ManageMeetingDB[0].viewMeet[indexPath.row].Meeting_Agenda
            cell.separatorInset = UIEdgeInsetsMake(0, 10000, 0, 0);
            cell.selectionStyle = .none
            }
            return cell
        } else if(indexPath.section == 1) {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! DetailMyMeetingTableViewCell
            
             if(self.ManageMeetingDB.count > 0){
            cell.lblDate.text = self.ManageMeetingDB[0].viewMeet[indexPath.row].Meeting_Date + " at " + self.ManageMeetingDB[0].viewMeet[indexPath.row].Meeting_Time
            cell.lblDuration.text = "Duration - " + self.ManageMeetingDB[0].viewMeet[indexPath.row].Meeting_Duration
            cell.lblUser.text = self.ManageMeetingDB[0].viewMeet[indexPath.row].Owner_Name + " - " + self.ManageMeetingDB[0].viewMeet[indexPath.row].Meeting_Owner
            cell.lblLocation.text = self.ManageMeetingDB[0].viewMeet[indexPath.row].Meeting_Venue
            cell.separatorInset = UIEdgeInsetsMake(0, 10000, 0, 0);
            cell.selectionStyle = .none
            }
            return cell
        } else if(indexPath.section == 2) {
            let cell = tableView.dequeueReusableCell(withIdentifier: "area", for: indexPath) as! PartnerTableViewCell
             if(self.ManageMeetingDB.count > 0){
            if(self.ManageMeetingDB[0].viewInternal.count > 0) {
                
                cell.lblEmpName.text = String(indexPath.row + 1) + ") " + self.ManageMeetingDB[0].viewInternal[indexPath.row].Employee_Name + " - " + self.ManageMeetingDB[0].viewInternal[indexPath.row].Employee_ID
            }else{
                cell.lblEmpName.text = "No Internal member added"
                cell.lblEmpName.textAlignment = NSTextAlignment.center
            }
            cell.clearBtn.isHidden = true
            cell.separatorInset = UIEdgeInsetsMake(0, 10000, 0, 0);
            cell.selectionStyle = .none
            }
            return cell
        }else if(indexPath.section == 3){
            let cell = tableView.dequeueReusableCell(withIdentifier: "area", for: indexPath) as! PartnerTableViewCell
             if(self.ManageMeetingDB.count > 0){
            if(self.ManageMeetingDB[0].viewExternal.count > 0) {
                
                cell.lblEmpName.text = String(indexPath.row + 1) + ") " + self.ManageMeetingDB[0].viewExternal[indexPath.row].Email_ID
                cell.lblEmpName.textAlignment = NSTextAlignment.left
            }else{
                cell.lblEmpName.text = "No External member added"
                cell.lblEmpName.textAlignment = NSTextAlignment.center
            }
            cell.clearBtn.isHidden = true
            cell.separatorInset = UIEdgeInsetsMake(0, 10000, 0, 0);
            cell.selectionStyle = .none
            }
            return cell
        }else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "task", for: indexPath) as! CompletedMamageMeetingTableViewCell
             if(self.ManageMeetingDB.count > 0){
            if(self.ManageMeetingDB[0].viewTask.count > 0) {
                
                cell.lblTask.text = String(indexPath.row + 1) + ") Assigned To : " + self.ManageMeetingDB[0].viewTask[indexPath.row].Assigned_Name + "-" +  self.ManageMeetingDB[0].viewTask[indexPath.row].Assigned_To
                cell.lblTask.textAlignment = NSTextAlignment.left
                if(self.ManageMeetingDB[0].viewTask[indexPath.row].Status == "Pending"){
                    cell.btnTask.backgroundColor = UIColor.orange
                }else{
                    cell.btnTask.backgroundColor = UIColor.colorwithHexString("75BF44", alpha: 1.0)
                }
                cell.btnTask.setTitle(self.ManageMeetingDB[0].viewTask[indexPath.row].Status, for: .normal)
                cell.btnTask.isHidden = false
                
                
            }else{
                cell.lblTask.text = "No Task added"
                cell.lblTask.textAlignment = NSTextAlignment.center
                 cell.btnTask.isHidden = true
            }
           
            cell.separatorInset = UIEdgeInsetsMake(0, 10000, 0, 0);
            cell.selectionStyle = .none
            }
            return cell
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableViewAutomaticDimension
        
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}
