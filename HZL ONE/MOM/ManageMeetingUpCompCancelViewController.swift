//
//  ManageMeetingUpCompCancelViewController.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 08/04/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit

class ManageMeetingUpCompCancelViewController: CommonVSClass ,UITableViewDelegate, UITableViewDataSource {
    var typeData = String()
    @IBOutlet weak var tableView: UITableView!
    var refreshControl = UIRefreshControl()
    
    var ManageMeetingDB:[ManageMeetingModel] = []
    var ManageMeetingAPI = ManageMeetingModelDataAPI()

    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.estimatedRowHeight = 65
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.tableFooterView = UIView()
        
        refreshControl.addTarget(self, action: #selector(getMeetingData), for: .valueChanged)
        
        self.tableView.addSubview(refreshControl)
        let swipe = UISwipeGestureRecognizer(target: self, action: #selector(self.swipeRecognizer))
        swipe.direction = ([.down])
        self.view.addGestureRecognizer(swipe)
        
   
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.tabBarController?.tabBar.isHidden = true
        self.title = "Manage Meeting"
        let nav = self.navigationController?.navigationBar
        nav?.tintColor = UIColor.white
        nav?.barTintColor = UIColor(hexString: "2c3e50", alpha: 1.0)
        nav?.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        getMeetingData()
        
    }
    var reachability = Reachability()!
    @objc func swipeRecognizer(_ sender: UISwipeGestureRecognizer) {
        if (sender.direction == .down) {
            
            if(self.reachability.connection != .none)
            {
                
                self.getMeetingData()
            }
            else
            {
                self.startLoading(view: self.view)
                DispatchQueue.main.asyncAfter(deadline: .now() + 4.5) {
                    self.noDataLabel(text: "No internet connection found. Check your internet connection and try again.")
                    self.stopLoading(view: self.view)
                }
            }
            
        }
    }
   
    var ReqId = String()
    //  let ActionTakenAlertVC = RACPopupViewController.instantiate()
    @objc func tapViewTapClicked(_ sender: UITapGestureRecognizer) {
        let buttonPosition:CGPoint = sender.view!.convert(.zero, to:self.tableView)
        let indexPath = self.tableView.indexPathForRow(at: buttonPosition)
        if(flagMeeting == "Upcoming"){
            getMeetingDetailsData(meeting_Id: String(self.ManageMeetingDB[(indexPath?.row)!].ID), row: (indexPath?.row)!)
     
        }else if(flagMeeting == "Completed"){
            let storyboard1 = UIStoryboard(name: "MOM", bundle: nil)
            let auditVC = storyboard1.instantiateViewController(withIdentifier: "CompletedMeetingManagerViewController") as! CompletedMeetingManagerViewController
            auditVC.meeting_Id = String(self.ManageMeetingDB[(indexPath?.row)!].ID)
          
            self.navigationController?.pushViewController(auditVC, animated: true)
        }
        
        //
        
    }
    
    var ManageMeetingDetailsDB:[ViewMeetingManagerModel] = []
    var ManageMeetingDetailsAPI = ViewManageMeetingAssignModelDataAPI()
    @objc func getMeetingDetailsData(meeting_Id : String , row : Int){
        var param = [String:String]()
        
        
        param = ["Meeting_ID": meeting_Id ]
        print(param)
        
        
        ManageMeetingDetailsAPI.serviceCalling(obj: self,  parameter: param ) { (dict) in
            
            self.ManageMeetingDetailsDB = dict as! [ViewMeetingManagerModel]
            
            
            let storyboard1 = UIStoryboard(name: "MOM", bundle: nil)
            let auditVC = storyboard1.instantiateViewController(withIdentifier: "MeetingManagerAssignTaskViewController") as! MeetingManagerAssignTaskViewController
            auditVC.meetingDetailsDB = self.ManageMeetingDetailsDB[0]
            auditVC.meetingCellDB = self.ManageMeetingDB[row]
            self.navigationController?.pushViewController(auditVC, animated: true)
            
        }
        
        self.tableView.reloadData()
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    var flagMeeting = String()
    @objc func getMeetingData(){
        var param = [String:String]()
        let pno : String = (UserDefaults.standard.string(forKey: "EmployeeID")!)
        
        param = ["Employee_ID": pno ,"Flag": flagMeeting  ]
        print(param)
        
      
        ManageMeetingAPI.serviceCalling(obj: self,  parameter: param ) { (dict) in
            
            self.ManageMeetingDB = dict as! [ManageMeetingModel]
            
            self.tableView.reloadData()
            
        }
    }
 
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    var data: String?
    var lastObject: String?
    var indPath = IndexPath()
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        
        
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if(section == 0) {
            return 10.0
        } else {
            return 0.0
        }
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.ManageMeetingDB.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! ManageMeetingMOMTableViewCell
            
            let TapGesture = UITapGestureRecognizer(target: self, action: #selector(self.tapViewTapClicked(_:)))
            cell.addGestureRecognizer(TapGesture)
            cell.isUserInteractionEnabled = true
            TapGesture.numberOfTapsRequired = 1
        cell.lblOwner.text = "Owner : " + self.ManageMeetingDB[indexPath.row].Meeting_Owner_Name + " - " + self.ManageMeetingDB[indexPath.row].Meeting_Owner
              cell.lblVenue.text = "Venue : " + self.ManageMeetingDB[indexPath.row].Meeting_Venue
cell.lblDesc.text = self.ManageMeetingDB[indexPath.row].Meeting_Title
            cell.containerView.layer.cornerRadius = 10
            
            let milisecondDate = self.ManageMeetingDB[indexPath.row].Meeting_DateTime
            let dateVarDate = Date.init(timeIntervalSince1970: TimeInterval(milisecondDate)!/1000)
            let dateFormatterday = DateFormatter()
            dateFormatterday.dateFormat = "dd"
            let valDate =  dateFormatterday.string(from: dateVarDate)
        cell.lblDate.text = valDate
            let dateFormattermon = DateFormatter()
            dateFormattermon.dateFormat = "MMMM yy"
            let valDateMon =  dateFormattermon.string(from: dateVarDate)
            cell.lblMonYear.text = valDateMon
            
            let dateFormatterWeekDay = DateFormatter()
            dateFormatterWeekDay.dateFormat = "EEEE"
            let valDateWeekDay =  dateFormatterWeekDay.string(from: dateVarDate)
            cell.lblDay.text = valDateWeekDay
            
        cell.lblTime.text = "Time : " + self.ManageMeetingDB[indexPath.row].Meeting_Time
            
            
            
        
            
            
            
            
            
        
            cell.separatorInset = UIEdgeInsetsMake(0, 10000, 0, 0);
            cell.selectionStyle = .none
            return cell
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableViewAutomaticDimension
        
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}
