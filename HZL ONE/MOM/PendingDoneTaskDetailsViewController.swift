//
//  PendingDoneTaskDetailsViewController.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 09/04/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit

class PendingDoneTaskDetailsViewController: CommonVSClass ,UITableViewDelegate, UITableViewDataSource {
    var typeData = String()
    @IBOutlet weak var tableView: UITableView!
    var refreshControl = UIRefreshControl()
    
    @IBOutlet weak var btnProcess: UIButton!
    @IBOutlet weak var heightProcess: NSLayoutConstraint!
    var status = String()
    var task_Id = String()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.estimatedRowHeight = 65
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.tableFooterView = UIView()
        
        refreshControl.addTarget(self, action: #selector(getMeetingData), for: .valueChanged)
        
        self.tableView.addSubview(refreshControl)
        let swipe = UISwipeGestureRecognizer(target: self, action: #selector(self.swipeRecognizer))
        swipe.direction = ([.down])
        self.view.addGestureRecognizer(swipe)
        if(status == "Pending"){
            btnProcess.isHidden = false
            heightProcess.constant = 45
        }else{
            btnProcess.isHidden = true
            heightProcess.constant = 0
        }
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.tabBarController?.tabBar.isHidden = true
        self.title = "Task Updates"
        let nav = self.navigationController?.navigationBar
        nav?.tintColor = UIColor.white
        nav?.barTintColor = UIColor(hexString: "2c3e50", alpha: 1.0)
        nav?.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        getMeetingData()
    }
    var reachability = Reachability()!
    @objc func swipeRecognizer(_ sender: UISwipeGestureRecognizer) {
        if (sender.direction == .down) {
            
            if(self.reachability.connection != .none)
            {
                
                self.getMeetingData()
            }
            else
            {
                self.startLoading(view: self.view)
                DispatchQueue.main.asyncAfter(deadline: .now() + 4.5) {
                    self.noDataLabel(text: "No internet connection found. Check your internet connection and try again.")
                    self.stopLoading(view: self.view)
                }
            }
            
        }
    }
    
    @IBAction func btnProcessClicked(_ sender: UIButton) {
        let storyboard1 = UIStoryboard(name: "MOM", bundle: nil)
        let auditVC = storyboard1.instantiateViewController(withIdentifier: "PendingDoneTaskProcessViewController") as! PendingDoneTaskProcessViewController
        auditVC.PendingDoneDB = PendingDoneDB
        self.navigationController?.pushViewController(auditVC, animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    var PendingDoneDB = ManageMeetingModel()
    var ManageMeetingDB:[TaskListModel] = []
    var ManageMeetingAPI = TaskListModelDataAPI()
    @objc func getMeetingData(){
        var param = [String:String]()
        
        
        param = ["Task_ID": task_Id   ]
        print(param)
      
        
        ManageMeetingAPI.serviceCalling(obj: self,  parameter: param ) { (dict) in
            
            self.ManageMeetingDB = dict as! [TaskListModel]
            
            self.tableView.reloadData()
            
        }
        
        self.tableView.reloadData()
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    var data: String?
    var lastObject: String?
    var indPath = IndexPath()
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        
        
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if(section == 0 ) {
            return 10.0
        } else {
            return 0.0
        }
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(section == 0){
            return 1
        } else {
        return self.ManageMeetingDB.count
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if(indexPath.section == 0) {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! MyTaskMOMTableViewCell
            
            
            cell.lblOwner.text = "Owner : " + self.PendingDoneDB.Meeting_Owner
            cell.textViewTask.text = "Task : " + self.PendingDoneDB.Task
            cell.textViewDesc.text = self.PendingDoneDB.Meeting_Title
            cell.containerView.layer.cornerRadius = 10
            cell.lblDate.text = "Date : " + self.PendingDoneDB.Meeting_Date
            cell.separatorInset = UIEdgeInsetsMake(0, 10000, 0, 0);
            cell.selectionStyle = .none
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellTask", for: indexPath) as! TaskListTableViewCell
            
            
            cell.lblDate.text = self.ManageMeetingDB[indexPath.row].Created_ON
            cell.textViewTask.text = self.ManageMeetingDB[indexPath.row].TaskUpdate
            cell.lblStatus.text = self.ManageMeetingDB[indexPath.row].Status
           
            cell.separatorInset = UIEdgeInsetsMake(0, 10000, 0, 0);
            cell.selectionStyle = .none
            return cell
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableViewAutomaticDimension
        
    }
  
}
