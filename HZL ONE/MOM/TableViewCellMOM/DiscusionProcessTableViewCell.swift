//
//  DiscusionProcessTableViewCell.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 10/04/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit

class DiscusionProcessTableViewCell: UITableViewCell {

    @IBOutlet weak var viewDiscussion: UIView!
    @IBOutlet weak var textViewDiscussion: UITextView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        viewDiscussion.layer.borderWidth = 1.0
        viewDiscussion.layer.borderColor = UIColor.black.cgColor
        viewDiscussion.layer.cornerRadius = 10.0
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
