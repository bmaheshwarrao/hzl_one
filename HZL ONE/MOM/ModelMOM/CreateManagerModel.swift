//
//  CreateManagerModel.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 09/04/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import Foundation
class EmpListMOMModel: NSObject {
    
    
    
    var Employee_ID = String()
    var Employee_Name = String()
    var Email_ID = String()
    var Mobile_Number = String()
      var isSelected  = Bool()

    
    func setDataInModel(cell:[String:AnyObject])
    {
        
        if cell["Employee_ID"] is NSNull || cell["Employee_ID"] == nil {
            self.Employee_ID = ""
        }else{
            let app = cell["Employee_ID"]
            self.Employee_ID = (app?.description)!
        }
        
        if cell["Employee_Name"] is NSNull || cell["Employee_Name"] == nil {
            self.Employee_Name = ""
        }else{
            let app = cell["Employee_Name"]
            self.Employee_Name = (app?.description)!
        }
        
        if cell["Email_ID"] is NSNull || cell["Email_ID"] == nil {
            self.Email_ID = ""
        }else{
            let app = cell["Email_ID"]
            self.Email_ID = (app?.description)!
        }
        if cell["Mobile_Number"] is NSNull || cell["Mobile_Number"] == nil {
            self.Mobile_Number = ""
        }else{
            let app = cell["Mobile_Number"]
            self.Mobile_Number = (app?.description)!
        }
        self.isSelected = false
        //
        
       
    }
}
class EmpListModelDataAPI
{
    
    var reachablty = Reachability()!
    
    
    func serviceCalling(obj:SearchNameListMOMViewController,parameter:[String:String], success:@escaping (AnyObject)-> Void)
    {
        
        obj.startLoading()
        
        if(reachablty.connection != .none)
        {
            
            WebServices.sharedInstances.sendPostRequest(url: URLConstants.EmployeeDetails_MOM, parameters: parameter, successHandler: { (dict) in
                
                if let response = dict["response"]{
                     print(response)
                    let statusString : String = response["status"] as! String
                    let objectmsg = MessageCallServerModel()
                    
                    let msg = objectmsg.sealizeMessage(cell: response as! [String : AnyObject])
                    
                    if(statusString == "success")
                    {
                        
                        obj.tableView.isHidden = false
                        obj.noDataLabel(text: "" )
                        obj.label.isHidden = true
                        
                        if let data = dict["data"] as? [AnyObject]
                        {
                            
                            var dataArray : [EmpListMOMModel] = []
                            for i in 0..<data.count
                            {
                                
                                if let cell = data[i] as? [String:AnyObject]
                                {
                                    let object = EmpListMOMModel()
                                    object.setDataInModel(cell: cell)
                                    dataArray.append(object)
                                }
                                
                            }
                            
                            
                            success(dataArray as AnyObject)
                            
                            
                        }
                        
                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                    }
                    else
                    {
                        
                        
                        obj.label.isHidden = false
                        obj.tableView.isHidden = true
                        obj.noDataLabel(text: msg )
                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                        
                    }
                    
                    
                }
                
                
                
            }, failureHandler: { (error) in
                let errorStr : String = error.description
                
                print("DATA:error",errorStr)
                
                // obj.errorChecking(error: error)
                obj.stopLoading()
            })
            
            
        }
        else{
            obj.noDataLabel(text:  "No internet connection found. Check your internet connection and try again.")
            
            obj.label.isHidden = false
            obj.tableView.isHidden = true
           // obj.noDataLabel(text: "No Data Found!" )
            obj.refresh.endRefreshing()
            obj.stopLoading()
        }
        
        
    }
    
    
}


class EmpListAssignModelDataAPI
{
    
    var reachablty = Reachability()!
    
    
    func serviceCalling(obj:AssignSearchViewController,parameter:[String:String], success:@escaping (AnyObject)-> Void)
    {
        
        obj.startLoading()
        
        if(reachablty.connection != .none)
        {
            
            WebServices.sharedInstances.sendPostRequest(url: URLConstants.EmployeeDetails_MOM, parameters: parameter, successHandler: { (dict) in
                
                if let response = dict["response"]{
                    print(response)
                    let statusString : String = response["status"] as! String
                    let objectmsg = MessageCallServerModel()
                    
                    let msg = objectmsg.sealizeMessage(cell: response as! [String : AnyObject])
                    if(statusString == "success")
                    {
                        
                        obj.tableView.isHidden = false
                        obj.noDataLabel(text: "" )
                        obj.label.isHidden = true
                        
                        if let data = dict["data"] as? [AnyObject]
                        {
                            
                            var dataArray : [EmpListMOMModel] = []
                            for i in 0..<data.count
                            {
                                
                                if let cell = data[i] as? [String:AnyObject]
                                {
                                    let object = EmpListMOMModel()
                                    object.setDataInModel(cell: cell)
                                    dataArray.append(object)
                                }
                                
                            }
                            
                            
                            success(dataArray as AnyObject)
                            
                            
                        }
                        
                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                    }
                    else
                    {
                        
                        
                        obj.label.isHidden = false
                        obj.tableView.isHidden = true
                        obj.noDataLabel(text: msg )
                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                        
                    }
                    
                    
                }
                
                
                
            }, failureHandler: { (error) in
                let errorStr : String = error.description
                
                print("DATA:error",errorStr)
                
                // obj.errorChecking(error: error)
                obj.stopLoading()
            })
            
            
        }
        else{
            obj.noDataLabel(text:  "No internet connection found. Check your internet connection and try again.")
            
            obj.label.isHidden = false
            obj.tableView.isHidden = true
            // obj.noDataLabel(text: "No Data Found!" )
            obj.refresh.endRefreshing()
            obj.stopLoading()
        }
        
        
    }
    
    
}
