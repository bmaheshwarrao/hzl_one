//
//  ManageMeetingModel.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 08/04/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import Foundation


class ManageMeetingModel: NSObject {
    
    
    
    var ID = String()
    var Meeting_Title = String()
    var Meeting_Agenda = String()
    var Meeting_Venue = String()
    
    var Meeting_DateTime = String()
    var Meeting_Date = String()
    var Meeting_Time = String()
    var Meeting_Duration = String()
    
    
    var Meeting_Status = String()
    var Meeting_ActualStartedOn = String()
    var Meeting_ActualDuration = String()
    var Meeting_Discussion = String()
    var Meeting_Owner = String()
    var Meeting_Owner_Name = String()
    
    var Task = String()
    var Assigned_To = String()
    var Status = String()
    var Task_ID = String()
    var Created_Date = String()
   
   

   
    
    

    
    

    
    func setDataInModel(cell:[String:AnyObject])
    {
        
        if cell["ID"] is NSNull || cell["ID"] == nil {
            self.ID = ""
        }else{
            let app = cell["ID"]
            self.ID = (app?.description)!
        }
        
        if cell["Meeting_Title"] is NSNull || cell["Meeting_Title"] == nil {
            self.Meeting_Title = ""
        }else{
            let app = cell["Meeting_Title"]
            self.Meeting_Title = (app?.description)!
        }
        
        if cell["Meeting_Agenda"] is NSNull || cell["Meeting_Agenda"] == nil {
            self.Meeting_Agenda = ""
        }else{
            let app = cell["Meeting_Agenda"]
            self.Meeting_Agenda = (app?.description)!
        }
        if cell["Meeting_Venue"] is NSNull || cell["Meeting_Venue"] == nil {
            self.Meeting_Venue = ""
        }else{
            let app = cell["Meeting_Venue"]
            self.Meeting_Venue = (app?.description)!
        }
        
        //
        
      
        if cell["Meeting_DateTime"] is NSNull || cell["Meeting_DateTime"] == nil {
            self.Meeting_DateTime = ""
        }else{
            let app = cell["Meeting_DateTime"]
            self.Meeting_DateTime = (app?.description)!
        }
        
        if cell["Meeting_Date"] is NSNull || cell["Meeting_Date"] == nil {
            self.Meeting_Date = ""
        }else{
            let app = cell["Meeting_Date"]
            self.Meeting_Date = (app?.description)!
        }
        
        if cell["Meeting_Time"] is NSNull || cell["Meeting_Time"] == nil {
            self.Meeting_Time = ""
        }else{
            let app = cell["Meeting_Time"]
            self.Meeting_Time = (app?.description)!
        }
        if cell["Meeting_Duration"] is NSNull || cell["Meeting_Duration"] == nil {
            self.Meeting_Duration = ""
        }else{
            let app = cell["Meeting_Duration"]
            self.Meeting_Duration = (app?.description)!
        }
        //
  
        
        if cell["Meeting_Status"] is NSNull || cell["Meeting_Status"] == nil {
            self.Meeting_Status = ""
        }else{
            let app = cell["Meeting_Status"]
            self.Meeting_Status = (app?.description)!
        }
        
        if cell["Meeting_ActualStartedOn"] is NSNull || cell["Meeting_ActualStartedOn"] == nil {
            self.Meeting_ActualStartedOn = ""
        }else{
            let app = cell["Meeting_ActualStartedOn"]
            self.Meeting_ActualStartedOn = (app?.description)!
        }
        
        if cell["Meeting_ActualDuration"] is NSNull || cell["Meeting_ActualDuration"] == nil {
            self.Meeting_ActualDuration = ""
        }else{
            let app = cell["Meeting_ActualDuration"]
            self.Meeting_ActualDuration = (app?.description)!
        }
        if cell["Meeting_Discussion"] is NSNull || cell["Meeting_Discussion"] == nil {
            self.Meeting_Discussion = ""
        }else{
            let app = cell["Meeting_Discussion"]
            self.Meeting_Discussion = (app?.description)!
        }
      
        if cell["Meeting_Owner"] is NSNull || cell["Meeting_Owner"] == nil {
            self.Meeting_Owner = ""
        }else{
            let app = cell["Meeting_Owner"]
            self.Meeting_Owner = (app?.description)!
        }
        if cell["Meeting_Owner_Name"] is NSNull || cell["Meeting_Owner_Name"] == nil {
            self.Meeting_Owner_Name = ""
        }else{
            let app = cell["Meeting_Owner_Name"]
            self.Meeting_Owner_Name = (app?.description)!
        }
        
 
        if cell["Task"] is NSNull || cell["Task"] == nil {
            self.Task = ""
        }else{
            let app = cell["Task"]
            self.Task = (app?.description)!
        }
        if cell["Assigned_To"] is NSNull || cell["Assigned_To"] == nil {
            self.Assigned_To = ""
        }else{
            let app = cell["Assigned_To"]
            self.Assigned_To = (app?.description)!
        }
        if cell["Status"] is NSNull || cell["Status"] == nil {
            self.Status = ""
        }else{
            let app = cell["Status"]
            self.Status = (app?.description)!
        }
        if cell["Task_ID"] is NSNull || cell["Task_ID"] == nil {
            self.Task_ID = ""
        }else{
            let app = cell["Task_ID"]
            self.Task_ID = (app?.description)!
        }
        if cell["Created_Date"] is NSNull || cell["Created_Date"] == nil {
            self.Created_Date = ""
        }else{
            let app = cell["Created_Date"]
            self.Created_Date = (app?.description)!
        }
        
    }
}
class MessageCallServerModel : NSObject {
    var message = String("No Data Found !")
    func sealizeMessage(cell : [String:AnyObject]) -> String {
        
        
        
        let message1 = cell["Msg"]
        let message2 = cell["message"]
        let message3 = cell["msg"]
        
        if message1 != nil {
            message = (message1?.description)!
        }
        if  message2 != nil {
            message = (message2?.description)!
        }
        if  message3 != nil {
            message = (message3?.description)!
        }
        
        return message
    }
}
class MessageCallServerModel1 : NSObject {
  
}

class ManageMeetingModelDataAPI
{
    
    var reachablty = Reachability()!
    
    
    func serviceCalling(obj:ManageMeetingUpCompCancelViewController,parameter:[String:String], success:@escaping (AnyObject)-> Void)
    {
        
        obj.startLoading()
        
        if(reachablty.connection != .none)
        {
            
            WebServices.sharedInstances.sendPostRequest(url: URLConstants.Manage_Meeting_MOM, parameters: parameter, successHandler: { (dict) in
                
                if let response = dict["response"]{
                    
                    
                    
                    
                    
                    
                    let statusString : String = response["status"] as! String
                    let objectmsg = MessageCallServerModel()
                    
                    let msg = objectmsg.sealizeMessage(cell: response as! [String : AnyObject])
                    print(msg)
                    if(statusString == "success")
                    {
                        obj.tableView.isHidden = false
                        obj.noDataLabel(text: "" )
                        obj.label.isHidden = true
                        
                        
                        if let data = dict["data"] as? [AnyObject]
                        {
                            
                            var dataArray : [ManageMeetingModel] = []
                            for i in 0..<data.count
                            {
                                
                                if let cell = data[i] as? [String:AnyObject]
                                {
                                    let object = ManageMeetingModel()
                                    object.setDataInModel(cell: cell)
                                    dataArray.append(object)
                                }
                                
                            }
                       
                            
                            success(dataArray as AnyObject)
                            
                            
                        }
                        
                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                    }
                    else
                    {
                        
                        
                        obj.label.isHidden = false
                        obj.tableView.isHidden = true
                        obj.noDataLabel(text: msg)
                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                        
                    }
                    
                    
                }
                
                
                
            }, failureHandler: { (error) in
                let errorStr : String = error.description
                
                print("DATA:error",errorStr)
                
                // obj.errorChecking(error: error)
                obj.stopLoading()
            })
            
            
        }
        else{
             obj.noDataLabel(text:  "No internet connection found. Check your internet connection and try again.")
            
            obj.label.isHidden = false
            obj.tableView.isHidden = true
           // obj.noDataLabel(text: "No Data Found!" )
            obj.refresh.endRefreshing()
            obj.stopLoading()
        }
        
        
    }
    
    
}

class MyTaskMOMModelDataAPI
{
    
    var reachablty = Reachability()!
    
    
    func serviceCalling(obj:PendingDoneTaskMOMViewController,parameter:[String:String], success:@escaping (AnyObject)-> Void)
    {
        
        obj.startLoading()
        
        if(reachablty.connection != .none)
        {
            
            WebServices.sharedInstances.sendPostRequest(url: URLConstants.My_Task_MOM, parameters: parameter, successHandler: { (dict) in
                
                if let response = dict["response"]{
                    
                    let statusString : String = response["status"] as! String
                    let objectmsg = MessageCallServerModel()
                    
                    let msg = objectmsg.sealizeMessage(cell: response as! [String : AnyObject])
                    print(response)
                    if(statusString == "success")
                    {
                        
                        obj.tableView.isHidden = false
                        obj.noDataLabel(text: "" )
                        obj.label.isHidden = true
                        
                        if let data = dict["data"] as? [AnyObject]
                        {
                            
                            var dataArray : [ManageMeetingModel] = []
                            for i in 0..<data.count
                            {
                                
                                if let cell = data[i] as? [String:AnyObject]
                                {
                                    let object = ManageMeetingModel()
                                    object.setDataInModel(cell: cell)
                                    dataArray.append(object)
                                }
                                
                            }
                            
                            
                            success(dataArray as AnyObject)
                            
                            
                        }
                        
                        obj.refresh.endRefreshing()
                         obj.stopLoading()
                    }
                    else
                    {
                        
                        
                        obj.label.isHidden = false
                        obj.tableView.isHidden = true
                        obj.noDataLabel(text: msg )
                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                        obj.refresh.endRefreshing()
                        
                         obj.stopLoading()
                        
                    }
                    
                    
                }
                
                
                
            }, failureHandler: { (error) in
                let errorStr : String = error.description
                
                print("DATA:error",errorStr)
                
                // obj.errorChecking(error: error)
                 obj.stopLoading()
            })
            
            
        }
        else{
            obj.noDataLabel(text:  "No internet connection found. Check your internet connection and try again.")
            
            obj.label.isHidden = false
            obj.tableView.isHidden = true
          //  obj.noDataLabel(text: "No Data Found!" )
            obj.refresh.endRefreshing()
            obj.stopLoading()
        }
        
        
    }
    
    
}
class MyMeetingMOMModelDataAPI
{
    
    var reachablty = Reachability()!
    
    
    func serviceCalling(obj:UpcomingMissedCompletedMOMViewController,parameter:[String:String], success:@escaping (AnyObject)-> Void)
    {
        
        obj.startLoading()
        
        if(reachablty.connection != .none)
        {
            
            WebServices.sharedInstances.sendPostRequest(url: URLConstants.My_Meeting_MOM, parameters: parameter, successHandler: { (dict) in
                
                if let response = dict["response"]{
                    
                    let statusString : String = response["status"] as! String
                    let objectmsg = MessageCallServerModel()
                    
                    let msg = objectmsg.sealizeMessage(cell: response as! [String : AnyObject])
                    print(msg)
                    if(statusString == "success")
                    {
                        
                        obj.tableView.isHidden = false
                        obj.noDataLabel(text: "" )
                        obj.label.isHidden = true
                        
                        if let data = dict["data"] as? [AnyObject]
                        {
                            
                            var dataArray : [ManageMeetingModel] = []
                            for i in 0..<data.count
                            {
                                
                                if let cell = data[i] as? [String:AnyObject]
                                {
                                    let object = ManageMeetingModel()
                                    object.setDataInModel(cell: cell)
                                    dataArray.append(object)
                                }
                                
                            }
                            
                            
                            success(dataArray as AnyObject)
                            
                            
                        }
                        
                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                    }
                    else
                    {
                        
                        
                        
                        obj.label.isHidden = false
                        obj.tableView.isHidden = true
                        obj.noDataLabel(text: msg )
                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                        
                    }
                    
                    
                }
                
                
                
            }, failureHandler: { (error) in
                let errorStr : String = error.description
                
                print("DATA:error",errorStr)
                
                // obj.errorChecking(error: error)
                obj.stopLoading()
            })
            
            
        }
        else{
             obj.noDataLabel(text:  "No internet connection found. Check your internet connection and try again.")
            
            obj.label.isHidden = false
            obj.tableView.isHidden = true
           // obj.noDataLabel(text: "No Data Found!" )
            obj.refresh.endRefreshing()
            obj.stopLoading()
        }
        
        
    }
    
    
}


