//
//  WellDoneIssueCardViewController.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 21/02/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit

class WellDoneIssueCardViewController: CommonVSClass {
    
    
    
    var MSG = String()
    var Location_ID =  String()
    var Location_Name = String()
    var Is_Location = String()
    
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
 
        // Do any additional setup after loading the view.
    }
    
    @IBAction func btnGoClicked(_ sender: UIButton) {
        
        if(cellData.textEmpId.text != ""){
            getEmpList()
        }else{
            cellData.textViewLSRComment.isHidden = false
            cellData.textViewLSRComment.text = "Please provide Employee Id"
        }
    }
    var reachablty = Reachability()!
    @IBAction func btnNextClicked(_ sender: UIButton) {
        if reachablty.connection == .none{
            
            self.view.makeToast("Internet is not available, please check your internet connection try again.")
            
        }
        else if empIdSearch == "" {
            
            self.view.makeToast("Please provide Employee Id")
        }else if cellData.textViewContribution.text == "" {
            
            self.view.makeToast("Please write Contribution")
        }
        else{
            
            self.startLoadingPK(view: self.view)
            
            
            
            
            
            
            let parameter = [
                "Employee_ID":empIdSearch,
                
                
                "Contribution":cellData.textViewContribution.text! ,
                
                "Manager_EmpID": UserDefaults.standard.string(forKey: "EmployeeID")!
                
                ] as [String:String]
            
        
            print("parameter",parameter)
            
            WebServices.sharedInstances.sendPostRequest(url: URLConstants.Issue_Card_Create, parameters: parameter, successHandler: { (response:[String : AnyObject]) in
                
                let respon = response["response"] as! [String:AnyObject]
                self.stopLoadingPK(view: self.view)
                let objectmsg = MessageCallServerModel()
                let msg = objectmsg.sealizeMessage(cell: respon as! [String : AnyObject])
                if respon["status"] as! String == "success" {
                    
                    self.stopLoadingPK(view: self.view)
                    
                    MoveStruct.isMove = true
                    MoveStruct.message = msg
                    self.navigationController?.popViewController(animated: false)
                    
                    
                }else{
                    self.stopLoadingPK(view: self.view)
                    self.view.makeToast(msg)
                }
                
            }) { (err) in
                self.stopLoadingPK(view: self.view)
                print(err.description)
            }
            
        }
        
        
    
    
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
        
        
        self.tabBarController?.tabBar.isHidden = true
        self.title = "Issue Card"
        let nav = self.navigationController?.navigationBar
        nav?.tintColor = UIColor.white
       nav?.barTintColor = UIColor(hexString: "2c3e50", alpha: 1.0)
        nav?.isTranslucent = false
        nav?.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
    }

    var empDB:[EmployeeListWellDoneModel] = []
    var empIdSearch  = String()
    var empAPI = EmployeelListWellDoneDataAPI()
    var onceDone : Bool = false
    @objc func getEmpList(){
        
        let paramm  : [String:String] = ["Employee_ID":cellData.textEmpId.text!]
        
        self.empAPI.serviceCalling(obj: self , param : paramm,empid : cellData.textEmpId.text!) { (dict) in
            
            self.empDB = [EmployeeListWellDoneModel]()
            self.empDB = dict as! [EmployeeListWellDoneModel]
            if(self.empDB.count > 0){
                if(self.empDB[0].Employee_ID != ""){
                    self.cellData.lblName.text = self.empDB[0].Employee_Name!
                    self.cellData.lblCompany.text = self.empDB[0].Company!
                    self.empIdSearch = self.empDB[0].Employee_ID!
                   self.cellData.lblContribution.isHidden = false
                    self.cellData.textViewContribution.isHidden = false
                    self.cellData.stackView.isHidden = false
                    self.cellData.btnNext.isHidden = false
                }
            }
            
            
            
            
            
        }
    }
    var cellData : WellDoneIssueCardTableViewCell!
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
extension WellDoneIssueCardViewController : UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 608
        
    }
}
extension WellDoneIssueCardViewController : UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! WellDoneIssueCardTableViewCell
        
        cellData = cell
        cell.btnGo.bringSubview(toFront: self.view)
        cell.stackView.isHidden = true
        cell.textViewLSRComment.isHidden = true
        cell.btnNext.isHidden = true
        
        cell.lblContribution.isHidden = true
        cell.textViewContribution.isHidden = true
        
        cell.textViewHeightCon.constant = 0
        cell.stackViewHeightCon.constant = 80
        cell.separatorInset = UIEdgeInsetsMake(0, 10000, 0, 0);
        return cell
        
        
        
    }
}
