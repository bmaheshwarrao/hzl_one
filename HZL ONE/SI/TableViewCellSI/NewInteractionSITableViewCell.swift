//
//  NewInteractionSITableViewCell.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 23/03/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit

class NewInteractionSITableViewCell: UITableViewCell {

    @IBOutlet weak var switcher: UISwitch!
    @IBOutlet weak var btnLocation: UIButton!
    @IBOutlet weak var lblLocation: UILabel!
    @IBOutlet weak var viewLocation: UIView!
    
    @IBOutlet weak var btnCategory: UIButton!
    @IBOutlet weak var lblCategory: UILabel!
    @IBOutlet weak var viewCategory: UIView!
    @IBOutlet weak var btnSubCategory: UIButton!
    @IBOutlet weak var lblSubCategory: UILabel!
    @IBOutlet weak var viewSubCategory: UIView!
    
    @IBOutlet weak var btnSubSubCategory: UIButton!
    @IBOutlet weak var lblSubSubCategory: UILabel!
    @IBOutlet weak var viewSubSubCategory: UIView!
    
    @IBOutlet weak var btnRiskPotential: UIButton!
    @IBOutlet weak var lblRiskPotential: UILabel!
    @IBOutlet weak var viewRiskPotential: UIView!
    
    @IBOutlet weak var btnSituation: UIButton!
    @IBOutlet weak var lblSituation: UILabel!
    @IBOutlet weak var viewSituation: UIView!
    
   
    
    @IBOutlet weak var txtNoPeople: UITextField!
    @IBOutlet weak var viewNoofpeople: UIView!
    
    
    @IBOutlet weak var textViewOnservationn: UITextView!
    

    @IBOutlet weak var textViewAction: UITextView!
    
    @IBOutlet weak var heightViewImage: NSLayoutConstraint!
    
    
    
    @IBOutlet weak var btnGallery: UIButton!
    
    @IBOutlet weak var heightImageselected: NSLayoutConstraint!
    @IBOutlet weak var heightViewofClose: NSLayoutConstraint!
 
    @IBOutlet weak var btnClose: UIButton!
    @IBOutlet weak var imageViewPhoto: UIImageView!
    @IBOutlet weak var btnCamera: UIButton!
    @IBOutlet weak var lblOptional: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        txtNoPeople.keyboardType = UIKeyboardType.numberPad
        viewLocation.layer.borderWidth = 1.0
        viewLocation.layer.borderColor = UIColor.black.cgColor
        viewLocation.layer.cornerRadius = 10.0
        
        
        viewCategory.layer.borderWidth = 1.0
        viewCategory.layer.borderColor = UIColor.black.cgColor
        viewCategory.layer.cornerRadius = 10.0
        viewSubCategory.layer.borderWidth = 1.0
        viewSubCategory.layer.borderColor = UIColor.black.cgColor
        viewSubCategory.layer.cornerRadius = 10.0
        viewSubSubCategory.layer.borderWidth = 1.0
        viewSubSubCategory.layer.borderColor = UIColor.black.cgColor
        viewSubSubCategory.layer.cornerRadius = 10.0
        
        viewRiskPotential.layer.borderWidth = 1.0
        viewRiskPotential.layer.borderColor = UIColor.black.cgColor
        viewRiskPotential.layer.cornerRadius = 10.0
        
        viewSituation.layer.borderWidth = 1.0
        viewSituation.layer.borderColor = UIColor.black.cgColor
        viewSituation.layer.cornerRadius = 10.0
        
        viewNoofpeople.layer.borderWidth = 1.0
        viewNoofpeople.layer.borderColor = UIColor.black.cgColor
        viewNoofpeople.layer.cornerRadius = 10.0
        
        textViewOnservationn.layer.borderWidth = 1.0
        textViewOnservationn.layer.borderColor = UIColor.black.cgColor
        textViewOnservationn.layer.cornerRadius = 10.0
        
        textViewAction.layer.borderWidth = 1.0
        textViewAction.layer.borderColor = UIColor.black.cgColor
        textViewAction.layer.cornerRadius = 10.0
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
