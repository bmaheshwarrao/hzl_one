//
//  SearchPartnerModel.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 30/03/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import Foundation
class SearchSiNameListModel: NSObject {
    
    

    var PartnerTypeId: String?
     var PartnerType: String?
     var PartnerName: String?
     var PartnerCode: String?
    var isSelected: Bool?
    func setDataInModel(cell:[String:AnyObject])
    {
        
        
        if cell["PartnerTypeId"] is NSNull || cell["PartnerTypeId"] == nil {
            self.PartnerTypeId = ""
        }else{
            let app = cell["PartnerTypeId"]
            self.PartnerTypeId = (app?.description)!
        }
        if cell["PartnerType"] is NSNull || cell["PartnerType"] == nil {
            self.PartnerType = ""
        }else{
            let app = cell["PartnerType"]
            self.PartnerType = (app?.description)!
        }
        if cell["PartnerName"] is NSNull || cell["PartnerName"] == nil {
            self.PartnerName = ""
        }else{
            let app = cell["PartnerName"]
            self.PartnerName = (app?.description)!
        }
        if cell["PartnerCode"] is NSNull || cell["PartnerCode"] == nil {
            self.PartnerCode = ""
        }else{
            let app = cell["PartnerCode"]
            self.PartnerCode = (app?.description)!
        }
        self.isSelected = false
        
    }
}


class SearchSINameListApi
{
    var reachablty = Reachability()!
    func serviceCalling(obj:NewPartnerSearchViewController,  param: [String:String] ,success:@escaping (AnyObject)-> Void)
    {
        
        var param1 : [String:String] = [:]
       
        
        if(reachablty.connection != .none)
        {
            
            
            obj.startLoading()
            
            WebServices.sharedInstances.sendPostRequest(url: URLConstants.GetPartnerList, parameters: param, successHandler: { (dict) in
                
                if let response = dict["ReponseCode"]{
                    
                    //statusString  = dict["ResponseMessage"] as! String
                    
                    if(response as! Int == 200)
                    {
                        
                        
                        obj.tableView.isHidden = false
                        obj.noDataLabel(text: "" )
                        obj.label.isHidden = true
                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                        if let data = dict["ResponseValue"] as? [AnyObject]
                        {
                            
                            var dataArray : [SearchSiNameListModel] = []
                            
                            for i in 0..<data.count
                            {
                                
                                if let celll = data[i] as? [String:AnyObject]
                                {
                                    let object = SearchSiNameListModel()
                                    object.setDataInModel(cell: celll)
                                    dataArray.append(object)
                                }
                                
                            }
                            
                            success(dataArray as AnyObject)
                            
                            
                            
                            
                            
                        }
                        
                        
                    }
                    else{
                        obj.label.isHidden = false
                        obj.tableView.isHidden = true
                        obj.noDataLabel(text: "No Data Found!" )
                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                    }
                    
                    
                }
                
                
                
            }, failureHandler: { (error) in
                let errorStr : String = error.description
                
                print("DATA:error",errorStr)
                obj.refresh.endRefreshing()
                obj.stopLoading()
                //  obj.errorChecking(error: error)
                
            })
            
        }
        else{
            
            obj.label.isHidden = false
            obj.tableView.isHidden = true
            obj.noDataLabel(text: "Internet is not available, please check your internet connection try again." )
            obj.stopLoading()
        }
    }
}
