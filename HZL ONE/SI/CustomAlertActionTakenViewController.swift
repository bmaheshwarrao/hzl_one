
//
//  CustomAlertActionTakenViewController.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 26/03/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit

class CustomAlertActionTakenViewController: UIViewController {
var actionTaken = String()
    @IBOutlet weak var textViewActionTaken: UITextView!
    @IBOutlet weak var lblActionTaken: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        textMsgAction = ""
        lblActionTaken.text = "Action Taken " + actionTaken
        textViewActionTaken.layer.borderWidth = 1.0
        textViewActionTaken.layer.borderColor = UIColor.black.cgColor
        textViewActionTaken.layer.cornerRadius = 10.0
        // Do any additional setup after loading the view.
    }

    @IBAction func btnProcessClicked(_ sender: UIButton) {
        if(textViewActionTaken.text == ""){
            self.view.makeToast("Enter Remark")
        }else{
            textMsgAction = textViewActionTaken.text!
        NotificationCenter.default.post(name:NSNotification.Name.init("ProcessActionTaken"), object: nil)
        dismiss(animated: true, completion: nil)
        }
    }
    
    @IBAction func btnCancelClicked(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
