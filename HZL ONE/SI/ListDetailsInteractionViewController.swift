//
//  ListDetailsInteractionViewController.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 25/03/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit
import EzPopup
class ListDetailsInteractionViewController: CommonVSClass ,UITableViewDelegate, UITableViewDataSource{
    
    @IBOutlet weak var tableView: UITableView!
    var InteractionDetailCode  = String()
    var RequestorCode = String()
    var refreshControl = UIRefreshControl()
    
    var InteractionDB:[InteractionDetailsListReportedDataModel] = []
    var InteractionAPI = IntercationMyDetailsListApi()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        getInteractionData()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.estimatedRowHeight = 65
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.tableFooterView = UIView()
        
        refreshControl.addTarget(self, action: #selector(getInteractionData), for: .valueChanged)
        
        self.tableView.addSubview(refreshControl)
        let swipe = UISwipeGestureRecognizer(target: self, action: #selector(self.swipeRecognizer))
        swipe.direction = ([.down])
        self.view.addGestureRecognizer(swipe)
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.ProcessActionTakenData), name: NSNotification.Name?.init(NSNotification.Name(rawValue: "ProcessActionTaken")), object: nil)
        
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.tabBarController?.tabBar.isHidden = true
        self.title = "Interaction Detail"
        let nav = self.navigationController?.navigationBar
        nav?.tintColor = UIColor.white
        nav?.barTintColor = UIColor(hexString: "2c3e50", alpha: 1.0)
        nav?.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        
    }
   
     @objc func ProcessActionTakenData(){
        
        self.startLoadingPK(view: self.view)
        
        
         let pno : String = (UserDefaults.standard.string(forKey: "EmployeeID")!)
        var name = String()
        
        let fname = UserDefaults.standard.string(forKey: "FirstName")
        let Mname = UserDefaults.standard.string(forKey: "MiddleName")
        let Lname = UserDefaults.standard.string(forKey: "Lastname")
      
        
        if(Mname == ""){
            name = fname! + " " + Lname!
        }else{
            name = fname! + " " + Mname!  + " " + Lname!
        }
        
        
        let parameter = [
            "Action": action,
            "CreatedOn": "",
            "EmployeeCode": pno,
            "EmployeeName": name,
            "FWDCCEmployeeCode": "",
            "InteractionDetailCode": InteractionDetailCode,
            "ModifiedOn": "",
            "ModuleId": "1",
            "Remark": textMsgAction,
            "UTC": ""
            
            ] as [String:String]
        
        
        
        print("parameter",parameter)
        
        WebServices.sharedInstances.sendPostRequest(url: URLConstants.PostApproverAction, parameters: parameter, successHandler: { (response:[String : AnyObject]) in
            
            self.stopLoadingPK(view: self.view)
            
            if let response = response["ReponseCode"]{
                if(response as! Int == 200)
                {
                    // let message = response["ResponseMessage"] as! String
                    let message = "Interaction \(self.action) Successfully"
                    self.stopLoadingPK(view: self.view)
                    
                    MoveStruct.isMove = true
                    MoveStruct.message = message
                    self.navigationController?.popViewController(animated: false)
                    
                }
                else{
                    self.stopLoadingPK(view: self.view)
                    self.view.makeToast("Please try again, something went wrong.")
                }
            }else{
                self.stopLoadingPK(view: self.view)
                self.view.makeToast("Please try again, something went wrong.")
            }
            
        }) { (err) in
            self.stopLoadingPK(view: self.view)
            print(err.description)
        }
        
    }
    var action = String()
    @IBAction func btnAcceptClicked(_ sender: UIButton) {
        action = "Approve"
        let storyBoard = UIStoryboard(name: "SI", bundle: nil)
        let customAlertVC = storyBoard.instantiateViewController(withIdentifier: "CustomAlertActionTakenViewController") as! CustomAlertActionTakenViewController
        customAlertVC.actionTaken = action
        
        let popupVC = PopupViewController(contentController: customAlertVC,  popupWidth: self.view.frame.width - 50, popupHeight: 240)
        
        popupVC.cornerRadius = 5
        popupVC.canTapOutsideToDismiss = true
        present(popupVC, animated: true, completion: nil)
    }
    @IBAction func btnForwardClicked(_ sender: UIButton) {
        action = "Forward"
        let storyBoard = UIStoryboard(name: "SI", bundle: nil)
        let customAlertVC = storyBoard.instantiateViewController(withIdentifier: "CustomAlertActionTakenViewController") as! CustomAlertActionTakenViewController
        customAlertVC.actionTaken = action
        
        let popupVC = PopupViewController(contentController: customAlertVC,  popupWidth: self.view.frame.width - 50, popupHeight: 240)
        
        popupVC.cornerRadius = 5
        popupVC.canTapOutsideToDismiss = true
        present(popupVC, animated: true, completion: nil)
    }
    @IBAction func btnRejectClicked(_ sender: UIButton) {
        action = "Reject"
        let storyBoard = UIStoryboard(name: "SI", bundle: nil)
        let customAlertVC = storyBoard.instantiateViewController(withIdentifier: "CustomAlertActionTakenViewController") as! CustomAlertActionTakenViewController
        customAlertVC.actionTaken = action
        
        let popupVC = PopupViewController(contentController: customAlertVC,  popupWidth: self.view.frame.width - 50, popupHeight: 240)
        
        popupVC.cornerRadius = 5
        popupVC.canTapOutsideToDismiss = true
        present(popupVC, animated: true, completion: nil)
    }
    var reachability = Reachability()!
    @objc func swipeRecognizer(_ sender: UISwipeGestureRecognizer) {
        if (sender.direction == .down) {
            
            if(self.reachability.connection != .none)
            {
                
                self.getInteractionData()
            }
            else
            {
                self.startLoading(view: self.view)
                DispatchQueue.main.asyncAfter(deadline: .now() + 4.5) {
                    self.noDataLabel(text: "No internet connection found. Check your internet connection and try again.")
                    self.stopLoading(view: self.view)
                }
            }
            
        }
    }
    @IBAction func tapViewClicked(_ sender: Any) {
        
        
        
        
        
        
        
    }
    @objc func tapViewTapClicked(_ sender: UITapGestureRecognizer) {
      
        
        
    }
    @IBOutlet var tapView: UITapGestureRecognizer!
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    var data: String?
    var lastObject: String?
    var indPath = IndexPath()
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(self.InteractionDB.count == 0){
            return 0
        }else{
            return 1
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        

            
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! ListDetailsInteractionTableViewCell
            
            
            cell.lblCorrectiveAction.text = self.InteractionDB[indexPath.row].CorrectiveAction!
            

        cell.lblObservation.text = self.InteractionDB[indexPath.row].ObservationDetails!
        cell.lblSituation.text = self.InteractionDB[indexPath.row].RiskSituation!
        cell.lblRiskPotential.text = self.InteractionDB[indexPath.row].RiskPotential!
        cell.lblSubCategory.text = self.InteractionDB[indexPath.row].SubCategory!
        cell.lblCategory.text = self.InteractionDB[indexPath.row].Category!
        cell.lblInteractionCodeDetails.text = self.InteractionDB[indexPath.row].InteractionDetailCode!
        cell.lblIntercationCode.text = self.InteractionDB[indexPath.row].InteractionCode!
          cell.lblStatus.text = self.InteractionDB[indexPath.row].Status!
            
            let dateFormatter = DateFormatter()
            dateFormatter.timeZone = NSTimeZone.system
            dateFormatter.dateFormat = "yyyy-MM-dd'T'hh:mm:ss"
            let date = dateFormatter.date(from: self.InteractionDB[indexPath.row].TargetDate!)
            
            let dateFormatter2 = DateFormatter()
            dateFormatter2.timeZone = NSTimeZone.system
            dateFormatter2.dateFormat = "dd MMM yyyy"
            
            switch date {
            case nil:
                let date_TimeStr = dateFormatter2.string(from: Date())
                cell.lblRegisterDate.text = date_TimeStr
                break;
            default:
                let date_TimeStr = dateFormatter2.string(from: date!)
                
                cell.lblRegisterDate.text = date_TimeStr
                break;
            }
            
            
            
            
            
            cell.separatorInset = UIEdgeInsetsMake(0, 10000, 0, 0);
            cell.selectionStyle = .none
            return cell
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 500
        
    }
    
    @objc func getInteractionData(){
        var param = [String:String]()
        
        param = ["InteractionDetailCode": InteractionDetailCode ,"RequestorCode": RequestorCode   ]
        
        
        
        
        InteractionAPI.serviceCalling(obj: self,  param: param ) { (dict) in
            
            self.InteractionDB = dict as! [InteractionDetailsListReportedDataModel]
            
            self.tableView.reloadData()
            
        }
    }
    
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
