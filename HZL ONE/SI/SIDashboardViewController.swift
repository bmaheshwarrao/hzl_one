//
//  SIDashboardViewController.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 23/03/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//


import UIKit
import Charts


import MobileCoreServices
import Alamofire
import CoreData
import CRToast
import IQKeyboardManagerSwift
import AVFoundation

class SIDashboardViewController: CommonVSClass,UINavigationBarDelegate,UINavigationControllerDelegate,UITableViewDelegate, UITableViewDataSource,ChartViewDelegate{
    
    
    
    @IBOutlet weak var homeTableView: UITableView!
    
    
    var applicationName = String()
    
    
    var statusStrNav = String()
    
    
    var indPath = IndexPath()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        deleteSearchNameData()
        let useCodeData : String = UserDefaults.standard.string(forKey: "UserCode")!
        StrResponseValue = "0"
        if(useCodeData == "" || useCodeData == nil){
            getUserSessionData()
        }else{
            UserDefaults.standard.set("", forKey: "ResponseValue")
            self.getTransactionData(strUser: useCodeData)
        }
        self.view.frame.size.height = self.homeTableView.frame.size.height
        
        self.homeTableView.delegate = self
        self.homeTableView.dataSource = self
        
        self.homeTableView.sectionIndexBackgroundColor = UIColor.clear
        self.homeTableView.sectionIndexColor = UIColor.lightGray
        
        self.homeTableView.sectionIndexColor = UIColor.blue
        
       
  refresh()
        
        
        getBannerImageData()
        if reachability.connection == .none{
            self.view.makeToast("Internet is not available, please check your internet connection try again." )
        }
        
        
    }
    
    func refresh(){
        FilterDataFromServer.unit_Id = Int()
        FilterDataFromServer.unit_Name = String()
        FilterDataFromServer.area_Id = Int()
        FilterDataFromServer.area_Name = String()
        FilterDataFromServer.sub_Area_Id = Int()
        FilterDataFromServer.sub_Area_Name = String()
        FilterDataFromServer.location_name = String()
        FilterDataFromServer.location_id = Int()
        searchNameArrayDB = []
        dataArrayInteraction = []
    }
    var reachability = Reachability()!
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        if(MoveStruct.isMove == true){
            MoveStruct.isMove = false
            self.view.makeToast(MoveStruct.message)
        }
        refresh()
        // NotificationCenter.default.post(name: NSNotification.Name.init("sendReportFromHome"), object: nil)
        self.title = statusStrNav
        self.tabBarController?.tabBar.isHidden = true
        let nav = self.navigationController?.navigationBar
        nav?.tintColor = UIColor.white
        nav?.barTintColor = UIColor(hexString: "2c3e50", alpha: 1.0)
        nav?.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        
        
        
    }
    var UserCodeDB:[SiAppersessionListModel] = []
    var UserCodeAPI = SiAppersessionListDataAPI()
    var ResponseValue = String()
     var StrResponseValue = String()
    @objc func getUserSessionData(){
        var param = [String:String]()
        let pno : String = (UserDefaults.standard.string(forKey: "EmployeeID")!)
      
        param = ["EmployeeCode": pno   ]
        print(param)
        UserCodeAPI.serviceCalling(obj: self , param: param ) { (dict) in
            
            self.UserCodeDB = dict as! [SiAppersessionListModel]
            
            UserDefaults.standard.set(self.UserCodeDB[0].UserSessionCode, forKey: "UserCode")
            self.getTransactionData(strUser: self.UserCodeDB[0].UserSessionCode!)
        }
    }
    var UserTransactionDB:[UserTransactionListModel] = []
    var UserTransactionAPI = UserTransactionListDataAPI()
    @objc func getTransactionData(strUser : String){
        var param = [String:String]()
        let pno : String = (UserDefaults.standard.string(forKey: "EmployeeID")!)
       
        param = ["ProcessID":"1","UserSessionCode":strUser ]
        print(param)
        UserTransactionAPI.serviceCalling(obj: self , param: param ) { (dict) in
            
            self.UserTransactionDB = dict as! [UserTransactionListModel]
            
      
            
        }
    }
    @objc func getBannerImageData() {
        
        var imageDB = [BannerImage]()
        do {
            
            imageDB = try context.fetch(BannerImage.fetchRequest())
            //self.refresh.endRefreshing()
            if(imageDB.count > 0) {
                //BannerStruct.bannerPath = imageDB[0].text_Message!
            }
            self.homeTableView.reloadData()
            
        } catch {
            print("Fetching Failed")
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == 0{
            return 1
        }else{
            return 1
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0 {
            let page = "page"
            
            
            let cell = tableView.dequeueReusableCell(withIdentifier: page) as! PageIdeaTableViewCell
            
            cell.contentView.frame.size.height = cell.imageCollectionView.frame.size.height
            cell.contentView.frame.size.width = cell.imageCollectionView.frame.size.width
            
            
            cell.pageView(ApplicationName: applicationName)
            
            return cell
            
        }else if indexPath.section == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell1") as! UITableViewCell
            
            
            return cell
        }else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell2") as! UITableViewCell
            
            
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        
        
        let screenSize = UIScreen.main.bounds
        //let screenHeight = screenSize.height-navheight
        
        let screenWidth = screenSize.width
        if indexPath.section == 0{
            return (screenWidth/2) + 25
        }else{
            return 123
        }
        
    }
    
    func deleteSearchNameData()
    {
        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        
        let req = NSFetchRequest<NSFetchRequestResult>.init(entityName: "SearchName")
        let batchDeleteRequest = NSBatchDeleteRequest(fetchRequest: req)
        
        do {
            try context.execute(batchDeleteRequest)
            
        } catch {
            // Error Handling
        }
    }
    @IBAction func NewInteractionClicked(_ sender: UIButton) {
        let useCodeData : String = UserDefaults.standard.string(forKey: "UserCode")!
        if(useCodeData == "" || useCodeData == nil){
            getUserSessionData()
        }else{
            if(ResponseValue == ""){
            StrResponseValue = "0"
            self.getTransactionData(strUser: useCodeData)
            }else{
                let storyboard1 = UIStoryboard(name: "SI", bundle: nil)
                let auditVC = storyboard1.instantiateViewController(withIdentifier: "NewInteractionMainViewController") as! NewInteractionMainViewController
                auditVC.ResponseValue = ResponseValue
                self.navigationController?.pushViewController(auditVC, animated: true)
            }
        }
       
        
//        let ResponseValue : String = UserDefaults.standard.string(forKey: "ResponseValue")!
//        if(ResponseValue != "" && ResponseValue != nil){
        
//        }else{
//            
//        }
    }
    @IBAction func ViewInteractionClicked(_ sender: UIButton) {
        let storyboard1 = UIStoryboard(name: "SI", bundle: nil)
        let auditVC = storyboard1.instantiateViewController(withIdentifier: "PendingInteractionViewController") as! PendingInteractionViewController
        
        self.navigationController?.pushViewController(auditVC, animated: true)
    }
    
    
    
    
    var reachablty = Reachability()!
    
    
    func getContext () -> NSManagedObjectContext {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        return appDelegate.persistentContainer.viewContext
    }
    
    
    
    
    
    
}
