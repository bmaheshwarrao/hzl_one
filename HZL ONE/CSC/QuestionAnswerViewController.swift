//
//  QuestionAnswerViewController.swift
//  HZL CSC
//
//  Created by Bunga Maheshwar Rao on 28/10/18.
//  Copyright © 2018 SARVANG INFOTCH. All rights reserved.
//

import UIKit
import DLRadioButton
import CoreData
struct QuestionsData {
   static var text1 = String()
   static var text2 = String()
  static  var text3 = String()
   static var text4 = String()
   static var text5 = String()
   static var text6 = String()
   static var popVal = String()
   static var Rate1 = String()
  static  var Rate2 = String()
   static var Rate3 = String()
   static var conditionGAB1 = String()
   static var conditionGAB2 = String()
  static  var conditionGAB3 = String()
   static var conditionGAB4 = String()
   static var conditionYN1 = String()
   static var conditionYN2 = String()
   static var conditionYN3 = String()
   static var conditionYN4 = String()

}

class QuestionAnswerViewController: CommonVSClass {

    @IBOutlet weak var tableView: UITableView!
    var QuestionArray : [String] = ["","No of safe acts identified & appreciated ?" ,"Level of housekeeping observed ?","How will you rate structural & equipment conditions in terms of cleanliness, painting etc ?","No of Unsafe Act Identified ?","No of Unsafe conditions Identified ?","Safety issues identified with potential fatalities ?","Safety issues with potential with serious injuries ?","How will you rate overall awareness of people at site on various standard ?","Whether people were aware of recent incidents happened in HZL ?","Whether updated passport was available with them ?","Do you feel AECT has made its impact to workers safe behaviour ?","Improvement observed with respect to last visit ?"," No of LSR Violation observed ?","Ramp condition ?","Ventilation ?","GCPM Compliance ?","Competency of MM / OPERATORS ?","Major non compliance were observed related to which standard?",""]
   
    var ObserverDataApiCall = ObserverDataApi()
    var ObsrvervedActsDB:[Observed] = []
  //  let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self;
        tableView.dataSource = self;
        tableView.estimatedRowHeight = 290
        tableView.rowHeight = UITableViewAutomaticDimension
        

        // Do any additional setup after loading the view.
        callObserveredData()
        
        
    }
    override func viewWillAppear(_ animated: Bool) {
        if(MoveStruct.isMove == true){
            self.navigationController?.popViewController(animated: false)
        }
        moveLight = 1
        self.tabBarController?.tabBar.isHidden = true
        self.title = "CSC"
        let nav = self.navigationController?.navigationBar
        nav?.tintColor = UIColor.white
       nav?.barTintColor = UIColor(hexString: "2c3e50", alpha: 1.0)
        nav?.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
    }
 
    @IBAction func btnGABDLRadioButtonClickedd(_ sender: DLRadioButton) {
        
        if(sender.tag == 14){
            for button in sender.selectedButtons() {
                // button.iconColor = UIColor.orange
                QuestionsData.conditionGAB1 =   button.titleLabel!.text!
            }
        }else if(sender.tag == 15){
            for button in sender.selectedButtons() {
                // button.iconColor = UIColor.orange
                QuestionsData.conditionGAB2 =   button.titleLabel!.text!
            }
        }else if(sender.tag == 16){
            for button in sender.selectedButtons() {
                // button.iconColor = UIColor.orange
                QuestionsData.conditionGAB3 =   button.titleLabel!.text!
            }
        }
            
            
        else{
            for button in sender.selectedButtons() {
                // button.iconColor = UIColor.orange
                QuestionsData.conditionGAB4 =   button.titleLabel!.text!
            }
        }
        
        
    }
    
    
    var dataYesNo9 = String()
    var dataYesNo10 = String()
     var dataYesNo11 = String()
     var dataYesNo12 = String()
    @IBAction func btnDLRadioButtonClickedd(_ sender: DLRadioButton) {
        
        if(sender.tag == 9){
            for button in sender.selectedButtons() {
               // button.iconColor = UIColor.orange
              QuestionsData.conditionYN1 =   button.titleLabel!.text!
            }
        }else if(sender.tag == 10){
            for button in sender.selectedButtons() {
               // button.iconColor = UIColor.orange
                QuestionsData.conditionYN2 =   button.titleLabel!.text!
            }
        }else if(sender.tag == 11){
            for button in sender.selectedButtons() {
                // button.iconColor = UIColor.orange
                QuestionsData.conditionYN3 =   button.titleLabel!.text!
            }
        }
        
        
        else{
            for button in sender.selectedButtons() {
                // button.iconColor = UIColor.orange
                QuestionsData.conditionYN4 =   button.titleLabel!.text!
            }
        }
        
        
    }
    
    
    
    func callObserveredData() {
        
        self.ObserverDataApiCall.serviceCalling(obj: self)
        { (dict) in
            self.ObsrvervedActsDB = dict as! [Observed]
            
            
          
        }
    }
    
    @objc func getObserveredData() {
        
//        self.ObsrvervedActsDB = [ObsrvervedActs]()
//        do {
//
//            self.ObsrvervedActsDB = try context.fetch(ObsrvervedActs.fetchRequest())
//
//
//
//        } catch {
//            print("Fetching Failed")
//        }
    }
    
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnNextClicked(_ sender: UIButton) {
        if( QuestionsData.text1 == "") {
            self.view.makeToast( "Please Enter Question no 1")
            // self.showSingleButtonWithMessage(title: "Question 1", message: "Please Enter Question no 1", buttonName: "OK")
        } else if( QuestionsData.Rate1 == "") {
            self.view.makeToast( "Please Rate Question no 2")
            // self.showSingleButtonWithMessage(title: "Question 2", message: "Please Rate Question no 2", buttonName: "OK")
        }
        else if( QuestionsData.Rate2 == "") {
            self.view.makeToast( "Please Rate Question no 3")
            // self.showSingleButtonWithMessage(title: "Question 3", message: "Please Rate Question no 3", buttonName: "OK")
        }else if( QuestionsData.text2 == "") {
            self.view.makeToast( "Please Enter Question no 4")
            // self.showSingleButtonWithMessage(title: "Question 4", message: "Please Enter Question no 4", buttonName: "OK")
        } else if( QuestionsData.text3 == "") {
            self.view.makeToast( "Please Enter Question no 5")
            // self.showSingleButtonWithMessage(title: "Question 5", message: "Please Enter Question no 5", buttonName: "OK")
        }  else if( QuestionsData.text4 == "") {
            self.view.makeToast( "Please Enter Question no 6")
            //  self.showSingleButtonWithMessage(title: "Question 6", message: "Please Enter Question no 6", buttonName: "OK")
        } else if( QuestionsData.text5 == "") {
            self.view.makeToast( "Please Enter Question no 7")
            // self.showSingleButtonWithMessage(title: "Question 7", message: "Please Enter Question no 7", buttonName: "OK")
        }  else if( QuestionsData.Rate3 == "") {
            self.view.makeToast( "Please Rate Question no 8")
            //self.showSingleButtonWithMessage(title: "Question 8", message: "Please Rate Question no 8", buttonName: "OK")
        } else if( QuestionsData.conditionYN1 == "") {
            self.view.makeToast( "Please Select Question no 9")
            //  self.showSingleButtonWithMessage(title: "Question 9", message: "Please Select Question no 9", buttonName: "OK")
        } else if( QuestionsData.conditionYN2 == "") {
            self.view.makeToast( "Please Select Question no 10")
            // self.showSingleButtonWithMessage(title: "Question 10", message: "Please Select Question no 10", buttonName: "OK")
        } else if( QuestionsData.conditionYN3 == "") {
            self.view.makeToast( "Please Select Question no 11")
            // self.showSingleButtonWithMessage(title: "Question 11", message: "Please Select Question no 11", buttonName: "OK")
        } else if( QuestionsData.conditionYN4 == "") {
            self.view.makeToast( "Please Select Question no 12")
            //self.showSingleButtonWithMessage(title: "Question 12", message: "Please Rate Question no 12", buttonName: "OK")
        } else if( QuestionsData.text6 == "") {
            self.view.makeToast( "Please Select Question no 13")
            //            self.showSingleButtonWithMessage(title: "Question 13", message: "Please Enter Question no 13", buttonName: "OK")
        } else if( QuestionsData.conditionGAB1 == "") {
            self.view.makeToast( "Please Select Question no 14")
            // self.showSingleButtonWithMessage(title: "Question 14", message: "Please Select Question no 14", buttonName: "OK")
        }
        else if( QuestionsData.conditionGAB2 == "") {
            self.view.makeToast( "Please Select Question no 15")
            // self.showSingleButtonWithMessage(title: "Question 15", message: "Please Select Question no 15", buttonName: "OK")
        } else if( QuestionsData.conditionGAB3 == "") {
            self.view.makeToast( "Please Select Question no 16")
            //self.showSingleButtonWithMessage(title: "Question 16", message: "Please Select Question no 16", buttonName: "OK")
        } else if( QuestionsData.conditionGAB4 == "") {
            self.view.makeToast( "Please Select Question no 17")
            // self.showSingleButtonWithMessage(title: "Question 17", message: "Please Select Question no 17", buttonName: "OK")
        } else if( QuestionsData.popVal == "") {
            self.view.makeToast( "Please Select Question no 18")
            // self.view.makeToast( "Please Select Question no 18")
        } else {
            
            
            FilterDataFromServer.unit_Id = Int()
            FilterDataFromServer.unit_Name = String()
            FilterDataFromServer.area_Id = Int()
            FilterDataFromServer.area_Name = String()
            FilterDataFromServer.sub_Area_Id = Int()
            FilterDataFromServer.sub_Area_Name = String()
            
            let storyBoard : UIStoryboard = UIStoryboard(name: "CSC",bundle : nil)
            let questionVC = storyBoard.instantiateViewController(withIdentifier: "SubmitDataCSCViewController") as! SubmitDataCSCViewController

            self.navigationController?.pushViewController(questionVC, animated: true)
            
            
            
            
//            let storyBoard : UIStoryboard = UIStoryboard(name: "CSC",bundle : nil)
//            let questionVC = storyBoard.instantiateViewController(withIdentifier: "SubmitDataViewController") as! SubmitDataViewController
//            self.navigationController?.pushViewController(questionVC, animated: true)
//            let navigationVC = UINavigationController(rootViewController: questionVC)
//            navigationVC.navigationBar.barTintColor = UIColor(red: 33/255, green: 48/255, blue: 85/255, alpha: 1.0)
//            navigationVC.navigationBar.isTranslucent = false;
//
//            self.present(navigationVC, animated: true, completion: nil)
            
        }
    }
    @objc func tapPopUp(_ sender: UITapGestureRecognizer) {
        
        let buttonPosition:CGPoint = sender.view!.convert(.zero, to:self.tableView)
        let indexPath = self.tableView.indexPathForRow(at: buttonPosition)
        let cell = tableView.cellForRow(at: indexPath!) as! TextFieldSelectTableViewCell
        
        var nameArray : [String] = []
        if(ObsrvervedActsDB.count == 0){
            callObserveredData()
        }
        if(ObsrvervedActsDB.count > 0){
            for i in 0..<ObsrvervedActsDB.count
            {
                nameArray.append(ObsrvervedActsDB[i].name!)
            }
        }
        print(ObsrvervedActsDB.count)
        DispatchQueue.main.async(execute: {() -> Void in
            let popup = PopUpTableViewController()
            
            popup.itemsArray = nameArray
            
            popup.sourceView = cell.textFieldAnswer
            popup.isScroll = true
            popup.backgroundColor = UIColor.white
            if popup.itemsArray.count > 5{
                popup.popUpHeight = 200
            }
            else{
                popup.popUpHeight = CGFloat(popup.itemsArray.count)*45
            }
            popup.popUpWidth = UIScreen.main.bounds.size.width-60
            popup.backgroundImage = nil
            popup.itemTitleColor = UIColor.white
            popup.itemSelectionColor = UIColor.lightGray
            popup.arrowDirections = .any
            popup.arrowColor = UIColor.white
            popup.popCellBlock = {(_ popupVC: PopUpTableViewController, _ popupCell: UITableViewCell, _ row: Int, _ section: Int) -> Void in
                
                
                cell.textFieldAnswer.text = nameArray[row]
                QuestionsData.popVal = nameArray[row]
                print(QuestionsData.popVal)
//                self.lblHazard.text = self.HazardTypeDB[row].hazard_Name
//                self.HazardIDVal = self.HazardTypeDB[row].hazardUniqueId!
//                if(self.lblHazard.text == "Select Hazard Type") {
//                    self.lblHazard.textColor = UIColor.lightGray
//                } else {
//                    self.lblHazard.textColor = UIColor.black
//                }
                
                popupVC.dismiss(animated: false, completion: nil)
                
            }
            self.present(popup, animated: true, completion: {() -> Void in
            })
            
            
        })
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
extension QuestionAnswerViewController : UITableViewDelegate {
func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return UITableViewAutomaticDimension
}
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if(section == 0) {
            return 0.0
        } else {
            return 20.0
        }
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return QuestionArray.count
    }
}
extension QuestionAnswerViewController : UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        
            return 1
        
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if(indexPath.section == 0) {
        let cell = tableView.dequeueReusableCell(withIdentifier: "topView", for: indexPath) as! TopTableViewCell
        var area = UserDefaults.standard.string(forKey: "visitArea")
        if(area == nil) {
            area = ""
        }
        cell.lblVisitArea.text =  area
        
        return cell
        } else if(indexPath.section == 1 || indexPath.section == 4 || indexPath.section == 5 || indexPath.section == 6 || indexPath.section == 7  ) {
        
            let cell = tableView.dequeueReusableCell(withIdentifier: "text", for: indexPath) as! TextFieldTableViewCell
            cell.textViewQuestions.tag = indexPath.section
            cell.lblNumber.text = String(indexPath.section) + ")"
            let agreeAttributedString = NSMutableAttributedString(string: QuestionArray[indexPath.section], attributes: [NSAttributedStringKey.foregroundColor: UIColor.black, NSAttributedStringKey.font: UIFont.systemFont(ofSize: 15.0, weight: .medium)])
            cell.textViewQuestions.attributedText = agreeAttributedString
           
            if(indexPath.section == 1) {
               
                    cell.textFieldAnswer.text = QuestionsData.text1
                
            }
            if(indexPath.section == 4) {
               
                    cell.textFieldAnswer.text = QuestionsData.text2
                
            }
            if(indexPath.section == 5) {
                
                    cell.textFieldAnswer.text = QuestionsData.text3
                
            }
            if(indexPath.section == 6) {
                
                    cell.textFieldAnswer.text = QuestionsData.text4
                
            }
            if(indexPath.section == 7) {
                
                    cell.textFieldAnswer.text = QuestionsData.text5
                
            }
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            return cell
        }else if( indexPath.section == 2 || indexPath.section == 8 ) {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "rate", for: indexPath) as! RateTableViewCell
            print(String(indexPath.section) + ")")
            cell.lblNumber.text = String(indexPath.section) + ")"
             cell.textViewQuestion.tag = indexPath.section
            let agreeAttributedString = NSMutableAttributedString(string: QuestionArray[indexPath.section], attributes: [NSAttributedStringKey.foregroundColor: UIColor.black, NSAttributedStringKey.font: UIFont.systemFont(ofSize: 15.0, weight: .medium)])
            cell.textViewQuestion.attributedText = agreeAttributedString
            return cell;
        }else if(indexPath.section == 3  ) {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "rate", for: indexPath) as! RateTableViewCell
            
            cell.lblNumber.text = "3)"
            cell.textViewQuestion.tag = indexPath.section
            let agreeAttributedString = NSMutableAttributedString(string: QuestionArray[indexPath.section], attributes: [NSAttributedStringKey.foregroundColor: UIColor.black, NSAttributedStringKey.font: UIFont.systemFont(ofSize: 15.0, weight: .medium)])
            cell.textViewQuestion.attributedText = agreeAttributedString
            return cell;
        }
        
        
        else if( indexPath.section == 13 ) {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "textres", for: indexPath) as! TextFieldResTableViewCell
            cell.textViewQuestions.tag = indexPath.section
            cell.lblNumber.text = String(indexPath.section) + ")"
            if(indexPath.section == 13) {
            if(QuestionsData.text6 != ""){
                cell.textFieldAnswer.text = QuestionsData.text6
            }
            }
            let agreeAttributedString = NSMutableAttributedString(string: QuestionArray[indexPath.section], attributes: [NSAttributedStringKey.foregroundColor: UIColor.black, NSAttributedStringKey.font: UIFont.systemFont(ofSize: 15.0, weight: .medium)])
            cell.textViewQuestions.attributedText = agreeAttributedString
            
            return cell
        }
            
        else if( indexPath.section == 18) {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "textSelect", for: indexPath) as! TextFieldSelectTableViewCell
            cell.textViewQuestions.tag = indexPath.section
           
            let agreeAttributedString = NSMutableAttributedString(string: QuestionArray[indexPath.section], attributes: [NSAttributedStringKey.foregroundColor: UIColor.black, NSAttributedStringKey.font: UIFont.systemFont(ofSize: 15.0, weight: .medium)])
            cell.textViewQuestions.attributedText = agreeAttributedString
            
               cell.lblNumber.text = String(indexPath.section) + ")"
                let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tapPopUp(_:)))
                cell.textFieldAnswer.isUserInteractionEnabled = true;
                cell.textFieldAnswer.addGestureRecognizer(tapGesture)
            
            return cell
        }
        
        else if(indexPath.section == 9 || indexPath.section == 10 || indexPath.section == 12 ) {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellYesNo", for: indexPath) as! YesNoTableViewCell
            cell.btnYes.tag = indexPath.section
            cell.btnNo.tag = indexPath.section
             cell.lblNumber.text = String(indexPath.section) + ")"
            let agreeAttributedString = NSMutableAttributedString(string: QuestionArray[indexPath.section], attributes: [NSAttributedStringKey.foregroundColor: UIColor.black, NSAttributedStringKey.font: UIFont.systemFont(ofSize: 15.0, weight: .medium)])
            cell.textViewQuestion.attributedText = agreeAttributedString
            return cell;
        }else if( indexPath.section == 11 ) {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellYesNoNot", for: indexPath) as! YesNoNotSureTableViewCell
          cell.btnYes.tag = indexPath.section
             cell.btnNo.tag = indexPath.section
             cell.btnNotSure.tag = indexPath.section
            cell.lblNumber.text = String(indexPath.section) + ")"
            let agreeAttributedString = NSMutableAttributedString(string: QuestionArray[indexPath.section], attributes: [NSAttributedStringKey.foregroundColor: UIColor.black, NSAttributedStringKey.font: UIFont.systemFont(ofSize: 15.0, weight: .medium)])
            cell.textViewQuestion.attributedText = agreeAttributedString
            return cell;
        }
        
        
        
        else if(indexPath.section == 14 || indexPath.section == 15 || indexPath.section == 16 || indexPath.section == 17 ) {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "GAB", for: indexPath) as! GABTableViewCell
            
             cell.lblNumber.text = String(indexPath.section) + ")"
            cell.btnGood.tag = indexPath.section
            cell.btnAverage.tag = indexPath.section
            cell.btnBad.tag = indexPath.section
            let agreeAttributedString = NSMutableAttributedString(string: QuestionArray[indexPath.section], attributes: [NSAttributedStringKey.foregroundColor: UIColor.black, NSAttributedStringKey.font: UIFont.systemFont(ofSize: 15.0, weight: .medium)])
            cell.textViewQuestion.attributedText = agreeAttributedString
            return cell;
        }
        else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "buttonNext", for: indexPath) as! ButtonNextTableViewCell
            
                return cell
        }
}
}
