
//
//  ModelHZLQuestions.swift
//  HZL CSC
//
//  Created by Bunga Maheshwar Rao on 28/10/18.
//  Copyright © 2018 SARVANG INFOTCH. All rights reserved.
//
import UIKit
import Foundation
import CoreData
class ObserverDataApi
{
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    var reachablty = Reachability()!
    var name = String()
    var ID = String()
    
    
    
    func serviceCalling(obj:QuestionAnswerViewController, success:@escaping (AnyObject)-> Void)
    {
        //obj.startLoading(isStart: true)
        
        if(reachablty.connection != .none)
        {
            
            let parameters = ["App":"HZL"]
            WebServices.sharedInstances.sendPostRequest(url: URLConstants.unsafeActsObservedCSC, parameters: parameters, successHandler: { (dict) in
                
                
                if let response = dict["response"]{
                    
                    let statusString : String = response["status"] as! String
                    
                    if(statusString == "success")
                    {
                        
                        
                        if let data = dict["data"] as? [AnyObject]
                        {
                            
                         
                                 var dataArray : [Observed] = []
                                for i in 0..<data.count
                                {
                                    
                                    if let cell = data[i] as? [String:AnyObject]
                                    {
                                        let object = Observed()
                                        object.setDataInModel(cell: cell)
                                        dataArray.append(object)
                                    
                                    }
                            }
                             success(dataArray as AnyObject)
                            
                        }
                        
                        // obj.refreshControl.endRefreshing()
                        //obj.startLoading(isStart: false)
                    }
                    else
                    {
                        print("DATA:fail")
                        // obj.tableView.isHidden = true
                        //obj.noDataLabel(text: "No Data Found!" )
                        // obj.refreshControl.endRefreshing()
                        //obj.startLoading(isStart: false)
                        
                    }
                    
                    
                }
                
                
                
                
            })
            { (error) in
                
                //                let errorStr : String = error.localizedDescription
                //
                //                print("DATA:error",errorStr)
                //
                //                obj.errorChecking(error: error)
                //                obj.startLoading(isStart: false)
            }
            
            
        }
        else{
            //obj.startLoading(isStart: false)
        }
        
        
    }
    
    
}

class Observed: NSObject {
    var name: String?
  
    var ID =  String()
    
    func setDataInModel(cell:[String:AnyObject])
    {
        if cell["Name"] is NSNull{
            self.name = ""
        }else{
            self.name = (cell["Name"] as? String)!
        }
        if cell["ID"] is NSNull{
            self.ID = ""
        }else{
            let idd = (cell["ID"] as? Int)!
            self.ID = String(idd)
        }
    }
}
func saveObservedListData (name:String,id:String,privateContext:NSManagedObjectContext){
    
    //let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    let tasks = ObsrvervedActs(context: privateContext)
    
    tasks.id = id
    tasks.name = name
   
    
    do {
        try privateContext.save()
    }catch{
        
    }
    
    
}

func deleteObservedListData()
{
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    let req = NSFetchRequest<NSFetchRequestResult>.init(entityName: "ObsrvervedActs")
    let batchDeleteRequest = NSBatchDeleteRequest(fetchRequest: req)
    
    do {
        try context.execute(batchDeleteRequest)
        
    } catch {
        // Error Handling
    }
}
