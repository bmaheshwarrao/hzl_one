

//
//  SubmitDataTableViewCell.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 20/02/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit

class SubmitDataTableViewCell: UITableViewCell {

    
    @IBOutlet weak var viewVisitDate: UIView!
    
    @IBOutlet weak var textVisitDate: UITextField!
    
    @IBOutlet weak var viewCompany: UIView!
    @IBOutlet weak var textCompany: UITextField!
    @IBOutlet weak var lblUnit: UILabel!
    @IBOutlet weak var btnUnit: UIButton!
    @IBOutlet weak var viewUnit: UIView!
    @IBOutlet weak var lblArea: UILabel!
    @IBOutlet weak var btnArea: UIButton!
    @IBOutlet weak var viewArea: UIView!
    @IBOutlet weak var lblDept: UILabel!
    @IBOutlet weak var btnDept: UIButton!
    @IBOutlet weak var viewDept: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        viewArea.layer.borderWidth = 1.0
        viewArea.layer.borderColor = UIColor.black.cgColor
        viewArea.layer.cornerRadius = 10.0
        viewUnit.layer.borderWidth = 1.0
        viewUnit.layer.borderColor = UIColor.black.cgColor
        viewUnit.layer.cornerRadius = 10.0
        viewDept.layer.borderWidth = 1.0
        viewDept.layer.borderColor = UIColor.black.cgColor
        viewDept.layer.cornerRadius = 10.0
        
        
        viewCompany.layer.borderWidth = 1.0
        viewCompany.layer.borderColor = UIColor.black.cgColor
        viewCompany.layer.cornerRadius = 10.0
        
        viewVisitDate.layer.borderWidth = 1.0
        viewVisitDate.layer.borderColor = UIColor.black.cgColor
        viewVisitDate.layer.cornerRadius = 10.0
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
