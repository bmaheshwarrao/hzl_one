//
//  MovieDetailTableViewCell.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 20/02/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit

class MovieDetailTableViewCell: UITableViewCell {

    @IBOutlet weak var textViewTiming: UITextView!
    @IBOutlet weak var textViewTalkies: UITextView!
    @IBOutlet weak var textViewCast: UITextView!
    @IBOutlet weak var btnRate1: UIImageView!
      @IBOutlet weak var btnRate2: UIImageView!
      @IBOutlet weak var btnRate3: UIImageView!
      @IBOutlet weak var btnRate4: UIImageView!
      @IBOutlet weak var btnRate5: UIImageView!
    @IBOutlet weak var textViewDesc: UITextView!
    @IBOutlet weak var lblLanguage: UILabel!
    @IBOutlet weak var lblMovie: UILabel!
    @IBOutlet weak var imageViewMovie: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
