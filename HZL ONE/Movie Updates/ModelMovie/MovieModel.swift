//
//  MovieModel.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 20/02/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import Foundation
import Reachability


class MovieModel: NSObject {
    
    
    var Movie_ID = String()
    
    var Movie_Name = String()
    var Description = String()
    var Image = String()
    var Rating = String()
    var Language = String()
   
    var Cast = String()
    
    var movieTiming = [MovieTimingModel]()





    
    
    func setDataInModel(str:[String:AnyObject])
    {
        if str["Cast"] is NSNull || str["Cast"] == nil{
            self.Cast = ""
        }else{
            
            let ros = str["Cast"]
            
            self.Cast = (ros?.description)!
            
            
        }
        if str["Movie_ID"] is NSNull || str["Movie_ID"] == nil{
            self.Movie_ID = ""
        }else{
            
            let ros = str["Movie_ID"]
            
            self.Movie_ID = (ros?.description)!
            
            
        }
      
       
        if str["Movie_Name"] is NSNull || str["Movie_Name"] == nil{
            self.Movie_Name = "0"
        }else{
            let emp1 = str["Movie_Name"]
            
            self.Movie_Name = (emp1?.description)!
            // print(self.Employee_1)
        }
        if str["Description"] is NSNull || str["Description"] == nil{
            self.Description = ""
        }else{
            
            let empname = str["Description"]
            
            self.Description = (empname?.description)!
            
        }
        
        if str["Image"] is NSNull || str["Image"] == nil{
            self.Image = ""
        }else{
            let emp2 = str["Image"]
            
            self.Image = (emp2?.description)!
            
            
            
        }
        
   
        
        if str["Rating"] is NSNull || str["Rating"] == nil{
            self.Rating = ""
        }else{
            let empname2 = str["Rating"]
            
            self.Rating = (empname2?.description)!
            
        }
        
        
        if str["Language"] is NSNull || str["Language"] == nil{
            self.Language = ""
        }else{
            let locid = str["Language"]
            
            self.Language = (locid?.description)!
            
        }
        if str["Movie_Timing_Details"] is NSNull || str["Movie_Timing_Details"] == nil{
            self.movieTiming = []
        }else{
            if let data1 = str["Movie_Timing_Details"] as? [AnyObject] {
            for i in 0..<data1.count
            {
                if let cell = data1[i] as? [String:AnyObject]
                {
                   let object = MovieTimingModel()
                    object.setDataInModel(str: cell)
                    movieTiming.append(object)
                }
            }
        }
        }
        
        
        
    }
    
}

class MovieTimingModel: NSObject {
    
    
    
    var Talkies_Name = String()
    var Timing = String()
    var Movie_Date = String()
    
    
    func setDataInModel(str:[String:AnyObject])
    {
        
        if str["Talkies_Name"] is NSNull || str["Talkies_Name"] == nil{
            self.Talkies_Name = ""
        }else{
            let locname = str["Talkies_Name"]
            
            self.Talkies_Name = (locname?.description)!
            
        }
        
        
        
        if str["Timing"] is NSNull || str["Timing"] == nil{
            self.Timing = ""
        }else{
            let locname = str["Timing"]
            
            self.Timing = (locname?.description)!
            
        }
        
        
        if str["Movie_Date"] is NSNull || str["Movie_Date"] == nil{
            self.Movie_Date =  ""
        }else{
            
            let fross = str["Movie_Date"]
            
            self.Movie_Date = (fross?.description)!
            
            
            //  self.Description = (str["Description"] as? String)!
        }
    }
}



class MovieDataAPI
{
    
    var reachablty = Reachability()!
    
    func serviceCalling(obj:MovieListViewController, param : [String: String] , success:@escaping (AnyObject)-> Void)
    {
        obj.startLoading()
        
        if(reachablty.connection != .none)
        {
            
            
            WebServices.sharedInstances.sendPostRequest(url: URLConstants.Movie_Details, parameters: param , successHandler: { (dict) in
                
                if let response = dict["response"]{
                    
                    let statusString : String = response["status"] as! String
                    let objectmsg = MessageCallServerModel()
            let msg = objectmsg.sealizeMessage(cell: response as! [String : AnyObject])
                    print(response)
                    if(statusString == "success")
                    {
                        obj.tableView.isHidden = false
                        obj.noDataLabel(text: "" )
                        obj.label.isHidden = true
                        if let data = dict["data"] as? [AnyObject]
                        {
                            
                            var dataArray: [MovieModel] = []
                            
                            for i in 0..<data.count
                            {
                                
                                if let cell = data[i] as? [String:AnyObject]
                                {
                                    let object = MovieModel()
                                    object.setDataInModel(str: cell)
                                    dataArray.append(object)
                                }
                                
                            }
                            
                            success(dataArray as AnyObject)
                            
                            
                        }
                        
                        obj.refreshControl.endRefreshing()
                        obj.stopLoading()
                    }
                    else
                    {
                        obj.label.isHidden = false
                        obj.tableView.isHidden = true
                        obj.noDataLabel(text: msg )
                        obj.refreshControl.endRefreshing()
                        obj.stopLoading()
                        
                    }
                    
                    
                }
                
                
                
                
            })
            { (error) in
                
                let errorStr : String = error.description
                
                print("DATA:error",errorStr)
                
                //                obj.errorChecking(error: error)
                obj.stopLoading()
            }
            
            
        }
        else{
            obj.label.isHidden = false
            obj.tableView.isHidden = true
            obj.noDataLabel(text: "Internet is not available, please check your internet connection try again." )
            obj.stopLoading()
        }
        
        
    }
    
    
}

