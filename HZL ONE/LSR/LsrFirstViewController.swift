//
//  LsrFirstViewController.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 21/02/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit

class LsrFirstViewController: CommonVSClass {

    
var StrNav = String()
    var MSG = String()
    var Location_ID =  String()
    var Location_Name = String()
    var Is_Location = String()
    
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
       // getDataList()
        // Do any additional setup after loading the view.
    }

    @IBAction func btnGoClicked(_ sender: UIButton) {
        
      getEmpList()
            cellData.textViewLSRComment.isHidden = false
            cellData.textViewLSRComment.text = MSG
        
    }
    @IBAction func btnNextClicked(_ sender: UIButton) {
        if(empSearch != ""){
            
             if(self.empDB.count > 0){
            let storyBoard = UIStoryboard(name: "LSR", bundle: nil)
            let ZIVC = storyBoard.instantiateViewController(withIdentifier: "LsrSubmitViewController") as! LsrSubmitViewController
           ZIVC.empListDB = self.empDB[0]
                ZIVC.location_Id = Location_ID
            self.navigationController?.pushViewController(ZIVC, animated: true)
            }
        }
       
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
       
        if(MoveStruct.isMove == true){
            self.navigationController?.popViewController(animated: false)
        }
        self.tabBarController?.tabBar.isHidden = true
        self.title = StrNav
        let nav = self.navigationController?.navigationBar
        nav?.tintColor = UIColor.white
       nav?.barTintColor = UIColor(hexString: "2c3e50", alpha: 1.0)
        nav?.isTranslucent = false
        nav?.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
    }
    var memberDB:[CheckLocationModel] = []
    
    var MemberAPI = CheckLocationDataAPI()
   @objc func getDataList(){
    var para = [String:String]()
    let parameter = ["Employee_ID":UserDefaults.standard.string(forKey: "EmployeeID")!]
    
    para = parameter.filter { $0.value != ""}
    print("para",para)
    self.MemberAPI.serviceCalling(obj: self, parameter: para) { (dict) in
        
        self.memberDB = [CheckLocationModel]()
        self.memberDB = dict as! [CheckLocationModel]
        
        if(self.memberDB.count > 0){
            if(self.memberDB[0].Is_Location == "Yes"){
                self.MSG = self.memberDB[0].MSG!
                self.Is_Location = self.memberDB[0].Is_Location!
                 self.Location_ID = self.memberDB[0].Location_ID!
                 self.Location_Name = self.memberDB[0].Location_Name!
            }
        }
    }
    }
    var onceDone : Bool = false
    var empDB:[EmployeeListLsrModel] = []
    var empSearch = String()
    var empAPI = EmployeelListLsrDataAPI()
    @objc func getEmpList(){
        
        let paramm  : [String:String] = ["Employee_ID":cellData.textEmpId.text!]
        
        self.empAPI.serviceCalling(obj: self , param : paramm , empid : cellData.textEmpId.text!) { (dict) in
            
            self.empDB = [EmployeeListLsrModel]()
            self.empDB = dict as! [EmployeeListLsrModel]
            if(self.empDB.count > 0){
                if(self.empDB[0].Employee_Name != ""){
                    self.cellData.lblName.text = self.empDB[0].Employee_Name!
                    self.cellData.lblCompany.text = self.empDB[0].Company!
                   self.cellData.lblCategory.text = self.empDB[0].User_Type!
                    self.empSearch = self.empDB[0].Employee_ID!
                    self.cellData.stackView.isHidden = false
                    self.cellData.btnNext.isHidden = false
                    self.Location_ID = self.empDB[0].Location!
                }
            }
            
            
            
            
            
        }
    }
    var cellData : LsrFirstTableViewCell!
     var applicationName = String()
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
extension LsrFirstViewController : UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        

        
        let screenSize = UIScreen.main.bounds
       // let screenHeight = screenSize.height-navheight
        
        let screenWidth = screenSize.width
        if indexPath.section == 0{
            return (screenWidth/2) + 25
        }else {
            return UITableViewAutomaticDimension
        }
        
    }
}
extension LsrFirstViewController : UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0{
            return 1
        }else{
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let page = "page"
            
            
            let cell = tableView.dequeueReusableCell(withIdentifier: page) as! PageIdeaTableViewCell
            
            cell.contentView.frame.size.height = cell.imageCollectionView.frame.size.height
            cell.contentView.frame.size.width = cell.imageCollectionView.frame.size.width
            
            
            cell.pageView(ApplicationName: applicationName)
            
            return cell
            
        }else{
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! LsrFirstTableViewCell
        
        cellData = cell
        
       cell.stackView.isHidden = true
        cell.textViewLSRComment.isHidden = true
        cell.btnNext.isHidden = true
        cell.stackViewHeightCon.constant = 80
        cell.textViewHeightCon.constant = 0
        cell.separatorInset = UIEdgeInsetsMake(0, 10000, 0, 0);
        return cell
        
        }
        
    }
}
