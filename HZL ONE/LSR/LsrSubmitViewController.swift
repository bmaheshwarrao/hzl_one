//
//  LsrSubmitViewController.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 21/02/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit
import Alamofire
class LsrSubmitViewController: CommonVSClass , UIImagePickerControllerDelegate,UINavigationControllerDelegate {
  @IBOutlet weak var tableView: UITableView!
    var empListDB = EmployeeListLsrModel()
    var location_Id = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
        getBreakRuleList()
        // Do any additional setup after loading the view.
    }
var idBreak = String()
    @IBAction func btnBreakUpClicked(_ sender: UIButton) {
        
        DispatchQueue.main.async(execute: {() -> Void in
            let popup = PopUpTableViewController()
            
            popup.itemsArray = self.breakDataListArray
            
            popup.sourceView = self.cellData.textViewBreak
            popup.isScroll = true
            popup.backgroundColor = UIColor.white
            if popup.itemsArray.count > 5{
                popup.popUpHeight = 200
            }
            else{
                popup.popUpHeight = CGFloat(popup.itemsArray.count)*45
            }
            popup.popUpWidth = UIScreen.main.bounds.size.width-60
            popup.backgroundImage = nil
            popup.itemTitleColor = UIColor.white
            popup.itemSelectionColor = UIColor.lightGray
            popup.arrowDirections = .any
            popup.arrowColor = UIColor.white
            popup.popCellBlock = {(_ popupVC: PopUpTableViewController, _ popupCell: UITableViewCell, _ row: Int, _ section: Int) -> Void in
                
                
                
                
                
                self.cellData.textViewBreak.text = self.breakDataListArray[row]
                self.cellData.textViewBreak.textColor = UIColor.black
                self.idBreak = self.breakDataIdArray[row]
                popupVC.dismiss(animated: false, completion: nil)
                
            }
            self.present(popup, animated: true, completion: {() -> Void in
            })
            
            
        })
        
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.title = "Submit LSR"
        
        let nav = self.navigationController?.navigationBar
        nav?.tintColor = UIColor.white
       nav?.barTintColor = UIColor(hexString: "2c3e50", alpha: 1.0)
        nav?.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
       nav?.isTranslucent = false
        
        
    }
    func imageResize (image:UIImage, sizeChange:CGSize)-> UIImage{
        
        let hasAlpha = true
        let scale: CGFloat = 0.0 // Use scale factor of main screen
        
        UIGraphicsBeginImageContextWithOptions(sizeChange, !hasAlpha, scale)
        image.draw(in: CGRect(origin: CGPoint.zero, size: sizeChange))
        
        let scaledImage = UIGraphicsGetImageFromCurrentImageContext()
        return scaledImage!
    }
     var reachablty = Reachability()!
    @IBAction func btnSubmitClicked(_ sender: UIButton) {
        

        
        if reachablty.connection == .none{
            
            self.view.makeToast("Internet is not available, please check your internet connection try again.")
            
        }
        else if ( self.cellData.textViewBreak.text == String()) {
            
            self.view.makeToast("Please Select Lsr Violation")
        }
        else if ( self.cellData.textViewRemark.text == String()) {
            
            self.view.makeToast("Please write remark.")
        }
        else{
            
            
            
            self.startLoadingPK(view: self.view)
            
            
            let empId = UserDefaults.standard.string(forKey: "EmployeeID")!
            
            
            let parameter = ["Employee_ID":empListDB.Employee_ID!,
                             "Company_ID":empListDB.Company_ID!,
                             "Break_Rule":self.idBreak,
                             "Employee_Category": empListDB.Category!,
                             "Remark":cellData.textViewRemark.text! ,
                             "By_Employee_ID": empId,
                             "Employee_Location_ID":location_Id
                ] as [String:String]
            
            print(parameter)
            let jsonData: Data? = try? JSONSerialization.data(withJSONObject: parameter, options: [])
            let para: [String: Data] = ["data": jsonData!]
            
            Alamofire.upload(multipartFormData: { multipartFormData in
                
                if !(self.imagedata == nil) {
                    var res : UIImage = UIImage()
                    
                    let size = CGSize(width: 150, height: 150)
                    res = self.imageResize(image: self.cellData.imageViewPhoto.image!,sizeChange: size)
                    
                    self.imagedata = UIImageJPEGRepresentation(res, 1.0)!
                    multipartFormData.append(self.imagedata!, withName: "file", fileName: "file.jpg", mimeType: "image/jpeg")
                }
                
                for (key, value) in para {
                    multipartFormData.append(value, withName: key)
                    
                    
                }
                
            },
                             to:URLConstants.Register_Violation)
            { (result) in
                switch result {
                case .success(let upload, _, _):
                    
                    upload.uploadProgress(closure: { (progress) in
                        
                    })
                    
                    
                    upload.responseJSON { response in
                        
                        
                        if let json = response.result.value
                        {
                            var dict = json as! [String: AnyObject]
                            
                            if let response = dict["response"]{
                                print(response)
                                let statusString = response["status"] as! String
                                let objectmsg = MessageCallServerModel()
                    let msg = objectmsg.sealizeMessage(cell: response as! [String : AnyObject])
                                print("HZl Dict :",dict)
                                if(statusString == "success")
                                {
                                    
                                    
                                    self.stopLoadingPK(view: self.view)
                                    MoveStruct.isMove = true
                                    
                                    MoveStruct.message = msg
                                    
                                    
                                    self.navigationController?.popViewController(animated: false)
                                    
                                    
                                    
                                    
                                }
                                else{
                                    
                                    self.stopLoadingPK(view: self.view)
                                    self.view.makeToast(msg)
                                    
                                }
                                
                                
                                
                            }
                            
                            
                        }
                        
                        
                    }
                    
                case .failure(let encodingError):
                    
                    self.stopLoadingPK(view: self.view)
                    self.errorChecking(error: encodingError)
                    
                    print(encodingError.localizedDescription)
                }
            }
        }
        
        
        
    }
    
    
    @IBAction func btnImageButtonClicked(_ sender: UIButton) {
        
        let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
            self.openCamera()
        }))
        
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
            self.openGallary()
        }))
        
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
        
    }
    
    
    @objc func pressCamera(_ sender : UITapGestureRecognizer) {
        
    }
    let imagePicker = UIImagePickerController()
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera))
        {
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func openGallary()
    {
        imagePicker.delegate = self
        imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
        imagePicker.allowsEditing = true
        self.present(imagePicker, animated: true, completion: nil)
    }
    internal func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
            cellData.imageViewPhoto.image = image
            imagedata = UIImageJPEGRepresentation(image, 1.0)!
            cellData.btnCamera.isHidden = true
            cellData.lblOptional.isHidden = true
            cellData.btnClose.isHidden = false
            cellData.imageViewPhoto.isHidden = false
            updateConst()
            
            
        }else{
            print("Something went wrong")
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    // var reachability = Reachability()!
    var imagedata: Data? = nil
    @IBAction func btnCloseButtonClicked(_ sender: UIButton) {
        
        self.imagedata = nil
        self.cellData.btnClose.isHidden = true
        cellData.btnCamera.isHidden = false
        cellData.lblOptional.isHidden = false
        updateConst()
    }
    func updateConst(){
        
        
        if(self.cellData.btnClose.isHidden == true){
            self.cellData.heightViewofClose.constant = 0
            self.cellData.heightImageselected.constant = 0
            self.cellData.heightViewImage.constant = 40
        }else{
            self.cellData.heightViewofClose.constant = 25
            self.cellData.heightImageselected.constant = 100
            self.cellData.heightViewImage.constant = 150
        }
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
      var indexing : Int = 1
     var cellData : LsrSubmitTableViewCell!
    var breakDB:[BreakModel] = []
    
    var breakAPI = BreakDataAPI()
    
    var breakDataListArray : [String] = []
    var breakDataIdArray  : [String]  = []
    @objc func getBreakRuleList(){
        
        let paramm  : [String:String] = [:]
         breakDataListArray = []
         breakDataIdArray = []
        self.breakAPI.serviceCalling(obj: self , param : paramm) { (dict) in
            
            self.breakDB = [BreakModel]()
            self.breakDB = dict as! [BreakModel]
            if(self.breakDB.count > 0){
                for i in 0...self.breakDB.count - 1 {
                    self.breakDataIdArray.append(self.breakDB[i].ID!)
                     self.breakDataListArray.append(self.breakDB[i].LSRViolation!)
                }
            }
            
            
            
            
            
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
extension LsrSubmitViewController : UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 608
        
    }
}
extension LsrSubmitViewController : UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! LsrSubmitTableViewCell
        
        cellData = cell
        if(indexing == 1){
            cellData.btnClose.isHidden = true
            indexing = indexing + 1
        }
        updateConst()
        
        cell.separatorInset = UIEdgeInsetsMake(0, 10000, 0, 0);
        return cell
        
        
        
    }
}
