//
//  MyInboxHeaderTableViewCell.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 09/02/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit

class MyInboxHeaderTableViewCell: UITableViewCell {

    @IBOutlet weak var lblHeader: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
