//
//  InboxViewController.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 20/11/18.
//  Copyright © 2018 SARVANG INFOTCH. All rights reserved.
//

import UIKit

class InboxViewController: CommonVSClass ,UITableViewDelegate, UITableViewDataSource{
   var refreshControl = UIRefreshControl()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.rowHeight = 70
        tableView.rowHeight = UITableViewAutomaticDimension
        
        
        refreshControl.addTarget(self, action: #selector(getMyinboxData), for: .valueChanged)

        self.tableView.addSubview(refreshControl)
        let swipe = UISwipeGestureRecognizer(target: self, action: #selector(self.swipeRecognizer))
        swipe.direction = ([.down])
        self.view.addGestureRecognizer(swipe)
        
        tableView.tableFooterView = UIView()
        // Do any additional setup after loading the view.
    }
    var reachability = Reachability()!
    @objc func swipeRecognizer(_ sender: UISwipeGestureRecognizer) {
        if (sender.direction == .down) {
            
            if(self.reachability.connection != .none)
            {
                
                self.getMyinboxData()
            }
            else
            {
                self.startLoading(view: self.view)
                DispatchQueue.main.asyncAfter(deadline: .now() + 4.5) {
                    self.noDataLabel(text: "No internet connection found. Check your internet connection and try again.")
                    self.stopLoading(view: self.view)
                }
            }
            
        }
    }
    var MyInboxAPI = MyInboxDataAPI()
    @IBOutlet weak var tableView: UITableView!
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        getMyinboxData()
        // NotificationCenter.default.post(name: NSNotification.Name.init("sendReportFromHome"), object: nil)
        self.title = "My Inbox"
        self.tabBarController?.tabBar.isHidden = false
        let nav = self.navigationController?.navigationBar
        nav?.tintColor = UIColor.white
       nav?.barTintColor = UIColor(hexString: "2c3e50", alpha: 1.0)
        
        
        nav?.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        
        
        
    }
    @IBAction func btnInboxDataClicked(_ sender: UIButton) {
        let buttonPosition:CGPoint = sender.convert(.zero, to:self.tableView)
        let indexPath = self.tableView.indexPathForRow(at: buttonPosition)
        if(CategoryMyData[(indexPath?.section)!].AppData == "Theme_Based_Safety"){
            let storyBoard = UIStoryboard(name: "ThemeSafety", bundle: nil)
            UserDefaults.standard.set(2, forKey: "hazard")
            let displayhazardVC = storyBoard.instantiateViewController(withIdentifier: "DisplayHazardandActionViewController") as! DisplayHazardandActionViewController
            self.navigationController?.title = "Hazard Reported By Me"
            self.navigationController?.pushViewController(displayhazardVC, animated: true)
        }else  if(CategoryMyData[(indexPath?.section)!].AppData == "Idea_Generation"){
            let storyBoard = UIStoryboard(name: "IdeaGeneration", bundle: nil)
            UserDefaults.standard.set(2, forKey: "hazard")
            let InboxVC = storyBoard.instantiateViewController(withIdentifier: "InboxDisplayViewController") as! InboxDisplayViewController
            
            self.navigationController?.pushViewController(InboxVC, animated: true)
        }else  if(CategoryMyData[(indexPath?.section)!].AppData == "NDSO"){
            let storyBoard = UIStoryboard(name: "NDSO", bundle: nil)
            let ZIVC = storyBoard.instantiateViewController(withIdentifier: "NDSODashboardViewController") as! NDSODashboardViewController
            ZIVC.applicationName = CategoryMyData[(indexPath?.section)!].AppData
            ZIVC.StrNav = CategoryMyData[(indexPath?.section)!].Category
            self.navigationController?.pushViewController(ZIVC, animated: true)
        }else  if(CategoryMyData[(indexPath?.section)!].AppData == "ODSO"){
            let storyBoard = UIStoryboard(name: "ODSO", bundle: nil)
            
            
            let ZIVC = storyBoard.instantiateViewController(withIdentifier: "ODSODashboardViewController") as! ODSODashboardViewController
            ZIVC.applicationName = CategoryMyData[(indexPath?.section)!].AppData
            ZIVC.StrNav = CategoryMyData[(indexPath?.section)!].Category
            self.navigationController?.pushViewController(ZIVC, animated: true)
            
        }else  if(CategoryMyData[(indexPath?.section)!].AppData == "M_Quiz"){
            let storyBoard = UIStoryboard(name: "mQuiz", bundle: nil)
            let submitVC = storyBoard.instantiateViewController(withIdentifier: "mQuizFirstViewController") as! mQuizFirstViewController
            
            self.navigationController?.pushViewController(submitVC, animated: true)
        }else  if(CategoryMyData[(indexPath?.section)!].AppData == "HZL MOM"){
            //            let storyBoard = UIStoryboard(name: "mQuiz", bundle: nil)
            //            let submitVC = storyBoard.instantiateViewController(withIdentifier: "mQuizFirstViewController") as! mQuizFirstViewController
            //
            //            self.navigationController?.pushViewController(submitVC, animated: true)
        }else  if(CategoryMyData[(indexPath?.section)!].AppData == "HZL_SAM"){
            let storyboard1 = UIStoryboard(name: "SI", bundle: nil)
            let auditVC = storyboard1.instantiateViewController(withIdentifier: "PendingInteractionViewController") as! PendingInteractionViewController
            
            self.navigationController?.pushViewController(auditVC, animated: true)
        }else  if(CategoryMyData[(indexPath?.section)!].AppData == "HZL_SI"){
            let storyboard1 = UIStoryboard(name: "SI", bundle: nil)
            let auditVC = storyboard1.instantiateViewController(withIdentifier: "PendingInteractionViewController") as! PendingInteractionViewController
            
            self.navigationController?.pushViewController(auditVC, animated: true)        }else  if(CategoryMyData[(indexPath?.section)!].AppData == "HZL Travel"){
            //            let storyBoard = UIStoryboard(name: "mQuiz", bundle: nil)
            //            let submitVC = storyBoard.instantiateViewController(withIdentifier: "mQuizFirstViewController") as! mQuizFirstViewController
            //
            //            self.navigationController?.pushViewController(submitVC, animated: true)
        }else  if(CategoryMyData[(indexPath?.section)!].AppData == "HZL Guest House"){
            let storyBoard = UIStoryboard(name: "GuestHouse", bundle: nil)
            let submitVC = storyBoard.instantiateViewController(withIdentifier: "PendingApprovalViewController") as! PendingApprovalViewController
            
            self.navigationController?.pushViewController(submitVC, animated: true)
        }
       
        
    }
//    override func viewWillDisappear(_ animated: Bool) {
//        self.tabBarController?.tabBar.isHidden = false
//    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        
        
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
       
        let header = tableView.dequeueReusableCell(withIdentifier: "cellHeader") as! MyInboxHeaderTableViewCell
      
       
        header.lblHeader.text = CategoryMyData[section].Category.uppercased()
        return header
        
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return CategoryMyData.count
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        
        
        return 35.0
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return CategoryMyData[section].InboxData.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! MyInboxTableViewCell
  
        
        
        
        cell.lblShowHazAction.text = CategoryMyData[indexPath.section].InboxData[indexPath.row].Action_Text
        cell.lblAddData.text = CategoryMyData[indexPath.section].InboxData[indexPath.row].Count_Record
      
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {

        return UITableViewAutomaticDimension

    }
    var CategoryMyData = [CategoryInboxDataModel]()
    @objc func getMyinboxData(){
        var param = [String:String]()
        
        param =  ["Employee_ID":UserDefaults.standard.string(forKey: "EmployeeID")!]
        
        MyInboxAPI.serviceCalling(obj: self,  param: param ) { (dict) in
           
             self.CategoryMyData = dict as! [CategoryInboxDataModel]
          
            self.tableView.reloadData()
            
            
        }
    }
    
    
   
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
