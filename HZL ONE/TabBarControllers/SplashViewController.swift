//
//  SplashViewController.swift
// RAS
//
//  Created by SARVANG INFOTCH on 03/11/18.
//  Copyright © 2018 Bunga Maheshwar Rao. All rights reserved.
//

import UIKit

class SplashViewController: CommonVSClass {
var timer = Timer()
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var imageNOInternet: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.white
//alertForAppstore()
      
        // Do any additional setup after loading the view.
    }

    @IBOutlet weak var btnInternet: UIButton!
    @IBAction func btnSplashClicked(_ sender: UIButton) {
        updateApp()
    }
    var splashVal = 0
    @objc func callBaseUrl(){
        BASE_URL =  UserDefaults.standard.string(forKey: "baseUrl")!
        if(BASE_URL != nil){
            BASE_URL =  UserDefaults.standard.string(forKey: "baseUrl")!
        }
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        if(BASE_URL != "" && BASE_URL != nil && reachablity.connection != .none && splashVal == 0) {
            splashVal = 1
            if UserDefaults.standard.bool(forKey: "isWorkmanLogin") == true {
                if UserDefaults.standard.bool(forKey: "isLogin") == true {
                    
                    
                    let dashBoardVC = storyboard.instantiateViewController(withIdentifier: "DashBoardViewController") as! DashBoardViewController
                    
                    let navigationVC = UINavigationController(rootViewController: dashBoardVC)
                    navigationVC.navigationBar.barTintColor =  UIColor(red: 41/255, green: 88/255, blue: 144/255, alpha: 1.0)
                    navigationVC.navigationBar.isTranslucent = false;
                    self.present(navigationVC, animated: true, completion: nil)
                }
                else
                {
                    
                    let loginVC  =  storyboard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                    self.present(loginVC, animated: true, completion: nil)
                    //                    self.window?.rootViewController = loginVC
                    //                    self.window?.makeKeyAndVisible()
                }
            } else {
                if UserDefaults.standard.bool(forKey: "isLogin") == true {
                    
                    
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let loginVC  =  storyboard.instantiateViewController(withIdentifier: "Home") as! HomeViewController
                    let navigationVC = UINavigationController(rootViewController: loginVC)
                    navigationVC.navigationBar.barTintColor =  UIColor(red: 41/255, green: 88/255, blue: 144/255, alpha: 1.0)
                    navigationVC.navigationBar.isTranslucent = false;
                    //                    self.window?.rootViewController = navigationVC
                    //                    self.window?.makeKeyAndVisible()
                    self.present(navigationVC, animated: true, completion: nil)
                }
                else
                {
                    let loginVC  =  storyboard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                    self.present(loginVC, animated: true, completion: nil)
                    //                    self.window?.rootViewController = loginVC
                    //                    self.window?.makeKeyAndVisible()
                    
                }
            }
        }
        
        if(splashVal == 1){
            self.timer.invalidate()
        }else if(reachablity.connection == .none){
            self.timer.invalidate()
        }else{
            
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewDidAppear(_ animated: Bool) {
        //alertForAppstore()
    }
    var reachablity = Reachability()!
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.title = "No Internet"
        self.tabBarController?.tabBar.isHidden = true
        let nav = self.navigationController?.navigationBar
        nav?.barStyle = UIBarStyle.black
        nav?.tintColor = UIColor.white
        nav?.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        
       self.timer = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.callBaseUrl), userInfo: nil, repeats: true)
        
    }
//    func releaseData() {
//                let storyboard = UIStoryboard(name: "Main", bundle: nil)
//        if UserDefaults.standard.bool(forKey: "isWorkmanLogin") == true {
//            if UserDefaults.standard.bool(forKey: "isLogin") == true {
//
//
//                let dashBoardVC = storyboard.instantiateViewController(withIdentifier: "DashBoardViewController") as! DashBoardViewController
//
//                let navigationVC = UINavigationController(rootViewController: dashBoardVC)
//                navigationVC.navigationBar.barTintColor =  UIColor(red: 41/255, green: 88/255, blue: 144/255, alpha: 1.0)
//                navigationVC.navigationBar.isTranslucent = false;
//                self.window.rootViewController = navigationVC
//                self.window.makeKeyAndVisible()
//            }
//            else
//            {
//
//                let loginVC  =  storyboard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
//
//                self.window.rootViewController = loginVC
//                self.window.makeKeyAndVisible()
//            }
//        } else {
//            if UserDefaults.standard.bool(forKey: "isLogin") == true {
//
//
//                let storyboard = UIStoryboard(name: "Main", bundle: nil)
//                let loginVC  =  storyboard.instantiateViewController(withIdentifier: "Home") as! HomeViewController
//                let navigationVC = UINavigationController(rootViewController: loginVC)
//                navigationVC.navigationBar.barTintColor =  UIColor(red: 41/255, green: 88/255, blue: 144/255, alpha: 1.0)
//                navigationVC.navigationBar.isTranslucent = false;
//                self.window.rootViewController = navigationVC
//                self.window.makeKeyAndVisible()
//            }
//            else
//            {
//                let loginVC  =  storyboard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
//
//                self.window.rootViewController = loginVC
//                self.window.makeKeyAndVisible()
//
//            }
//        }
//    }
//
  
//    func alertForAppstore(){
//
//         let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
//            print(version!)
//
//        var versionUpdate = UserDefaults.standard.string(forKey: "version")
//        if(version != versionUpdate ) {
//
//
//        let actionSheetController: UIAlertController = UIAlertController(title: "Alert", message: "Swiftly Now! Choose an option!", preferredStyle: .alert)
//
//        //Create and add the Cancel action
//        let cancelAction: UIAlertAction = UIAlertAction(title: "Cancel", style: .cancel) { action -> Void in
//
//            self.releaseData()
//
//            }
//
//        actionSheetController.addAction(cancelAction)
//        //Create and an option action
//        let nextAction: UIAlertAction = UIAlertAction(title: "Next", style: .default) { action -> Void in
//            //Do some other stuff
//        }
//        actionSheetController.addAction(nextAction)
//        //Add a text field
//
//
//        //Present the AlertController
//        self.present(actionSheetController, animated: true, completion: nil)
//        } else {
//            self.releaseData()
//        }
//    }
     var window: UIWindow?
    var UpdateDB = [UpdateAppData]()
    var UrlDB = [UrlData]()
    var UpDataAPI = UpdateSplashDataAPI()
    func updateApp() {
        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
          let storyboard = UIStoryboard(name: "Main", bundle: nil)
        self.UpdateDB = [UpdateAppData]()
        self.UrlDB = [UrlData]()
         self.startLoadingPK(view: self.view)
        self.UpDataAPI.serviceCalling(obj: self)
            { (dict) in
                do {
//                    self.UpdateDB = try context.fetch(UpdateAppData.fetchRequest())
//                    self.UrlDB = try context.fetch(UrlData.fetchRequest())
//                    UserDefaults.standard.set(self.UrlDB[0].baseUrl!, forKey: "baseUrl")
//                    self.stopLoadingPK(view: self.view)
//                    BASE_URL = self.UrlDB[0].baseUrl!
                    
                    
                    
                } catch {
                    self.stopLoadingPK(view: self.view)
                }
        }
        
        
        
        
        
    }
    
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
