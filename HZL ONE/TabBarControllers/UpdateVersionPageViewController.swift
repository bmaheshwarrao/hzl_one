//
//  UpdateVersionPageViewController.swift
//  mQuiz
//
//  Created by SARVANG INFOTCH on 23/11/18.
//  Copyright © 2018 SARVANG INFOTCH. All rights reserved.
//

import UIKit

class UpdateVersionPageViewController: UIViewController {
    var message = String()
    static func instantiate() -> UpdateVersionPageViewController? {
        return UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "\(UpdateVersionPageViewController.self)") as? UpdateVersionPageViewController
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        if(message == "Mandatory"){
            Cancel.isHidden = true
        }else{
            Cancel.isHidden = false
        }
        // Do any additional setup after loading the view.
    }

    @IBOutlet weak var Cancel: UIButton!
    @IBOutlet weak var UpdateNow: UIButton!
    @IBAction func UpdateNowClicked(_ sender: UIButton) {
        if(message == "Mandatory"){
             UpadteVal = 2
                        if let url = URL(string: "itms://itunes.apple.com/us/app/apple-store/id1458316828?mt=8"),
                            UIApplication.shared.canOpenURL(url){
                            //   UIApplication.shared.openURL(url)
            
                            if #available(iOS 10, *) {
                                UIApplication.shared.open(url)
                            } else {
                                UIApplication.shared.openURL(url)
                            }
                        }
        }else{
                        if let url = URL(string: "itms://itunes.apple.com/us/app/apple-store/id1458316828?mt=8"),
                            UIApplication.shared.canOpenURL(url){
                            //   UIApplication.shared.openURL(url)
            
                            if #available(iOS 10, *) {
                                UIApplication.shared.open(url)
                            } else {
                                UIApplication.shared.openURL(url)
                            }
                        }
            UpadteVal = 2
           dismiss(animated: true, completion: nil)
        }
    }
    @IBAction func CancelClicked(_ sender: UIButton) {
         UpadteVal = 2
        dismiss(animated: true, completion: nil)
    }
    @IBOutlet weak var textViewMessage: UITextView!
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
