//
//  HzlPdfReaderViewController.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 28/02/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit

class HzlPdfReaderViewController: CommonVSClass,UIWebViewDelegate {
    
    
    @IBOutlet weak var webView: UIWebView!
    
    var prentId = String()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        webView.delegate = self
        self.prentId = HZlPolicyTempData.Cat_ID
        
        loadPdf()
        
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.tabBarController?.tabBar.isHidden = true
        self.title = HZlPolicyTempData.file_Name
        let nav = self.navigationController?.navigationBar
        nav?.tintColor = UIColor.white
       nav?.barTintColor = UIColor(hexString: "2c3e50", alpha: 1.0)
        nav?.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        
    }
    
    
    func loadPdf() {
        
        if HZlPolicyTempData.file_Url != ""{
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.0) {
                
                self.startLoading()
                
                self.checkPDFIsAvailable(linkName: HZlPolicyTempData.ELCat_ID+self.prentId , success: { (filepath) in
                    
                    
                    var pdfURL = (FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)).last! as URL
                    pdfURL = pdfURL.appendingPathComponent(HZlPolicyTempData.ELCat_ID+self.prentId ) as URL
                    
                    let data = try! Data(contentsOf: pdfURL)
                    
                    self.webView.load(data, mimeType: "application/pdf", textEncodingName:"", baseURL: pdfURL.deletingLastPathComponent())
                    
                    self.stopLoading()
                    
                    
                    
                }, failure: { () in
                    
                    self.downloadPDF(linkString: HZlPolicyTempData.file_Url, linkName: HZlPolicyTempData.ELCat_ID+self.prentId , completionHandler: { () in
                        
                        
                        self.checkPDFIsAvailable(linkName: HZlPolicyTempData.ELCat_ID+self.prentId , success: { (filepath) in
                            
                            
                            var pdfURL = (FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)).last! as URL
                            pdfURL = pdfURL.appendingPathComponent(HZlPolicyTempData.ELCat_ID+self.prentId ) as URL
                            
                            let data = try! Data(contentsOf: pdfURL)
                            DispatchQueue.main.async {
                                self.webView.load(data, mimeType: "application/pdf", textEncodingName:"", baseURL: pdfURL.deletingLastPathComponent())
                            }
                            self.stopLoading()
                            
                            
                            
                        }, failure: { () in
                            
                            //print(error)
                            self.stopLoading()
                            
                        })
                        
                        
                    })
                    
                })
            }
        }
    }
    
}
