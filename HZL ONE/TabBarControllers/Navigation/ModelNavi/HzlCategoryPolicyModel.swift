//
//  HzlCategoryPolicyModel.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 28/02/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import Foundation
import Reachability
import CoreData

class HzlCategoryPolicyModel: NSObject {
    
    
    var File_Type = String()
    var Link = String()
    var Title = String()
    var Description = String()
    var ID = Int()
   
    
    
    func setELPolicyDataInModel(str:[String:AnyObject])
    {
        if str["Link"] is NSNull || str["Link"] == nil{
            self.Link = ""
        }else{
            
            let tit = str["Link"]
            
            self.Link = (tit?.description)!
            
            
        }
        
        if str["File_Type"] is NSNull || str["File_Type"] == nil{
            self.File_Type = ""
        }else{
            
            let tit = str["File_Type"]
            
            self.File_Type = (tit?.description)!
            
            
        }
        
        if str["Title"] is NSNull || str["Title"] == nil{
            self.Title = ""
        }else{
            
            let tit = str["Title"]
            
            self.Title = (tit?.description)!
            
            
        }
        if str["Description"] is NSNull || str["Description"] == nil{
            self.Description = ""
        }else{
            
            let tit = str["Description"]
            
            self.Description = (tit?.description)!
            
            
        }
        
        
        
       
        
    }
}

class HzlCategoryPolicyDataAPI
{
    
    var reachablty = Reachability()!
   
    
    func serviceCalling(obj:HZlPolicyViewController,Cat_ID:String, success:@escaping (AnyObject)-> Void)
    {
        
        obj.startLoading()
        
        if(reachablty.connection != .none)
        {
           
            WebServices.sharedInstances.sendPostRequest(url: URLConstants.Company_Policies, parameters: ["Category_ID" : Cat_ID], successHandler: { (dict) in
                
                if let response = dict["response"]{
                    
                    let statusString : String = response["status"] as! String
                    let objectmsg = MessageCallServerModel()
                    
                    let msg = objectmsg.sealizeMessage(cell: response as! [String : AnyObject])
                    if(statusString == "success")
                    {
                        obj.tableView.isHidden = false
                        obj.noDataLabel(text: "" )
                        obj.label.isHidden = true
                        if let data = dict["data"] as? [AnyObject]
                        {
                            var dataArray : [HzlCategoryPolicyModel] = []
                            
                            for i in 0..<data.count
                            {
                                
                                if let cell = data[i] as? [String:AnyObject]
                                {
                                    
                                    let object = HzlCategoryPolicyModel()
                                    object.setELPolicyDataInModel(str: cell)
                                    dataArray.append(object)
                                    
                                    
                                }
                                
                            }
                            
                            success(dataArray as AnyObject)
                            
                            
                        }
                        
                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                    }
                    else
                    {
                        print("DATA:fail")
                        obj.tableView.isHidden = true
                        obj.label.isHidden = false
                        obj.noDataLabel(text: msg )
                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                        
                    }
                    
                    
                }
                
                
                
                
            })
            { (error) in
                
                let errorStr : String = error.description
                
                print("DATA:error",errorStr)
                
                // obj.errorChecking(error: error)
                obj.refresh.endRefreshing()
                obj.stopLoading()
            }
            
            
        }
        else{
            obj.tableView.isHidden = true
            obj.label.isHidden = false
            obj.noDataLabel(text: "Internet is not available, Please check internet connection and try again." )
            obj.refresh.endRefreshing()
            obj.stopLoading()
            
        }
        
        
    }
    
    
}
