//
//  UpdateModel.swift
// RAS
//
//  Created by SARVANG INFOTCH on 03/11/18.
//  Copyright © 2018 Bunga Maheshwar Rao. All rights reserved.
//
import Foundation
import Reachability
import CoreData
import ESTabBarController_swift
var splash = Int()
func saveUpdateBaseUrl (base_Url:String,urlName:String){
  
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    let tasks = UrlData(context: context)
    
    tasks.baseUrl = base_Url
    tasks.urlType = urlName
    
    
    (UIApplication.shared.delegate as! AppDelegate).saveContext()
    
  
}
func saveUpdateData(ID:String,appCode:String,appName:String,appPlatform:String,latestVersion:String,updateType:String,updateMessage:String,gcmSenderId:String,packageName:String){
    
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    let tasks = UpdateAppData(context: context)

    tasks.appCode = appCode
    tasks.appName = appName
    tasks.gcmSenderId = gcmSenderId
    tasks.id = ID
    tasks.updateMessage = updateMessage
    tasks.packageName = packageName
    tasks.appPlatform = appPlatform
    tasks.updateType = updateType
    tasks.latestVersion = latestVersion
    (UIApplication.shared.delegate as! AppDelegate).saveContext()
    
    
    
    
}
func deleteUpdateData()
{
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    let req = NSFetchRequest<NSFetchRequestResult>.init(entityName: "UpdateAppData")
    let batchDeleteRequest = NSBatchDeleteRequest(fetchRequest: req)
    
    do {
        try context.execute(batchDeleteRequest)
        
    } catch {
        // Error Handling
    }
}

func deleteBaseUrl()
{
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    let req = NSFetchRequest<NSFetchRequestResult>.init(entityName: "UrlData")
    let batchDeleteRequest = NSBatchDeleteRequest(fetchRequest: req)
    
    do {
        try context.execute(batchDeleteRequest)
        
    } catch {
        // Error Handling
    }
}

class UpdateDataAPI
{
    
    var reachablty = Reachability()!
     var ID = Int()
    var appCode = String()
    var appName = String()
     var appPlatform = String()
    var latestVersion = String()
     var updateType = String()
     var updateMessage = String()
    var gcmSenderId = String()
    var packageName = String()
    var urlName = String()
    var baseURL = String()
   
    
    func serviceCalling( success:@escaping (AnyObject)-> Void)
    {
        
   splash = 0
        
        if(reachablty.connection != .none)
        {
           splash = 0
            
            WebServices.sharedInstances.sendPostRequest(url: URLConstants.Get_App_Data, parameters: ["AppCode":URLConstants.appNameInner,"AppPlatform":URLConstants.platform], successHandler: { (dict) in
                
                if let response = dict["response"]{
                    
                    let statusString : String = response["status"] as! String
                    
                    if(statusString == "success")
                    {
                        deleteUpdateData()
               
                       
                           splash = 1
                                
                                if let cell = dict["data"] as? [String:AnyObject]
                                {
                                   
                                    if cell["AppCode"] is NSNull{
                                        self.appCode = ""
                                    }else{
                                        self.appCode = (cell["AppCode"] as? String)!
                                    }
                                    if cell["AppName"] is NSNull{
                                        self.appName = ""
                                    }else{
                                        self.appName = (cell["AppName"] as? String)!
                                    }
                                    
                                    if cell["AppPlatform"] is NSNull{
                                        self.appPlatform = ""
                                    }else{
                                        self.appPlatform = (cell["AppPlatform"] as? String)!
                                    }
                                    if cell["LatestVersion"] is NSNull{
                                        self.latestVersion = ""
                                    }else{
                                        
                                        
                                        self.latestVersion = (cell["LatestVersion"] as? String)!
                                         UserDefaults.standard.set(self.latestVersion, forKey: "version")
                                    }
                                    
                                    if cell["UpdateType"] is NSNull{
                                        self.updateType = ""
                                    }else{
                                        self.updateType = (cell["UpdateType"] as? String)!
                                         // print(self.updateType)
                                        UserDefaults.standard.set(self.updateType, forKey: "updateType")
                                    }
                                    if cell["UpdateMessage"] is NSNull{
                                        self.updateMessage = ""
                                    }else{
                                        self.updateMessage = (cell["UpdateMessage"] as? String)!
                                         UserDefaults.standard.set(self.updateMessage, forKey: "updateMessage")
                                    }
                                    
                                    if cell["GcmSenderId"] is NSNull{
                                        self.gcmSenderId = ""
                                    }else{
                                        self.gcmSenderId = (cell["GcmSenderId"] as? String)!
                                    }
                                    if cell["PackageName"] is NSNull{
                                        self.packageName = ""
                                    }else{
                                        self.packageName = (cell["PackageName"] as? String)!
                                    }
                                    
                                    
//                                    if cell["ID"] is NSNull{
//                                        self.ID = 0
//                                    }else{
//                                        self.ID = (cell["ID"] as? String)!
//                                    }
                                   
                                    saveUpdateData(ID: String(100), appCode: self.appCode, appName:  self.appName, appPlatform:  self.appPlatform, latestVersion:  self.latestVersion, updateType:  self.updateType, updateMessage:  self.updateMessage, gcmSenderId:  self.gcmSenderId, packageName:  self.packageName)
                                }
                                
                        
                        
                        
                        
                        
                           
//                        if let dataBaseUrl = dict["Base_Url"] as? [AnyObject]
//                        {
//                            deleteBaseUrl()
//                            for i in 0..<dataBaseUrl.count
//                            {
//
//                                if let cell = dataBaseUrl[i] as? [String:AnyObject]
//                                {
//                                    if cell["urlName"] is NSNull{
//                                        self.urlName = ""
//                                    }else{
//                                        self.urlName = (cell["urlName"] as? String)!
//                                    }
//                                    if(self.urlName == "HZL_ONE"){
//                                    if cell["baseURL"] is NSNull{
//                                        self.baseURL = ""
//                                    }else{
//                                        self.baseURL = (cell["baseURL"] as? String)!
//                                     // print(self.baseURL)
//                                        UserDefaults.standard.set(self.baseURL, forKey: "baseUrl")
//                                        BASE_URL = self.baseURL
//                                    }
//                                    }else{
//                                        self.baseURL = ""
//                                    }
//
//                                    saveUpdateBaseUrl(base_Url: self.baseURL , urlName:  self.urlName)
//                                }
//
//                            }
//
//
//                        }
                        
                            
                        
                        
                      
                    }
                    else
                    {
                       // deleteELibraryPolicyData()
                        print("DATA:fail")
                
                        
                    }
                    
                    
                }
                
                
                
                
            })
            { (error) in
                
                let errorStr : String = error.description
                print("Mahi Error")
                print("DATA:error",errorStr)
          
            }
            
            
        }
        else{
          //  deleteELibraryPolicyData()
          
        }
        
        
    }
    
    
}


class UpdateSplashDataAPI
{
    
    var reachablty = Reachability()!
    var ID = Int()
    var appCode = String()
    var appName = String()
    var appPlatform = String()
    var latestVersion = String()
    var updateType = String()
    var updateMessage = String()
    var gcmSenderId = String()
    var packageName = String()
    var urlName = String()
    var baseURL = String()
    
    
    func serviceCalling(obj:SplashViewController, success:@escaping (AnyObject)-> Void)
    {
        
        splash = 0
       
        if(reachablty.connection != .none)
        {
            splash = 0
            
            WebServices.sharedInstances.sendPostRequest(url: URLConstants.Get_App_Data, parameters: ["AppCode":URLConstants.appNameInner,"AppPlatform":URLConstants.platform], successHandler: { (dict) in
                
                if let response = dict["response"]{
                    
                    let statusString : String = response["status"] as! String
                    
                    if(statusString == "success")
                    {
                        deleteUpdateData()
                        print(response)
                        
                        obj.splashVal = 1
                        obj.stopLoadingPK(view: obj.view)
                        if let cell = dict["data"] as? [String:AnyObject]
                        {
                            
                            if cell["AppCode"] is NSNull{
                                self.appCode = ""
                            }else{
                                self.appCode = (cell["AppCode"] as? String)!
                            }
                            if cell["AppName"] is NSNull{
                                self.appName = ""
                            }else{
                                self.appName = (cell["AppName"] as? String)!
                            }
                            
                            if cell["AppPlatform"] is NSNull{
                                self.appPlatform = ""
                            }else{
                                self.appPlatform = (cell["AppPlatform"] as? String)!
                            }
                            if cell["LatestVersion"] is NSNull{
                                self.latestVersion = ""
                            }else{
                                
                                
                                self.latestVersion = (cell["LatestVersion"] as? String)!
                                
                                UserDefaults.standard.set(self.latestVersion, forKey: "version")
                            }
                            
                            if cell["UpdateType"] is NSNull{
                                self.updateType = ""
                            }else{
                                self.updateType = (cell["UpdateType"] as? String)!
                                print(self.updateType)
                                UserDefaults.standard.set(self.updateType, forKey: "updateType")
                            }
                            if cell["UpdateMessage"] is NSNull{
                                self.updateMessage = ""
                            }else{
                                self.updateMessage = (cell["UpdateMessage"] as? String)!
                                UserDefaults.standard.set(self.updateMessage, forKey: "updateMessage")
                            }
                            
                            if cell["GcmSenderId"] is NSNull{
                                self.gcmSenderId = ""
                            }else{
                                self.gcmSenderId = (cell["GcmSenderId"] as? String)!
                            }
                            if cell["PackageName"] is NSNull{
                                self.packageName = ""
                            }else{
                                self.packageName = (cell["PackageName"] as? String)!
                            }
                            
                            
                            //                                    if cell["ID"] is NSNull{
                            //                                        self.ID = 0
                            //                                    }else{
                            //                                        self.ID = (cell["ID"] as? String)!
                            //                                    }
                            
                            saveUpdateData(ID: String(100), appCode: self.appCode, appName:  self.appName, appPlatform:  self.appPlatform, latestVersion:  self.latestVersion, updateType:  self.updateType, updateMessage:  self.updateMessage, gcmSenderId:  self.gcmSenderId, packageName:  self.packageName)
                        }
                        
                        
                        
                        
                        
                        
                        
//                        if let dataBaseUrl = dict["Base_Url"] as? [AnyObject]
//                        {
//                            deleteBaseUrl()
//                            for i in 0..<dataBaseUrl.count
//                            {
//                                
//                                if let cell = dataBaseUrl[i] as? [String:AnyObject]
//                                {
//                                    if cell["urlName"] is NSNull{
//                                        self.urlName = ""
//                                    }else{
//                                        self.urlName = (cell["urlName"] as? String)!
//                                    }
//                                    if(self.urlName == "HZL_ONE"){
//                                        if cell["baseURL"] is NSNull{
//                                            self.baseURL = ""
//                                        }else{
//                                            self.baseURL = (cell["baseURL"] as? String)!
//                                            print(self.baseURL)
//                                            UserDefaults.standard.set(self.baseURL, forKey: "baseUrl")
//                                            BASE_URL = self.baseURL
//                                        }
//                                    }else{
//                                        self.baseURL = ""
//                                    }
//                                    
//                                    saveUpdateBaseUrl(base_Url: self.baseURL , urlName:  self.urlName)
//                                }
//                                
//                            }
//                            
//                            
//                        }
                        
                          let storyboard = UIStoryboard(name: "Main", bundle: nil)
                        if(BASE_URL != "") {
                            if UserDefaults.standard.bool(forKey: "isWorkmanLogin") == true {
                                if UserDefaults.standard.bool(forKey: "isLogin") == true {
                                    
                                    
                                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                    let loginVC  =  ExampleProvider.customBouncesStyle()
                                    let navigationVC = UINavigationController(rootViewController: loginVC)
                                    navigationVC.navigationBar.barTintColor =  UIColor(red: 41/255, green: 88/255, blue: 144/255, alpha: 1.0)
                                    navigationVC.navigationBar.isTranslucent = false;
                                    //                    self.window?.rootViewController = navigationVC
                                    //                    self.window?.makeKeyAndVisible()
                                    obj.present(navigationVC, animated: true, completion: nil)
                                }
                                else
                                {
                                    
                                    let loginVC  =  storyboard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                                    obj.present(loginVC, animated: true, completion: nil)
                                    //                    self.window?.rootViewController = loginVC
                                    //                    self.window?.makeKeyAndVisible()
                                }
                            } else {
                                if UserDefaults.standard.bool(forKey: "isLogin") == true {
                                    
                                    
                                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                    let loginVC  =  ExampleProvider.customBouncesStyle()
                                    let navigationVC = UINavigationController(rootViewController: loginVC)
                                    navigationVC.navigationBar.barTintColor =  UIColor(red: 41/255, green: 88/255, blue: 144/255, alpha: 1.0)
                                    navigationVC.navigationBar.isTranslucent = false;
                                    //                    self.window?.rootViewController = navigationVC
                                    //                    self.window?.makeKeyAndVisible()
                                    obj.present(navigationVC, animated: true, completion: nil)
                                }
                                else
                                {
                                    let loginVC  =  storyboard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                                    obj.present(loginVC, animated: true, completion: nil)
                                    //                    self.window?.rootViewController = loginVC
                                    //                    self.window?.makeKeyAndVisible()
                                    
                                }
                            }
                        }
                        
                        
                        
                    }
                    else
                    {
                        // deleteELibraryPolicyData()
                        print("DATA:fail")
                        obj.stopLoadingPK(view: obj.view)
                        
                    }
                    
                    
                }
                
                
                
                
            })
            { (error) in
                obj.stopLoadingPK(view: obj.view)
                let errorStr : String = error.description
                print("Mahi Error")
                print("DATA:error",errorStr)
                
            }
            
            
        }
        else{
            //  deleteELibraryPolicyData()
            obj.stopLoadingPK(view: obj.view)
            obj.view.makeToast("No Internet Connection , Please Connect to Internet and Click Reload.")
        }
        
        
    }
    
    
}
